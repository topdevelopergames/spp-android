using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Loader : MonoBehaviour 
{
    const float WAIT_LOAD_AND_RETURN_MAIN_MENU = 15f;
	private AsyncOperation unloadUnusedAssets;

	IEnumerator Start ()
	{
		if(MenuControllerGenerator.controller)
		{
			unloadUnusedAssets = Resources.UnloadUnusedAssets();
			StartCoroutine("UpdateLoader");
			yield return unloadUnusedAssets;
			StopCoroutine("UpdateLoader");
			MenuControllerGenerator.controller.LoaderIsDoneUnload = true;
			MenuControllerGenerator.controller.progress = 0.2f;
			yield return new WaitForEndOfFrame();
			Debug.Log("before load scene");
			/*if(MenuControllerGenerator.controller.levelName != "")
				SceneManager.LoadScene(MenuControllerGenerator.controller.levelName);
			else
				SceneManager.LoadScene(MenuControllerGenerator.controller.levelNumber);*/
			if (ServerController.serverController && PhotonNetwork.isMasterClient/*ServerController.serverController.isMyQueue*/)
			{
				Debug.Log("PhotonNetwork.LoadLevel");
				if (MenuControllerGenerator.controller.levelName != "")
					PhotonNetwork.LoadLevel(MenuControllerGenerator.controller.levelName);
				else
					PhotonNetwork.LoadLevel(MenuControllerGenerator.controller.levelNumber);
			}
			else if (!ServerController.serverController)
			{
				Debug.Log("SceneManager.LoadScene");
				if (MenuControllerGenerator.controller.levelName != "")
					SceneManager.LoadScene(MenuControllerGenerator.controller.levelName);
				else
					SceneManager.LoadScene(MenuControllerGenerator.controller.levelNumber);
			}
			Debug.Log("after load scene");
		}
		else
		{
			yield return null;
#if UNITY_EDITOR
			if (SceneManager.sceneCountInBuildSettings >= 3)
				SceneManager.LoadScene("GameStart");
			else
				Debug.LogError("Please add the scenes (GameStart, Game and Loader) in the File/Build Settings" +
				               " as shown in the image  Assets/BallPool/TagAndLayers.png");
#endif
		}
        // if not load new scene - return to MainMenu
        float wTime = Time.realtimeSinceStartup + WAIT_LOAD_AND_RETURN_MAIN_MENU;
        while (Time.realtimeSinceStartup < wTime)
            yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Normal main screen");
    }

    IEnumerator UpdateLoader ()	
	{
		if(MenuControllerGenerator.controller.levelName != "")
		{
			while(true)
			{
				MenuControllerGenerator.controller.preloader.UpdateLoader( 0.2f*unloadUnusedAssets.progress );
				yield return null;
			}
		}
		else
		{
			while(true)
			{
				MenuControllerGenerator.controller.preloader.UpdateLoader( 0.2f*unloadUnusedAssets.progress );
				yield return null;
			}
		}
	}
}
