﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallController : MonoBehaviour 
{
	public bool isMain = false;
	public bool isBlack = false;
	public int ballType = 1;
	public int id = 0;
	public Transform ballReflaction;
	[System.NonSerialized]
	public CueController cueController;
	[System.NonSerialized]
	public bool ballIsSelected = false;
	[System.NonSerialized]
	public bool ballIsOut = false;
	[System.NonSerialized]
	public bool ballIsHolle = false;
	[System.NonSerialized]
	public bool inMove = false;
	
	[System.NonSerialized]
	public float step = 0.0f;
	[System.NonSerialized]
	public float speed = 0.0f;

	private float velocityNormalized = 0.0f;

	[System.NonSerialized]
	public AnimationSpline holeSpline;
	[System.NonSerialized]
	public HolleController holleController;
	[System.NonSerialized]
	public float holeSplineLungth = 0.0f;
	private Vector3 ballVeolociyInHole;
	private Vector3 checkVelocity = Vector3.zero;
	private Vector3 checkAngularVelocity = Vector3.zero;
	
	private BallController inBall = null;
	
	private bool firstHitIsChecked = false;
	
	private bool hasFirstHit = false;
	private bool haveCollision = false;
	[System.NonSerialized]
	public bool inForceMove = false;
	[System.NonSerialized]
	public Rigidbody body;

	[System.NonSerialized]
	public Vector3 startPosition;

	bool WasCollision;
	//PhysicMaterial material;
	[System.NonSerialized]
	public Material materialBallForMove;
	public Transform shadow;
	public Transform ballForMove;
	public Vector3 correctBallPos = Vector3.zero; //We lerp towards this
	float lastTimePosition;
	//	public Quaternion correctBallRot = Quaternion.identity; //We lerp towards this
	public AudioClip audioClipBallBall1;
	public AudioClip audioClipBallBall2;
	public AudioClip audioClipBallBallPower;
	public AudioClip audioClipBallSide;
	AudioSource audioSource;
	bool workEndMove;
	float velocityBrake;
	Vector3 velocityDirection;
	Vector3 angleVelocityDirection;
	bool wasChangeDirection;

	int countCushion;
	bool hitCueBall;

	public int CountCushion { get { return countCushion; } }
	public bool HitCueBall { get { return hitCueBall; } }

	public AudioSource BallAudioSource { get { return audioSource; } }

	void Awake ()
	{
		body = GetComponent<Rigidbody> ();
		velocityBrake = Physics.sleepThreshold * 200f;//50f;
		//body.collisionDetectionMode = CollisionDetectionMode.Discrete;
		//body.interpolation = RigidbodyInterpolation.Interpolate;
		materialBallForMove = ballForMove.GetComponent<MeshRenderer>().material;
		audioSource = GetComponent<AudioSource>();
	}
	public void ForceSetMove (Vector3 position, Vector3 velocity, Vector3 angularVelocity, bool interpolation = true)
	{
        //if (ServerController.serverController != null && ServerController.serverController.isMyQueue)
            //return;
		//body.position = position;
		if (!interpolation)
		{
			body.position = position;
			correctBallPos = Vector3.zero;
		}
		else
			correctBallPos = position;

		//Debug.Log(body.position);
		lastTimePosition = Time.realtimeSinceStartup;
		if(!body.isKinematic)
		{
			body.velocity = velocity;
			body.angularVelocity = angularVelocity;
		}
	}

	public void OnStart ()
	{
		if(MenuControllerGenerator.controller)
		{
			if(isMain)
			{
				InvokeRepeating("CheckInBall", 0.0f, 1.0f);
			}
		}

		body.maxDepenetrationVelocity = cueController.ballMaxVelocity;
		body.maxAngularVelocity = 250.0f;
		body.GetComponent<SphereCollider>().contactOffset = 0.01f;
	}
	void OnDestroy ()
	{
		CancelInvoke( "CheckInBall");
	}
	void CheckInBall ()
	{

		if(body && !cueController.allIsSleeping)
		{
			inBall = null;
			foreach (BallController item in cueController.ballControllers)
			{
				if(item != this)
				{
					float inBallDistance = Vector3.Distance(transform.position, item.transform.position);
					if(inBallDistance < 1.99f*cueController.ballRadius)
					{
						inBall = item;
						if(inBall)
						{
							Vector3 normal = (transform.position - inBall.transform.position).normalized;
							float dist = 2.0f*cueController.ballRadius - inBallDistance;

							body.position += 0.5f*dist*normal;
							inBall.body.position += -0.5f*dist*normal;
						}
						break;
					}
				}
			}

		}
	}
	public bool IsSleeping ()
	{
		//return rigidbody.IsSleeping();
		if (!workEndMove && !ballIsHolle && !ballIsOut && body.velocity.magnitude < velocityBrake && body.angularVelocity.magnitude < velocityBrake)
			StartCoroutine(EndMove());

		return body.velocity.magnitude < Physics.sleepThreshold && body.angularVelocity.magnitude * cueController.ballRadius < Physics.sleepThreshold;
	}
	public void OnSetHoleSpline (float lenght, int holleId)
	{
		HolleController holleController = HolleController.FindeHoleById(holleId);
		holeSpline = holleController.ballSpline;
		holeSplineLungth = lenght;
		this.holleController = holleController;
		if(!isMain && cueController.ballController.ballIsOut && (this.holleController == cueController.ballController.holleController || this.holleController.haveNeighbors(cueController.ballController.holleController)))
		{
			cueController.ballController.holeSplineLungth = holeSplineLungth - 2.0f*cueController.ballRadius;
			if(cueController.ballController.step >= cueController.ballController.holeSplineLungth)
			{
				cueController.ballController.body.position = cueController.ballController.holeSpline.Evaluate(holeSplineLungth);
			}
		}
	}

	public void RessetPosition (Vector3 position, bool forceResset)
	{
		Vector3 newStrPosition = position;

		if(!forceResset)
		{
			ballIsOut = false;
			cueController.ballIsOut = false;

			body.useGravity = false;
			body.isKinematic = true;
			body.GetComponent<Collider>().enabled = false;
	
			Ray ray = new Ray(position + (5.0f*cueController.ballRadius)*Vector3.up, -Vector3.up);
			RaycastHit hit;
			int tryCunt = 0;
			while (tryCunt < 7 && Physics.SphereCast(ray, (1.05f * cueController.ballRadius), out hit, 10.0f * cueController.ballRadius, cueController.mainBallMask | cueController.ballMask | cueController.pinMask))
			{
				tryCunt ++;
				ray = new Ray(newStrPosition + (2.5f*cueController.ballRadius)*Vector3.up, -Vector3.up);
				newStrPosition += (3.0f*cueController.ballRadius)*Vector3.right;
			}
			if (isMain)
				cueController.cueFSMController.setMoveInTable();
		}

		//body.position = newStrPosition;
		transform.position = newStrPosition;
		transform.eulerAngles = Vector3.zero;

		body.isKinematic = false;
		body.GetComponent<Collider>().enabled = true;
		body.useGravity = true;

		body.velocity = Vector3.zero;
		body.angularVelocity = Vector3.zero;
		if(ServerController.serverController && ServerController.serverController.isMyQueue)
			ServerController.serverController.SendRPCToServer("SetBallSleeping", ServerController.serverController.otherNetworkPlayer, id, body.position);
	}

	public void ShotBall ()
	{
        foreach (BallController item in cueController.ballControllers)
        {
            item.countCushion = 0;
            item.hitCueBall = false;
            item.haveCollision = false;
        }
        if (ServerController.serverController)
		{
		    cueController.gameManager.StopCalculateShotTime();
		}
		cueController.OnPlayCueSound(cueController.ballShotVelocity.magnitude/cueController.ballMaxVelocity);
		if(!body.isKinematic)
		{
//			float changeValue = cueController.ballChangeDirection.Evaluate(Mathf.Abs(cueController.rotationDisplacement.x));
//			if (cueController.rotationDisplacement.x < 0)
//				changeValue *= -1;
//			cueController.ballShotVelocity = Quaternion.AngleAxis(-90 * changeValue, Vector3.up) * cueController.ballShotVelocity;

//			if (cueController.ballShotAngularVelocity != Vector3.zero)
//				body.rotation = Quaternion.EulerAngles(Vector3.zero);
/*			Vector3 v1 = Vector3.ClampMagnitude(cueController.ballShotVelocity, cueController.ballMaxVelocity);
			//v1.z = 0f;
			v1.y = 0f;
			Vector3 v2 = v1;//cueController.ballShotAngularVelocity;
			v2.x = v1.z;
			v2.z = v1.x;
			//v2.y = 0f;
			//v2.x = 0f;

			//Vector3 v1 = new Vector3(18f, 0f, 0f);
			//Vector3 v2 = new Vector3(0f, 0f, 18f);//new Vector3(-0.2f, 0f, 16f);
			*/

			body.velocity = Vector3.ClampMagnitude(cueController.ballShotVelocity, cueController.ballMaxVelocity);
			body.angularVelocity = cueController.ballShotAngularVelocity;
			//body.velocity = v1;
			//body.angularVelocity = v2;
		//	cueController.ballShotAngularVelocity = v2;
		}
		checkVelocity = body.velocity;
		checkAngularVelocity = cueController.ballShotAngularVelocity;
		body.AddTorque(body.mass * /*body.angularVelocity*/cueController.ballShotAngularVelocity, ForceMode.Impulse);
		firstHitIsChecked = false;
		wasChangeDirection = false;

		velocityDirection = body.velocity.normalized;
		angleVelocityDirection = body.angularVelocity.normalized;

		//Debug.Log(body.velocity + " " + body.angularVelocity + " " + body.mass + " " + cueController.ballShotAngularVelocity);

		if(ServerController.serverController)
		{
			if(ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
			{
				SetBallCollisionData ();
			}
		}
		cueController.cueBallPivotLocalPosition = Vector3.zero;
        cueController.largeCueBallPivotLocalPosition = Vector3.zero;
		cueController.cueRotationLocalPosition = Vector3.zero;

		//if(!ServerController.serverController || ServerController.serverController.isMyQueue || MenuControllerGenerator.controller.playWithAI)
		//{
		//	StartCoroutine(StartMove ());
		//}

	}

	/*IEnumerator StartMove ()
	{
		yield return new WaitForFixedUpdate();
		
	//	while(!IsSleeping() && !haveCollision && inMove && !body.isKinematic)
	//	{
	//		float decreaseSpeed = body.velocity.magnitude/cueController.ballShotVelocity.magnitude;
	//		body.velocity = decreaseSpeed * cueController.ballShotVelocity;
	//		yield return new WaitForFixedUpdate();
	//	}
	}*/

	IEnumerator EndMove()
	{
		workEndMove = true;
		yield return new WaitForFixedUpdate();
		while (!IsSleeping() && inMove && !ballIsHolle && !ballIsOut && !body.isKinematic)
		{
			//Debug.Log(body.velocity.magnitude + "  " + body.angularVelocity.magnitude);
			float magnitude = body.velocity.magnitude;
			float magnitudeAngular = body.angularVelocity.magnitude;
			Vector3 direct = body.velocity / magnitude;
			Vector3 directAngular = body.angularVelocity / magnitudeAngular;
			body.velocity = magnitude * .98f * direct;
			body.angularVelocity = magnitudeAngular * .98f * directAngular;
			//Debug.Log(body.velocity.magnitude + "  2  " + body.angularVelocity.magnitude);

			//float decreaseSpeed = .005f;//magnitude / 200f;
			//body.velocity -= decreaseSpeed * cueController.ballVelocityOrient ;
			//body.angularVelocity -= decreaseSpeed * cueController.ballVelocityOrient;
			//if (magnitude < body.velocity.magnitude)
//				body.velocity = Vector3.zero;
	//		if (magnitudeAngular < body.angularVelocity.magnitude)
		//		body.angularVelocity = Vector3.zero;
			//yield return new WaitForFixedUpdate();
			yield return new WaitForSeconds(.05f);
		}
		workEndMove = false;
	}

	public void UpdateReflaction ()
	{
		if (shadow)
		{
			Vector3 pos = transform.position;
			pos.y -= cueController.ballRadius - .05f;
			shadow.position = pos;
			shadow.eulerAngles = Vector3.zero;
		}
		if(!ballReflaction)
			return;
		ballReflaction.localPosition = transform.localPosition;
		ballReflaction.localRotation = transform.localRotation;
	}
	public void OnBallIsOut (float _holeSplineLungth)
	{
		body.useGravity = false;
		body.isKinematic = true;
		GetComponent<Collider>().enabled = false;

		body.position = holeSpline.Evaluate(_holeSplineLungth);

	/*	if(!isMain)
		{
			enabled = false;
		}*/
	}

	void FixedUpdate ()
	{
		if(!cueController || !cueController.ballsIsCreated)
			return;

		if(ballIsOut)
		{
			if(!body.isKinematic && step < holeSplineLungth)
			{
				holeSpline.AnimationSlider(transform, Mathf.Clamp( 0.2f*cueController.ballMaxVelocity, 0.01f, 20.0f ), ref step, out ballVeolociyInHole, 1, false);

				body.velocity = ballVeolociyInHole;
			}
			else
			{
				if(GetComponent<Collider>().enabled)
				OnBallIsOut (holeSplineLungth);
			}
		}
		else
		{
			if(!ballIsSelected && inMove && !body.isKinematic)
			{
				//if(!ServerController.serverController || ServerController.serverController.isMyQueue)
				//{
			        velocityNormalized = body.velocity.magnitude/cueController.ballMaxVelocity;

					if(velocityNormalized < 0.01f)
					{
						body.velocity = Vector3.Lerp(body.velocity, Vector3.zero, 5.0f*Time.fixedDeltaTime);
						body.angularVelocity = Vector3.Lerp(body.angularVelocity, Vector3.zero, 1.5f*Time.fixedDeltaTime);
					}

					// special for motion forward and down
					if (!firstHitIsChecked && cueController.rotationDisplacement.x == 0f && angleVelocityDirection != Vector3.zero)
					{
						Vector3 vDirection = body.velocity.normalized;
						Vector3 avDirection = body.angularVelocity.normalized;
						if (!wasChangeDirection && body.velocity.sqrMagnitude < .1f)
						{
							wasChangeDirection = true;
							velocityDirection *= -1;
						}
						if (vDirection != velocityDirection && vDirection != Vector3.zero)
							body.velocity = velocityDirection * body.velocity.magnitude;
						if (avDirection != angleVelocityDirection && avDirection != Vector3.zero)
							body.angularVelocity = angleVelocityDirection * body.angularVelocity.magnitude;
					}

				//}

			}

			//UpdateReflaction ();
		}
		UpdateReflaction();

        //if (isMain)
        //    Debug.Log("Current ball velocity: " + checkVelocity.magnitude);

		checkVelocity = Vector3.Lerp(checkVelocity, body.velocity, 10.0f*Time.fixedDeltaTime);

        //if (isMain)
        //    Debug.Log("<b>New ball velocity: </b>" + checkVelocity.magnitude);

        checkAngularVelocity = Vector3.Lerp(checkAngularVelocity, body.angularVelocity, 10.0f*Time.fixedDeltaTime);
		if (ServerController.serverController && !ServerController.serverController.isMyQueue && correctBallPos != Vector3.zero && lastTimePosition + .2f > Time.realtimeSinceStartup)
		{
			if (ballIsHolle || ballIsOut)	
			{
				body.position = correctBallPos;
				//Debug.Log("balll " + gameObject.name + " ballIsHolle || ballIsOut " + correctBallPos.ToString());
			}
			else
				body.position = Vector3.Lerp(body.position, correctBallPos, Time.deltaTime * 1);
			if (correctBallPos == body.position)
				correctBallPos = Vector3.zero;
		}
        /*else
        {
            if (ServerController.serverController && !ServerController.serverController.isMyQueue && correctBallPos != Vector3.zero && lastTimePosition + .2f < Time.realtimeSinceStartup)
            {
                body.velocity = Vector3.zero;
                body.angularVelocity = Vector3.zero;
            }
        }*/
	}
	public void OnCheckHolle ()
	{
		if (!ballReflaction)
			Destroy(ballReflaction.gameObject);
		cueController.ballControllers.Remove(this);
		cueController.ballControllers.TrimExcess();
	}
	public void OnSetBallReflaction (bool show)
	{
		if (!ballReflaction)
			return;
		ballReflaction.GetComponent<Renderer>().enabled = show;
	}

	void OnCollisionExit(Collision collision)
	{
		if(ServerController.serverController && ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
		{
			SetBallCollisionData ();
		}
	}
	public void CancelInvokeSetBallCollisionData ()
	{
		StopCoroutine("SetBallCollisionDataRepeating");
	}
	public void SetBallCollisionData ()
	{
		StopCoroutine("SetBallCollisionDataRepeating");
		if(inMove /*&& !ballIsOut*/)
			StartCoroutine("SetBallCollisionDataRepeating");
	}
	IEnumerator SetBallCollisionDataRepeating ()
	{
		inForceMove = true;
		if(!ballIsOut)
		{
			ServerController.serverController.SendRPCToServer("ForceSetBallMove", ServerController.serverController.otherNetworkPlayer, id, body.position, body.velocity, body.angularVelocity, true);
		}
		yield return new WaitForSeconds(0.1f);
		while (!IsSleeping() && !ballIsOut)
		{
			if(inMove && !ballIsOut)
			{
				ServerController.serverController.SendRPCToServer("ForceSetBallMove", ServerController.serverController.otherNetworkPlayer, id, body.position, body.velocity, body.angularVelocity, true);
//				Debug.Log("balll " + gameObject.name + " send mess " + body.position + " " + transform.position);
			}
			yield return new WaitForSeconds(0.1f);
		}
		
		while (ballIsOut || ballIsHolle)
		{
			ServerController.serverController.SendRPCToServer("ForceSetBallMove", ServerController.serverController.otherNetworkPlayer, id, body.position, body.velocity, body.angularVelocity, false);
//			Debug.Log("ballIsOut balll " + gameObject.name + " send mess " + body.position + " " + transform.position);
			yield return new WaitForSeconds(0.1f);
		}

		//ServerController.serverController.SendRPCToServer("ForceSetBallMove", ServerController.serverController.otherNetworkPlayer, id, body.position, body.velocity, body.angularVelocity, false);

		//if (!ballIsOut)
		//{
			ServerController.serverController.SendRPCToServer("SetBallSleeping", ServerController.serverController.otherNetworkPlayer, id, body.position);
		//}
		inForceMove = false;
	}

	public void SendForceSetBallMove()
	{
		ServerController.serverController.SendRPCToServer("ForceSetBallMove", ServerController.serverController.otherNetworkPlayer, id, body.position, body.velocity, body.angularVelocity, false);
	}

	public void OnPlayBallAudio (float audioVolume, bool ballBall)
	{
		//audioSource.volume = audioVolume;
		// ball ball
		if (ballBall)
		{
			if (audioVolume > .8f)
				audioSource.clip = audioClipBallBallPower;
			else
				audioSource.clip = Random.value > .5 ? audioClipBallBall1 : audioClipBallBall2;
		}
		// wall
		else
		{
			audioSource.clip = audioClipBallSide;
		}
        //audioSource.Play();
        SoundManager.Instance.PlayAudioSource(audioSource, audioVolume);
	}

	IEnumerator ResetCollision()
	{
		yield return new WaitForSeconds(1f);
		WasCollision = false;
		//body.GetComponent<SphereCollider>().material = material;
	}
	void OnCollisionEnter(Collision collision) 
	{
		string layerName = LayerMask.LayerToName(collision.collider.gameObject.layer);
		if (layerName == "Pocket" && !WasCollision)
		{
			float magnitude = body.velocity.magnitude;
			body.velocity = -cueController.CenterUnderTable.up * magnitude / 10f; // +(cueController.CenterUnderTable.position - body.transform.position);
			WasCollision = true;
			Vector3 posBall = body.position;
			posBall.y -= 1f;
			body.position = posBall;
			StartCoroutine(ResetCollision());
		}
		if(layerName != "Wall" && layerName != "Ball" && layerName != "MainBall")
			return;

		haveCollision = true;
		if(ServerController.serverController && !ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
		{
			ServerController.serverController.SendRPCToServer("SetBallMoveRequest", ServerController.serverController.otherNetworkPlayer, id);
		}
        // TODO NEED TEST
		/*else
		if(ServerController.serverController && (ServerController.serverController.isMyQueue || MenuControllerGenerator.controller.playWithAI))
		{
			if(layerName == "Wall")
			{
				if(cueController.gameManager.isFirstShot && !hasFirstHit && !isMain && !isBlack)
				{
					hasFirstHit = true;
					cueController.gameManager.firstShotHitCount ++;
				}
			}
			else if(isMain && layerName == "Ball")
			{
                Debug.Log("nain to ball dzin!!");
				BallController firstHitBall = collision.collider.GetComponent<BallController>();
				if(!cueController.gameManager.firstHitBall && firstHitBall)
				{
					cueController.gameManager.firstHitBall = firstHitBall;

					if(ServerController.serverController.isMyQueue)
					{
						if((!cueController.gameManager.tableIsOpened && 
						    ( cueController.gameManager.firstHitBall.ballType == cueController.gameManager.ballType || 
						      (cueController.gameManager.firstHitBall.isBlack && cueController.gameManager.afterRemainedBlackBall)
						    )
						   ) ||
						    (!cueController.gameManager.isFirstShot && cueController.gameManager.tableIsOpened && !cueController.gameManager.firstHitBall.isBlack)
						   )
						{
							cueController.gameManager.setMoveInTable = false;
						}
						else if(cueController.gameManager.gameInfoErrorText == "")
						{
							if(!cueController.gameManager.tableIsOpened && cueController.gameManager.firstHitBall.ballType != cueController.gameManager.ballType)
							{
								cueController.gameManager.gameInfoErrorText = "need to hit a " + (cueController.gameManager.ballType == 1? "solid":"striped") +  " ball";
							}
						}
					} else
					{
						if((!cueController.gameManager.tableIsOpened && 
						    (  cueController.gameManager.firstHitBall.ballType == -cueController.gameManager.ballType || 
						 (cueController.gameManager.firstHitBall.isBlack && cueController.gameManager.afterOtherRemainedBlackBall)
						 )) ||
						   (!cueController.gameManager.isFirstShot && cueController.gameManager.tableIsOpened && !cueController.gameManager.firstHitBall.isBlack)
						   )
						{
							cueController.gameManager.setMoveInTable = false;
						}
						else if(cueController.gameManager.gameInfoErrorText == "")
						{
							if(!cueController.gameManager.tableIsOpened && cueController.gameManager.firstHitBall.ballType == cueController.gameManager.ballType)
							{
								cueController.gameManager.gameInfoErrorText = "need to hit a " + (cueController.gameManager.ballType == -1? "solid":"striped") +  " ball";
							}
						}
					}
				}

			}
		}*/
		if(cueController.ballsAudioPlayingCount < 3)
		{
			float audioVolume = Mathf.Clamp01( collision.relativeVelocity.magnitude/cueController.ballMaxVelocity );
			if(!ServerController.serverController || ServerController.serverController.isMyQueue || MenuControllerGenerator.controller.playWithAI)
			{
				bool isBall = layerName == "Ball";
				OnPlayBallAudio(audioVolume, isBall);
				if(ServerController.serverController && !MenuControllerGenerator.controller.playWithAI)
					ServerController.serverController.SendRPCToServer("OnPlayBallAudio", ServerController.serverController.otherNetworkPlayer, id, audioVolume, isBall);
			}
		}
		if (layerName == "Wall")
		{
			countCushion++;
			Rules.Instance.addStateForShot(Hits.HitCushion);
		}
		if (isMain)
		{
            
			if (layerName == "Ball")
			{
                Debug.Log("Before hit ball velocity: " + checkVelocity.magnitude);
                BallController hitBall = collision.collider.GetComponent<BallController>();
				if (Rules.Instance.FirstBallHit == null || Rules.Instance.FirstBallHit == hitBall)
				{
					Rules.Instance.addStateForShot(Hits.HitObjectBall);
					Rules.Instance.FirstBallHit = hitBall;
				}
				else
					Rules.Instance.addStateForShot(Hits.HitOtherObjectBall);
			}

			if (inMove && cueController && collision.collider.GetComponent<Rigidbody>())
			{
				firstHitIsChecked = true;
				if (cueController.currentHitBallController && cueController.currentHitBallController.body == collision.collider.GetComponent<Rigidbody>())
					CheckCurrentHitBall(collision.relativeVelocity.magnitude, cueController.ballShotVelocity.magnitude);
				else
					cueController.currentHitBallController = null;
			}
			else
				if (layerName == "Wall" && !body.isKinematic && !ballIsOut)
				{
					if (!firstHitIsChecked)
					{
						if (Vector3.Distance(cueController.collisionSphere.position, body.position) < cueController.ballRadius)
						{
							firstHitIsChecked = true;
							float decreaseSpeed = checkVelocity.magnitude / cueController.ballShotVelocity.magnitude;
							body.velocity = 0.9f * decreaseSpeed * cueController.secondVelocity;
						}
					}
					else
						SetBallVelocity(checkVelocity);
				}
		}
		else if (layerName == "MainBall")
			hitCueBall = true;
	}

    public static float speedCoef = 0.9f;

	void SetBallVelocity(Vector3 velocity)
	{
		//if(!ServerController.serverController || ServerController.serverController.isMyQueue || MenuControllerGenerator.controller.playWithAI)
		{
            Debug.Log("Old velocity: " + velocity.magnitude);
			body.velocity = Vector3.ClampMagnitude( /*0.9f**/ 0.79f * VectorOperator.getBallWallVelocity(0.99f * cueController.ballRadius, velocity, body.position,
																								  cueController.wallMask, 20.0f * cueController.ballRadius, Vector3.Project(checkAngularVelocity, Vector3.up)), 1000.0f);
            Debug.Log("New velocity: " + body.velocity.magnitude);
            
		}
			checkVelocity = body.velocity;
			cueController.currentHitBallController = null;

			if(ServerController.serverController && ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
			{
				SetBallCollisionData ();
			}
	}
	public void CheckCurrentHitBall (float velocityMagnitude, float ballShotSpeed)
	{
		//Debug.Log(" --------------------------------------- [" + LayerMask.LayerToName(gameObject.layer) + "]");
		//Debug.Log(" " + velocityMagnitude + " " + ballShotSpeed + " " + cueController.hitBallVelocity);
		//Debug.Log(" ---------------------------------------");
		//if(!ServerController.serverController || ServerController.serverController.isMyQueue || MenuControllerGenerator.controller.playWithAI)
		{
			float decreaseSpeed = velocityMagnitude/ballShotSpeed;
			cueController.currentHitBallController.body.velocity = decreaseSpeed*cueController.hitBallVelocity;
			Vector3 addVelocity = cueController.ballRadius*VectorOperator.getPerpendicularXZ( VectorOperator.getProjectXZ( checkAngularVelocity, false ));
			Vector3 velocityBefore = body.velocity;
			body.velocity = Vector3.ClampMagnitude(decreaseSpeed * (cueController.secondVelocity + cueController.ballVelocityCurve.Evaluate(cueController.cueForceValue) * addVelocity), body.velocity.magnitude);
//			if (velocityBefore.magnitude * 5f < ballShotSpeed)
//				body.velocity *= 5f;
		}
			StartCoroutine( WaitAndUncheck ());
		
		if(ServerController.serverController && ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
		{
			SetBallCollisionData ();
			cueController.currentHitBallController.SetBallCollisionData ();
		}
	}
	IEnumerator WaitAndUncheck ()
	{
		yield return new WaitForFixedUpdate ();
		yield return new WaitForFixedUpdate ();
		cueController.currentHitBallController = null;
	}
}
