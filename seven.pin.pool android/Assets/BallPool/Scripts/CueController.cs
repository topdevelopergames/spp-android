﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CueController : MonoBehaviour
{
	#region SevenPin Parameters
	public Pin[] pins;
	public Texture2D cursorHand;
	public GameObject ballMaskTrim;
	public Transform forwardCue;
	//public CueAngleControllRotator cueAngleController;
    public AngleController cueAngleController;
    public Transform CenterUnderTable;
	public bool CanControlCue = true;
	[System.NonSerialized]
	public float CueVerticalAngle = 0f;
	[SerializeField] Material CueMaterial;
	[SerializeField] Texture2D [] CueTextureStandart;
	[SerializeField] Texture2D [] CueTexturePremium;
	[SerializeField] Texture2D [] CueTextureCountry;
	[SerializeField] Transform Camera3DForStart;
    [SerializeField] Transform [] positionsCamera3D;

    Vector3 optimalPosCamera3D;
    Quaternion optimalRotationCamera3D;

    Ray rayShared = new Ray();
	float radiusCue = .15f;
	float realCueVerticalAngle;
	MeshRenderer meshRenderer;

	float timeUnFade = 1f;
	Vector2 offsetCursor = new Vector2(10.0f, 10.0f);
	bool bCursorHand;
	LineRenderer firstLineRender;
	LineRenderer secondLineRender;
	LineRenderer ballLineRender;
	float widthLine = .15f;
	float aspectRationCamera;
	float aspectRationCamera3D;
	CueForceSlider _cueForceSlider;
	Coroutine fadeCoroutine;
	AudioSource ballAudio;
	AudioSource cueControlAudio;
	bool myHit;
	Renderer collisionSphereRenderer;
	int cueIdOpponent = -1;
	Cue_Shop_Type cueTypeOpponent;
	// for test
	[System.NonSerialized]
	static public bool allBallMove;
    static public bool SpecialDebug;
    //
    const float MIN_ANGLE_QUE = 7.5f;

    // for autozoom
    const float MAX_ZOOM_CAMERA_3D = -65f;
    const float MIN_ZOOM_CAMERA_3D = -25f;
    const float ANGLE_FOR_ZOOM = -39.3f;
    float timeAutoMoveCamera = 0.5f;
    float currentTimeAutoMoveCamera;
    Vector3 posCameraBeforeAutoZoom = new Vector3(0f,0f, ANGLE_FOR_ZOOM);
    float zForZoom = ANGLE_FOR_ZOOM;
    bool zoomInOut;
    bool prevAllVisible;
    float prevZoom = MIN_ZOOM_CAMERA_3D;
    bool activeZoom;
    #endregion

    #region Parametrs
    //Distance of balls in the start, (correctly when it is a zero),
    [SerializeField]
	private float ballsDistance = 0.0f;
	//Maximum displacement of the cue during shot,
	public float cueMaxDisplacement = 9.0f;
	//Maximum velocity of the ball during shot, with maximum displacement of the cue,
	public float defaultBallMaxVelocity = 85.0f;
	//Maximum length of the line, which shows the direction of the velocity after hitting balls,
	[SerializeField]
	private float defaultBallLineLength = 7.0f;

	//change direction depending on the deviation from the center point of impact
	public AnimationCurve ballChangeDirection;
	//change direction depending on the deviation from the center point of impact
	public AnimationCurve ballChangeVelocity;
	//change direction depending on the deviation from the center point of impact
	public AnimationCurve ballChangeVelocity2;

	//Defines the angular velocity of the ball at impact, depending on the impact force,
	public AnimationCurve ballAngularVelocityCurve;
	//When the ball collides with another ball, he loses the linear velocity,but, because it also has a angular velocity, he can ride forward after collision
	//depending on the increased the force shot , the  ball gets a less angular velocity because no time to roll on the ground,
	public AnimationCurve ballVelocityCurve;
	//Control  sensitivity of cue for the mobile devices.
    public float touchSensitivity = 0.075f;
#endregion
	[SerializeField]
	private Menu menu;
	//[SerializeField]
	//private Camera guiCamera;
	[SerializeField]
	private Camera camera2D;
	[SerializeField]
	private Camera camera3D;
	[SerializeField]
	private Texture2D collisionBall;
	[SerializeField]
	private Texture2D collisionBallRed;
	private bool checkMyBall = false;
	private bool oldCheckMyBall = false;

	public CueFSMController cueFSMController;
	
	[SerializeField]
	private Transform mainBallPoint;
	[SerializeField]
	private Transform firstBallPoint;
	public Transform centerPoint;
	[System.NonSerialized]
	public List<BallController> ballControllers;
	[System.NonSerialized]
	public BallController[] startBallControllers;
	
	public Texture2D[] ballTextures;
	[SerializeField]
	private Vector2[] deltaPositions;
    [System.NonSerialized]
	public int ballsCount = 16;

	[SerializeField]
	private Transform ballsParent;
	[SerializeField]
	private Transform ballsReflactionParent;
	//[SerializeField]
	//private Renderer lights;


	[SerializeField]
	private BallController ballControllerPrefab;
	[System.NonSerialized]
	public BallController ballController;

	[System.NonSerialized]
	public BallController currentSelectedBallController;

	private Camera currentCamera;
	public Transform cuePivot; 

	public Transform cueRotation;
	private Vector3 checkCuePosition = Vector3.zero;

	//public BallPivotController cueBallPivot;
    public BallPivotMover cueBallPivot, largeBallPivot;
	[System.NonSerialized]
	public float ballRadius = 0.35f;
	[System.NonSerialized]
	public float cueDisplacement = 0.0f;


	[System.NonSerialized]
	public bool shotingInProgress = false;
	[System.NonSerialized]
	public bool thenInshoting = true;

	[System.NonSerialized]
	public bool allIsSleeping = true;
	[System.NonSerialized]
	public bool othersSleeping = true;
	private bool checkAllIsSleeping = true;
	private bool checkOthersIsSleeping = true;
	private bool checkInProgress = false;
	[System.NonSerialized]
	public bool inMove = false;
	[System.NonSerialized]
	public bool canMoveBall = true;
	[System.NonSerialized]
	public bool isFirsTime = true;
	[System.NonSerialized]
	public bool ballIsOut = false;

	private bool oldAllIsSleeping = true;

	[SerializeField]
	//private GameObject cameraCircularSlider;
	public Transform StartCube;
	public Transform MoveCube;
	public Transform collisionSphere;
	[SerializeField]
	private LineRenderer firstCollisionLine;
	[SerializeField]
	private LineRenderer secondCollisionLine;
	[SerializeField]
	private LineRenderer ballCollisionLine;

	private Vector3 ballSelectPosition = Vector3.zero;

	private Vector3 ballCurrentPosition = Vector3.zero;
	[System.NonSerialized]
	public Vector3 ballShotVelocity = Vector3.zero;
	[System.NonSerialized]
	public Vector3 ballShotAngularVelocity = Vector3.zero;

	[System.NonSerialized]
	public Vector3 ballVelocityOrient = Vector3.forward;
	[System.NonSerialized]
	public Vector3 OutBallFirstVelocityOrient;
	[System.NonSerialized]
	public Vector3 OutBallSecondVelocityOrient;
	[System.NonSerialized]
	public Vector3 secondVelocity = Vector3.zero;
	
	[System.NonSerialized]
	public Vector2 rotationDisplacement = Vector2.zero;
	[System.NonSerialized]
	public Vector3 hitBallVelocity = Vector3.zero;
	[System.NonSerialized]
	public BallController currentHitBallController = null;
	[System.NonSerialized]
	public Collider hitCollider = null;

	[System.NonSerialized]
	public bool haveFirstCollision = false;
	[System.NonSerialized]
	public bool haveSecondCollision = false;
	[System.NonSerialized]
	public bool haveThrthCollision = false;
		

	[System.NonSerialized]
	 public LayerMask mainBallMask;
	[System.NonSerialized]
	public LayerMask ballMask;
	[System.NonSerialized]
	public LayerMask pinMask;
	[System.NonSerialized]
	public LayerMask pocketMask;
	[System.NonSerialized]
	public LayerMask canvasMask;
	[System.NonSerialized]
	public LayerMask wallMask;
	[System.NonSerialized]
	public LayerMask wallAndBallMask;
	private LayerMask canvasAndBallMask;
	private LayerMask wallAndBallAndPinMask;
	private bool hitCanvas = false;
	
	[System.NonSerialized]
	public bool is3D = true;

	public Camera3DController camera3DController;
	private bool canDrawLinesAndSphere = false;
	[System.NonSerialized]
	public int ballsAudioPlayingCount = 0;
	private Vector3 cueRotationStrLocalPosition = Vector3.zero;


	private Vector3 cue2DStartPosition = Vector3.zero;

	[System.NonSerialized]
	public float cueForceValue = 1.0f;
	[System.NonSerialized]
	public bool inTouchForceSlider = false;
	[System.NonSerialized]
	public bool cueForceisActive = false;
	[System.NonSerialized]
	public float timeAfterShot = 0.0f;
	[System.NonSerialized]
	public bool networkAllIsSleeping = true;

	[System.NonSerialized]
	public GameManager gameManager;

	[System.NonSerialized]
	public Vector3 cueRotationLocalPosition = Vector3.zero;
	private Quaternion cuePivotLocalRotation = Quaternion.identity;
	[System.NonSerialized]
	public Vector3 cueBallPivotLocalPosition = Vector3.zero;
    [System.NonSerialized]
    public Vector3 largeCueBallPivotLocalPosition = Vector3.zero;
	[System.NonSerialized]
	public Vector3 ballMovePosition = Vector3.zero;
	private float sendTime = 0.0f;
	[System.NonSerialized]
	public bool ballsIsCreated = false;

	[System.NonSerialized]
	public bool otherWantToPlayAgain = false;
	private float touchRotateAngle;

    private CameraManager cameraManager;

	public float ballMaxVelocity { get { return defaultBallMaxVelocity + GameData.Instance.selectedCue.force; } }

	public float ballLineLength { get { return defaultBallLineLength + ((float)GameData.Instance.selectedCue.aim) / 2f; } }


	void Awake ()
	{
		cueControlAudio = GetComponent<AudioSource>();
		if(!MenuControllerGenerator.controller)
			return;
		
		if(ServerController.serverController)
		{
			gameManager = GameManager.FindObjectOfType<GameManager>();
			gameManager.ShowGameInfo("Waiting for your opponent");
			enabled = false;
		}

        /*
		is3D = PlayerPrefs.GetInt("Current Camera") == 1;
		lights.enabled = is3D;
		currentCamera = is3D? camera3D : camera2D;
		currentCamera.enabled = true;*/
		
		ballControllers = new List<BallController>(0);
		
		CreateAndSortBalls ();
		ballsIsCreated = true;

		foreach (BallController item in ballControllers)
		{
			item.cueController = this;
			item.OnStart();
		}
		
		mainBallMask = 1 << LayerMask.NameToLayer("MainBall");
		ballMask = 1 << LayerMask.NameToLayer("Ball");
		canvasMask = 1 << LayerMask.NameToLayer("Canvas");
		wallMask = 1 << LayerMask.NameToLayer("Wall");
		pinMask = 1 << LayerMask.NameToLayer("Pins");
		pocketMask = 1 << LayerMask.NameToLayer("Pocket");
		wallAndBallMask = wallMask | ballMask;// | pocketMask;
		canvasAndBallMask = canvasMask | ballMask | pinMask;
        wallAndBallAndPinMask = wallMask | ballMask | pinMask;// | pocketMask;
		
		collisionSphere.GetComponent<Renderer>().sharedMaterial.mainTexture = collisionBall;
		aspectRationCamera = camera2D.aspect;
		aspectRationCamera3D = camera3D.aspect;
		camera2D.orthographicSize = 31 / camera2D.aspect - camera2D.aspect;
		camera3D.fieldOfView = 80 / camera3D.aspect;
		
		int i = 0;
		foreach (Pin pin in pins)
			pin.Id = i++;

		collisionSphereRenderer = collisionSphere.GetComponent<Renderer>();

		if (!MenuControllerGenerator.controller.isTouchScreen)
			Camera3DForStart.parent = null;
    }

    void OnEnable ()
	{
		if(ServerController.serverController)
		{
			if (gameManager)
			{
				gameManager.HideGameInfo();
			}
        }
    }

    public void SetActiveSlider(CueForceSlider forceSlider)
    {
        _cueForceSlider = forceSlider;
    }

	void Start ()
	{
        //**************************************************
        cameraManager = CameraManager.Instance;
        /*      is3D = PlayerPrefs.GetInt("Current Camera") == 1;
              if(is3D)
              {
                  InGameUIManager.Instance.SwitchCameraTo3D();
              }
              else
              {
                  InGameUIManager.Instance.SwitchCameraTo2D();
              }
      */
        //**************************************************
        // Rendered of Cue and Line for fade effect
        meshRenderer = ballMaskTrim.GetComponent<MeshRenderer>();
		firstLineRender = firstCollisionLine.GetComponent<LineRenderer>();
		secondLineRender = secondCollisionLine.GetComponent<LineRenderer>();
		ballLineRender = ballCollisionLine.GetComponent<LineRenderer>();

        if (!MenuControllerGenerator.controller)
			return;
		FirstStart ();
		cueFSMController.setMoveInTable();

		MenuControllerGenerator.controller.canRotateCue = true;
    }
	void FixedUpdate ()
	{
		if (shotingInProgress)
		{
			UpdateShotCue();
		}
		else
			UnFadeCue();
		if(!allIsSleeping)
		timeAfterShot += Time.fixedDeltaTime;
		if (aspectRationCamera != camera2D.aspect)
		{
			camera2D.orthographicSize = 30 / camera2D.aspect - camera2D.aspect;
			aspectRationCamera = camera2D.aspect;
		}
		else if (aspectRationCamera3D != camera3D.aspect)
		{
			camera3D.fieldOfView = 80 / camera3D.aspect;
			aspectRationCamera3D = camera3D.aspect;
		}
	}

#region FSMController
	void FirstStart ()
	{
		cueRotationStrLocalPosition = cueRotation.localPosition;
		OnShowLineAndSphereFirstTime ();
	}

	BallController bcForMove;
	Vector3 prevScreenPoint = Vector3.zero;
	//When  player can move the cue ball in  table
	public void MoveInTable ()
	{
        SpecialDebulLog("MoveInTable", "MoveInTable -> CanControlCue = " + CanControlCue + " SinleChat = " + ChatSinglePlayer.СhatEnabled + " MultiChat = " + ChatMultiPlayer.СhatEnabled + " IsChangingPointShot = " + GameData.Instance.IsChangingPointShot + " " + (oldAllIsSleeping != allIsSleeping));
		if (!CanControlCue || ChatSinglePlayer.СhatEnabled || ChatMultiPlayer.СhatEnabled || GameData.Instance.IsChangingPointShot)
			return;
		if(oldAllIsSleeping != allIsSleeping)
		{
			SetOnChengSleeping ();
			cueFSMController.setInMove();
			if(allIsSleeping && ServerController.serverController)
			{
				//Debug.Log("MoveInTable..." + Time.realtimeSinceStartup);
				gameManager.StartCalculateShotTime(ServerController.serverController.isMyQueue);
			}
			return;
		}
		
		// not do raycast if direction not changed
		Vector3 screenPoint = menu.GetScreenPoint();
        //if (prevScreenPoint != screenPoint)
        //	prevScreenPoint = screenPoint;
        //else
        //	return;

        // Requirement of customer. Work only in debug version
        if (allBallMove)
        {
            if ((!ServerController.serverController || (ServerController.serverController.isMyQueue && networkAllIsSleeping)) && menu.GetButtonDown() && prevScreenPoint != screenPoint)
            {
                prevScreenPoint = screenPoint;
                Ray ray = cameraManager.CurrentCamera.ScreenPointToRay(menu.GetScreenPoint());
                RaycastHit hit;

                if (Physics.SphereCast(ray, 3.0f/*5.0f*/ * ballRadius, out hit, 1000.0f, mainBallMask))
                {
                    OnSelectBall(ballController.transform.position);
                    if (!bCursorHand)
                    {
                        bCursorHand = true;
                        Cursor.SetCursor(cursorHand, offsetCursor, CursorMode.Auto);
                        if (fadeCoroutine != null)
                            StopCoroutine(fadeCoroutine);
                        fadeCoroutine = StartCoroutine(ballUnFadeAndFade(ballController, 77f / 255f, .5f, true));
                    }
                    if (ServerController.serverController && ServerController.serverController.isMyQueue)
                    {
                        ServerController.serverController.SendRPCToServer("OnSelectBall", ServerController.serverController.otherNetworkPlayer, ballController.transform.position);
                    }
                }

                // TODO only for test
                if (allBallMove && Physics.SphereCast(ray, 2.0f/*5.0f*/ * ballRadius, out hit, 1000.0f, ballMask))
                {
                    bcForMove = hit.collider.gameObject.GetComponent<BallController>();
                    Debug.Log("MoveOnTable");
                    HideLineAndSphere();
                    MenuControllerGenerator.controller.canControlCue = false;
                    ballMovePosition = bcForMove.transform.position;
                    ballSelectPosition = bcForMove.transform.position;
                    ballCurrentPosition = ballSelectPosition;
                    bcForMove.ballIsSelected = true;
                    bcForMove.GetComponent<Collider>().enabled = false;
                    bcForMove.GetComponent<Rigidbody>().useGravity = false;
                    bcForMove.GetComponent<Rigidbody>().isKinematic = true;
                    canMoveBall = true;
                    cueFSMController.setMoveBall();
                    if (!bCursorHand)
                    {
                        bCursorHand = true;
                        Cursor.SetCursor(cursorHand, offsetCursor, CursorMode.Auto);
                        if (fadeCoroutine != null)
                            StopCoroutine(fadeCoroutine);
                        fadeCoroutine = StartCoroutine(ballUnFadeAndFade(bcForMove, 77f / 255f, .5f, true));
                    }
                    if (ServerController.serverController && ServerController.serverController.isMyQueue)
                    {
                        ServerController.serverController.SendRPCToServer("SetBallSleeping", ServerController.serverController.otherNetworkPlayer, bcForMove.id, bcForMove.transform.position);
                    }
                }
                // TODO only for test END
            }
            else if (bCursorHand)
            {
                bCursorHand = false;
                Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
                if (fadeCoroutine != null)
                    StopCoroutine(fadeCoroutine);
                // TODO  only for test
                if (bcForMove)
                    fadeCoroutine = StartCoroutine(ballUnFadeAndFade(bcForMove, 77f / 255f, .5f, false));
                else
                    // TODO only for test END
                    fadeCoroutine = StartCoroutine(ballUnFadeAndFade(ballController, 77f / 255f, .5f, false));
                // TODO  only for test
                bcForMove = null;
                // TODO  only for test END
            }
        }

        SpecialDebulLog("MoveInTable2", "MoveInTable 2 ->allIsSleeping = " + allIsSleeping + " = " + !MenuControllerGenerator.controller.isTouchScreen + " canDrawLinesAndSphere = " + canDrawLinesAndSphere + " !shotingInProgress = " + !shotingInProgress);
        if (allIsSleeping && canDrawLinesAndSphere && !shotingInProgress/* && !ballIsOut*/)
		DrawLinesAndSphere ();

		canDrawLinesAndSphere = true;
	}

	public void MoveBall ()
	{
		if(ballIsOut)
		{
			OnBallIsOut(false);
		}

		if(ballController.ballIsSelected)
		{
			if((!ServerController.serverController || ServerController.serverController.isMyQueue) && menu.MouseIsMove )
			OnMoveBall();
			if(ServerController.serverController && !ServerController.serverController.isMyQueue)
			{
				ballController.transform.position = Vector3.Lerp(ballController.transform.position, ballMovePosition, 10.0f*Time.deltaTime);
			}
		}
		if(ballController.ballIsSelected && menu.GetButtonUp())
		{
            OnUnselectBall();
			if(ServerController.serverController && ServerController.serverController.isMyQueue)
			{
				ServerController.serverController.SendRPCToServer("OnUnselectBall",ServerController.serverController.otherNetworkPlayer);
			}
		}

		// TODO only for test
		if (bcForMove)
		{
            if ((!ServerController.serverController || ServerController.serverController.isMyQueue) && menu.MouseIsMove)
				OnMoveBall();
		}
		if (bcForMove && menu.GetButtonUp())
		{
            OnUnselectBall();
		}
		// TODO only for test END
}
	public void InMove ()
	{
		CheckAllIsSleeping ();

		if(oldAllIsSleeping != allIsSleeping)
		{
			SetOnChengSleeping ();
			if(allIsSleeping)
			{
				StopCoroutine("WaitAndShowLineAndSphere");
				StartCoroutine("WaitAndShowLineAndSphere", false);
				if(ServerController.serverController)
				{
					//Debug.Log("InMove..." + Time.realtimeSinceStartup);
					gameManager.StartCalculateShotTime(ServerController.serverController.isMyQueue);
				}

				// restory position pins
				foreach (Pin prp in pins)
					prp.RestoryPosition();
                //Rules.Instance.GetPointsForShot(!ServerController.serverController || ServerController.serverController.isMyQueue);

                if (!ServerController.serverController || (ServerController.serverController.isMyQueue && myHit))
                {
                    if (Rules.Instance.InNeedRestoryPositionCueBall())
                        ballController.RessetPosition(centerPoint.position, false);
                    Rules.Instance.GetPointsForShot();
                }
            }
			else
			{
				HideLineAndSphere ();
			}

			foreach (BallController item in ballControllers)
			{
				item.inMove = !allIsSleeping;
			}
		}
        SpecialDebulLog("InMove", "InMove -> allIsSleeping = " + allIsSleeping + " = " + allIsSleeping + " !shotingInProgress = " + !shotingInProgress + " !ballIsOut = " + !ballIsOut);
        if (allIsSleeping && !shotingInProgress && !ballIsOut)
		DrawLinesAndSphere ();

		if(!allIsSleeping)
		{
			if (ballController.ballReflaction)
			{
				ballController.ballReflaction.parent.localScale *= -1.0f;
				ballController.ballReflaction.parent.localScale *= -1.0f;
			}
		}
	}
#endregion
	//When player select the ball
	public void OnSelectBall (Vector3 ballPosition)
	{
        Debug.Log("OnSelectBall");
		HideLineAndSphere ();
		MenuControllerGenerator.controller.canControlCue = false;
		ballMovePosition = ballPosition;
		ballSelectPosition = ballPosition;
		ballCurrentPosition = ballSelectPosition;

		ballController.ballIsSelected = true;



		ballController.GetComponent<Collider>().enabled = false;
		ballController.GetComponent<Rigidbody>().useGravity = false;
		ballController.GetComponent<Rigidbody>().isKinematic = true;

		canMoveBall = true;	
		cueFSMController.setMoveBall();
	}
	
    //When  player move the cue ball in table
	void OnMoveBall ()
	{
		Transform StartOrMoveCube = isFirsTime? StartCube : MoveCube;
		Ray ray = cameraManager.CurrentCamera.ScreenPointToRay(menu.GetScreenPoint());
		RaycastHit hit;
		
		if(Physics.SphereCast(ray, 1.0f*ballRadius , out hit, 1000.0f, canvasAndBallMask))
		{
			VectorOperator.MoveBallInQuad(StartOrMoveCube, ballRadius, hit.point, ref ballCurrentPosition);
			// TODO For test
			if (bcForMove)
			{
				bcForMove.transform.position = ballCurrentPosition + 1.5f * ballRadius * Vector3.up;
                if (ServerController.serverController && ServerController.serverController.isMyQueue)
				    ServerController.serverController.SendRPCToServer("SetBallSleeping", ServerController.serverController.otherNetworkPlayer, bcForMove.id, bcForMove.transform.position);
			}
			else
				ballController.transform.position = ballCurrentPosition + 1.5f * ballRadius * Vector3.up;

			if(ServerController.serverController && ServerController.serverController.isMyQueue)
			{
				sendTime += Time.deltaTime;
				if(sendTime > 1.0f/10.0f)
				{
					sendTime = 0.0f;
					ServerController.serverController.SendRPCToServer("SetOnMoveBall", ServerController.serverController.otherNetworkPlayer, ballController.transform.position);
				}
			}
		}
	}
	//When player unselect the ball
	public void OnUnselectBall ()
	{
		MenuControllerGenerator.controller.canControlCue = true;

		Ray ray = new Ray(ballController.transform.position + 3.0f*ballRadius*Vector3.up, -Vector3.up);
		RaycastHit hit;
		
		if(Physics.SphereCast(ray, 1.0f*ballRadius , out hit, 1000.0f, canvasAndBallMask))
		{

			hitCanvas = hit.collider.gameObject.layer == LayerMask.NameToLayer("Canvas");
		}
		else
		{
			hitCanvas = false;
		}

		ballController.ballIsSelected = false;
		canMoveBall = false;
		cueFSMController.setMoveInTable();


		if(hitCanvas)
		{
			transform.position = ballController.transform.position;
		}
		else
		{
			ballController.RessetPosition( ballSelectPosition, true );
		}

		ballController.GetComponent<Collider>().enabled = true;
		ballController.GetComponent<Rigidbody>().useGravity = true;
		ballController.GetComponent<Rigidbody>().isKinematic = false;

		// TODO For test
		if (bcForMove)
		{
            if (ServerController.serverController && ServerController.serverController.isMyQueue)
			    ServerController.serverController.SendRPCToServer("SetBallSleeping", ServerController.serverController.otherNetworkPlayer, bcForMove.id, bcForMove.transform.position);
			bcForMove.ballIsSelected = false;
			bcForMove.GetComponent<Collider>().enabled = true;
			bcForMove.GetComponent<Rigidbody>().useGravity = true;
			bcForMove.GetComponent<Rigidbody>().isKinematic = false;
		}
		// TODO For test END

		if(ServerController.serverController && ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
		{
			ServerController.serverController.SendRPCToServer("ForceSetBallMove", ServerController.serverController.otherNetworkPlayer, ballController.id, ballController.GetComponent<Rigidbody>().position, Vector3.zero, Vector3.zero, false);
		}
		StopCoroutine("WaitAndShowLineAndSphere");
		StartCoroutine("WaitAndShowLineAndSphere", true);
	}


	public void CheckAllIsSleeping ()
	{
		if(checkInProgress)
			return;
		checkOthersIsSleeping = thenInshoting;
		checkAllIsSleeping = thenInshoting;

		if(thenInshoting)
		{
			foreach (BallController ballC in ballControllers)
			{
				if(ballC != ballController)
				checkOthersIsSleeping = checkOthersIsSleeping && ballC.IsSleeping();
			}
			checkAllIsSleeping = checkOthersIsSleeping && (ballController.IsSleeping() || ballController.ballIsOut);
			StartCoroutine(WaitAndCheckAllIsSleeping ());
		}

		if(!checkAllIsSleeping)
		{
			ballsAudioPlayingCount = 0;
			foreach (BallController item in ballControllers)
			{
				//if (item.BallAudioSource.isPlaying)
                if(SoundManager.Instance.IsPlaying(item.BallAudioSource))
					ballsAudioPlayingCount ++;
			}
		}
	}
	IEnumerator WaitAndCheckAllIsSleeping ()
	{
		checkInProgress = true;
		yield return new WaitForSeconds(0.5f); // 1.0f);
	
		bool _allIsSleeping = thenInshoting;
		bool _othersSleeping = thenInshoting;
		foreach (BallController ballC in ballControllers)
		{

			if(ballC != ballController)
			{
				_othersSleeping = _othersSleeping && ballC.IsSleeping();
				_allIsSleeping = _allIsSleeping && ballC.IsSleeping();

			}
			else
			{
				_allIsSleeping = _allIsSleeping && (ballController.IsSleeping() || ballController.ballIsOut);
			}
		}
		if(_allIsSleeping == checkAllIsSleeping)
		{
			allIsSleeping = _allIsSleeping;
		}
		if(_othersSleeping == checkOthersIsSleeping)
		{
			othersSleeping = _othersSleeping;
		}
		checkInProgress = false;
	}
	void CreateAndSortBalls ()
	{
		ballRadius = 0.5f*ballControllerPrefab.transform.lossyScale.x;
		float newBallRadius = ballRadius + ballsDistance;
		ballsCount = ballTextures.Length;

		startBallControllers = new BallController[ballsCount];

		for (int i = 0; i < ballsCount; i++) 
		{
			BallController bc = null;
			float deltaX = deltaPositions[i].x;
			float deltaZ = deltaPositions[i].y;
            Vector3 position = i == 0 ? mainBallPoint.localPosition : new Vector3(deltaX, mainBallPoint.localPosition.y, deltaZ) ;
            bc = BallController.Instantiate(ballControllerPrefab) as BallController;
            bc.transform.SetParent(ballsParent, false);
            bc.transform.localPosition = position;
            bc.startPosition = bc.transform.position;
            bc.isMain = i == 0;
			if(i == 0)
			{
				bc.ballType = 0;
				ballController = bc;
				ballController.gameObject.layer = LayerMask.NameToLayer("MainBall");
				ballAudio = ballController.GetComponent<AudioSource>();
			}
			else
			{
				bc.gameObject.layer = LayerMask.NameToLayer("Ball");
				if(i == 8)
				{
					bc.isBlack = true;
					bc.ballType = 2;
				}
				else if(i >= 1 && i <= 7)
				{
					bc.ballType = 1;
				}
				else
				{
					bc.ballType = -1;
				}

			}
			bc.id = i;
			bc.cueController = this;
			bc.GetComponent<Renderer>().material.mainTexture = ballTextures[i];
			if (bc.ballReflaction)
			{
				bc.ballReflaction.GetComponent<Renderer>().material.mainTexture = ballTextures[i];
				bc.ballReflaction.parent = ballsReflactionParent;
				bc.ballReflaction.localScale = bc.transform.localScale;
			}
		//	if (bc.shadow)
			//	bc.shadow.parent = ballsReflactionParent;
			ballControllers.Add(bc);
			startBallControllers[i] = bc;
		}

	}


	IEnumerator WaitAndShowLineAndSphere (bool isFirst)
	{
		yield return new WaitForEndOfFrame();
		while(ballIsOut)
		{
			yield return null;
		}
		if(ServerController.serverController)
		{
			while(!networkAllIsSleeping)
			{
				yield return null;
			}
		}
		if(allIsSleeping)
		ShowLineAndSphere (isFirst);
	}
	public void OnBallIsOut (bool isOut)
	{
		ballIsOut = isOut;
		ballController.ballIsOut = ballIsOut;
	}

	void SetOnChengSleeping ()
	{
		//Set Some On Cheng Sleeping
		oldAllIsSleeping = allIsSleeping;
	}

	void DrawLinesAndSphere ()
	{
        SpecialDebulLog("DrawLinesAndSphere", "DrawLinesAndSphere -> CanControlCue = " + CanControlCue + " SinleChat = " + ChatSinglePlayer.СhatEnabled + " MultiChat = " + ChatMultiPlayer.СhatEnabled + " IsChangingPointShot = " + GameData.Instance.IsChangingPointShot + " " + !ballController.ballIsSelected + " " + allIsSleeping + " " + (!ServerController.serverController || networkAllIsSleeping));
        if (!ChatSinglePlayer.СhatEnabled && !ChatMultiPlayer.СhatEnabled && CanControlCue && !ballController.ballIsSelected && allIsSleeping && (!ServerController.serverController || networkAllIsSleeping))
			OnDrawLinesAndSphere ();
	}
	IEnumerator WaitAndUncheckForceSlider ()
	{
		yield return new WaitForSeconds(0.3f);
	
		if(!cueForceisActive)
		inTouchForceSlider = false;
	}
	IEnumerator WaitAndRotateCue (Vector3 hitPoint)
	{
		yield return new WaitForSeconds (0.1f);
		if(MenuControllerGenerator.controller.canRotateCue)
			cuePivot.LookAt(hitPoint + ballRadius*Vector3.up);
	}

	Vector3 prevPos = Vector3.zero;
	void ControleCueThenDraw ()
	{
        SpecialDebulLog("ControleCueThenDraw", "ControleCueThenDraw -> MenuControllerGenerator.controller.canControlCue = " + MenuControllerGenerator.controller.canControlCue + " = " + !MenuControllerGenerator.controller.isTouchScreen + " " + menu.GetButton());
        if (MenuControllerGenerator.controller.canControlCue)
		{
			if(!MenuControllerGenerator.controller.isTouchScreen || menu.GetButton())
			{
			
				Ray ray = cameraManager.CurrentCamera.ScreenPointToRay(menu.GetScreenPoint());
				RaycastHit hit;
				if(Physics.Raycast(ray, out hit, 1000.0f, canvasMask))
				{
					if(menu.GetButtonDown() && allIsSleeping)
					{
						if(!MenuControllerGenerator.controller.isTouchScreen)
						{
							cue2DStartPosition = hit.point;
						}
						else
						{
							prevPos.x = menu.GetScreenPoint().x;
							prevPos.y = menu.GetScreenPoint().y;
							//StartCoroutine(WaitAndRotateCue(hit.point));
							inTouchForceSlider = true;
							StartCoroutine(WaitAndUncheckForceSlider ());
						}
					}


					if((!menu.GetButton() || (MenuControllerGenerator.controller.isTouchScreen)) && !menu.GetButtonUp() && allIsSleeping && !inTouchForceSlider)
					{
						if(MenuControllerGenerator.controller.isTouchScreen)
						{
							if (prevPos.x != menu.GetScreenPoint().x || prevPos.y != menu.GetScreenPoint().y)
							{
								Vector3 cuePivotScreenPoint = cameraManager.CurrentCamera.WorldToScreenPoint(cuePivot.position);
								Vector3 newVector = menu.GetScreenPoint() - cuePivotScreenPoint;
								Vector3 oldVector = prevPos - cuePivotScreenPoint;

								float orientY = camera3D.enabled ? 1f : menu.GetScreenPoint().y - cuePivotScreenPoint.y > 0.0f ? 1f : -1f;
								float orientX = camera3D.enabled ? 0f : menu.GetScreenPoint().x - cuePivotScreenPoint.x > 0.0f ? 1f : -1f;

								float deltaX = menu.GetScreenPoint().x - prevPos.x;
								float deltaY = menu.GetScreenPoint().y - prevPos.y;
								float direction = orientY * deltaX - orientX * (menu.GetScreenPoint().y - prevPos.y) >= 0f ? 1f : -1f;
								touchRotateAngle = Vector3.Angle(oldVector, newVector) * direction;
                                if (!activeZoom)
                                {
                                    Vector3 posCamera = camera3D.transform.localPosition;
                                    //bool cAllInnerVisible = Beacon.IsAllVisible;
                                    if (!BeaconTrigger.IsAllVisible && camera3D.enabled)
                                    {
                                        zForZoom = MIN_ZOOM_CAMERA_3D;
                                        zoomInOut = false;
                                        currentTimeAutoMoveCamera = 3f;
                                        StartCoroutine("СoroutineCameraZoomInOut");
                                    }
                                    else if (!camera3D.enabled || Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
                                    {
                                        Vector3 v3 = cuePivot.eulerAngles;
                                        v3.y += touchRotateAngle;
                                        cuePivot.eulerAngles = v3;
                                    } // // change angle of camera
                                    //else if (/*cAllInnerVisible || */deltaY > 0f/* || !prevAllVisible*/)
                                    else if (deltaY != 0f)
                                    {
                                        prevZoom = posCamera.z;
                                        posCamera.z = Mathf.Clamp(posCamera.z + deltaY * .1f, MAX_ZOOM_CAMERA_3D, MIN_ZOOM_CAMERA_3D);
                                        if (posCamera.z > ANGLE_FOR_ZOOM)
                                            posCamera.y = 0f;
                                        else
                                            posCamera.y = (-posCamera.z + ANGLE_FOR_ZOOM) * 0.9f;
                                        camera3D.transform.localPosition = posCamera;
                                        Vector3 rotation = Vector3.zero;
                                        rotation.x = posCamera.y * 2f;


                                        Quaternion rotationCamera = Quaternion.Euler(rotation);
                                        camera3D.transform.localRotation = rotationCamera;
                                        //prevAllVisible = cAllInnerVisible;
                                    }
                                    else
                                    {
                                        Debug.Log("Active auto zoom currentZoom=" + camera3D.transform.localPosition.z + " -> " + zForZoom);
                                    }
                                    //else
                                    //{
                                    //    zForZoom = prevZoom;
                                    //    zoomInOut = false;
                                    //    StartCoroutine("СoroutineCameraZoomInOut");
                                    //}
                                }
                                prevPos.x = menu.GetScreenPoint().x;
								prevPos.y = menu.GetScreenPoint().y;
							}
						}
                        else
                        {
                            cuePivot.LookAt(hit.point + ballRadius*Vector3.up);
						}
						cueDisplacement = 0.0f;
					}

					if(menu.GetButton())
					{
						if(!MenuControllerGenerator.controller.isTouchScreen)
						cueDisplacement = cueMaxDisplacement*Mathf.Clamp01( Vector3.Dot(hit.point - cue2DStartPosition, (cuePivot.position - ballRadius*Vector3.up - cue2DStartPosition).normalized)/cueMaxDisplacement);
						cueForceValue = 1.0f;
					}
				}
			}
			
			if(menu.GetButtonUp())
			{
				MenuControllerGenerator.controller.canRotateCue = true;
				CheckShotCue ();
			}
		}
		else
		{
			cueDisplacement = 0.0f;
		}

		if(menu.GetButtonUp())
		{
			MenuControllerGenerator.controller.canControlCue = true;
		}
	}

	public void CheckShotCue ()
	{
		cueForceValue = Mathf.Clamp(cueDisplacement/cueMaxDisplacement, 0.01f, 1.0f);
		inTouchForceSlider = false;
		cueForceisActive = false;
		if(cueForceValue > 0.011f)
		{
			// for achiements
			Rules.Instance.PowerQue = cueForceValue;
			ShotCue ();
		}
		else
		{
			cueForceValue = 1.0f;
			cueRotation.localPosition = cueRotationStrLocalPosition;
		}
		if(MenuControllerGenerator.controller.isTouchScreen)
		{
            //CueForceSlider cueForceSlider = FindObjectOfType(typeof(CueForceSlider)) as CueForceSlider;
            if (_cueForceSlider != null)
            {
                //Debug.Log("Reset slider on touchscreen in CheckShotCue");
                _cueForceSlider.Reset();
            }
			//(CueForce.FindObjectOfType(typeof(CueForce)) as CueForce).Resset();
		}
	}

	void SendCueControlToNetwork ()
	{
		sendTime += Time.deltaTime;
		if(sendTime > .1f)//1.0f/10.0f/*Network.sendRate*/)
		{
			sendTime = 0.0f;
			if(cueRotationLocalPosition != cueRotation.localPosition || cuePivotLocalRotation != cuePivot.localRotation)
			{
				cueRotationLocalPosition = cueRotation.localPosition; 
				cuePivotLocalRotation = cuePivot.localRotation;
				ServerController.serverController.SendRPCToServer("SendCueControl", ServerController.serverController.otherNetworkPlayer, cuePivot.localRotation, cueRotation.localPosition, new Vector3(rotationDisplacement.x, rotationDisplacement.y, 0.0f), (int)(GameData.Instance.selectedCue.cueType), GameData.Instance.selectedCue.cueId);
			}
		}
	}
	public void SetCueControlFromNetwork(Quaternion localRotation, Vector3 localPosition, Vector2 displacement, Cue_Shop_Type cueType, int cueId)
	{
		cuePivotLocalRotation = localRotation;
		cueRotationLocalPosition = localPosition;
		cueBallPivotLocalPosition = new Vector3(displacement.x*cueBallPivot.radius, displacement.y*cueBallPivot.radius, cueBallPivot.transform.localPosition.z);
        largeCueBallPivotLocalPosition = new Vector3(displacement.x * largeBallPivot.radius, displacement.y * largeBallPivot.radius, largeBallPivot.transform.localPosition.z);
        rotationDisplacement.x = displacement.x;
		rotationDisplacement.y = displacement.y;
		if (cueIdOpponent == -1)
		{
			UpdateCueMaterial(cueType, cueId);
		}
		cueTypeOpponent = cueType;
		cueIdOpponent = cueId;
	}
	void SendCueControl ( Quaternion localRotation, Vector3 localPosition)
	{
        cuePivot.localRotation = Quaternion.Lerp(cuePivot.localRotation, localRotation, 10.0f*Time.deltaTime);
		cueRotation.localPosition = Vector3.Lerp(cueRotation.localPosition, localPosition, 10.0f*Time.deltaTime);
        cueBallPivot.transform.localPosition = Vector3.Lerp(cueBallPivot.transform.localPosition, cueBallPivotLocalPosition, 10.0f*Time.deltaTime);
        largeBallPivot.transform.localPosition = Vector3.Lerp(largeBallPivot.transform.localPosition, largeCueBallPivotLocalPosition, 10.0f * Time.deltaTime);
    }

	public void OnDrawLinesAndSphere ()
	{
        SpecialDebulLog("OnDrawLinesAndSphere", "OnDrawLinesAndSphere -> = " + !ServerController.serverController + " " + (!ServerController.serverController || (ServerController.serverController.isMyQueue && networkAllIsSleeping)));
        if (!ServerController.serverController || (ServerController.serverController.isMyQueue && networkAllIsSleeping))
		{
			ControleCueThenDraw ();
		}
		if(ServerController.serverController)
		{
			if(!MenuControllerGenerator.controller.playWithAI)
			{
				if (ServerController.serverController.isMyQueue)
				{
					SendCueControlToNetwork();
					if (_cueForceSlider != null && !_cueForceSlider.IsEnabledSlider && !shotingInProgress)
						_cueForceSlider.EnableSForceSlider();
				}
				else
				{
                    SendCueControl(cuePivotLocalRotation, cueRotationLocalPosition);
					if (_cueForceSlider != null && _cueForceSlider.IsEnabledSlider)
					{
						_cueForceSlider.DisableForceSlider();
					}
				}
			}
			checkMyBall = false;
		}
		currentHitBallController = null;
		if(allIsSleeping)
		transform.position = ballController.transform.position;
		// new
		float displacementFactor = .075f;//0.1f;
		ballVelocityOrient = (VectorOperator.getProjectXZ(cuePivot.forward, true) - displacementFactor * rotationDisplacement.x * cuePivot.right).normalized;

		//ballVelocityOrient = VectorOperator.getProjectXZ( cuePivot.forward, true);

		//float changeValue = ballChangeDirection.Evaluate(Mathf.Abs(rotationDisplacement.x));
		//if (rotationDisplacement.x < 0)
		//	changeValue *= -1;
		//ballVelocityOrient = Quaternion.AngleAxis(-90 * changeValue, Vector3.up) * ballVelocityOrient;

		float lostEnergy = 1.0f - Mathf.Sqrt( (Mathf.Pow(rotationDisplacement.y,2.0f) + Mathf.Pow(rotationDisplacement.x,2.0f)) );
		lostEnergy = Mathf.Clamp(lostEnergy, 0.75f, 1.0f);
	
		//ballShotVelocity = ballMaxVelocity*cueForceValue*lostEnergy*ballVelocityOrient;
		float correctFromVertAngle = ballChangeVelocity2.Evaluate(MIN_ANGLE_QUE / (realCueVerticalAngle + MIN_ANGLE_QUE));
		//Debug.Log(correctFromVertAngle + "   " + MIN_ANGLE_QUE / (realCueVerticalAngle + MIN_ANGLE_QUE));

		ballShotVelocity = ballMaxVelocity * cueForceValue * lostEnergy * ballVelocityOrient * ballChangeVelocity.Evaluate(rotationDisplacement.magnitude) * correctFromVertAngle;
//		ballShotVelocity = ballMaxVelocity * cueForceValue * ballVelocityOrient * ballChangeVelocity.Evaluate(rotationDisplacement.magnitude) * correctFromVertAngle;

		/*if (rotationDisplacement.x < .1f && rotationDisplacement.x > -.1f)
			rotationDisplacement.x = 0f;
		if (rotationDisplacement.y < .1f && rotationDisplacement.y > -.1f)
			rotationDisplacement.y = 0f;
		*/

		ballShotAngularVelocity = ballAngularVelocityCurve.Evaluate(cueForceValue)*Mathf.Pow( cueForceValue, 2.0f )*20.0f*(Mathf.Clamp(rotationDisplacement.y, -1f,1f)*cuePivot.right - rotationDisplacement.x*cuePivot.up);
		// new
		ballShotAngularVelocity -= 20.0f * rotationDisplacement.x * cuePivot.forward * ballAngularVelocityCurve.Evaluate(cueForceValue);

		//ballShotAngularVelocity = ballAngularVelocityCurve.Evaluate(cueForceValue) * 20.0f * (rotationDisplacement.y * cuePivot.up - rotationDisplacement.x * cuePivot.right);

		/*if (MIN_ANGLE_QUE + 10f > realCueVerticalAngle)
			;//ballShotAngularVelocity = Vector3.zero;
		else if (cueAngleController.MaxAngle - 10f < realCueVerticalAngle)
		{
			float magnitude = ballShotVelocity.magnitude * 2.5f;
			ballShotAngularVelocity = -1 * cuePivot.up + rotationDisplacement.x * cuePivot.right;
			ballShotVelocity = ballShotAngularVelocity.normalized * magnitude;
			ballShotAngularVelocity = Vector3.zero;
		}
		else
		{
			float magnitude = ballShotAngularVelocity.magnitude;
			if (rotationDisplacement.x > 0.1f || rotationDisplacement.x < -0.1f)
			{
				//ballShotVelocity /= 2.4f;
				ballShotVelocity /= 1.5f;
				//magnitude *= 0.5f;
				//magnitude *= 20f;
				ballShotAngularVelocity = -1f * cuePivot.right - rotationDisplacement.x * cuePivot.up;
			}
			else
			{
				ballShotVelocity /= 2f;
				magnitude *= .8f;
				if (magnitude < 1f)
				{
					ballShotVelocity *= 1.5f;
					magnitude = ballShotVelocity.magnitude / 2f;
				}
				ballShotAngularVelocity = (rotationDisplacement.y <= 0f ? -1f : 1f) * cuePivot.right;
			}
			ballShotAngularVelocity = ballShotAngularVelocity.normalized * magnitude;
		}
		*/
        Ray firstRey = new Ray(cuePivot.position, ballVelocityOrient);
		RaycastHit firstHit;

		if(Physics.SphereCast(firstRey, /*0.99f**/ballRadius, out firstHit, 1000.0f, wallAndBallMask))
		{
			collisionSphere.position = firstHit.point + ballRadius*firstHit.normal;

		    Vector3	outVelocity = VectorOperator.getProjectXZ(ballShotVelocity, true);

			secondVelocity = VectorOperator.getBallVelocity(ballRadius, outVelocity, collisionSphere.position, 
			                                                wallAndBallMask, 20.0f*ballRadius, ref hitBallVelocity, ref hitCollider, rotationDisplacement.x);
			if (hitCollider)
			{
				currentHitBallController = hitCollider.GetComponent<BallController>();
				ballCollisionLine.enabled = currentHitBallController && allIsSleeping;

				firstCollisionLine.SetVertexCount(2);
				firstCollisionLine.SetPosition(0, ballController.transform.position);
				firstCollisionLine.SetPosition(1, collisionSphere.position);

				secondCollisionLine.SetVertexCount(2);
				secondCollisionLine.SetPosition(0, collisionSphere.position);
				float angle1 = secondVelocity.magnitude / ballMaxVelocity;
				RaycastHit hit;
				Vector3 pos1 = collisionSphere.position;
				Vector3 pos2 = collisionSphere.position + (angle1 * ballLineLength + 0.7f * ballRadius) * secondVelocity.normalized;
				pos2.y = pos1.y;
				Ray ray = new Ray(pos1, (pos2 - pos1).normalized);
				bool isWall = Physics.Raycast(ray, out hit, Vector3.Distance(pos1, pos2), wallMask.value/* | pocketMask.value*/);
				secondCollisionLine.SetPosition(1, isWall ? hit.point : pos2);

				if (currentHitBallController)
				{
					ballCollisionLine.SetVertexCount(2);
					ballCollisionLine.SetPosition(0, currentHitBallController.GetComponent<Rigidbody>().position);
					Vector3 hbvOrient = (currentHitBallController.GetComponent<Rigidbody>().position - collisionSphere.position).normalized;
					float angle2 = Mathf.Abs(Vector3.Dot(hbvOrient, cuePivot.forward));
					pos1 = currentHitBallController.GetComponent<Rigidbody>().position;
					pos2 = currentHitBallController.GetComponent<Rigidbody>().position + (angle2 * ballLineLength + 0.99f * ballRadius) * hbvOrient;
					pos2.y = pos1.y;
					ray = new Ray(collisionSphere.position, (pos2 - pos1).normalized);
					isWall = Physics.Raycast(ray, out hit, Vector3.Distance(pos1, pos2), wallMask.value/* | pocketMask.value*/);
					ballCollisionLine.SetPosition(1, isWall ? hit.point : pos2);
				}
				else if (!currentHitBallController)
				{
					ballCollisionLine.SetVertexCount(0);
				}
			}
		}

		/*if (Mathf.Abs(rotationDisplacement.y) > .1f || Mathf.Abs(rotationDisplacement.x) > .1f)
		{
			ballCollisionLine.SetVertexCount(0);
			firstCollisionLine.SetVertexCount(0);
			secondCollisionLine.SetVertexCount(0);
			collisionSphereRenderer.enabled = false;
		}
		else if (firstCollisionLine.enabled)
			collisionSphereRenderer.enabled = true;
		*/
		if(ServerController.serverController)
		{
			if((gameManager.tableIsOpened && (!currentHitBallController || !currentHitBallController.isBlack)) || !currentHitBallController || 
			   (currentHitBallController.isBlack && gameManager.ballType != 0 && (ServerController.serverController.isMyQueue? gameManager.remainedBlackBall: gameManager.otherRemainedBlackBall)))
			{
				checkMyBall = true;
			}
			else
			{
				checkMyBall = !currentHitBallController.isBlack && 
					(ServerController.serverController.isMyQueue? currentHitBallController.ballType == gameManager.ballType :
					 currentHitBallController.ballType != gameManager.ballType);
			}

			if(oldCheckMyBall != checkMyBall)
			{
				oldCheckMyBall = checkMyBall;
				collisionSphere.GetComponent<Renderer>().sharedMaterial.mainTexture = checkMyBall? collisionBall:collisionBallRed;
			}
		}
	}
	void HideLineAndSphere ()
	{
		OnHideLineAndSphere ();
        //CueForceSlider cueForceSlider = FindObjectOfType(typeof(CueForceSlider)) as CueForceSlider;
        if (_cueForceSlider != null)
        {
            Debug.Log("Hiding line and sphere");
            _cueForceSlider.DisableForceSlider();
        }
    }
	public void OnHideLineAndSphere ()
	{
		collisionSphere.GetComponent<Renderer>().enabled = false;
		firstCollisionLine.enabled = false;
		secondCollisionLine.enabled = false;
		ballCollisionLine.enabled = false;
	}

	void ShowLineAndSphere (bool isFirst)
	{
		if(ServerController.serverController && ServerController.serverController.isMyQueue)
		{
			ServerController.serverController.SendRPCToServer("SendCueControl", ServerController.serverController.otherNetworkPlayer, cuePivot.localRotation, cueRotationStrLocalPosition, Vector3.zero, (int)(GameData.Instance.selectedCue.cueType), GameData.Instance.selectedCue.cueId);
		}
		if(isFirst)
			OnShowLineAndSphereFirstTime();
		else
		    OnShowLineAndSphere ();

        //CueForceSlider cueForceSlider = FindObjectOfType(typeof(CueForceSlider)) as CueForceSlider;
        if (_cueForceSlider != null)
        {
            _cueForceSlider.EnableSForceSlider();
        }

    }
	public void OnShowLineAndSphereFirstTime ()
	{
		MenuControllerGenerator.controller.canControlCue = true;
		cueForceValue = 1.0f;
		collisionSphere.GetComponent<Renderer>().enabled = true;
		firstCollisionLine.enabled = true;
		secondCollisionLine.enabled = true;
		ballCollisionLine.enabled = true;
	}
	public void OnShowLineAndSphere ()
	{
		foreach (BallController item in ballControllers) 
		{
			if(item.isMain)
				StartCoroutine(WaitWhenResse (item));
			else
			item.GetComponent<Rigidbody>().Sleep();
		}
		OnShowLineAndSphereFirstTime ();
	}

	IEnumerator WaitWhenResse (BallController item)
	{
		SetAllAlpha(0f);
		timeUnFade = 0f;
		ballMaskTrim.SetActive(true);

		yield return new WaitForSeconds(0.1f); // 0.5f
		if (allIsSleeping)
			item.GetComponent<Rigidbody>().Sleep();
	}

	void SetAllAlpha(float alpha)
	{
		SetAlpha(meshRenderer.materials, alpha);
		SetWidthLine(firstLineRender, alpha);
		SetWidthLine(secondLineRender, alpha);
		SetWidthLine(ballLineRender, alpha);
	}
	void SetWidthLine(LineRenderer renderer, float percent)
	{
		float newWidth = widthLine * percent;
		renderer.SetWidth(newWidth, newWidth);
	}

	void SetAlpha(Material[] materials, float alpha)
	{
		foreach (Material material in materials)
		{
			if (!material.HasProperty("_Color"))
				continue;
			Color color = material.color;
			color.a = alpha;
			material.color = color;
		}
	}

	void UnFadeCue()
	{
		if (timeUnFade >= 1f)
			return;
		timeUnFade = Mathf.Clamp(timeUnFade + Time.deltaTime / 1f, 0f, 1f);
		SetAllAlpha(timeUnFade);
	}
	
	public void OnControlCue ()
	{
		if(ServerController.serverController && !ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
			return;
		float x = ballRadius * cueBallPivot.transform.localPosition.x * .6f / BallPivotMover.MAX_RADIUS; //cueBallPivot.radius; // BallPivotMover.MAX_RADIUS; //
		float y = ballRadius * cueBallPivot.transform.localPosition.y * .6f / BallPivotMover.MAX_RADIUS; //cueBallPivot.radius;

        //float x = ballRadius * largeBallPivot.transform.localPosition.x / largeBallPivot.radius;
        //float y = ballRadius * largeBallPivot.transform.localPosition.y / largeBallPivot.radius;

        rotationDisplacement = (1.0f/(ballRadius))*(new Vector2(x, y));

		float z = - Mathf.Sqrt(Mathf.Clamp(  Mathf.Pow( ballRadius, 2.0f ) - ( Mathf.Pow( x, 2.0f) +  Mathf.Pow( y, 2.0f )), 0.0f, Mathf.Pow( ballRadius, 2.0f ) ));

		if(!shotingInProgress)
		{
			checkCuePosition = new Vector3(x, y, z);
		}


		cueRotation.localPosition = checkCuePosition - cueDisplacement*Vector3.forward;
	}

	void ShotCue ()
	{
		Rules.Instance.startShoot();
		OnShotCue();
		if(ServerController.serverController && ServerController.serverController.isMyQueue && !MenuControllerGenerator.controller.playWithAI)
			ServerController.serverController.SendRPCToServer("OnShotCue", ServerController.serverController.otherNetworkPlayer);
	}
	IEnumerator WaitAndSendOnShotCue ()
	{
		yield return new WaitForSeconds(0.5f);
		ServerController.serverController.SendRPCToServer("OnShotCue", ServerController.serverController.otherNetworkPlayer);
	}
	private float shotTime = 0.0f;
	//Player is shot (for cue)
	public void OnShotCue (bool inMyHit = true)
	{
		myHit = inMyHit;
		foreach (Pin pin in pins)
			pin.SetNoKinematic();
		if (!allIsSleeping)
			return;
		timeAfterShot = 0.0f;

		shotTime = 0.0f;
		isFirsTime = false;
        Debug.Log("OnShotCue");
		HideLineAndSphere();
		allIsSleeping = false;
		othersSleeping = false;
		inMove = true;

		shotingInProgress = true;
		thenInshoting = false;
		OnBallIsOut(false);

        zoomInOut = false;
        if (!Beacon.IsAllVisible)
            GetOptimalPositionCamera3D();
        CameraZoomInOut();

		StopCoroutine("WaitWhenAllIsSleeping");
		StartCoroutine("WaitWhenAllIsSleeping");
	}

	//When all balls is sleeping 
	IEnumerator WaitWhenAllIsSleeping ()
	{
		if(ServerController.serverController)
		{
			if(!MenuControllerGenerator.controller.playWithAI)
			{
			    networkAllIsSleeping = false;
			}
			gameManager.ballsInMove = true;
		}
		while(ballController.ballIsSelected || !allIsSleeping || ballIsOut || ballController.ballIsOut)
		{
			yield return null;
		}

		if (ServerController.serverController)
		{
			//if(!MenuControllerGenerator.controller.playWithAI)
			//{
				ServerController.serverController.SendRPCToServer("OnChanghAllIsSleeping", ServerController.serverController.otherNetworkPlayer);
			//}
			bool isMyQueue = ServerController.serverController.isMyQueue;

			if(gameManager.needToChangeQueue)
			{
				ServerController.serverController.isMyQueue = false;
				foreach (BallController bc in ballControllers)
					bc.correctBallPos = Vector3.zero;
			}

			//if(MenuControllerGenerator.controller.playWithAI)
			//{
			//	networkAllIsSleeping = true;
			//}
			if(isMyQueue/* || MenuControllerGenerator.controller.playWithAI*/)
			{
				//if(!MenuControllerGenerator.controller.playWithAI)
				//{
					// wait not more 1 second
					float waitTime = Time.realtimeSinceStartup + 1f;
					while (!networkAllIsSleeping && waitTime > Time.realtimeSinceStartup)
					{
						foreach (BallController item in ballControllers)
						{
							if(networkAllIsSleeping)
								break;
							//item.CancelInvokeSetBallCollisionData ();
							ServerController.serverController.SendRPCToServer("SetBallSleeping", ServerController.serverController.otherNetworkPlayer, item.id, item.GetComponent<Rigidbody>().position);
							yield return null;
						}
					}
					networkAllIsSleeping = true;
				//}

				if(gameManager.needToForceChangeQueue || gameManager.needToChangeQueue)
				{

					while(!networkAllIsSleeping)
					{
						yield return null;
					}
					if((gameManager.isFirstShot && gameManager.firstShotHitCount < 4) || gameManager.setMoveInTable)
					{
						//if(isMyQueue)
						//{
							ServerController.serverController.SendRPCToServer("SetMoveInTable", ServerController.serverController.otherNetworkPlayer);
						//}
						//else
					//	{
						//	cueFSMController.setMoveInTable();
					//	}
					
						string gameInfoErrorText = "";
						string  otherGameInfoErrorText = "";

						if(gameManager.gameInfoErrorText == "")
						{
							if (Rules.Instance.LastPoint < 0)
							{
								gameInfoErrorText = "You made an not legacy break\n" + ServerController.serverController.otherName + " has ball in hand";
								otherGameInfoErrorText = (MenuControllerGenerator.controller.playWithAI ? ServerController.serverController.otherName : ServerController.serverController.myName) +
									" made an not legacy break" + "\nYou have ball in hand";

							}
							else if (Rules.Instance.LastPoint == 0)
							{
								gameInfoErrorText = "You made an safety break\n" + ServerController.serverController.otherName + " has ball in hand";
								otherGameInfoErrorText = (MenuControllerGenerator.controller.playWithAI ? ServerController.serverController.otherName : ServerController.serverController.myName) +
									" made an safety break" + "\nYou have ball in hand";
							}

							/*if(gameManager.isFirstShot && gameManager.firstShotHitCount < 4)
							{
								gameInfoErrorText = "You made an illegal break\n" + ServerController.serverController.otherName + " has ball in hand";
								otherGameInfoErrorText = (MenuControllerGenerator.controller.playWithAI? ServerController.serverController.otherName :ServerController.serverController.myName) + 
									" made an illegal break" + "\nYou have ball in hand";
							}
							else if(!gameManager.isFirstShot && gameManager.setMoveInTable)
							{
								if(gameManager.tableIsOpened)
								{
									gameInfoErrorText = "You need to hit either a solid or striped ball" + "\n" + ServerController.serverController.otherName + " has ball in hand";
									otherGameInfoErrorText = (MenuControllerGenerator.controller.playWithAI? ServerController.serverController.otherName :ServerController.serverController.myName)
										+ " need to hit either a solid or striped ball" + "\nYou have ball in hand";
								}
								else
								{
									gameInfoErrorText = "The cue ball did not strike another ball" + "\n" + ServerController.serverController.otherName + " has ball in hand";
									otherGameInfoErrorText = "The cue ball did not strike another ball" + "\nYou have ball in hand";
								}
							}*/
						}
						else
						{
							gameInfoErrorText = "You " +  gameManager.gameInfoErrorText + "\n" + ServerController.serverController.otherName + " has ball in hand";
							otherGameInfoErrorText = (MenuControllerGenerator.controller.playWithAI? ServerController.serverController.otherName :ServerController.serverController.myName)
								+ "  " + gameManager.gameInfoErrorText + "\nYou have ball in hand";
						}
						if(isMyQueue)
						{
						    gameManager.ShowGameInfoError(gameInfoErrorText, 5.0f);
						} else
						{
							gameManager.ShowGameInfoError(otherGameInfoErrorText, 5.0f);
						}
						if(!MenuControllerGenerator.controller.playWithAI)
						{
							ServerController.serverController.SendRPCToServer("SetErrorText", ServerController.serverController.otherNetworkPlayer, otherGameInfoErrorText);
						}

					}
					//if(!(gameManager.blackBallInHolle || gameManager.otherBlackBallInHolle))
					if (Rules.Instance.LastPoint <= 0)
					{
						//if(MenuControllerGenerator.controller.playWithAI && !isMyQueue)
						//    ServerController.serverController.ChangeQueue(true);
						//else
						//	ServerController.serverController.ChangeQueue(false);
						Debug.Log("1 ------- WaitWhenAllIsSleeping () " + Rules.Instance.LastPoint);
						ServerController.serverController.ChangeQueue(false);
						Debug.Log("2 ------- WaitWhenAllIsSleeping () " + Rules.Instance.LastPoint);
					}
					else
					{
						Debug.Log("1 +++++++ WaitWhenAllIsSleeping () " + Rules.Instance.LastPoint);
						ServerController.serverController.ChangeQueue(true);
						Debug.Log("2 +++++++ WaitWhenAllIsSleeping () " + Rules.Instance.LastPoint);
					}
				}
			}
			/*else
			{
				if (Rules.Instance.LastPoint <= 0)
				{
					ServerController.serverController.ChangeQueue(true);
					Debug.Log("---- NoMyQueue --- WaitWhenAllIsSleeping ()");
				}
				else
				{
					ServerController.serverController.ChangeQueue(false);
					Debug.Log("++++ NoMyQueue +++ WaitWhenAllIsSleeping ()");
				}
			}*/

			if(gameManager.needToForceChangeQueue || gameManager.needToChangeQueue)
			{

			} else
			{

			}

			/*if(gameManager.blackBallInHolle || gameManager.otherBlackBallInHolle)
			{
				if((isMyQueue || MenuControllerGenerator.controller.playWithAI))
				{
					if(gameManager.mainBallIsOut)
					{
						if(isMyQueue)
						{
							gameManager.ShowGameInfoError("you potted the cue ball with black ball", 5.0f);
						} else
						{
							gameManager.ShowGameInfoError(ServerController.serverController.otherName + " potted the cue ball with black ball", 5.0f);
						}
						
						if(!MenuControllerGenerator.controller.playWithAI)
						{
							ServerController.serverController.SendRPCToServer("SetErrorText", ServerController.serverController.otherNetworkPlayer, ServerController.serverController.myName + " potted the cue ball with black ball");
						}
					}else
					{
						if(isMyQueue)
						{
							if(gameManager.firstHitBall && !gameManager.firstHitBall.isBlack &&  gameManager.afterRemainedBlackBall && gameManager.ballType != gameManager.firstHitBall.ballType)
							{
								gameManager.HideGameInfoError();
								gameManager.ShowGameInfoError("need to hit a " + (gameManager.ballType == 1? "solid":"striped") +  " or black ball", 5.0f);
								if(!MenuControllerGenerator.controller.playWithAI)
								{
									ServerController.serverController.SendRPCToServer("SetErrorText", ServerController.serverController.otherNetworkPlayer, ServerController.serverController.myName + " need to hit a " + (gameManager.ballType == 1? "solid":"striped") +  " or black ball");
								}
							}
						} else
						{
							if(gameManager.firstHitBall && !gameManager.firstHitBall.isBlack &&  gameManager.afterOtherRemainedBlackBall && gameManager.ballType == gameManager.firstHitBall.ballType)
							{
								gameManager.HideGameInfoError();
								gameManager.ShowGameInfoError(ServerController.serverController.otherNetworkPlayer + " need to hit a " + (gameManager.ballType == -1? "solid":"striped") +  " or black ball", 5.0f);
							}
						}
						
					}
				}
				
				gameManager.ActivateMenuButtons(true);
			}

			if(gameManager.remainedBlackBall)
			{
				gameManager.afterRemainedBlackBall = true;
			}
			if(gameManager.otherRemainedBlackBall)
			{
				gameManager.afterOtherRemainedBlackBall = true;
			}
			*/

			gameManager.isFirstShot = false;
			gameManager.setMoveInTable = true;
			gameManager.needToChangeQueue = true;
			gameManager.needToForceChangeQueue = false;
			gameManager.firstHitBall = null;
			gameManager.gameInfoErrorText = "";
			gameManager.ballsInMove = false;

			gameManager.mainBallIsOut = false;
			gameManager.otherMainBallIsOut = false;

			/*if(MenuControllerGenerator.controller.playWithAI && !isMyQueue)
			{
				ServerController.serverController.serverMessenger.ShotWithAI ();
			}*/
        }
		RessetShotOptions();
	}

	public void OnPlayBallSound (float volume)
	{
		//ballAudio.volume = volume;
		//ballAudio.Play();

        SoundManager.Instance.PlayAudioSource(ballAudio, volume);

	}
	public void OnPlayCueSound (float volume)
	{
		//cueControlAudio.volume = volume;
		//cueControlAudio.Play();

        SoundManager.Instance.PlayAudioSource(cueControlAudio, volume);
	}

	IEnumerator CueFade()
	{
		yield return new WaitForSeconds(.05f);
		ballMaskTrim.SetActive(false);
	}
	void UpdateShotCue ()
	{
		shotTime += Time.deltaTime;
		if(shotingInProgress && Vector3.Distance(cueRotation.localPosition, checkCuePosition /*+ cueRotationStrLocalPosition*/) < 0.1f*ballRadius)
		{
			StartCoroutine(CueFade());
			cueRotation.localPosition = checkCuePosition /*+ cueRotationStrLocalPosition*/;
			shotingInProgress = false;

			cueDisplacement = 0.0f;

			foreach (BallController item in ballControllers)
			{
				item.inMove = !allIsSleeping;
			}
			StartCoroutine(WaitAndSetThenInshoting ());
			if(!ServerController.serverController || ServerController.serverController.isMyQueue || MenuControllerGenerator.controller.playWithAI)
			{
			    ballController.ShotBall();
				if(ServerController.serverController && !MenuControllerGenerator.controller.playWithAI)
					ServerController.serverController.SendRPCToServer("ShotBall", ServerController.serverController.otherNetworkPlayer, ballShotVelocity, hitBallVelocity, secondVelocity, ballShotAngularVelocity);
			}
		}
		else
		{
			cueRotation.localPosition = Vector3.Lerp(cueRotation.localPosition, checkCuePosition/* + cueRotationStrLocalPosition*/, 100.0f*( 0.9f*cueForceValue + 0.1f)*Time.deltaTime);
		}
	}
	public void SetWhenShotCue ()
	{

	}
	IEnumerator WaitAndSendShotBall ()
	{
		yield return new WaitForSeconds(0.5f + shotTime);
		ServerController.serverController.SendRPCToServer("ShotBall", ServerController.serverController.otherNetworkPlayer, ballShotVelocity, hitBallVelocity, secondVelocity, ballShotAngularVelocity);
	}
	IEnumerator WaitAndSetThenInshoting ()
	{
		yield return new WaitForSeconds(1.0f); // 3.0f
		thenInshoting = true;
	}
    /*
	void SetCamera (Button btn)
	{
		is3D = btn.state;
		//lights.enabled = is3D;
		currentCamera = is3D? camera3D : camera2D;
		camera3D.enabled = false;
		camera2D.enabled = false;
		currentCamera.enabled = true;
		cameraCircularSlider.SetActive( btn.state );
		PlayerPrefs.SetInt("Current Camera", btn.state? 1:0);
	}*/

    public void RessetShotOptions ()
	{
        cueBallPivot.Reset();
        largeBallPivot.Reset();
        if (largeBallPivot.gameObject.activeInHierarchy)
            largeBallPivot.circleSlider.showAnim.HidePopup(largeBallPivot.circleSlider.transform.parent.gameObject);
        cueAngleController.Reset();
		if(ballController.ballIsSelected)
		{
			OnUnselectBall();
		}
        // shifts the camera
        if (MenuControllerGenerator.controller.isTouchScreen)
        {
            zoomInOut = true;
            CameraZoomInOut();
        }
	}

	IEnumerator ballUnFadeAndFade(BallController bc, float maxAlpha, float time, bool unFade)
	{
		Material material = bc.materialBallForMove;
		bc.ballForMove.gameObject.SetActive(true);
		bc.ballForMove.eulerAngles = Vector3.zero;
		float alpha = 0f;
		float increse = maxAlpha / time;
		Color color = material.color;
		if (!unFade)
		{
			increse *= -1f;
			alpha = color.a;
		}
		do
		{
			alpha += increse * Time.deltaTime;
			color.a = alpha;
			material.color = color;
			yield return new WaitForFixedUpdate();
		}
		while (alpha <= maxAlpha && alpha >= 0f);
		color.a = Mathf.Clamp(alpha, 0f, maxAlpha);
		material.color = color;
		if (!unFade)
			bc.ballForMove.gameObject.SetActive(false);
		fadeCoroutine = null;
	}

	public void UpdateCueMaterial(bool myQue = true)
	{
		if (myQue)
			UpdateCueMaterial(GameData.Instance.selectedCue.cueType, GameData.Instance.selectedCue.cueId);
		else
			UpdateCueMaterial(cueTypeOpponent, cueIdOpponent);
	}

	void UpdateCueMaterial(Cue_Shop_Type cueType, int cueId)
	{
		if (cueId == -1)
			return;
		
		Texture2D texture;
		switch (cueType)
		{
			case Cue_Shop_Type.standard:
				texture = CueTextureStandart[cueId];
				break;
			case Cue_Shop_Type.premium:
				texture = CueTexturePremium[cueId];
				break;
			case Cue_Shop_Type.country:
				texture = CueTextureCountry[cueId];
				break;
			default:
				texture = CueTextureStandart[cueId];
				break;
		}
		CueMaterial.mainTexture = texture;

	}

    IEnumerator СoroutineCameraZoomInOut()
    {
        if (activeZoom)
            yield break;
        activeZoom = true;
        posCameraBeforeAutoZoom = camera3D.transform.localPosition;
        Vector3 endPosCamera = posCameraBeforeAutoZoom;
        Vector3 curPosCamera = posCameraBeforeAutoZoom;

        Vector3 posCameraInWorld = camera3D.transform.position;
        Quaternion rotationCameraInWorld = camera3D.transform.rotation;
        Quaternion curRotationCamera = rotationCameraInWorld;

        endPosCamera.z = zForZoom;
        float time = 0f;
        float fTime;
        do
        {
            time += Time.deltaTime;
            if (optimalPosCamera3D == Vector3.zero)
            {
                if (!zoomInOut && ((currentTimeAutoMoveCamera != timeAutoMoveCamera && BeaconTrigger.IsAllVisible) || (currentTimeAutoMoveCamera == timeAutoMoveCamera && Beacon.IsAllVisible)))
                    break;
                curPosCamera.z = Mathf.Lerp(posCameraBeforeAutoZoom.z, endPosCamera.z, time / currentTimeAutoMoveCamera);
                curPosCamera.y = (curPosCamera.z > ANGLE_FOR_ZOOM) ? 0f : (-curPosCamera.z + ANGLE_FOR_ZOOM) * .9f;
                curPosCamera.x = 0f;
                camera3D.transform.localPosition = curPosCamera;
                Vector3 rotation = Vector3.zero;
                rotation.x = curPosCamera.y * 2f;
                Quaternion rotationCamera = Quaternion.Euler(rotation);
                camera3D.transform.localRotation = rotationCamera;
            }
            else
            {
                fTime = time / currentTimeAutoMoveCamera;
                curPosCamera.x = Mathf.Lerp(posCameraInWorld.x, optimalPosCamera3D.x, fTime);
                curPosCamera.y = Mathf.Lerp(posCameraInWorld.y, optimalPosCamera3D.y, fTime);
                curPosCamera.z = Mathf.Lerp(posCameraInWorld.z, optimalPosCamera3D.z, fTime);
                curRotationCamera.x = Mathf.Lerp(rotationCameraInWorld.x, optimalRotationCamera3D.x, fTime);
                curRotationCamera.y = Mathf.Lerp(rotationCameraInWorld.y, optimalRotationCamera3D.y, fTime);
                curRotationCamera.z = Mathf.Lerp(rotationCameraInWorld.z, optimalRotationCamera3D.z, fTime);
                curRotationCamera.w = Mathf.Lerp(rotationCameraInWorld.w, optimalRotationCamera3D.w, fTime);
                camera3D.transform.position = curPosCamera;
                camera3D.transform.rotation = curRotationCamera;
            }
            yield return new WaitForEndOfFrame();
        }
        while (time < currentTimeAutoMoveCamera);
        activeZoom = false;
        optimalPosCamera3D = Vector3.zero;
    }

    void CameraZoomInOut()
    {
        if (MenuControllerGenerator.controller.isTouchScreen/* && camera3D.enabled*/)
        {
            zForZoom = Mathf.Clamp(zoomInOut ? posCameraBeforeAutoZoom.z : MAX_ZOOM_CAMERA_3D, MAX_ZOOM_CAMERA_3D,  MIN_ZOOM_CAMERA_3D);
            currentTimeAutoMoveCamera = timeAutoMoveCamera;
            StartCoroutine("СoroutineCameraZoomInOut");
        }
    }

    void GetOptimalPositionCamera3D()
    {
        Vector3 curPos = camera3D.transform.position;
        float distance = -1f;
        Transform newPos = positionsCamera3D[0];
        foreach (Transform posTrasnform in positionsCamera3D)
        {
            float cDistance = Vector3.Distance(posTrasnform.position, curPos);
            if (distance == -1f || cDistance < distance)
            {
                distance = cDistance;
                newPos = posTrasnform;
            }
        }
        optimalPosCamera3D = newPos.position;
        optimalRotationCamera3D = newPos.rotation;
        if (Mathf.Abs(optimalRotationCamera3D.w - camera3D.transform.rotation.w) > 1f)
        {
            optimalRotationCamera3D.w += (optimalRotationCamera3D.w > 0f) ? -2f : 2f;
            optimalRotationCamera3D.x *= -1.2f;
        }
    }

    public void restoryValueAfterSleep()
	{
		Debug.LogError("Try restory after sleep!");
		//CheckAllIsSleeping();
		bool isSleepAll = true;
		foreach (BallController ballC in ballControllers)
		{
			isSleepAll = ballC.IsSleeping();//ballC.body.velocity == Vector3.zero;
			Debug.LogError("->" + isSleepAll + " " + ballC.body.velocity + ballC.body.angularVelocity);
			if (!isSleepAll)
				break;
		}
		if (isSleepAll)
		{
			Debug.LogError("Restory good!");
			allIsSleeping = true;
			othersSleeping = true;
			checkAllIsSleeping = true;
			checkOthersIsSleeping = true;
			ballIsOut = false;
			oldAllIsSleeping = true;
			networkAllIsSleeping = true;
			MenuControllerGenerator.controller.canControlCue = true;
			OnShowLineAndSphereFirstTime();
			timeUnFade = 0f;
			ballMaskTrim.SetActive(true);
			UnFadeCue();
		}
	}

    //bool t_CanControlCue;
    //bool t_shotingInProgress;
    //bool t_thenInshoting;
    //bool t_allIsSleeping;
    //bool t_othersSleeping;
    //bool t_checkAllIsSleeping;
    //bool t_checkOthersIsSleeping;
    //bool t_checkInProgress;
    //bool t_inMove;
    //bool t_canMoveBall;
    //bool t_ballIsOut;
    //bool t_oldAllIsSleeping;
    //bool t_haveFirstCollision;
    //bool t_haveSecondCollision;
    //bool t_haveThrthCollision;
    //bool t_canDrawLinesAndSphere;
    //bool t_cueForceisActive;
    //bool t_networkAllIsSleeping;
    //bool t_otherWantToPlayAgain;

    //	public string printAllValue()
    //	{
    //		string str = "";
    //		string str2 = "";
    //		/////

    //		if (t_CanControlCue != CanControlCue)
    //			str2 += "[CanControlCue]";
    //		if (t_shotingInProgress != shotingInProgress)
    //			str2 += "[shotingInProgress]";
    //		if (t_thenInshoting != thenInshoting)
    //			str2 += "[thenInshoting]";
    //		if (t_allIsSleeping != allIsSleeping)
    //			str2 += "[allIsSleeping]";
    //		if (t_othersSleeping != othersSleeping)
    //			str2 += "[othersSleeping]";
    //		if (t_checkAllIsSleeping != checkAllIsSleeping)
    //			str2 += "[checkAllIsSleeping]";
    //		if (t_checkOthersIsSleeping != checkOthersIsSleeping)
    //			str2 += "[checkOthersIsSleeping]";
    //		if (t_checkInProgress != checkInProgress)
    //			str2 += "[checkInProgress]";
    //		if (t_inMove != inMove)
    //			str2 += "[inMove]";
    //		if (t_canMoveBall != canMoveBall)
    //			str2 += "[canMoveBall]";
    //		if (t_ballIsOut != ballIsOut)
    //			str2 += "[ballIsOut]";
    //		if (t_oldAllIsSleeping != oldAllIsSleeping)
    //			str2 += "[oldAllIsSleeping]";
    //		if (t_haveFirstCollision != haveFirstCollision)
    //			str2 += "[haveFirstCollision]";
    //		if (t_haveSecondCollision != haveSecondCollision)
    //			str2 += "[haveSecondCollision]";
    //		if (t_haveThrthCollision != haveThrthCollision)
    //			str2 += "[haveThrthCollision]";
    //		if (t_canDrawLinesAndSphere != canDrawLinesAndSphere)
    //			str2 += "[canDrawLinesAndSphere]";
    //		if (t_cueForceisActive != cueForceisActive)
    //			str2 += "[cueForceisActive]";
    //		if (t_networkAllIsSleeping != networkAllIsSleeping)
    //			str2 += "[networkAllIsSleeping]";
    //		if (t_otherWantToPlayAgain != otherWantToPlayAgain)
    //			str2 += "[otherWantToPlayAgain]";

    //		if(str2 != "")
    //			Debug.LogError(str2);

    //		str += "CanControlCue=" + CanControlCue;
    //		str += " shotingInProgress=" + shotingInProgress;
    //		str += " thenInshoting=" + thenInshoting;
    //		str += " allIsSleeping=" + allIsSleeping;
    //		str += " othersSleeping=" + othersSleeping;
    //		str += " checkAllIsSleeping=" + checkAllIsSleeping;
    //		str += " checkOthersIsSleeping=" + checkOthersIsSleeping;
    //		str += " checkInProgress=" + checkInProgress;
    //		str += " inMove=" + inMove;
    //		str += " canMoveBall=" + canMoveBall;
    //		str += " ballIsOut=" + ballIsOut;
    //		str += " oldAllIsSleeping=" + oldAllIsSleeping;
    //		str += " haveFirstCollision=" + haveFirstCollision;
    //		str += " haveSecondCollision=" + haveSecondCollision;
    //		str += " haveThrthCollision=" + haveThrthCollision;
    //		str += " canDrawLinesAndSphere=" + canDrawLinesAndSphere;
    //		str += " cueForceisActive=" + cueForceisActive;
    //		str += " networkAllIsSleeping=" + networkAllIsSleeping;
    //		str += " otherWantToPlayAgain=" + otherWantToPlayAgain;
    ////	bool bCursorHand;
    ////	bool myHit;
    ////	private bool checkMyBall = false;
    ////	private bool oldCheckMyBall = false;
    ////	public bool isFirsTime = true;
    ////	private bool hitCanvas = false;
    ////	public bool is3D = true;
    ////	public bool inTouchForceSlider = false;
    ////	public bool ballsIsCreated = false;
    ///////

    //		t_CanControlCue =          CanControlCue;
    //		t_shotingInProgress =	   shotingInProgress;
    //		t_thenInshoting =		   thenInshoting;
    //		t_allIsSleeping =		   allIsSleeping;
    //		t_othersSleeping =		   othersSleeping;
    //		t_checkAllIsSleeping =	   checkAllIsSleeping;
    //		t_checkOthersIsSleeping =  checkOthersIsSleeping;
    //		t_checkInProgress =		   checkInProgress;
    //		t_inMove =				   inMove;
    //		t_canMoveBall =			   canMoveBall;
    //		t_ballIsOut =			   ballIsOut;
    //		t_oldAllIsSleeping =	   oldAllIsSleeping;
    //		t_haveFirstCollision =	   haveFirstCollision;
    //		t_haveSecondCollision =	   haveSecondCollision;
    //		t_haveThrthCollision =	   haveThrthCollision;
    //		t_canDrawLinesAndSphere =  canDrawLinesAndSphere;
    //		t_cueForceisActive =	   cueForceisActive;
    //		t_networkAllIsSleeping =   networkAllIsSleeping;
    //		t_otherWantToPlayAgain =   otherWantToPlayAgain;


    //		return str;
    //	}

    #region Debug
    public static float DelayDebugMessage = 1f;
    Dictionary<string, float> dictDebug = new Dictionary<string, float>();

    void SpecialDebulLog(string type, string msg)
    {
        if (SpecialDebug)
        {
            if (dictDebug.ContainsKey(type) && dictDebug[type] < Time.realtimeSinceStartup)
            {
                Debug.LogWarning(msg);
                dictDebug[type] = Time.realtimeSinceStartup + DelayDebugMessage;
            }
            else if (!dictDebug.ContainsKey(type))
            {
                Debug.LogWarning(msg);
                dictDebug.Add(type, Time.realtimeSinceStartup + DelayDebugMessage);
            }
        }
    }
    #endregion Debug
}
