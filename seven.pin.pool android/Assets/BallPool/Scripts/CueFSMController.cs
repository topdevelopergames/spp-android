﻿using UnityEngine;
using System.Collections;

public class CueFSMController : MonoBehaviour 
{
	[SerializeField]
	private CueController cueController;

	public bool moveInTable = false;
	public bool moveBall = false;
	public bool inMove = false;

	public void setMoveInTable ()
	{
		moveInTable = true;
		moveBall = false;
		inMove = false;
	}
	public void setMoveBall ()
	{
		moveInTable = false;
		moveBall = true;
		inMove = false;
	}
	public void setInMove ()
	{
		moveInTable = false;
		moveBall = false;
		inMove = true;
	}
	void Awake ()
	{
		if (ServerController.serverController) 
		{
			enabled = false;
		}
	}

	string prevStateUpdate1;
	string prevStateUpdate2;
	string prevValue;
	float timeChangeState;
	void Update() 
	{
		if(!MenuControllerGenerator.controller)
		{
			return;
		}
		//if(MenuControllerGenerator.controller.playWithAI)
		//{
		//	cueController.networkAllIsSleeping = true;
		//}
		string currentState1 = "";
		string currentState2 = "";
		string currentValue = "";
		if (moveInTable)
		{
			currentState1 = "moveInTable";
			cueController.MoveInTable();
		}
		else if (moveBall)
		{
			currentState1 = "moveBall";
			cueController.MoveBall();
		}
		else if (inMove)
		{
			{
				currentState1 = "inMove";
				cueController.InMove();
			}
		}

		if (cueController.allIsSleeping)
		{
			currentState2 = "cueController.allIsSleeping";
			cueController.OnControlCue();
			timeChangeState = 0f;
		}
		else if (moveInTable)
		{
			if (timeChangeState == 0f)
				timeChangeState = Time.realtimeSinceStartup + .1f;
			if (timeChangeState < Time.realtimeSinceStartup)
			{
				cueController.restoryValueAfterSleep();
			}
		}
		// TODO for test and debug
		/*if (!currentState1.Equals(prevStateUpdate1))
		{
			Debug.LogError("------- currentState1=[" + currentState1 + "] from [" + prevStateUpdate1 + "]");
			prevStateUpdate1 = currentState1;
		}
		if (!currentState2.Equals(prevStateUpdate2))
		{
			Debug.LogError("------- currentState2=[" + currentState2 + "] from [" + prevStateUpdate2 + "]");
			prevStateUpdate2 = currentState2;
		}

		currentValue = cueController.printAllValue();
		if (!currentValue.Equals(prevValue))
		{
			Debug.LogError("------- currentValue=[" + currentValue + "] from [" + prevValue + "]");
			prevValue = currentValue;
		}
		*/
	}
}
