﻿using UnityEngine;
using System.Collections;

public class CueForce : MonoBehaviour 
{
	public SliderSprite slider;
	[SerializeField]
	private MeshRenderer meshRender;
	public float cueForceValue = 0.0f;
	[SerializeField]
	private float startValue = 1.0f;
	[SerializeField]
	private CueController cueController;
    
	void Awake ()
	{
		if(MenuControllerGenerator.controller)
		{
			if(!MenuControllerGenerator.controller.isTouchScreen)
			{
				transform.parent.gameObject.SetActive(false);
				return;
			}
			else
			{
				slider.MoveSlider += MoveForceSlider;
				slider.CheckSlider += MoveForceSlider;
			}
		}
	}
	void Start ()
	{
		slider.Value = startValue;
		MoveForceSlider (slider);
		cueController.inTouchForceSlider = false;
		cueController.cueForceisActive = false;
	}

	void MoveForceSlider (Slider slider)
	{
		if(!cueController.allIsSleeping)
			return;
		if(ServerController.serverController && !ServerController.serverController.isMyQueue)
			return;

		MenuControllerGenerator.controller.canRotateCue = false;
		cueController.inTouchForceSlider = true;
		cueController.cueForceisActive = true;
		cueForceValue = slider.Value;
		transform.localScale = new Vector3(slider.Value, 1.0f, 1.0f);
		meshRender.sharedMaterial.SetTextureScale("_MainTex", new Vector2(1.0f, slider.Value));

		cueController.cueDisplacement = cueController.cueMaxDisplacement*cueForceValue;
	}

	public void Resset ()
	{
		StartCoroutine(WaitAndRessetValue ());
	}
	IEnumerator WaitAndRessetValue ()
	{
		yield return new WaitForEndOfFrame();
		slider.Value = startValue;
		slider.Resset();
		transform.localScale = new Vector3(slider.Value, 1.0f, 1.0f);
		meshRender.sharedMaterial.SetTextureScale("_MainTex", new Vector2(1.0f, slider.Value));
	}
}
