﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public CueController cueController;
    [SerializeField]
    private TextMesh gameInfo;
    [SerializeField]
    private TextMesh gameInfoError;
    [System.NonSerialized]
    public string gameInfoErrorText = "";
    [SerializeField]
    private Sprite firstAvatar;
    [SerializeField]
    private Sprite secondAvatar;
    [System.NonSerialized]
    public bool isFirstShot = true;
    [System.NonSerialized]
    public int firstShotHitCount = 0;
    public Profile profileLeft;
    public Profile profileRight;
    [System.NonSerialized]
    public Profile myProfile;
    [System.NonSerialized]
    public Profile otherProfile;
    [System.NonSerialized]
    public bool needToChangeQueue = true;
    [System.NonSerialized]
    public bool needToForceChangeQueue = false;
    [System.NonSerialized]
    public bool setMoveInTable = true;
    [System.NonSerialized]
    public bool tableIsOpened = true;
    [System.NonSerialized]
    public int ballType = 0;
    [SerializeField]
    private Menu menu;
    [System.NonSerialized]
    public List<BallController> firsBalls;
    [System.NonSerialized]
    public bool remainedBlackBall = false;
    [System.NonSerialized]
    public bool otherRemainedBlackBall = false;

    [System.NonSerialized]
    public bool afterRemainedBlackBall = false;
    [System.NonSerialized]
    public bool afterOtherRemainedBlackBall = false;

    [System.NonSerialized]
    public bool mainBallIsOut = false;
    [System.NonSerialized]
    public bool otherMainBallIsOut = false;

    [System.NonSerialized]
    public bool blackBallInHolle = false;
    [System.NonSerialized]
    public bool otherBlackBallInHolle = false;
    [System.NonSerialized]
    public BallController firstHitBall = null;
    [System.NonSerialized]
    public bool isWinner = false;
    [System.NonSerialized]
    public bool outOfTime = false;
    [SerializeField]
    private TextMesh prize;
    [SerializeField]
    private float shotTime = 15.0f;
    private float shotCurrentTime = 0.0f;
    private bool calculateShotTime = false;
    float waitTime = 0f;
    float waitTimeMessage = 3f;
    float waitTimeDisconnect = 30f;
    [System.NonSerialized]
    public bool ballsInMove = false;

    public int maxHighScore = 10;
    public AnimationCurve highScoreCurve;

    private InGameUIManager inGameUI;

    float timeLeft;
    int secondsLeft;
    bool startGamePlay;
    bool checkChallengeIsExecute;
    //double endGamePhotone;

    float saveTime;
    float timeSaveTime;
    bool timeGetFromsave;

    public float ShotCurrentTime
    {
        set
        {
            shotCurrentTime = value;
            endTime = PhotonNetwork.time + shotCurrentTime; 
            saveTime = value; 
            timeSaveTime = Time.realtimeSinceStartup + 1f; 
        }
        get { return shotCurrentTime; }
    }

    public bool TimeGetFromsave { set { timeGetFromsave = value; } }

    void Awake()
    {
        endTime = Time.time;
        timeLeft = GameData.Instance.TimeForGame;
        shotTime = InGameUIManager.TIME_FOR_TURN;
        if (!MenuControllerGenerator.controller)
            return;
        if (!ServerController.serverController && !MenuControllerGenerator.controller.playWithAI)
        {
            Destroy(menu.gameObject);
            Destroy(gameObject);
            return;
        }

        firsBalls = new List<BallController>(0);
        ActivateMenuButtons(false);
        myProfile = profileLeft;
        myProfile.isMain = true;
        otherProfile = profileRight;
        otherProfile.isMain = false;
        myProfile.gameManager = this;
        otherProfile.gameManager = this;
        myProfile.OnAwakeGameManager();
        otherProfile.OnAwakeGameManager();

        myProfile.playerName.text = ServerController.serverController.myName;
        otherProfile.playerName.text = ServerController.serverController.otherName;

        if (ServerController.serverController.isFirstPlayer || MenuControllerGenerator.controller.playWithAI)
        {
            prize.text = ServerController.serverController.prize.ToString();
            if (!MenuControllerGenerator.controller.playWithAI)
            {
                ServerController.serverController.SendRPCToServer("SetPrizeToOther", ServerController.serverController.otherNetworkPlayer, ServerController.serverController.prize);
            }
        }
        myProfile.coins.text = ServerController.serverController.coins.ToString();
        if (!MenuControllerGenerator.controller.playWithAI)
        {
            ServerController.serverController.SendRPCToServer("SetCoinsToOther", ServerController.serverController.otherNetworkPlayer, ServerController.serverController.coins);
        }
        else
        {
            SetCoinsToOther(ServerController.serverController.otherCoins);
        }

        int highScore = Profile.GetUserDate(ServerController.serverController.myName + "_High_Score");
        ServerController.serverController.highScore = highScore;
        myProfile.highScore.text = ServerController.serverController.highScore.ToString();
        if (!MenuControllerGenerator.controller.playWithAI)
        {
            ServerController.serverController.SendRPCToServer("SetHighScoreToOther", ServerController.serverController.otherNetworkPlayer, ServerController.serverController.highScore);
        }
        HideGameInfoError();

        StartCalculateShotTime();
        myProfile.timeSlider.SetValue(0.0f);
        otherProfile.timeSlider.SetValue(0.0f);

        ServerController.serverController.OnChangeQueueEvent += OnChangeCalculateShotTime;

        if (ServerController.serverController.isFirstPlayer)
        {
            StartCoroutine(myProfile.SetAvatar(firstAvatar));
            StartCoroutine(otherProfile.SetAvatar(secondAvatar));
        }
        else if (ServerController.serverController.isSecondPlayer)
        {
            StartCoroutine(otherProfile.SetAvatar(firstAvatar));
            StartCoroutine(myProfile.SetAvatar(secondAvatar));
        }
    }

    IEnumerator BackToMenu()
    {
        float timeWait = Time.realtimeSinceStartup + 12f;
        InGameUIManager.Instance.ShowWaitingScreen();
        while (!InGameUIManager.Instance.IsStatisticEnabled && Time.realtimeSinceStartup < timeWait)
        {
            yield return new WaitForSeconds(.1f);
        }
        if (!InGameUIManager.Instance.IsStatisticEnabled)
        {
            Debug.Log("Opponent not active! I lose!");
            MenuManager.Instance.BackToMainMenu();
        }
        else
        {
            Debug.Log("Opponent not active! I win!");
        }
        InGameUIManager.Instance.HideWaitingScreen();
    }

    bool showMessageWaitTime = false;
    double endTime;
    // = PhotonNetwork.time;

    void Update()
    {
        if (!MasterServerGUI.gameStarted)
        {
            //InGameUIManager.Instance.BackToMenu();
            if (!checkChallengeIsExecute)
            {
                Debug.Log("Opponent not active! I win or lose!");
                GameSparksManager.Instance.CheckChallengeParticipantsOnline();
                StartCoroutine("BackToMenu");
                checkChallengeIsExecute = true;
            }
            return;
        }
        if (calculateShotTime)
        {
            if (cueController.enabled)
            {
                //shotCurrentTime -= Time.deltaTime;
                shotCurrentTime = (float)(endTime - PhotonNetwork.time);
            }
            else
                endTime = PhotonNetwork.time + shotCurrentTime;

            if (waitTime != 0f || showMessageWaitTime)
            {
                showMessageWaitTime = false;
                InGameUIManager.Instance.HideWaitingForOpponentPopup();
                waitTime = 0f;
            }

            if (shotCurrentTime <= 0.0f)
            {
                StopCalculateShotTime();
                StartCoroutine("CoroutineChangeQueue");
            }
            if (ServerController.serverController.isMyQueue)
            {
                //myProfile.timeSlider.SetValue(shotCurrentTime/shotTime);
                //otherProfile.timeSlider.SetValue(0.0f);
                InGameUIManager.Instance.SetLeftUserTurnTimeVisual(shotCurrentTime);
                InGameUIManager.Instance.SetRightOpponentTurnTimeVisual(0f);
            }
            else
            {
                //myProfile.timeSlider.SetValue(0.0f);
                //otherProfile.timeSlider.SetValue(shotCurrentTime/shotTime);
                InGameUIManager.Instance.SetLeftUserTurnTimeVisual(0f);
                InGameUIManager.Instance.SetRightOpponentTurnTimeVisual(shotCurrentTime);
            }
        }
        else
        {
            if (cueController.allIsSleeping)
                waitTime += Time.deltaTime;
            if (waitTime > waitTimeMessage)
            {
                InGameUIManager.Instance.ShowWaitingForOpponentPopup();
                showMessageWaitTime = true;
            }
            if (waitTime > waitTimeMessage + waitTimeDisconnect)
            {
                //InGameUIManager.Instance.BackToMenu();
                if (!checkChallengeIsExecute)
                {
                    Debug.Log("Opponent not active! I win or lose!");
                    GameSparksManager.Instance.CheckChallengeParticipantsOnline();
                    StartCoroutine("BackToMenu");
                    checkChallengeIsExecute = true;
                }
                MasterServerGUI.Disconnect();
            }
        }

        /*if (GameData.Instance.selectedGameMode == 1)
		{
			if (startGamePlay && timeLeft > 0f)
			{
				timeLeft = (float)(endGamePhotone - PhotonNetwork.time);
				if (timeLeft <= 0f)
				{
					timeLeft = 0f;
					StartCoroutine("GameComplete");
				}
			}
			int seconds = Mathf.FloorToInt(timeLeft);
			if (!seconds.Equals(secondsLeft))	
			{
				secondsLeft = seconds;
				InGameUIManager.Instance.SetTime(secondsLeft);
			}
		}*/
    }

    IEnumerator CoroutineChangeQueue()
    {
        if (ServerController.serverController.isMyQueue)
        {
            ServerController.serverController.SendRPCToServer("SetMoveInTable", ServerController.serverController.otherNetworkPlayer);
            ServerController.serverController.ChangeQueue(false);
        }
        else
        {
            int wait;
            do
            {
                ServerController.serverController.serverMessenger.PingOpponet();
                wait = 5;
                do
                {
                    Debug.Log("Wait opponent " + wait + " ...");
                    yield return new WaitForSeconds(1f);
                    wait--;
                }
                while (ServerController.serverController.serverMessenger.WorkPing && wait >= 0 && !ServerController.serverController.isMyQueue);
            }
            while (wait >= 0 && !ServerController.serverController.isMyQueue);
            if (!ServerController.serverController.isMyQueue)
            {
                cueController.cueFSMController.setMoveInTable();
                ServerController.serverController.ChangeQueue(true);
                cueController.networkAllIsSleeping = true;
            }
            else
            {
                Debug.Log("Opponent itself changed queue!!!");
            }
        }
    }

    /*IEnumerator GameComplete()
	{
		while (!cueController.allIsSleeping)
			yield return null;
		yield return new WaitForSeconds(.1f);
		//GameSparksManager.Instance.CompleteCurrentChallenge();
		MasterServerGUI.gameStarted = false;
	}*/

    void OnDestroy()
    {
        if (ServerController.serverController)
        {
            ServerController.serverController.OnChangeQueueEvent -= OnChangeCalculateShotTime;
        }

        if (startGamePlay && !GameData.Instance.playAgain)
        {
            //Debug.Log("Disconnect from GameManager - not disconnect");
            Debug.Log("Disconnect from GameManager");
            MasterServerGUI.Disconnect();
        }
        else
        {
            Debug.Log("GameManager Destroy startGamePlay == false");
        }
        startGamePlay = false;
    }

    void OnChangeCalculateShotTime(bool myTurn)
    {
        Debug.Log("OnChangeCalculateShotTime StartCalculateShotTime()");
        cueController.RessetShotOptions();
        StartCalculateShotTime();
    }

    public void StartCalculateShotTime(bool nozero = true)
    {
        if (!nozero)
        {
            Debug.Log("StartCalculateShotTime not started!");
            return;
        }
        //shotCurrentTime = nozero ? shotTime : 0f;
        StopCoroutine("CoroutineChangeQueue");
        shotCurrentTime = shotTime;
        Debug.Log("StartCalculateShotTime StartCalculateShotTime() " + shotCurrentTime);
        calculateShotTime = true;
        if (timeGetFromsave && saveTime != 0f && timeSaveTime > Time.realtimeSinceStartup)
        {
            shotCurrentTime = saveTime;
            Debug.Log("from save StartCalculateShotTime StartCalculateShotTime() " + shotCurrentTime);
        }
        endTime = PhotonNetwork.time + shotCurrentTime;
        timeGetFromsave = false;
        saveTime = 0f;
        timeSaveTime = 0f;
        cueController.UpdateCueMaterial(ServerController.serverController.isMyQueue);
        if (ServerController.serverController && !ServerController.serverController.isMyQueue && Rules.Instance)
        {
            Rules.Instance.ChangeQueue();
        }
    }

    public void StopCalculateShotTime()
    {
        calculateShotTime = false;
        shotCurrentTime = 0.0f;
        endTime = PhotonNetwork.time;
    }

    public void SetPrizeToOther(int otherPrize)
    {
        ServerController.serverController.prize = otherPrize;
        prize.text = otherPrize.ToString();
    }

    public void SetHighScoreToOther(int otherHighScore)
    {
        ServerController.serverController.otherHighScore = otherHighScore;
        otherProfile.highScore.text = otherHighScore.ToString();
    }

    public void SetCoinsToOther(int otherCoins)
    {
        ServerController.serverController.otherCoins = otherCoins;
        otherProfile.coins.text = otherCoins.ToString();
    }

    public void ActivateMenuButtons(bool value)
    {
        if (!value)
        {
            menu.gameObject.SetActive(value);
        }
        ServerController.serverController.menuButtonsIsActive = value;
        if (value)
        {
            cueController.enabled = false;
            cueController.cueFSMController.enabled = false;

            if (!afterRemainedBlackBall && !afterOtherRemainedBlackBall)
            {
                isWinner = !ServerController.serverController.isMyQueue;
            }
            else
            {
                if (ServerController.serverController.isMyQueue)
                {
                    isWinner = (afterRemainedBlackBall && !mainBallIsOut);

                    if (MenuControllerGenerator.controller.playWithAI)
                    {
                        if (!mainBallIsOut && firstHitBall && !firstHitBall.isBlack && afterRemainedBlackBall && ballType != firstHitBall.ballType)
                        {
                            isWinner = false;
                        }
                    }
                }
                else
                {
                    isWinner = !(afterOtherRemainedBlackBall && !otherMainBallIsOut);

                    if (MenuControllerGenerator.controller.playWithAI)
                    {
                        if (!otherMainBallIsOut && firstHitBall && !firstHitBall.isBlack && afterOtherRemainedBlackBall && ballType == firstHitBall.ballType)
                        {
                            isWinner = true;
                        }
                    }
                }
            }
            ServerController.serverController.isMyQueue = isWinner;

            ServerController.serverController.coins += isWinner ? ServerController.serverController.prize : -ServerController.serverController.prize;
            ServerController.serverController.coins = Mathf.Clamp(ServerController.serverController.coins, ServerController.serverController.minCoins, ServerController.serverController.maxCoins);
            Profile.SetUserDate(ServerController.serverController.myName + "_Coins", ServerController.serverController.coins);
            myProfile.coins.text = ServerController.serverController.coins.ToString();

            ServerController.serverController.SendRPCToServer("SetCoinsToPlayerClient", ServerController.serverController.myNetworkPlayer, ServerController.serverController.coins);

            ServerController.serverController.otherCoins += isWinner ? -ServerController.serverController.prize : ServerController.serverController.prize;
            ServerController.serverController.otherCoins = Mathf.Clamp(ServerController.serverController.otherCoins, ServerController.serverController.minCoins, ServerController.serverController.maxCoins);
            if (MenuControllerGenerator.controller.playWithAI)
            {
                Profile.SetUserDate(ServerController.serverController.otherName + "_Coins", ServerController.serverController.otherCoins);
            }
            otherProfile.coins.text = ServerController.serverController.otherCoins.ToString();

            int highScore = (int)((float)maxHighScore * (highScoreCurve.Evaluate((float)ServerController.serverController.coins / (float)ServerController.serverController.maxCoins)));
            if (ServerController.serverController.highScore < highScore)
            {
                ServerController.serverController.highScore = highScore;
            }
            int otherHighScore = (int)((float)maxHighScore * (highScoreCurve.Evaluate((float)ServerController.serverController.otherCoins / (float)ServerController.serverController.maxCoins)));

            if (ServerController.serverController.otherHighScore < otherHighScore)
            {
                ServerController.serverController.otherHighScore = otherHighScore;
            }
            Profile.SetUserDate(ServerController.serverController.myName + "_High_Score", ServerController.serverController.highScore);
            if (MenuControllerGenerator.controller.playWithAI)
            {
                Profile.SetUserDate(ServerController.serverController.otherName + "_High_Score", ServerController.serverController.otherHighScore);
            }
            myProfile.highScore.text = ServerController.serverController.highScore.ToString();
            otherProfile.highScore.text = ServerController.serverController.otherHighScore.ToString();

            if (isWinner)
            {
                myProfile.winner.GetComponent<Renderer>().enabled = true;
            }
            else
            {
                otherProfile.winner.GetComponent<Renderer>().enabled = true;
            }
            if (ServerController.serverController.coins < ServerController.serverController.prize)
            {
                if (!MenuControllerGenerator.controller.playWithAI)
                {
                    StartCoroutine(WaitAndDisconnect());
                }
                else
                {
                    MenuControllerGenerator.controller.playWithAI = false;
                    MenuControllerGenerator.controller.OnGoBack();
                }
            }
            else
            {
                menu.gameObject.SetActive(value);
            }
            if (MenuControllerGenerator.controller.playWithAI)
            {
                cueController.otherWantToPlayAgain = true;
                otherProfile.WantToPlayAgain.gameObject.SetActive(true);
            }
        }
    }

    IEnumerator WaitAndDisconnect()
    {
        yield return new WaitForSeconds(1.5f);
        MasterServerGUI.Disconnect();
    }

    public void ShowGameInfo(string info)
    {
        gameInfo.GetComponent<Renderer>().enabled = true;
        gameInfo.text = info;
    }

    public void ShowGameInfo(string info, float visibleTime)
    {
        gameInfo.GetComponent<Renderer>().enabled = true;
        gameInfo.text = info;
        Invoke("HideGameInfo", visibleTime);
    }

    public void ShowGameInfoError(string info, float visibleTime)
    {
        if (MenuControllerGenerator.controller.playWithAI)
        {
            cueController.cueFSMController.setMoveInTable();
        }
        gameInfoError.GetComponent<Renderer>().enabled = true;
        gameInfoError.text = info;
        Invoke("HideGameInfoError", visibleTime);
    }

    public void HideGameInfoError()
    {
        gameInfoError.GetComponent<Renderer>().enabled = false;
    }

    public void HideGameInfo()
    {
        gameInfo.GetComponent<Renderer>().enabled = false;
        checkChallengeIsExecute = false;
        int timeForGame = GameData.Instance.TimeForGame;
        //endGamePhotone = PhotonNetwork.time + timeForGame;
        timeLeft = timeForGame;
        //GameSparksManager.Instance.StartCompleteChallengeTimer(timeForGame);
        startGamePlay = true;
        //ServerController.serverController.serverMessenger.RepeatingPing();
    }

    public void SetBallType(int ballType)
    {
        this.ballType = ballType;
        if (ballType == 1)
        {
            for (int i = 1; i <= 7; i++)
            {
                myProfile.AddGuiBall(i, cueController.ballTextures[i]);
            }
            for (int i = 9; i <= 15; i++)
            {
                otherProfile.AddGuiBall(i, cueController.ballTextures[i]);
            }
        }
        else if (ballType == -1)
        {
            for (int i = 1; i <= 7; i++)
            {
                otherProfile.AddGuiBall(i, cueController.ballTextures[i]);
            }
            for (int i = 9; i <= 15; i++)
            {
                myProfile.AddGuiBall(i, cueController.ballTextures[i]);
            }
        }
    }

    void GetMenu()
    {
        if (MenuControllerGenerator.controller.playWithAI)
        {
            MenuControllerGenerator.controller.playWithAI = false;
            MenuControllerGenerator.controller.OnGoBack();
        }
        else
        {
            MasterServerGUI.Disconnect();
        }

    }

    public void PlayAgain()
    {
        //myProfile.WantToPlayAgain.gameObject.SetActive(true);
        StatisticScreenPopup.Instance.ShowPlayAgainMyMessage();
        ServerController.serverController.SendRPCToServer("WantToPlayAgain", ServerController.serverController.otherNetworkPlayer);
        ActivateMenuButtons(false);
        ShowGameInfo("Waiting for your opponent");
        StartCoroutine(WaitWhenOtherWantToPlayAgain());
    }

    IEnumerator WaitWhenOtherWantToPlayAgain()
    {
        while (!cueController.otherWantToPlayAgain)
        {
            yield return null;
        }
        yield return new WaitForSeconds(1.5f);
        GameData.Instance.playAgain = true;
        MenuControllerGenerator.controller.LoadLevel(Application.loadedLevel);
        MasterServerGUI.gameStarted = true;
        checkChallengeIsExecute = false;
    }

    public void HideWaitingForOpponentPopup(float seconds, float time)
    {
        timeLeft = time;
        calculateShotTime = true;
        shotCurrentTime = seconds;
        endTime = PhotonNetwork.time + shotCurrentTime;
        showMessageWaitTime = false;
        waitTime = 0f;
        InGameUIManager.Instance.HideWaitingForOpponentPopup();
    }
}

public class SkillzMatchRules
{
    public SkillzMatchType type;
    public int timeinseconds;
    public int points;

    public SkillzMatchRules(SkillzMatchType type, int timeinseconds, int points)
    {
        this.type = type;
        this.timeinseconds = timeinseconds;
        this.points = points;
    }

}

public enum SkillzMatchType
{
    Timed,
    Points_51,
    None
}
