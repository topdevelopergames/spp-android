﻿using UnityEngine;
using System.Collections;

public class ServerMessenger : MonoBehaviour 
{
	public CueController cueController;

	GameManager _gameManager;
	float timeOfSleep;

	void Awake ()
	{
		Application.runInBackground = true;
	}

	void OnEnable () 
	{
		MenuControllerGenerator.controller.OnLoadLevel += FindCueController;
	}

	void FindCueController (MenuController menuController)
	{
		StopCoroutine("WaitAndFindCueController");
		StopCoroutine("WaitAndEnabledCueController");

		StartCoroutine("WaitAndFindCueController", menuController.preloader);
	}
	void OnDisable ()
	{
		MenuControllerGenerator.controller.OnLoadLevel -= FindCueController;
	}
	IEnumerator WaitAndFindCueController (Preloader preloader)
	{
		cueController = null;
		while(!preloader.isDone)
		{
			yield return null;
		}
		while(!cueController)
		{
			cueController = CueController.FindObjectOfType<CueController>();
			yield return null;
		}
		Debuger.DebugOnScreen("CueController " + (cueController != null).ToString());
		OnFindCueController();

	}
	public void ShotWithAI ()
	{
		ServerController.logicAI.ShotCue(cueController);
	}

	void OnFindCueController()
	{
		if(MenuControllerGenerator.controller.playWithAI)
		{
			OnReadyToPlay ();
			if(!ServerController.serverController.isMyQueue)
			{
				ShotWithAI();
			}
		}
		else
		{
			Debuger.DebugOnScreen("SendRPCToServer ReadyToPlay");
			ServerController.serverController.SendRPCToServer("ReadyToPlay", ServerController.serverController.otherNetworkPlayer);
		}
		_gameManager = GameManager.FindObjectOfType<GameManager>();
	}
	public void ShowOtherMessage (string message)
	{
		ProfileMessenger.FindObjectOfType<ProfileMessenger>().ShowOtherMessage(message);
	}
	public void SetMoveInTable ()
	{
		if(!cueController)
			return;
		cueController.cueFSMController.setMoveInTable();
	}
	public void OnChangeQueue(bool myTurn, int numberQueue)
	{
		if (cueController)
		{
			cueController.StopCoroutine("WaitWhenAllIsSleeping");
			//Debug.Log("after cueController.StopCoroutine(WaitWhenAllIsSleeping);");
			//SetMoveInTable();
			//cueController.networkAllIsSleeping = true;
			cueController.RessetShotOptions();
		}
	}
	public void OnChanghAllIsSleeping ()
	{
		if(!cueController)
			return;
		cueController.networkAllIsSleeping = true;
	}
	public void OnWantToPlayAgain ()
	{
		StartCoroutine(WaitForOtherWantToPlayAgain());
	}
	IEnumerator WaitForOtherWantToPlayAgain ()
	{
		while(!cueController)
		{
			yield return null;
		}
		yield return new WaitForEndOfFrame(); 
		cueController.otherWantToPlayAgain = true;
        //cueController.gameManager.otherProfile.WantToPlayAgain.gameObject.SetActive(true);
        StatisticScreenPopup.Instance.ShowPlayAgainOtherMessage();
	}
	public void SetPrizeToOther (int otherPrize)
	{
		StartCoroutine(WaitAndSetPrizeToOther (otherPrize));
	}
	IEnumerator WaitAndSetPrizeToOther (int  otherPrize)
	{
		while(!cueController)
		{
			yield return null;
		}
		yield return new WaitForEndOfFrame();
		GameManager gameManager = GameManager.FindObjectOfType<GameManager>();
		gameManager.SetPrizeToOther(otherPrize);
	}
	public void SetHighScoreToOther (int otherHighScore)
	{
		StartCoroutine(WaitAndSetHighScoreToOther (otherHighScore));
	}
	IEnumerator WaitAndSetHighScoreToOther (int otherHighScore)
	{
		while(!cueController)
		{
			yield return null;
		}
		yield return new WaitForEndOfFrame();
		GameManager gameManager = GameManager.FindObjectOfType<GameManager>();
		gameManager.SetHighScoreToOther(otherHighScore);

		InGameUIManager.Instance.SetRightOpponentScore(otherHighScore.ToString());
	}
	public void SetCoinsToOther(int otherCoins)
	{
		StartCoroutine(WaitAndSetCoinsToOther (otherCoins));
	}
	IEnumerator WaitAndSetCoinsToOther (int otherCoins)
	{
		while(!cueController)
		{
			yield return null;
		}
		yield return new WaitForEndOfFrame();
		GameManager gameManager = GameManager.FindObjectOfType<GameManager>();
		gameManager.SetCoinsToOther(otherCoins);
	}
	public void SetErrorText (string errorText)
	{
		if(!cueController)
			return;
		cueController.gameManager.ShowGameInfoError(errorText, 5.0f);
	}
	public void OnReadyToPlay ()
	{
		Debuger.DebugOnScreen("OnReadyToPlay");
		StartCoroutine("WaitAndEnabledCueController");
	}
	IEnumerator WaitAndEnabledCueController ()
	{
		Debuger.DebugOnScreen("StartEnabledCueController");
		Debuger.DebugOnScreen((cueController != null).ToString());
		Debuger.DebugOnScreen((ServerController.serverController.menuButtonsIsActive).ToString());

		Debuger.DebugOnScreen((ServerController.serverController.otherNetworkPlayer < 1).ToString());
		Debuger.DebugOnScreen((!MenuControllerGenerator.controller.playWithAI).ToString());

		while(!cueController || ServerController.serverController.menuButtonsIsActive || 
		      (ServerController.serverController.otherNetworkPlayer < 1 && !MenuControllerGenerator.controller.playWithAI))
		{
			cueController = CueController.FindObjectOfType<CueController>();
			yield return null;
		}
		Debuger.DebugOnScreen("WaitForEndOfFrame");
        yield return new WaitForEndOfFrame();
        cueController.enabled = true;
		cueController.cueFSMController.enabled = true;
		string info = ServerController.serverController.isMyQueue? "You are breaking\n Good luck!":"Your opponent is breaking\n Good luck!";
		cueController.gameManager.ShowGameInfo(info, 2.5f);
		Debuger.DebugOnScreen("EndEnabledCueController");
	}
	public void OnSelectBall (Vector3 position)
	{
		if(!cueController)
			return;
		cueController.OnSelectBall(position);
	}
	public void OnUnselectBall ()
	{
		if(!cueController)
			return;
		cueController.OnUnselectBall();
	}
	public void SetOnMoveBall(Vector3 positin )
	{
		if(!cueController)
			return;
		cueController.ballMovePosition = positin;
	}
	public void SetBallMoveRequest (int id)
	{
		if(!cueController)
			return;
		BallController ballController = cueController.startBallControllers[id];
		if(!ballController.inForceMove)
		{
			ServerController.serverController.SendRPCToServer("ForceSetBallMove", ServerController.serverController.otherNetworkPlayer, ballController.id, ballController.GetComponent<Rigidbody>().position, ballController.GetComponent<Rigidbody>().velocity, ballController.GetComponent<Rigidbody>().angularVelocity, true);
		}
	}
	public void ForceSetBallMove (int id, Vector3 positin, Vector3 velocity, Vector3 angularVelocity, bool interpolation)
	{
		if(!cueController)
			return;
		BallController ballController = cueController.startBallControllers[id];
		//if(ServerController.serverController.isMyQueue/* || ballController.ballIsOut*/)
		//	return;
		ballController.ForceSetMove(positin, velocity, angularVelocity, ServerController.canSend && interpolation);
	}
	public void OnPlayBallAudio(int id, float audioVolume, bool ballBall)
	{
		if(!cueController)
			return;
		BallController ballController = cueController.startBallControllers[id];
		if(ServerController.serverController.isMyQueue || ballController.ballIsOut)
			return;
		ballController.OnPlayBallAudio(audioVolume, ballBall);
	}
	public void SetBallSleeping(int id, Vector3 positin)
	{
		if(!cueController)
			return;
		BallController ballController = cueController.startBallControllers[id];
		if(ServerController.serverController.isMyQueue || ballController.ballIsOut)
			return;

		ballController.GetComponent<Rigidbody>().position = positin;
		if(!ballController.GetComponent<Rigidbody>().isKinematic)
		{
			ballController.GetComponent<Rigidbody>().velocity = Vector3.zero;
			ballController.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			StartCoroutine(WaitForFixedUpdateBall (ballController));
		}
	}
	IEnumerator WaitForFixedUpdateBall (BallController ballController)
	{
		yield return new WaitForFixedUpdate();
		if(ballController)
		{
			ballController.GetComponent<Rigidbody>().Sleep();
		}
	}
	private BallController FindBallById(int id)
	{
		return null;
	}
	public void SendCueControl(Quaternion localRotation, Vector3 localPosition, Vector3 rotationDisplacement, int cueType, int cueId)
	{
		if(!cueController)
			return;
		cueController.SetCueControlFromNetwork(localRotation, localPosition, new Vector2(rotationDisplacement.x, rotationDisplacement.y), (Cue_Shop_Type)cueType, cueId);
	}
	public void OnShotCue()
	{
		if(!cueController)
			return;
		cueController.OnShotCue(false);
	}
	//Player is shot (for ball)
	public void ShotBall(Vector3 ballShotVelocity, Vector3 hitBallVelocity, Vector3 secondVelocity, Vector3 ballShotAngularVelocity)
	{
		if(!cueController)
			return;
		cueController.ballShotVelocity = ballShotVelocity;
		cueController.hitBallVelocity = hitBallVelocity;
		cueController.secondVelocity = secondVelocity;
		cueController.ballShotAngularVelocity = ballShotAngularVelocity;
		cueController.ballController.ShotBall();
	}
	public void SendOnTriggerEnter (int ballId, float audioVolume, float currentLungth, int holleId)
	{
		HolleController holleController = HolleController.FindeHoleById(holleId);
		holleController.SendOnTriggerEnter(ballId, audioVolume, currentLungth, holleId);
	}

	public void ForceSetPinMove(int id, Vector3 positin, Vector3 velocity, Vector3 angularVelocity, Quaternion rotation, bool struck)
	{
		if (!cueController/* || ServerController.serverController.isMyQueue*/)
			return;
		Pin pin = cueController.pins[id];
		pin.ForceSetMove(positin, velocity, angularVelocity, rotation, struck);
	}

	public void ForceGetPinMove(int id)
	{
		if (!cueController/* || ServerController.serverController.isMyQueue*/)
			return;
		Pin pin = cueController.pins[id];
		pin.ForceGetMove();
	}

    public void GetStateAboutGame()
    {
        Debug.Log("GetStateAboutGame SendForceSetBallMove");
        foreach (BallController bc in cueController.ballControllers)
            bc.SendForceSetBallMove();
        Debug.Log("GetStateAboutGame SendForceSetPinMove");
        foreach (Pin pin in cueController.pins)
            pin.SendForceSetPinMove();

        //Debug.Log("GetStateAboutGame OnChangeQueue");
        //ServerController.serverController.SendRPCToServer("OnChangeQueue", ServerController.serverController.otherNetworkPlayer, !ServerController.serverController.isMyQueue);
        Debug.Log("GetStateAboutGame SendStateAboutGameCompleted queue = " + !ServerController.serverController.isMyQueue + " ShotCurrentTime = " + _gameManager.ShotCurrentTime);
        ServerController.serverController.SendRPCToServer("SendStateAboutGameCompleted", ServerController.serverController.otherNetworkPlayer, !ServerController.serverController.isMyQueue, _gameManager.ShotCurrentTime);
    }

	void OnApplicationFocus(bool focusStatus)
	{
		if (_gameManager == null || MasterServerGUI.gameStarted == false)
			return;

		if (Application.isMobilePlatform && cueController.allIsSleeping && ServerController.serverController)
		{
			if (focusStatus && timeOfSleep != 0f)
			{
				_gameManager.ShotCurrentTime = Mathf.Clamp(_gameManager.ShotCurrentTime - (Time.realtimeSinceStartup - timeOfSleep), 0f, _gameManager.ShotCurrentTime);
			}
			else if (!focusStatus)
			{
				timeOfSleep = Time.realtimeSinceStartup;
			}
			// for test
			//if (focusStatus)
			//{
			//	Debug.Log("OnApplicationFocus GetStateAboutGame new");
			//	ServerController.serverController.SendRPCToServer("GetStateAboutGame", ServerController.serverController.otherNetworkPlayer);
			//}
			// 
		}
	
		if (!Application.isMobilePlatform)// || cueController.allIsSleeping)
			return;

        if (focusStatus && ServerController.serverController)
		{
			ServerController.canSend = true;
			Debug.Log("OnApplicationFocus GetStateAboutGame");
			ServerController.serverController.SendRPCToServer("GetStateAboutGame", ServerController.serverController.otherNetworkPlayer);
		}
		ServerController.canSend = false;

		if (focusStatus)
			StartCoroutine("WaitAndSetSendTrue");
	}

	IEnumerator WaitAndSetSendTrue()
	{
		float waitSeconds = 5f;
		while (!ServerController.canSend && waitSeconds > 0f)
		{
			yield return new WaitForSeconds(.1f);
			waitSeconds -= .1f;
		}
		ServerController.canSend = true;
	}

	public void SetTime(float time)
	{
		_gameManager.ShotCurrentTime = time;
		_gameManager.TimeGetFromsave = true;
	}

	public void PingOtherPlayer(double time)
	{
		ServerController.serverController.SendRPCToServer("AnswerPingOtherPlayer", ServerController.serverController.otherNetworkPlayer, Time.realtimeSinceStartup);
	}

	public void AnswerPingOtherPlayer(double time)
	{
		//deltaTimePing = Time.realtimeSinceStartup - timePing;
		deltaTimePing = (float)(PhotonNetwork.time - timePing);
		workPing = false;
	}

	double timePing;
	float deltaTimePing;
	bool workPing;

	public bool WorkPing { get { return workPing; } set { workPing = value; } }

	IEnumerator Ping()
	{
		if (!workPing)
		{
			workPing = true;
			timePing = Time.realtimeSinceStartup;
			ServerController.serverController.SendRPCToServer("PingOtherPlayer", ServerController.serverController.otherNetworkPlayer, timePing);
			while (workPing)
				yield return null;
			Debug.Log("ping time: " + deltaTimePing);
		}
		else
			Debug.Log("ping has work yet!");
	}

	IEnumerator PingOneInSecond()
	{
		while(PhotonNetwork.room != null)
		{
			StartCoroutine("Ping");
			yield return new WaitForSeconds(1f);
		}
	}

	public void RepeatingPing()
	{
		workPing = false;
		StopCoroutine("PingOneInSecond");
		StartCoroutine("PingOneInSecond");
	}

	public void PingOpponet()
	{
		if (!workPing)
		{
			workPing = true;
			timePing = PhotonNetwork.time;
			ServerController.serverController.SendRPCToServer("PingOtherPlayer", ServerController.serverController.otherNetworkPlayer, timePing);
		}
		else
			Debug.Log("ping has work yet!");
	}

}
