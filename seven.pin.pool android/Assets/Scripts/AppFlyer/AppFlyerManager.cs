﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AppFlyerManager : MonoBehaviour {
    
    //private Dictionary<string, string> events;

    private static AppFlyerManager _instance;
    public static AppFlyerManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject); // and make this object persistant as we load new scenes
        }
        else
        {
            Destroy(gameObject);
        }
    }

	void Start () {
        AppsFlyer.setCustomerUserID(SystemInfo.deviceUniqueIdentifier);
        //Mandatory - set your AppsFlyer’s Developer key.
        AppsFlyer.setAppsFlyerKey("quvhP3tVY7DM23moVwzfsj");
#if UNITY_IOS
        //Mandatory - set your apple app ID
        AppsFlyer.setAppID ("1185917934");
#elif UNITY_ANDROID
        //Mandatory - set your Android package name
        AppsFlyer.setAppID("com.dmd.seven.pinpool");
        AppsFlyer.setCollectIMEI(true);
        AppsFlyer.setCollectAndroidID(true);
#endif
        AppsFlyer.trackAppLaunch();
        TrackAppOpen();
    }

    public void TrackAppOpen()
    {
        Dictionary<string, string> purchaseEvent = new Dictionary<string, string>();
        purchaseEvent.Add("af_app_id", SystemInfo.deviceUniqueIdentifier);
        AppsFlyer.trackRichEvent("app_opened", purchaseEvent);
    }

    public void TrackPurchaseUSD(string value, string currency)
    {
        Debug.Log("<b>!!!!Track purchase: </b>" + value);
        Debug.Log("<b>!!!!Track purchase currency code: </b>" + currency);

        Dictionary<string, string> purchaseEvent = new Dictionary<string, string>();
        purchaseEvent.Add("af_currency", currency);
        purchaseEvent.Add("af_revenue", value);
        purchaseEvent.Add("af_quantity", "1");
        AppsFlyer.trackRichEvent("af_purchase", purchaseEvent);
    }

    public void TrackClickEvent(string eventName)
    {
        Dictionary<string, string> purchaseEvent = new Dictionary<string, string>();
        purchaseEvent.Add("af_param_1", eventName);
        AppsFlyer.trackRichEvent(eventName, purchaseEvent);
    }

    public void didReceiveConversionData(string conversionData)
    {
        print("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
    }

    public void didReceiveConversionDataWithError(string error)
    {
        print("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
    }

    public void didFinishValidateReceipt(string validateResult)
    {
        print("AppsFlyerTrackerCallbacks:: got didFinishValidateReceipt  = " + validateResult);

    }

    public void didFinishValidateReceiptWithError(string error)
    {
        print("AppsFlyerTrackerCallbacks:: got idFinishValidateReceiptWithError error = " + error);

    }

    public void onAppOpenAttribution(string validateResult)
    {
        print("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);

    }

    public void onAppOpenAttributionFailure(string error)
    {
        print("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);

    }

    public void onInAppBillingSuccess()
    {
        print("AppsFlyerTrackerCallbacks:: got onInAppBillingSuccess succcess");

    }
    public void onInAppBillingFailure(string error)
    {
        print("AppsFlyerTrackerCallbacks:: got onInAppBillingFailure error = " + error);

    }

}
