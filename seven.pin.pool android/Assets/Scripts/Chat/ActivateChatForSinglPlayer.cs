﻿using UnityEngine;
using System.Collections;

public class ActivateChatForSinglPlayer : MonoBehaviour {

	void Start () {
		gameObject.SetActive(GameData.Instance.singleOrMulti == 0 && GameData.Instance.selectedGameMode == 1);
	}
}
