﻿using UnityEngine;
using System.Collections;

public class ChatMultiPlayer : MonoBehaviour {
	[SerializeField]
	MessageFromOpponent	mfo;
	[SerializeField]
	RectTransform Content;
	[SerializeField]
	MessageChat prefabItemChat;
	[SerializeField]
	GameObject windowChatMultiplayer;
	[SerializeField]
	GameObject messageChatMultiplayer;
	
	[System.NonSerialized]
	public static string Opponent;
	[System.NonSerialized]
	public static string OpponentName;

	static bool chatEnabled;
	public static bool СhatEnabled { get { return chatEnabled; } }

	void Start()
	{
		MessageChat message;
		float offsetY = 0f;
		Vector2 sizeContent = Content.sizeDelta;
		sizeContent.y = 0f;
		foreach (string msg in MessagesForMultiplayer.GetListMessages())
		{
			if (MessagesForMultiplayer.IsHiddenMessage(msg))
				continue;
			message = (MessageChat)Instantiate(prefabItemChat);
			message.transform.SetParent(Content);
			message.transform.localScale = Vector3.one;
			RectTransform rt = message.GetComponent<RectTransform>();
			offsetY -= offsetY != 0f ? rt.sizeDelta.y : rt.sizeDelta.y / 2;
			sizeContent.y += rt.sizeDelta.y;
			rt.anchoredPosition = new Vector3(0f, offsetY, 0f);
			message.SetTextMessage(msg);
			message.WindowChatMultiplayer = windowChatMultiplayer;
		}
		Content.sizeDelta = sizeContent;
		chatEnabled = false;
	}

	public void AddMessageOpponent(string text)
	{
		mfo.Message = text;
		mfo.gameObject.SetActive(true);
	}

	public static void ChatSetEnable()
	{
		chatEnabled = true;
	}

	public static void ChatSetDisable()
	{
		chatEnabled = false;
		InGameUIManager.Instance.UnblockCue();
	}

	public void ChatDisable()
	{
		chatEnabled = false;
		InGameUIManager.Instance.UnblockCue();
		windowChatMultiplayer.SetActive(false);
	}
}
