﻿using UnityEngine;
using UnityEngine.UI;

public class ChatSinglePlayer : MonoBehaviour {
	[SerializeField]
	MessageChat MessagePrefabMy;
	[SerializeField]
	MessageChat MessagePrefabOpponet;
	[SerializeField]
	Transform Content;
	[SerializeField]
	float OffsetXMessages = 100f;
	[SerializeField]
	InputField textInput;
	[SerializeField]
	RectTransform rTransformViewPort;
	[SerializeField]
	GameObject BoxCountMessages;
	[SerializeField]
	Text TextCountNotReadMessages;
	[SerializeField]
	GameObject chatSinglePlayerWindow;

	static bool chatEnabled;
	public static bool СhatEnabled { get { return chatEnabled; } }

	int CountNotReadMessage;
	RectTransform rTransformContent;
    TextGenerator textGen;
	float currentOffsetY;

    void Awake()
	{
		rTransformContent = Content.GetComponent<RectTransform>();
        textGen = new TextGenerator();
    }

	void Start()
	{
		chatEnabled = false;
	}

	public void AddMessageMy(string text)
	{
		if (text.Trim().Equals(""))
			return;
		MessageChat message = (MessageChat)Instantiate(MessagePrefabMy);
		RectTransform rt;
		Vector2 size;
		ChangeSizeMessage(text, message, 1, out rt, out size);
        GameSparksManager.Instance.SendChallengeChatMessage(GameData.Instance.opponentId, text);
		textInput.text = "";
		changeFocusInChat(rt, size.y / 2f + 10f);
	}

	public void AddMessageOpponent(string text)
	{
		MessageChat message = (MessageChat)Instantiate(MessagePrefabOpponet);
		RectTransform rt;
		Vector2 size;
		ChangeSizeMessage(text, message, -1, out rt, out size);
		changeFocusInChat(rt, size.y / 2f + 10f);

		if (!chatSinglePlayerWindow.activeSelf)
		{
			BoxCountMessages.SetActive(true);
			CountNotReadMessage = Mathf.Clamp(CountNotReadMessage + 1, 0, 99);
			TextCountNotReadMessages.text = CountNotReadMessage.ToString();
		}
	}

	void ChangeSizeMessage(string text, MessageChat message, int sign, out RectTransform rt, out Vector2 size)
	{
		rt = message.GetComponent<RectTransform>();
		message.transform.SetParent(Content);
		message.transform.localScale = Vector3.one;
		message.SetTextMessage(text);

		Text textMessage = message.GetText();
		TextGenerationSettings tgs = textMessage.GetGenerationSettings(textMessage.rectTransform.rect.size);
        tgs.scaleFactor = 1f;
        float height = textGen.GetPreferredHeight(text, tgs);// *4f;
		float width = textGen.GetPreferredWidth(text, tgs);

		size = textMessage.rectTransform.sizeDelta;
		//height *= Mathf.RoundToInt(width / size.x + 2.5f);
		size.y = Mathf.Max(size.y, height);
		textMessage.rectTransform.sizeDelta = size;

		size = rt.sizeDelta;
		size.y = Mathf.Max(size.y, height);
		rt.sizeDelta = size;

		currentOffsetY += size.y / 2f + 10f;
		rt.anchoredPosition = new Vector3(sign * OffsetXMessages, -currentOffsetY, 0f);
		currentOffsetY += size.y / 2f + 10f;
	}

	void changeFocusInChat(RectTransform rt, float offset)
	{
		rTransformContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, -rt.anchoredPosition.y + offset);
		float deltaY = rTransformContent.sizeDelta.y - rTransformViewPort.rect.height;
//		Debug.Log(deltaY + " rTransformContent.sizeDelta.y=" + rTransformContent.sizeDelta.y + " rTransformViewPort.rect.height=" + rTransformViewPort.rect.height);
		if (deltaY > 0f)
		{
			Vector2 pos = rTransformContent.anchoredPosition;
			pos.y = 30f + deltaY;
			rTransformContent.anchoredPosition = pos;
		}
	}

	public void Reset()
	{
		TextCountNotReadMessages.text = "0";
		CountNotReadMessage = 0;
		BoxCountMessages.SetActive(false);
		chatEnabled = true;
	}

	public void CueCotrolerActivate()
	{
		chatEnabled = false;
	}

	public void ChatDisable()
	{
		chatEnabled = false;
		chatSinglePlayerWindow.SetActive(false);
	}

}
