﻿using UnityEngine;
using UnityEngine.UI;

public class MessageChat : MonoBehaviour {

	[SerializeField] Text text;

	// only for multiplayer
	GameObject windowChatMultiplayer;

	public GameObject WindowChatMultiplayer { set { windowChatMultiplayer = value; } }

	public void SetTextMessage(string inText)
	{
		text.text = inText;
	}

    public Text GetText()
    {
        return text;
    }
	
	/**
	 * methode only for multiplayer 
	 */
	public void SendMessage()
	{
		string nameOpponent = ChatMultiPlayer.OpponentName.Length <= 9 ? ChatMultiPlayer.OpponentName : ChatMultiPlayer.OpponentName.Substring(0, 9);
		GameSparksManager.Instance.SendChallengeChatMessage(ChatMultiPlayer.Opponent, nameOpponent + ": " + text.text);
		windowChatMultiplayer.SetActive(false);
		ChatMultiPlayer.ChatSetDisable();
	}
}
