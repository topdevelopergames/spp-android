﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MessageFromOpponent : MonoBehaviour {

	[SerializeField]
	float timeDeactivation = 2f;
	[SerializeField]
	Text text;

    TextGenerator textGen;
    RectTransform rt;
    Vector2 size;
    float minWidth;

    float time;

	static List<string> listNotShowMessages = new List<string>();

	public string Message { set { listNotShowMessages.Add(value); } }

    void Awake()
    {
        textGen = new TextGenerator();
        rt = gameObject.GetComponent<RectTransform>();
        size = rt.sizeDelta;
        minWidth = size.x;
    }
    void OnEnable()
	{
		StartCoroutine("Deactivation");
	}

	private void GetNextMessage()
	{
		if (listNotShowMessages.Count != 0)
		{
            text.text = listNotShowMessages[0];
            TextGenerationSettings tgs = text.GetGenerationSettings(text.rectTransform.rect.size);
            tgs.scaleFactor = 1f;
            float width = textGen.GetPreferredWidth(text.text, tgs) + 30f;
            size.x = Mathf.Max(minWidth, width);
            rt.sizeDelta = size;
            listNotShowMessages.RemoveAt(0);
			time = Time.realtimeSinceStartup + timeDeactivation;
		}
	}

	IEnumerator Deactivation()
	{
		do
		{
			GetNextMessage();
			while (Time.realtimeSinceStartup <= time)
				yield return null;
		}
		while (listNotShowMessages.Count != 0);
		gameObject.SetActive(false);
	}
}
