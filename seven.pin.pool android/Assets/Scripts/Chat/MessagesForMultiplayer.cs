﻿using System.Collections.Generic;

class MessagesForMultiplayer
{
	static List<string> messages = new List<string>(){
		"Close!",
		"Nice shot!",
		"You're good",
		"Hehe",
		"Unlucky",
		"Nice Table",
		"Nice Cue",
		"Sorry Gotta Run",
		"Good game",
		"Nice try",
		"Oops",
		"Thanks",
		"Well played",
		"Good luck!",
		"I need more coins!"
	};

	/*static List<string> messages = new List<string>(){
		"message 1",
		"message 2",
		"message 3",
		"message 4",
		"message 5",
		"message 6",
		"message 7",
		"message 8",
		"message 9",
		"message 10",
		"message 11",
		"message 12",
		"message 13",
		"message 14",
		"message 15"
	};*/


	public static List<string> GetListMessages()
	{
		if (GameData.Instance.ListSortedMessageForChat == null)
			GameData.Instance.ListSortedMessageForChat = messages;
		return GameData.Instance.ListSortedMessageForChat;
	}

	public static List<string> GetListHiddenMessages()
	{
		if (GameData.Instance.ListHiddenMessageForChat == null)
			GameData.Instance.ListHiddenMessageForChat = new List<string>();
		return GameData.Instance.ListHiddenMessageForChat;
	}

	public static bool IsHiddenMessage(string msg)
	{
		return GetListHiddenMessages().Contains(msg);
	}

	public static void SaveListMessages(List<string> listMessages)
	{
		GameData.Instance.ListSortedMessageForChat = listMessages;
	}

	public static void HideMessage(string msg)
	{
		List<string> list = GetListHiddenMessages();
		if (!list.Contains(msg))
			list.Add(msg);
	}

	public static void ShowMessage(string msg)
	{
		List<string> list = GetListHiddenMessages();
		if (list.Contains(msg))
			list.Remove(msg);
	}

	public static void AddNewMessages(List<string> newList)
	{
		List<string> list = GetListMessages();
		if (list.Contains(newList[0]))
			return;
		list.AddRange(newList);
	}
}
