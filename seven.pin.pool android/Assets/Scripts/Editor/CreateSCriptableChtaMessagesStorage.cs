﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateSCriptableChtaMessagesStorage {

    [MenuItem("Assets/Create/ChatMessagesStorage")]
    public static void CreateMyAsset()
    {
        ChatMessagesStorage asset = ScriptableObject.CreateInstance<ChatMessagesStorage>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/ChatMessagesStorage.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
	
}
