﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptableCue {

    [MenuItem("Assets/Create/CuesStorage")]
    public static void CreateMyAsset()
    {
        CuesStorage asset = ScriptableObject.CreateInstance<CuesStorage>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/CuesStorage.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
