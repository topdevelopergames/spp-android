﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptableObject{

    [MenuItem("Assets/Create/PathToResources")]
    public static void CreateMyAsset()
    {
        PathToResources asset = ScriptableObject.CreateInstance<PathToResources>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/PathToResources.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

}
