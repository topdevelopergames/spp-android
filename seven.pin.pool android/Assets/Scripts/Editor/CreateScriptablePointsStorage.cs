﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptablePointsStorage {

    [MenuItem("Assets/Create/PointsStorage")]
    public static void CreateMyAsset()
    {
        LevelPointsStorage asset = ScriptableObject.CreateInstance<LevelPointsStorage>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/LevelPointsStorage.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
