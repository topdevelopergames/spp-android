﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptableShopItem {

    [MenuItem("Assets/Create/ShopItemsStorage")]
    public static void CreateMyAsset()
    {
        ShopItemStorage asset = ScriptableObject.CreateInstance<ShopItemStorage>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/ShopItemsStorage.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
