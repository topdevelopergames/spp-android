﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class RevertNormals 
{

    [MenuItem("Tools/Revet normals")]
    public static void Revert()
    {
        GameObject obj = Selection.activeGameObject;
        if (obj == null || obj.GetComponent<MeshFilter>() == null)
        {
            Debug.LogError("Please select object with mesh filter");
            return;
        }

        MeshFilter filter = obj.GetComponent(typeof(MeshFilter)) as MeshFilter;
        if (filter != null)
        {
            Mesh mesh = filter.mesh;

            Vector3[] normals = mesh.normals;
            for (int i = 0; i < normals.Length; i++)
                normals[i] = -normals[i];
            mesh.normals = normals;

            for (int m = 0; m < mesh.subMeshCount; m++)
            {
                int[] triangles = mesh.GetTriangles(m);
                for (int i = 0; i < triangles.Length; i += 3)
                {
                    int temp = triangles[i + 0];
                    triangles[i + 0] = triangles[i + 1];
                    triangles[i + 1] = temp;
                }
                mesh.SetTriangles(triangles, m);
            }
        }
        Debug.Log("Revert normals on object: " + obj.name + " is done");
    }
}