﻿using System;

public enum Cue_Shop_Type 
{
    standard = 0,
    premium,
    country
}

public enum Currency_Type_Shop
{
    Cash = 0,
    Coins
}

public class CueEntity: IEquatable<CueEntity>
{
    public const int MAX_CUE_FEATURE_LEVEL = 10;

    public int cueId { get; set; }
    public string cueName { get; set; }
    public string cueDescription { get; set; }
    public int force { get; set; }
    public int aim { get; set; }
    public int spin { get; set; }
    public int requiredLevel { get; set; }
    public bool owned { get; set; }
    public long price { get; set; }
    public Cue_Shop_Type cueType { get; set; }
    public Currency_Type_Shop currency { get; set; }

    public bool Equals(CueEntity other)
    {
        return this.cueId == other.cueId;
    }

    public static CueEntity DeepCopy(CueEntity origin)
    {
        CueEntity c = new CueEntity();
        c.aim = origin.aim;
        c.cueDescription = origin.cueDescription;
        c.cueId = origin.cueId;
        c.cueName = origin.cueName;
        c.cueType = origin.cueType;
        c.currency = origin.currency;
        c.force = origin.force;
        c.owned = origin.owned;
        c.price = origin.price;
        c.requiredLevel = origin.requiredLevel;
        c.spin = origin.spin;
        return c;
    }

}

