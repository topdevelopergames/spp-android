﻿using UnityEngine;
using System.Collections;
using System;

public class ShopitemEntity : IEquatable<ShopitemEntity>
{
    public int id { get; set; }
    public VirtualGoodType type { get; set; }
    public string title { get; set; }
    public int price { get; set; }
    public Currency_Type_Shop currency { get; set; }
    public bool owned { get; set; }
    [Newtonsoft.Json.JsonIgnore]
    public Sprite withPockets { get; set; }
    [Newtonsoft.Json.JsonIgnore]
    public Sprite withoutPockets { get; set; }
    [Newtonsoft.Json.JsonIgnore]
    public Sprite itemIcoWithPockets { get; set; }
    [Newtonsoft.Json.JsonIgnore]
    public Sprite itemIcoWithoutPockets { get; set; }

    public bool Equals(ShopitemEntity other)
    {
        return this.id == other.id;
    }

    public static ShopitemEntity DeepCopy(ShopitemEntity origin)
    {
        ShopitemEntity clone = new ShopitemEntity();
        clone.currency = origin.currency;
        clone.id = origin.id;
        clone.owned = origin.owned;
        clone.price = origin.price;
        clone.title = origin.title;
        clone.type = origin.type;
        clone.withPockets = origin.withPockets;
        clone.withoutPockets = origin.withoutPockets;
        clone.itemIcoWithPockets = origin.itemIcoWithPockets;
        clone.itemIcoWithoutPockets = origin.itemIcoWithoutPockets;
        return clone;
    }

}
