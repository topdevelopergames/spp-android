﻿using UnityEngine;
using System.Collections.Generic;

public class GameSparksTest : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreateChallenge()
    {
        ChallengeCreatingParam param = new ChallengeCreatingParam();
        param.challengerId = GameSparksManager.Instance.AuthorizedPlayerData.playerId;
        param.challengeType = ChallengeType.FRIEND;
        param.entryFee = 0;
        param.shortCode = ChallengeCode.MULTI_21;
        param.tableType = TableType.POCKET;
        param.timeLimit = 0;
        param.authType = AuthType.SIGNED;
        param.usersToChallenge = new List<string>() {
        "573f32b5dba6d604b71bea3e",
        "574ff0fa1474e704a6aac9a2",
        "574ff1201474e704a6aad42d",
        "573f14089f0f9404ca0d8f17",
        "575a78c29f0f9404cc5905ff",
        "575a9bdd18ead604b7b800e7"
        };

        GameSparksManager.Instance.CreateChallenge(param, null, null, null, null, null, null, null);
    }
}
