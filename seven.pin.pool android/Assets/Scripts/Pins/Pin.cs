﻿using UnityEngine;
using System.Collections;

public class Pin : MonoBehaviour {

	[SerializeField] int price = 1;
	[SerializeField] Transform holderPosition;
	[SerializeField] GameObject aure;
	[SerializeField] float radiusPin = .16f;
	[SerializeField] float heightPin = .5f;
	Vector3 position;
	Vector3 rotation;
	Rigidbody rb;

	//Vector3 prevPosition;
	//Vector3 prevRotation;

	int layerMainBall;
	int layerBall;
	int layerPin;
	int layerGround;

	int id;
	bool inForceMove;

	bool struck;
	bool prevStruck;

	LayerMask ballMask;
	AudioSource audioSource;

	public int Price { get { return price; } }
	public int Id { get { return id; } set { id = value; } }

	void Awake () {
		position = transform.position;
		rotation = transform.eulerAngles;
		rb = GetComponent<Rigidbody>();
		layerBall = LayerMask.NameToLayer("Ball");
		layerMainBall = LayerMask.NameToLayer("MainBall");
		layerPin = LayerMask.NameToLayer("Pins");
		layerGround = LayerMask.NameToLayer("Graund");
		ballMask = (1 << layerBall) | (1 << layerMainBall);
		audioSource = GetComponent<AudioSource>();
	}

#if UNITY_EDITOR
//	void OnEnable()
//	{
//		gameObject.SetActive(false);
//	}
#endif

	public void RestoryPosition()
	{
		StopCoroutine("WaitChangePositionPin");
		StopCoroutine("SetPinCollisionDataRepeating");
		aure.SetActive(true);
		rb.isKinematic = true;
		rb.velocity = Vector3.zero;

		Vector3 newStrPosition = position;
		Ray ray = new Ray(position + (heightPin + heightPin) * Vector3.up, -Vector3.up);
		RaycastHit hit;
		if (Physics.SphereCast(ray, radiusPin, out hit, heightPin + heightPin + heightPin, ballMask))
			newStrPosition = holderPosition.position;

		//prevPosition = transform.position;
		//prevRotation = transform.eulerAngles;
		prevStruck = struck;
		transform.position = newStrPosition;//position;
		transform.eulerAngles = rotation;
		struck = false;
		inForceMove = false;
		//rb.isKinematic = false;
		//StartCoroutine("SetNoKinematic");
	}

	public bool IsChangedPosition()
	{
		return prevStruck;//prevPosition != position || prevRotation != rotation;
	}


	IEnumerator WaitChangePositionPin(Collider other)
	{
		//Debug.Log("pin hot pin=" + gameObject.name + " HitPinBanned = " + other.gameObject.name + "IsKinematic=" + rb.isKinematic);
		while (Mathf.Abs(position.y - transform.position.y) < .05f)
			yield return null;
		struck = true;
		if (ServerController.serverController && ServerController.serverController.isMyQueue)
		{
			Rules.Instance.shotOnePin();
			GameSparksManager.Instance.IncreaseNumberField("countPins", 1);
		}
		if (other.gameObject.layer == layerMainBall)
		{
			Debug.Log("pin=" + gameObject.name + " HitPinBanned = " + other.gameObject.name);
			Rules.Instance.addStateForShot(Hits.HitPinBanned);
		}
		else if (other.gameObject.layer == layerBall || other.gameObject.layer == layerPin)
			Rules.Instance.addStateForShot(Hits.HitPin);
		aure.SetActive(false);
	}

	void OnTriggerEnter(Collider other)
	{
		if (ServerController.serverController != null)
			StartCoroutine("SetPinCollisionDataRepeating");
		
		if (struck)
			return;
		
		if (other.gameObject.layer == layerBall || other.gameObject.layer == layerMainBall || other.gameObject.layer == layerPin)
		{
            //audioSource.volume = .2f;
            ////
            //audioSource.volume = Mathf.Max(rb.velocity.magnitude, other.attachedRigidbody.velocity.magnitude) / 50f;
            //audioSource.Play();            
            ////
            SoundManager.Instance.PlayAudioSource(audioSource, Mathf.Max(rb.velocity.magnitude, other.attachedRigidbody.velocity.magnitude) / 50f);
			StopCoroutine("WaitChangePositionPin");
			//StartCoroutine(WaitChangePositionPin(other));
			StartCoroutine("WaitChangePositionPin", other);
		//	Debug.Log(other.name + " hit pin   " + other.gameObject.layer.ToString());
		}
	}

	IEnumerator SetPinCollisionDataRepeating()
	{
		if (!inForceMove)
		{
			inForceMove = true;
			if (ServerController.serverController.isMyQueue)
			{
				ServerController.serverController.SendRPCToServer("ForceSetPinMove", ServerController.serverController.otherNetworkPlayer, id, rb.position, rb.velocity, rb.angularVelocity, rb.rotation, struck);
				yield return new WaitForSeconds(0.1f);
				do
				{
					ServerController.serverController.SendRPCToServer("ForceSetPinMove", ServerController.serverController.otherNetworkPlayer, id, rb.position, rb.velocity, rb.angularVelocity, rb.rotation, struck);
					yield return new WaitForSeconds(0.1f);
				}
				while (!rb.IsSleeping() && !rb.isKinematic);
				ServerController.serverController.SendRPCToServer("ForceSetPinMove", ServerController.serverController.otherNetworkPlayer, id, rb.position, rb.velocity, rb.angularVelocity, rb.rotation, struck);
			}
			else
			{
				ServerController.serverController.SendRPCToServer("ForceGetPinMove", ServerController.serverController.otherNetworkPlayer, id);
				yield return new WaitForSeconds(0.1f);
			}
			inForceMove = false;
		}
	}

	public void SendForceSetPinMove()
	{
		ServerController.serverController.SendRPCToServer("ForceSetPinMove", ServerController.serverController.otherNetworkPlayer, id, rb.position, rb.velocity, rb.angularVelocity,
			rb.rotation, struck);
	}

	public void ForceSetMove(Vector3 position, Vector3 velocity, Vector3 angularVelocity, Quaternion rotation, bool inStruck)
	{
		if (!rb.isKinematic)
		{
			//Debug.Log("ForceSetMove id=" + id);
			rb.position = position;
			rb.velocity = velocity;
			rb.rotation = rotation;
			rb.angularVelocity = angularVelocity;
			struck = inStruck;
			aure.SetActive(!struck);
			if (inStruck)
				Rules.Instance.OpponentOvertunePin();
		}
	}

	public void ForceGetMove()
	{
		if (ServerController.serverController != null)
		{
			//Debug.Log("ForceGetMove id=" + id);
			StartCoroutine("SetPinCollisionDataRepeating");
		}
	}

	/*IEnumerator SetNoKinematic()
	{
		yield return new WaitForSeconds(1f);
		rb.isKinematic = false;
	}*/

	public void SetNoKinematic()
	{
		rb.isKinematic = false;
	}
}
