﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum Hits { HitObjectBall, HitOtherObjectBall, HitCushion, HitPin, ObjectBallOut, OtherObjectBallOut, CueBallOut, HitPinBanned, HitObjectBallAndCushion };

public class Rules : MonoBehaviour {
	[SerializeField]
	CueController cc;
	[SerializeField]
	int priceObjectBallOut = 1;
	[SerializeField]
	int priceCaromBall = 1;
	// price knock pin taken from class Pin
	[SerializeField]
	Text textStatistic;

	BallController firstBallHit;
	BallController firstBallOut;

	List<Hits[]> rightShots;
	List<Hits> stateShot;

	int numberShot;
	int allPointLeft;
	//int allPointRight;
	int lastPoint;
	static Rules instance;
	
	// For test user
	public static int coefPricePins = 1;

	public static Rules Instance { get { return instance; } }


	# region For Acievements
	bool isWasChangeQueue;
	bool isWasOpponentOvertunePins;
	float minPowerQue;
	float maxPowerQue;
	int countShotPin;
	int idPocketFirstBall;
	bool samePocket;
	bool superLoser;
	int countLegalShot;

	public bool IsWasChangeQueue { get { return isWasChangeQueue; } }
	public bool IsWasOpponentOvertunePins { get { return isWasOpponentOvertunePins; } }
	public float PowerQue
	{
		set
		{
			if (minPowerQue == 0f || minPowerQue > value) minPowerQue = value;
			if (maxPowerQue == 0f || maxPowerQue < value) maxPowerQue = value;
		}
	}
	public bool IsWasMoreHalfPowerQue { get { return maxPowerQue >= .5f; } }
    public float MaxPowerQue { get { return maxPowerQue; } }
    public bool IsWasOnlyMaxPowerQue { get { return minPowerQue == 1f; } }
	// for one shot
	public bool IsOvertune5PinsOneShot { get { return countShotPin == 5; } }
	public bool IsOvertune7PinsOneShot { get { return countShotPin == 7; } }
	# endregion For Acievements

	void Start()
	{
		instance = this;
		
		numberShot = 1;
		allPointLeft = 0;
		//allPointRight = 0;
		textStatistic.text = "";

		stateShot = new List<Hits>(6);
		rightShots = new List<Hits[]>();


        // Legacy
        //************
        rightShots.Add(new Hits[] { Hits.HitObjectBall, Hits.HitPin });
        rightShots.Add(new Hits[] { Hits.HitObjectBall, Hits.HitOtherObjectBall, Hits.HitPin });
        //************

        rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.HitPin });
        

        rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.ObjectBallOut });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.HitOtherObjectBall });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.HitOtherObjectBall, Hits.ObjectBallOut });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.HitOtherObjectBall, Hits.ObjectBallOut, Hits.OtherObjectBallOut });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.ObjectBallOut, Hits.OtherObjectBallOut });

		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.ObjectBallOut, Hits.HitPin });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.HitOtherObjectBall, Hits.HitPin });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.HitOtherObjectBall, Hits.ObjectBallOut, Hits.HitPin });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.HitOtherObjectBall, Hits.ObjectBallOut, Hits.OtherObjectBallOut, Hits.HitPin });
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion, Hits.ObjectBallOut, Hits.OtherObjectBallOut, Hits.HitPin });

		// Safety
		rightShots.Add(new Hits[] { Hits.HitObjectBallAndCushion });

        // test
        /*		startShoot();
                addStateForShot(Hits.HitObjectBall);
                addStateForShot(Hits.HitOtherObjectBall);
                addStateForShot(Hits.HitPin);
                addStateForShot(Hits.HitCushion);
                addStateForShot(Hits.ObjectBallOut);
                int res = GetPointsForShot();*/
    }

	void OptimizationShotResult()
	{
		int iBall = stateShot.IndexOf(Hits.HitObjectBall);
		int iCushion = stateShot.IndexOf(Hits.HitCushion);
		int iHitPin = stateShot.IndexOf(Hits.HitPin);
		int posInsert = Mathf.Min(iBall, iCushion);
		int posDelete = Mathf.Max(iBall, iCushion);
        
        if (iBall != -1 && iCushion != -1 /*&& (iHitPin == -1 || (iHitPin != -1 && iHitPin > iCushion))*/)
		{
			stateShot.RemoveAt(posDelete);
			stateShot.RemoveAt(posInsert);
			stateShot.Insert(posInsert, Hits.HitObjectBallAndCushion);
		}
        
		// for carambol and ballout - HitCushion is not need!
		int iObjectBallOut = stateShot.IndexOf(Hits.ObjectBallOut);
		int iHitOtherObjectBall = stateShot.IndexOf(Hits.HitOtherObjectBall);
		if ((iObjectBallOut != -1 || iHitOtherObjectBall != -1) && iCushion == -1)
		{
			stateShot.RemoveAt(posDelete);
			stateShot.Insert(posDelete, Hits.HitObjectBallAndCushion);
		}

		iHitPin = stateShot.IndexOf(Hits.HitPin);
		if (iHitPin != -1 && iHitPin != stateShot.Count - 1)
		{
			stateShot.RemoveAt(iHitPin);
			stateShot.Add(Hits.HitPin);
		}
	}

	int GetPoints()
	{
		int price = 0;
		if (stateShot.Contains(Hits.ObjectBallOut))
			price += priceObjectBallOut;
		if (stateShot.Contains(Hits.OtherObjectBallOut))
			price += priceObjectBallOut;
		if (stateShot.Contains(Hits.HitObjectBallAndCushion) && stateShot.Contains(Hits.HitOtherObjectBall))
			price += priceCaromBall;
		int countPinChangedPosition = 0;
		if (stateShot.Contains(Hits.HitPin) || stateShot.Contains(Hits.HitPinBanned))
		{
			foreach (Pin pin in cc.pins)
				if (pin.IsChangedPosition())
				{
					price += pin.Price;
					countPinChangedPosition++;
				}
		}
		superLoser = countPinChangedPosition == 7 && stateShot.Contains(Hits.ObjectBallOut) && stateShot.Contains(Hits.OtherObjectBallOut) && stateShot.Contains(Hits.CueBallOut);
		return price;
	}

	public BallController FirstBallHit { set { firstBallHit = value; } get { return firstBallHit; } }

	public BallController FirstBallOut { set { firstBallOut = value; } get { return firstBallOut; } }

	public void startShoot()
	{
		stateShot.Clear();
		firstBallHit = null;
		firstBallOut = null;
		countShotPin = 0;
		idPocketFirstBall = -1;
		samePocket = false;
		superLoser = false;
	}

	public void addStateForShot(Hits hit)
	{
		if (!stateShot.Contains(hit))
			stateShot.Add(hit);
	}

	public int GetPointsForShot(bool left = true)
	{
		string combination1 = "";
		foreach (Hits hit in stateShot)
			combination1 += hit.ToString() + "-";
		if(combination1.Length > 0)
			combination1 = combination1.Substring(0, combination1.Length - 1);

		OptimizationShotResult();

		List<Hits[]> tmpRightShots = rightShots.GetRange(0, rightShots.Count);
		int price = -1;
		foreach(Hits[] tmpRightShot in tmpRightShots)
		{
			if (tmpRightShot.Length != stateShot.Count)
				continue;
			int i = 0;
			// remove the dependency on the position of
			for (; i < tmpRightShot.Length; i++)
			{
			//	if (tmpRightShot[i] != stateShot[i])
				if (!stateShot.Contains(tmpRightShot[i]))
					break;
			}
			if (i == stateShot.Count)
			{
				price = GetPoints();
				break;
			}
		}
		// not right shot
		if (price == -1)
		{
			price = -GetPoints();
			//if (stateShot.Contains(Hits.CueBallOut))
//				price--;
			if (stateShot.Contains(Hits.CueBallOut) || stateShot.Count == 0)
				price--;
			if (price == 0)
				price = -1;
		}

		// for all
		string combination = "";
		foreach (Hits hit in stateShot)
			combination += hit.ToString() + "-";
		//Debug.Log(combination + " price=" + price.ToString());
		if (combination.Length > 0)
			combination = combination.Substring(0, combination.Length - 1);

		price *= coefPricePins;

		if (left)
		{
			allPointLeft += price;
			InGameUIManager.Instance.SetLeftUserScore(allPointLeft.ToString());
			textStatistic.text = numberShot++.ToString() + ". left " + price + " = " + allPointLeft + " " + combination1 + "\r\n" + textStatistic.text;

			if ((GameData.Instance.selectedGameMode >= 0 && GameData.Instance.singleOrMulti == 1) || (GameData.Instance.selectedGameMode > 0 && GameData.Instance.singleOrMulti == 0))
			{
				if (ServerController.serverController != null)
					ServerController.serverController.SendRPCToServer("SetHighScoreToOther", ServerController.serverController.otherNetworkPlayer, allPointLeft);
                Debug.Log("Updating current score");
                GameSparksManager.Instance.UpdateGameRuntimeLeaderboard(price);
                if (GameData.Instance.challengeTimeOver && (GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 1))
                    GameSparksManager.Instance.ReadyToCompleteCurrentChallenge();
			}

			// achievements
			if (!(GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 0))
			{
				if (IsOvertune5PinsOneShot)
					GameSparksManager.Instance.AddAchievement(AchievementType.POWER_COMBO_SHOT);
				if (IsOvertune7PinsOneShot)
					GameSparksManager.Instance.AddAchievement(AchievementType.OVERTURN_SUPER_COMBO);
				if (samePocket)
					GameSparksManager.Instance.AddAchievement(AchievementType.DOUBLE_DIP);
				if (superLoser)
					GameSparksManager.Instance.AddAchievement(AchievementType.SUPER_LOSER);
			}
		}
		lastPoint = price;
		if (GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 0 && lastPoint > 0)
		{
			if (++countLegalShot == 20)
				GameSparksManager.Instance.AddAchievement(AchievementType.TOP_OF_THE_CLASS);
		}
		return price;
	}

    public bool InNeedRestoryPositionCueBall()
    {
        return (stateShot.Count == 0 || (stateShot.Count == 1 && (stateShot.Contains(Hits.HitObjectBall) || stateShot.Contains(Hits.HitCushion))));
    }

    public int LastPoint { get { return lastPoint; } set { lastPoint = value; } }

	void OnDestroy()
	{
		Debug.Log("Rules.OnDestroy()");
//		if (ServerController.serverController)
//		   MasterServerGUI.Disconnect();
	}

	public void ChangeQueue()
	{
		isWasChangeQueue = true;
	}

	public void OpponentOvertunePin()
	{
		isWasOpponentOvertunePins = true;
	}

	public void shotOnePin()
	{
		countShotPin++;
	}

	public void ballInPocket(int idPocket)
	{
		Debug.Log("ballInPocket = " + idPocket);
        if (idPocketFirstBall == -1)
        {
            Debug.Log("idPocketFirstBall == -1 " + idPocket);
            idPocketFirstBall = idPocket;
            samePocket = false;
        }
        else if (idPocketFirstBall == idPocket)
        {
            Debug.Log("Double Dip idPocketFirstBall == idPocket " + idPocket);
            samePocket = true;
        }
        else
        {
            Debug.Log("idPocketFirstBall != idPocket " + idPocketFirstBall + " " + idPocket);
            samePocket = false;
        }
    }
}
