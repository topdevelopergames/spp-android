﻿using UnityEngine;
using System.Collections.Generic;

public class ChatMessagesStorage : ScriptableObject {

    public List<string> messages;

    private static ChatMessagesStorage _instance;

    public static ChatMessagesStorage Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = Resources.Load("ChatMessagesStorage") as ChatMessagesStorage;
            }
            return _instance;
        }
    }
    
    public List<string> GetInitialPack()
    {
        return GetPackMessage(-1);
    }

    public List<string> GetRematchPack()
    {
        return GetPackMessage(0);
    }

    public List<string> GetMotivationalPack()
    {
        return GetPackMessage(1);
    }

    public List<string> GetCheeringPack()
    {
        return GetPackMessage(2);
    }

    public List<string> GetFailPack()
    {
        return GetPackMessage(3);
    }

    public List<string> GetAcronymsPack()
    {
        return GetPackMessage(4);
    }

    public List<string> GetLaughPack()
    {
        return GetPackMessage(5);
    }

    public List<string> GetCompetitivePack()
    {
        return GetPackMessage(6);
    }

    public List<string> GetTalkPack()
    {
        return GetPackMessage(7);
    }

    public List<string> GetLovePack()
    {
        return GetPackMessage(8);
    }

    public List<string> GetCensoredPack()
    {
        return GetPackMessage(9);
    }

    public List<string> GetSpacePack()
    {
        return GetPackMessage(10);
    }

    public List<string> GetMoviePack()
    {
        return GetPackMessage(11);
    }

    public List<string> GetVanityPack()
    {
        return GetPackMessage(12);
    }

    public List<string> GetMovie2Pack()
    {
        return GetPackMessage(13);
    }

    public List<string> GetMemePack()
    {
        return GetPackMessage(14);
    }

    public List<string> GetGovernatorPack()
    {
        return GetPackMessage(15);
    }

    public List<string> GetDogePack()
    {
        return GetPackMessage(16);
    }

    public List<string> GetPassiveAgressivePack()
    {
        return GetPackMessage(17);
    }

    public List<string> GetExclamationPack()
    {
        return GetPackMessage(18);
    }

    public List<string> GetBragPack()
    {
        return GetPackMessage(19);
    }

    public List<string> GetTauntPack()
    {
        return GetPackMessage(20);
    }

    public List<string> GetBreakPack()
    {
        return GetPackMessage(21);
    }

    public List<string> GetPackMessage(int packId)
    {
        List<string> tempMsgs = new List<string>();
        switch(packId)
        {
            ///Initial pack
            case -1:
                tempMsgs = messages.GetRange(0, 15);
            break;
            ///Rematch pack
            case 0:
                tempMsgs = messages.GetRange(15, 4);
            break;
            ///Motivational pack
            case 1:
                tempMsgs = messages.GetRange(19, 6);
            break;
            ///Cheering pack
            case 2:
                tempMsgs = messages.GetRange(25, 6);
            break;
            ///Fail pack
            case 3:
                tempMsgs = messages.GetRange(31, 6);
            break;
            ///Acronyms pack
            case 4:
                tempMsgs = messages.GetRange(37, 6);
            break;
            ///Laugh pack
            case 5:
                tempMsgs = messages.GetRange(43, 6);
            break;
            ///Competitive pack
            case 6:
                tempMsgs = messages.GetRange(49, 6);
            break;
            ///Talk pack
            case 7:
                tempMsgs = messages.GetRange(55, 6);
            break;
            ///Love pack
            case 8:
                tempMsgs = messages.GetRange(61, 6);
            break;
            ///Censored swearing
            case 9:
                tempMsgs = messages.GetRange(67, 4);
            break;
            ///Space pack
            case 10:
                tempMsgs = messages.GetRange(71, 5);
            break;
            ///Movie quotes
            case 11:
                tempMsgs = messages.GetRange(76, 5);
            break;
            ///Vanity pack
            case 12:
                tempMsgs = messages.GetRange(81, 6);
            break;
            ///Movie quotes 2
            case 13:
                tempMsgs = messages.GetRange(87, 6);
            break;
            ///Meme oack
            case 14:
                tempMsgs = messages.GetRange(93, 5);
            break;
            ///Governator pack
            case 15:
                tempMsgs = messages.GetRange(98, 6);
            break;
            ///Doge pack
            case 16:
                tempMsgs = messages.GetRange(104, 4);
            break;
            ///Passive agressive pack
            case 17:
                tempMsgs = messages.GetRange(108, 6);
            break;
            ///Exclamation pack
            case 18:
                tempMsgs = messages.GetRange(114, 6);
            break;
            ///Brag pack
            case 19:
                tempMsgs = messages.GetRange(120, 6);
            break;
            ///Taunt pack
            case 20:
                tempMsgs = messages.GetRange(126, 6);
            break;
            ///Break pack
            case 21:
                tempMsgs = messages.GetRange(132, 6);
            break;
        }
        return tempMsgs;
    }


}
