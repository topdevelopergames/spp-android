﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChatPackStorage {

    private List<ShopitemEntity> chatpacks;

    public List<ShopitemEntity> ChatPacks
    {
        get
        {
            return chatpacks;
        }
    }

    private static ChatPackStorage _instance;

    public static ChatPackStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ChatPackStorage();
            }
            return _instance;
        }
    }

    public ChatPackStorage()
    {
        chatpacks = ShopItemStorage.GetShopItemsList("ChatPackStorage");
    }

    public void UpdateList(ShopitemEntity item)
    {
        chatpacks[item.id].owned = true;
    }

    public List<ShopitemEntity> GetOwned(List<VirtualGood> serverList)
    {
        List<ShopitemEntity> owned = new List<ShopitemEntity>();

        foreach (var item in serverList)
        {
            owned.Add(chatpacks[item.num]);
        }

        return owned;
    }

    public List<ShopitemEntity> GetAllItemsWithOwned(List<VirtualGood> serverList)
    {
        /*
        List<ShopitemEntity> allPacks = new List<ShopitemEntity>();

        foreach (ShopitemEntity item in chatpacks)
        {
            allPacks.Add(ShopitemEntity.DeepCopy(item));
        }
        
        foreach (var item in serverList)
        {
            allPacks[item.num].owned = true;
        }

        return allPacks;*/

        foreach (var item in serverList)
        {
            chatpacks[item.num].owned = true;
        }

        return chatpacks;

    }

    public Dictionary<string, List<string>> GetOwnedMessages(List<VirtualGood> serverList)
    {
        Dictionary<string, List<string>> ownedMessages = new Dictionary<string, List<string>>();

        foreach(var item in serverList)
        {
            ShopitemEntity itemPack = chatpacks[item.num];
            List<string> packMessages = ChatMessagesStorage.Instance.GetPackMessage(itemPack.id - 1);
            ownedMessages.Add(itemPack.title, packMessages);
        }

        return ownedMessages;
    }

    public void ResetList()
    {
        chatpacks = ShopItemStorage.GetShopItemsList("ChatPackStorage");
    }


}
