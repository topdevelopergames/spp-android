﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CuesStorage : ScriptableObject {

    public List<int> cueId;
    public List<string> cueName;
    public List<string> cueDescription;
    public List<int> force;
    public List<int> aim;
    public List<int> spin;
    public List<int> requiredLevel;
    public List<Sprite> cueImages;
    public List<long> price;
    public List<Currency_Type_Shop> currency;
    public List<bool> owned;
    public List<Cue_Shop_Type> cueType;


    private List<CueEntity> cues;
    private List<CueEntity> ownedCues;

    public List<CueEntity> Cues
    {
        get
        {
            if(cues == null)
            {
                CreateCuesList();
            }
            return cues;
        }
    }


    private static CuesStorage _instance;

    public static CuesStorage Instance
    {

        get
        {
            if(_instance == null)
            {
                _instance = Resources.Load<CuesStorage>("CuesStorage");
            }
            return _instance;
        }
    }

    void CreateCuesList()
    {
        cues = new List<CueEntity>();
        for (int i = 0; i < cueId.Count; i++)
        {
            CueEntity c = new CueEntity();
            c.aim = aim[i];
            c.cueName = cueName[i];
            c.cueType = cueType[i];
            c.force = force[i];
            c.requiredLevel = requiredLevel[i];
            c.spin = spin[i];
            c.cueId = cueId[i];
            c.currency = currency[i];
            c.owned = owned[i];
            c.price = price[i];
            c.cueDescription = cueDescription[i];
            cues.Add(c);
        }
    }

    public List<CueEntity> GetOwned(List<VirtualGood> serverCues)
    {
        List<CueEntity> owned = new List<CueEntity>();
        foreach (var item in serverCues)
        {
            //CueEntity c = cues.Find(x => x.cueId == item.num);
            CueEntity c = GetIten(item.num);
            c.owned = true;
            owned.Add(c);
        }
        return owned;
    }


    CueEntity GetIten(int id)
    {
        return cues[id];
    }

    public void ResetList()
    {
        CreateCuesList();
    }

   

	
}
