﻿using UnityEngine;
using System.Collections.Generic;

public class LevelPointsStorage : ScriptableObject {

    public List<int> nextLevelPoints;


    private static LevelPointsStorage _instance;

    public static LevelPointsStorage Instance
    {

        get
        {
            if (_instance == null)
            {
                _instance = Resources.Load<LevelPointsStorage>("LevelPointsStorage");
            }
            return _instance;
        }
    }

    public int GetNextLevelPoints(int currentLevel)
    {
		return currentLevel > 18 ? (int)(7761.477f * Mathf.Pow(1.0265f, currentLevel)) : GameData.Instance.pointsForLevel[currentLevel];
    }

}
