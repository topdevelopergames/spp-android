﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ShopItemStorage : ScriptableObject {

    public List<int> itemId;
    public List<VirtualGoodType> itemType;
    public List<string> itemTitle;
    public List<int> itemPrice;
    public List<Currency_Type_Shop> itemCurrency;
    public List<bool> itemOwned;
    public List<Sprite> withPockets, withoutPockets, itemIcoWithPockets, itemIcoWithoutPockets;

    public static List<ShopitemEntity> GetShopItemsList(string entityStorage)
    {
        ShopItemStorage storage = Resources.Load<ShopItemStorage>(entityStorage);
        List<ShopitemEntity> list = new List<ShopitemEntity>();

        for(int i = 0; i < storage.itemId.Count; i++)
        {
            ShopitemEntity item = new ShopitemEntity();
            item.id = storage.itemId[i];
            item.type = storage.itemType[i];
            item.title = storage.itemTitle[i];
            item.price = storage.itemPrice[i];
            item.currency = storage.itemCurrency[i];
            item.owned = storage.itemOwned[i];
            item.withPockets = storage.withPockets[i];
            item.withoutPockets = storage.withoutPockets[i];
            item.itemIcoWithPockets = storage.itemIcoWithPockets[i];
            item.itemIcoWithoutPockets = storage.itemIcoWithoutPockets[i];
            list.Add(item);
        }
        return list;
    }

      
}
