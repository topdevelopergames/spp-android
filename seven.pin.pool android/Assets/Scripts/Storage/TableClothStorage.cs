﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TableClothStorage {

    private List<ShopitemEntity> clothes;

    public List<ShopitemEntity> Clothes
    {
        get
        {
            return clothes;
        }
    }

    private static TableClothStorage _instance;

    public static TableClothStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new TableClothStorage();
            }
            return _instance;
        }
    }

    public TableClothStorage()
    {
        ResetList();
    }

    public void UpdateList(ShopitemEntity item)
    {
        clothes[item.id].owned = true;
    }
    /*
    public List<ShopitemEntity> GetOwned(List<VirtualGood> serverList)
    {
        List<ShopitemEntity> owned = new List<ShopitemEntity>();

        foreach (var item in serverList)
        {
            owned.Add(clothes[item.num]);
        }

        return owned;
    }*/

    public List<ShopitemEntity> GetAllWithOwned(List<VirtualGood> serverList)
    {
        /*
        List<ShopitemEntity> allCloth = new List<ShopitemEntity>();

        foreach (ShopitemEntity item in clothes)
        {
            allCloth.Add(ShopitemEntity.DeepCopy(item));
        }

        foreach (var item in serverList)
        {
            allCloth[item.num].owned = true;
        }
        return allCloth;
        */

        foreach (var item in serverList)
        {
            clothes[item.num].owned = true;
        }

        return clothes;

    }

    public void ResetList()
    {
        clothes = ShopItemStorage.GetShopItemsList("TableClothStorage");
    }

    
}
