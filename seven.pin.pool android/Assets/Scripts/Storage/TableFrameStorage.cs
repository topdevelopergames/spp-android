﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TableFrameStorage {

    private List<ShopitemEntity> frames;

    public List<ShopitemEntity> Frames
    {
        get
        {
            return frames;
        }
    }
    
    private static TableFrameStorage _instance;

    public static TableFrameStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new TableFrameStorage();
            }
            return _instance;
        }
    }
    
    public TableFrameStorage()
    {
        frames = ShopItemStorage.GetShopItemsList("TableFramesStorage");
    }

    public List<ShopitemEntity> GetOwned(List<VirtualGood> serverList)
    {
        List<ShopitemEntity> owned = new List<ShopitemEntity>();

        foreach(var item in serverList)
        {
            owned.Add(frames[item.num]);
        }

        return owned;
    }

    public void UpdateList(ShopitemEntity item)
    {
        frames[item.id].owned = true;
    }

    public List<ShopitemEntity> GetAllWithOwned(List<VirtualGood> serverList)
    {
        /*
        List<ShopitemEntity> allFranes = new List<ShopitemEntity>();

        foreach(ShopitemEntity item in frames)
        {
            allFranes.Add(ShopitemEntity.DeepCopy(item));
        }
        
        foreach (var item in serverList)
        {
            allFranes[item.num].owned = true;
        }
        return allFranes;
        */

        foreach (var item in serverList)
        {
            frames[item.num].owned = true;
        }

        return frames;
    }

    public void ResetList()
    {
        frames = ShopItemStorage.GetShopItemsList("TableFramesStorage");
    }
}
