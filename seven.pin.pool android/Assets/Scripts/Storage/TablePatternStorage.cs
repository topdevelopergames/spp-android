﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TablePatternStorage {

    private List<ShopitemEntity> patterns;

    public List<ShopitemEntity> Patterns
    {
        get
        {
            return patterns;
        }
    }

    private static TablePatternStorage _instance;

    public static TablePatternStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new TablePatternStorage();
            }
            return _instance;
        }
    }

    public TablePatternStorage()
    {
        patterns = ShopItemStorage.GetShopItemsList("TablePatternStorage");
    }

    public void UpdateList(ShopitemEntity item)
    {
        patterns[item.id].owned = true;
    }

    public List<ShopitemEntity> GetOwned(List<VirtualGood> serverList)
    {
        List<ShopitemEntity> owned = new List<ShopitemEntity>();

        foreach (var item in serverList)
        {
            owned.Add(patterns[item.num]);
        }

        return owned;
    }

    public List<ShopitemEntity> GetAllWithOwned(List<VirtualGood> serverList)
    {
        /*
        List<ShopitemEntity> allPatterns = new List<ShopitemEntity>();

        foreach (ShopitemEntity item in patterns)
        {
            allPatterns.Add(ShopitemEntity.DeepCopy(item));
        }

        foreach (var item in serverList)
        {
            allPatterns[item.num].owned = true;
        }
        return allPatterns;*/

        foreach (var item in serverList)
        {
            patterns[item.num].owned = true;
        }

        return patterns;
    }

    public void ResetList()
    {
        patterns = ShopItemStorage.GetShopItemsList("TablePatternStorage");
    }
}
