﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Beacon : MonoBehaviour {

	static Dictionary<string, bool> dict;
    static bool allVisible = false;

    void Start()
	{
        if (dict == null)
			dict = new Dictionary<string, bool>();
        if (!dict.ContainsKey(gameObject.name))
			dict.Add(gameObject.name, false);
    }

    void OnBecameVisible()
    {
        dict[gameObject.name] = true;
        bool cAllVisible = true;
        foreach (KeyValuePair<string, bool> item in dict)
            if (!item.Value)
            {
                cAllVisible = false;
                break;
            }
        allVisible = cAllVisible;
    }

    void OnBecameInvisible()
    {
        dict[gameObject.name] = false;
        allVisible = false;
    }

    public static bool IsAllVisible { get { return allVisible; } }
}
