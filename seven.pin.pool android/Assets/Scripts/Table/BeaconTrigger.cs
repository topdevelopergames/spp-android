﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeaconTrigger : MonoBehaviour {
    static bool allVisible = true;

    // need rigidbody
    void OnTriggerEnter(Collider other)
    {
        allVisible = false;
    }

    // need rigidbody
    void OnTriggerExit(Collider other)
    {
        allVisible = true;
    }

    public static bool IsAllVisible { get { return allVisible; } }
}
