﻿using UnityEngine;
using System.Collections;

public class TableSetup : MonoBehaviour {

    [SerializeField]
    private Texture2D[] framesWithPockets, framesWithoutPockets, cloth;
    [SerializeField]
    private Material frameWithPocketsMat, frameWithoutPocketsMat, clothMat;

    void Awake()
    {
        clothMat.mainTexture = cloth[GameData.Instance.selectedCloth.id];
        if (GameData.Instance.playTable == 0)
        {
            frameWithPocketsMat.mainTexture = framesWithPockets[GameData.Instance.selectedFrame.id];
            return;
        }
        frameWithoutPocketsMat.mainTexture = framesWithoutPockets[GameData.Instance.selectedFrame.id];
    }

}
