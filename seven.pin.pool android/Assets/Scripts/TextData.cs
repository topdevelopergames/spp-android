﻿using UnityEngine;

public class TextData : ScriptableObject
{
    public string authUnrecognised;
    public string authLocked;
    public string authUnknown;

    private static TextData instance = null;

    public static TextData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load("TextData") as TextData;
            }
            return instance;
        }
    }
}

