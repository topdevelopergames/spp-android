﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using System.Collections.Generic;

public class AchievemenPage : EnhancedScrollerCellView {

    [SerializeField]
    private GameObject achievementItemPrefab;


    private GameObject[] achivs;

    public void SetAchievements(List<Achievement> achievements, Sprite unlockedAchiv, Sprite lockedAchiv)
    {
        if (achivs == null)
        {
            GenerateAchivs(achievements, unlockedAchiv, lockedAchiv);
        }
        else
        {
            UpdateAchivs(achievements, unlockedAchiv, lockedAchiv);
        }
    }

    void UpdateAchivs(List<Achievement> achievements, Sprite unlockedAchiv, Sprite lockedAchiv)
    {
        for(int i = 0; i < achievements.Count; i++)
        {
            AchievementItem aitem = achivs[i].GetComponent<AchievementItem>();
            aitem.SetAchievement(achievements[i], unlockedAchiv, lockedAchiv);
        }
        int countToHide = achivs.Length - achievements.Count;
        if (countToHide > 0)
        {
            for (int i = countToHide + 1; i < achivs.Length; i++)
            {
                achivs[i].GetComponent<AchievementItem>().SetAchievement(null);
            }
        }
    }

    void GenerateAchivs(List<Achievement> achievements, Sprite unlockedAchiv, Sprite lockedAchiv)
    {
        achivs = new GameObject[achievements.Count];
        for (int i = 0; i < achievements.Count; i++)
        {
            GameObject item = GameObject.Instantiate(achievementItemPrefab);
            item.transform.SetParent(transform, false);
            AchievementItem achivItem = item.GetComponent<AchievementItem>();
            achivItem.SetAchievement(achievements[i], unlockedAchiv, lockedAchiv);
            achivs[i] = item;
        }
    }
	
}
