﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class AchievementController : MonoBehaviour, IEnhancedScrollerDelegate, IPointerDownHandler, IPointerUpHandler {

    [SerializeField]
    private AchievemenPage achievementPagePrefab;
    [SerializeField]
    private EnhancedScroller scroller;
    [SerializeField]
    private Sprite unlockedAchiv, lockedAchiv;
    [SerializeField]
    private UnityEngine.UI.Button prevButton, nextButton;

    private int disableCountDown = 2;

    private int defaultAchivsPerPage = 5;
    private int fullPages, currentDataIndex;

    private List<Achievement> achivs;

    public void OnPointerDown(PointerEventData data)
    {
        scroller.snapping = false;
    }

    public void OnPointerUp(PointerEventData data)
    {
        scroller.snapping = true;
        scroller.LinearVelocity = 100;
    }

    void OnEnable()
    {
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        currentDataIndex = 0;
        scroller.Delegate = this;

        if (achivs == null)
        {
            scroller.ScrollRect.enabled = false;
            scroller.enabled = false;
        }
        scroller.scrollerScrollingChanged -= JumpComplete; 
        scroller.scrollerScrollingChanged += JumpComplete;   
        //scroller.scrollerScrolled -= ScrollerScrolled;
        //scroller.scrollerScrolled += ScrollerScrolled;
    }

    void OnDisable()
    {
        scroller.scrollerScrollingChanged -= JumpComplete;
        //scroller.scrollerScrolled -= ScrollerScrolled;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        AchievemenPage achivPage = scroller.GetCellView(achievementPagePrefab) as AchievemenPage;       
        int achivsPerPage = defaultAchivsPerPage;
        int leftAchivs = achivs.Count - (dataIndex * defaultAchivsPerPage);
        if (leftAchivs > 0 && leftAchivs < defaultAchivsPerPage)
        {
            achivsPerPage = leftAchivs;
        }

        List<Achievement> achivsToPage = achivs.GetRange(dataIndex * defaultAchivsPerPage, achivsPerPage);

        achivPage.SetAchievements(achivsToPage, unlockedAchiv, lockedAchiv);
        return achivPage;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return achievementPagePrefab.gameObject.GetComponent<LayoutElement>().preferredWidth;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        fullPages = achivs.Count / defaultAchivsPerPage;
        int restItems = achivs.Count % defaultAchivsPerPage;
        fullPages += restItems > 0 ? 1 : 0;
        return fullPages;
    }

    public void SetAchievements(List<Achievement> achivements)
    {
        achivs = achivements;
        if(scroller.ScrollRect != null)
            scroller.ReloadData();
    }

    public void MoveNext()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex < fullPages - 1)
        {
            Debug.LogWarning("CurrentDataIndex: " + currentDataIndex);
            currentDataIndex++;            
            scroller.snapping = false;
            //nextButton.interactable = false;
            //prevButton.interactable = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, scroller.snapUseCellSpacing, scroller.snapTweenType, scroller.snapTweenTime, JumpComplete);
        }
    }

    public void MovePrev()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex > 0)
        {
            Debug.LogWarning("CurrentDataIndex: " + currentDataIndex);
            currentDataIndex--;            
            scroller.snapping = false;
            //nextButton.interactable = false;
            //prevButton.interactable = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, scroller.snapUseCellSpacing, scroller.snapTweenType, scroller.snapTweenTime, JumpComplete);
        }
    }

    void JumpComplete()
    {
        Debug.Log("<b>Jump complete</b>");
        scroller.snapping = true;
        scroller.SnapJumpCompleteExt();
        //scroller.Snap();
        //nextButton.interactable = true;
        //prevButton.interactable = true;
    }


    void ScrollerScrolled(EnhancedScroller scroller, Vector2 value, float position)
    {
        Debug.Log("<b>Scroller scrolled handler</b>");
        Debug.Log("StartDataIndex: " + scroller.StartDataIndex);
        Debug.Log("EndDataIndex: " + scroller.EndDataIndex);
        Debug.Log("<b>Current data index after swipe: </b>" + currentDataIndex);        
    }

    void JumpComplete(EnhancedScroller scroller, bool scrolling)
    {
        if(!scrolling)
        {
            int integerPart = Mathf.RoundToInt(scroller.ScrollPosition / scroller.ScrollRectSize);
            float fractPart = (scroller.ScrollPosition / scroller.ScrollRectSize) - integerPart;
            currentDataIndex = fractPart > scroller.snapWatchOffset ? integerPart + 1 : integerPart;

        }
    }



}
