﻿using UnityEngine;
using UnityEngine.UI;

public class AchievementItem : MonoBehaviour
{

    [SerializeField]
    private Image achivementImage;
    [SerializeField]
    private Image isAchieved;
    [SerializeField]
    private Text achivName;
    [SerializeField]
    private Text howToGet;

    public void SetAchievement(Achievement achiv, Sprite unlockedAchiv = null, Sprite lockedAchiv = null)
    {
        achivementImage.enabled = !(achiv == null);
        achivementImage.sprite = (achiv == null) ? null : Resources.Load<Sprite>(PathToResources.Instance.pathToHDAchievements + achiv.shortCode);
        isAchieved.enabled = !(achiv == null);
        bool isUnlocked = (achiv == null) ? false : achiv.earned;
        isAchieved.sprite = (achiv == null) ? null : (isUnlocked ? unlockedAchiv : lockedAchiv);
        achivName.text = (achiv == null) ? string.Empty : achiv.name;
        howToGet.text = (achiv == null) ? string.Empty : achiv.description;
    }

}
