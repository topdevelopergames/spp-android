﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementLast : MonoBehaviour {

    [SerializeField]
    private Image achivImage;

    public void SetAchivImage(Sprite s)
    {   if (!achivImage.enabled)
            SwitchOn();
        achivImage.sprite = s;
    }

    public void SwitchOff()
    {
        achivImage.enabled = false;
    }

    public void SwitchOn()
    {
        achivImage.enabled = true;
    }

}
