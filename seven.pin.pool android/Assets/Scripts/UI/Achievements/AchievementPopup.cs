﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections;

public class AchievementPopup : MonoBehaviour {

    [SerializeField]
    Image achivImage;
    [SerializeField]
    Text achivName;

    Coroutine popupTimer;

    Vector3 startPos;


    RectTransform rect;
    void Awake()
    {
        rect = GetComponent<RectTransform>();
        startPos = transform.localPosition;
    }

    public void RunAwake()
    {
        Awake();
    }

    public void Show(float speed)
    {
        transform.DOLocalMoveX(transform.localPosition.x - rect.rect.width, speed);
    }

    public void Hide(float speed)
    {
        transform.DOLocalMoveX(startPos.x, speed);
    }

    public void SetAchiv(AchievementType type, string name)
    {
        achivImage.sprite = Resources.Load<Sprite>(PathToResources.Instance.pathToHDAchievements + type);
        achivName.text = name;
    }


}
