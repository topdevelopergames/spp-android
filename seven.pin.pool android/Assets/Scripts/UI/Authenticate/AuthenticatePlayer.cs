﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AuthenticatePlayer : MonoBehaviour
{
    [SerializeField]
    private InputField m_loginInput,
        m_passwordInput;
    [SerializeField]
    private Text m_AuthenticateErrorText;

    [SerializeField]
    private UnityEngine.UI.Button m_btnFB;

    void Awake()
    {
        if (m_AuthenticateErrorText != null)
        {
            m_AuthenticateErrorText.text = "";
        }

#if UNITY_WEBGL
        m_btnFB.interactable = false;
#else
         m_btnFB.interactable = true;
#endif
    }
    /// <summary>
    /// Regitered user authorization
    /// </summary>   
    public void AuthorizePlayerBttn()
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("Authorizing Player...");
        string login = m_loginInput.text;
        string password = m_passwordInput.text;
        if (login.Length == 0 || password.Length == 0)
        {
            m_AuthenticateErrorText.text = "Login/Password is empty";
            return;
        }

        GameSparksManager.Instance.Authentication(login, password, () =>
        {
            if (m_AuthenticateErrorText != null)
                m_AuthenticateErrorText.text = "";
            //Player scene
            GameData.Instance.isInGame = false;
            GameData.Instance.Save();
            MenuManager.Instance.LoadLevel(MenuManager.Instance.playerSceneName);
        },
        (error) =>
        {

            if ("UNRECOGNISED".Equals(error))
            {
                m_AuthenticateErrorText.text = TextData.Instance.authUnrecognised;
            }
            else if ("LOCKED".Equals(error))
            {
                m_AuthenticateErrorText.text = TextData.Instance.authLocked;
            }
            else
            {
                m_AuthenticateErrorText.text = TextData.Instance.authUnknown;
            }

        });
    }

    /// <summary>
    /// Guest authentication
    /// </summary>
    public void AuthenticateDeviceBttn()
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("Authenticating Device...");
        GameSparksManager.Instance.AuthenticationGuest(() =>
        {
            if (m_AuthenticateErrorText != null)
                m_AuthenticateErrorText.text = "";
            //guest scene
            GameData.Instance.isInGame = false;
            GameData.Instance.Save();
            MenuManager.Instance.LoadLevel(MenuManager.Instance.guestSceneName);
            //SceneManager.LoadScene(levelName);
        },
        (error) =>
        {

            if ("UNRECOGNISED".Equals(error))
            {
                m_AuthenticateErrorText.text = TextData.Instance.authUnrecognised;
            }
            else if ("LOCKED".Equals(error))
            {
                m_AuthenticateErrorText.text = TextData.Instance.authLocked;
            }
            else
            {
                m_AuthenticateErrorText.text = TextData.Instance.authUnknown;
            }

        });
    }
#if !UNITY_WEBGL
    public void AuthenticateFacebookBttn()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.isInGame = false;
        GameData.Instance.Save();
        Debug.Log("Facebook Authenticating...");
        FBManager.Instance.FacebookLogin();
    }
#endif
}

