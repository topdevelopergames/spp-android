﻿using UnityEngine;
using GameSparks.Core;
using System.Collections;

public class Intro : MonoBehaviour
{

    [SerializeField]
    private GameObject m_loadingSplash;
	[SerializeField]
	private GameObject connectedAnotherDevice;

    private const int CHECK_CONNECTION_TIMEOUT = 10;

    void Awake()
    {
//        StartCoroutine(CheckConnectionCoroutine(CHECK_CONNECTION_TIMEOUT));
//        m_loadingSplash.SetActive(!GS.Available);
//        GS.GameSparksAvailable += HandleGameSparksAvailable;
//		connectedAnotherDevice.SetActive(GameData.Instance.IsSessionTerminatedMessage/* && GameData.Instance.User != null*/);
//		GameData.Instance.IsSessionTerminatedMessage = false;
//        GameData.Instance.isLogoutbyUser = true;
    }

    void OnDestroy()
    {
        GS.GameSparksAvailable -= HandleGameSparksAvailable;
    }

    private void HandleGameSparksAvailable(bool available)
    {
        Debug.Log("HandleGameSparksAvailable: " + available);
        if (GameData.Instance.User != null)
        {
            Debug.Log("Authorizing Player from Saving Data...");
            switch (GameData.Instance.User.Type)
            {
                case ProfileType.Guest:
                    GameSparksManager.Instance.AuthenticationGuest(() =>
                    {
                        GameData.Instance.isInGame = false;
                        GameData.Instance.Save();
                        MenuManager.Instance.LoadLevel(MenuManager.Instance.guestSceneName);
                    });
                    break;
#if !UNITY_WEBGL
                case ProfileType.FB:
                    GameData.Instance.isInGame = false;
                    GameData.Instance.Save();
                    FBManager.Instance.FacebookLogin();
                    break;
#endif
                case ProfileType.Normal:
                    GameSparksManager.Instance.Authentication(GameData.Instance.User.Login, GameData.Instance.Decrypt(GameData.Instance.User.Password), () =>
                    {
                        GameData.Instance.isInGame = false;
                        GameData.Instance.Save();
                        MenuManager.Instance.LoadLevel(MenuManager.Instance.playerSceneName);
                    });
                    break;
                default:
                    break;
            }

        }
        else
        {
            m_loadingSplash.SetActive(!available);
        }
    }

	public void GameSparksReConnect()
	{
		HandleGameSparksAvailable(true);
	}

    IEnumerator CheckConnectionCoroutine(float timeout)
    {
        yield return new WaitForSeconds(timeout);
        if (!GS.Available)
        {
			MenuManager.Instance.BackToMainMenu();
//            GameSparksManager.Instance.AuthenticationOfflineGuest(() =>
//            {
//                MenuManager.Instance.BackToMainMenu();
//            });
        }
    }

    public void PlayAuthorized()
    {
        MenuManager.Instance.PlayAsAuhtorizedPlayer();
    }


    


}
