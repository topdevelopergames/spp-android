﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text.RegularExpressions;

public class RegisterPlayer : MonoBehaviour
{

    [SerializeField]
    private InputField
        m_loginInput,
        m_displayNameInput,
        m_passwordInput;
    [SerializeField]
    private Image
        m_loginImage,
        m_displayNameImage,
        m_passwordImage;
    [SerializeField]
    private Text m_RegisterErrorText;
    [SerializeField]
    private UnityEngine.UI.Button m_BtnSignUp;

    [SerializeField]
    private Sprite
        m_ValidSprite,
        m_InvalidSprite;

    private bool isMailValid = false;
    private bool isDisplayNameValid = false;
    private bool isPasswordValid = false;

    private bool MailValid
    {
        get
        {
            return isMailValid;
        }
        set
        {
            isMailValid = value;
            m_loginImage.gameObject.SetActive(true);
            m_loginImage.sprite = value ? m_ValidSprite : m_InvalidSprite;
        }
    }
    private bool DisplayNameValid
    {
        get
        {
            return isDisplayNameValid;
        }
        set
        {
            isDisplayNameValid = value;
            m_displayNameImage.gameObject.SetActive(true);
            m_displayNameImage.sprite = value ? m_ValidSprite : m_InvalidSprite;
        }
    }
    private bool PasswordValid
    {
        get
        {
            return isPasswordValid;
        }
        set
        {
            isPasswordValid = value;
            m_passwordImage.gameObject.SetActive(true);
            m_passwordImage.sprite = value ? m_ValidSprite : m_InvalidSprite;
        }
    }

    private bool ValidRegisterData
    {
        get
        {
            return isMailValid && isDisplayNameValid && isPasswordValid;
        }
    }

    private void ShowError(string errText)
    {
        m_RegisterErrorText.text = errText;
        m_RegisterErrorText.gameObject.SetActive(true);
    }

    void Start()
    {
        m_RegisterErrorText.gameObject.SetActive(false);
        m_passwordImage.gameObject.SetActive(false);
        m_displayNameImage.gameObject.SetActive(false);
        m_loginImage.gameObject.SetActive(false);
    }

    private bool IsValidEmail(string strIn)
    {
        if (String.IsNullOrEmpty(strIn))
            return false;

        // Return true if strIn is in valid e-mail format.
        return Regex.IsMatch(strIn,
              @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
              RegexOptions.IgnoreCase);
    }

    private bool IsValidDisplayName(string strIn)
    {
        if (String.IsNullOrEmpty(strIn))
            return false;
        return true;
    }

    public void validateEmail()
    {
        string value = m_loginInput.text;
        value.Trim();
        if (!IsValidEmail(value))
        {
            MailValid = false;
            ShowError("Incorrect email!");
        }
        else
        {
            MailValid = true;
            m_RegisterErrorText.gameObject.SetActive(
              m_loginImage.gameObject.activeSelf &&
              m_displayNameImage.gameObject.activeSelf &&
              m_passwordImage.gameObject.activeSelf &&
              !ValidRegisterData);
        }

        m_BtnSignUp.interactable = ValidRegisterData;

    }

    public void validateDisplayName()
    {
        string value = m_displayNameInput.text;
        value.Trim();
        if (!IsValidDisplayName(value))
        {
            DisplayNameValid = false;
            ShowError("Incorrect Display Name!");
        }
        else
        {
            DisplayNameValid = true;
            m_RegisterErrorText.gameObject.SetActive(
              m_loginImage.gameObject.activeSelf &&
              m_displayNameImage.gameObject.activeSelf &&
              m_passwordImage.gameObject.activeSelf &&
              !ValidRegisterData);
        }

        m_BtnSignUp.interactable = ValidRegisterData;
    }

    public void validatePass()
    {
        string value = m_passwordInput.text;
        value.Trim();
        if (!IsValidDisplayName(value))
        {
            PasswordValid = false;
            ShowError("Incorrect Password!");
        }
        else
        {
            PasswordValid = true;
            m_RegisterErrorText.gameObject.SetActive(
              m_loginImage.gameObject.activeSelf &&
              m_displayNameImage.gameObject.activeSelf &&
              m_passwordImage.gameObject.activeSelf &&
              !ValidRegisterData);
        }

        m_BtnSignUp.interactable = ValidRegisterData;
    }

    private void registerSuccess()
    {
        m_RegisterErrorText.gameObject.SetActive(false);
        GameData.Instance.isInGame = false;
        GameData.Instance.Save();
        MenuManager.Instance.LoadLevel(MenuManager.Instance.playerSceneName);
    }

    private void registerError(string error)
    {
        m_RegisterErrorText.text = error;
        m_RegisterErrorText.gameObject.SetActive(true);
    }

    public void RegisterPlayerBttn()
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("Registering Player...");
        if (GameSparksManager.Instance.AuthorizedPlayerData == null)
        {
            GameSparksManager.Instance.Registration(m_loginInput.text, m_passwordInput.text, m_displayNameInput.text, registerSuccess, registerError);
        }
        else
        {
            GameSparksManager.Instance.ChangeGuestToPlayer(m_loginInput.text, m_displayNameInput.text, m_passwordInput.text, registerSuccess, registerError);
        }

    }

    public void CancelRegistrationBttn()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.LoadLevel(MenuManager.Instance.loginSceneName);
    }
}
