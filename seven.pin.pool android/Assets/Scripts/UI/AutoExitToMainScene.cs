﻿using UnityEngine;
using System.Collections;

public class AutoExitToMainScene : MonoBehaviour {

	[SerializeField]
	float waitForExit = 65f;
	// Use this for initialization
	void MoveToMainScene()
	{
		Debug.Log("Auto exit from wait screen!");
		MenuManager.Instance.BackToMainMenu();
	}

	void OnEnable () {
		Invoke("MoveToMainScene", waitForExit);
	}

	void OnDisable()
	{
		CancelInvoke("MoveToMainScene");
	}
}
