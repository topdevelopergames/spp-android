﻿using UnityEngine;
using System.Collections;

public class ButtonsManager : MonoBehaviour {

    [SerializeField]
    private GameObject noConnectionPopup, inviteMessagePopup, noNetworkMessagePopup, notEnoughMoneyPopup, popupError,
                        popupSettings, popupHelp, profileSettingsPopup, friendsListPopup, globalRankPopup, friendsListMessage,
                        cuesListPopup, achievementsListPopup, quitPopup, opponentNotFound, warningExitMessagePopup, chatSinglePlayerPopup,
                        chatMultiplayerPopup, statisticSingle, statisticMultiLess, statisticMultiGreater, popupChangeName, popupChatOrder, popupCredits, popupTutorial;

#if UNITY_ANDROID || UNITY_EDITOR || UNITY_WEBGL
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HandleBackButton();
        }
    }
#endif

    void HandleBackButton()
    {
		Debug.Log ("Unity: Game Scene 0");
        if(statisticSingle != null && statisticSingle.activeInHierarchy && !globalRankPopup.activeInHierarchy)
        {           
			Debug.Log ("Unity: Game Scene 1");
            statisticSingle.SetActive(false);
            MenuManager.Instance.BackToMainMenu();
            return;
        }
        if (statisticMultiLess != null && statisticMultiLess.activeInHierarchy && !globalRankPopup.activeInHierarchy)
        {
			Debug.Log ("Unity: Game Scene 2");
            statisticMultiLess.SetActive(false);
            MenuManager.Instance.BackToMainMenu();
            return;
        }
        if (statisticMultiGreater != null && statisticMultiGreater.activeInHierarchy && !globalRankPopup.activeInHierarchy)
        {
			Debug.Log ("Unity: Game Scene 3");
            statisticMultiGreater.SetActive(false);
            MenuManager.Instance.BackToMainMenu();
            return;
        }
        if (chatMultiplayerPopup != null && chatMultiplayerPopup.activeInHierarchy)
        {
            chatMultiplayerPopup.SetActive(false);
            return;
        }
        if(chatSinglePlayerPopup != null && chatSinglePlayerPopup.activeInHierarchy)
        {
            chatSinglePlayerPopup.SetActive(false);
            return;
        }
        if (warningExitMessagePopup != null && warningExitMessagePopup.activeInHierarchy && (!statisticSingle.activeInHierarchy || !statisticMultiLess.activeInHierarchy || !statisticMultiGreater.activeInHierarchy))
        {
            warningExitMessagePopup.SetActive(false);
            return;
        }
        if(opponentNotFound != null && opponentNotFound.activeInHierarchy)
        {
			Debug.Log ("Unity: Game Scene 4");
            opponentNotFound.SetActive(false);
            MenuManager.Instance.BackToMainMenu();
            return;
        }
        if(quitPopup != null && quitPopup.activeInHierarchy)
        {
            Popup pp = popupCredits.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(quitPopup);
                return;
            }
            quitPopup.SetActive(false);
            return;
        }
        if (noConnectionPopup != null && noConnectionPopup.activeInHierarchy)
        {
            noConnectionPopup.SetActive(false);
            return;
        }
        if(inviteMessagePopup != null && inviteMessagePopup.activeInHierarchy)
        {
            MenuManager.Instance.DeclineInvite();
            inviteMessagePopup.SetActive(false);
            return;
        }
        if(friendsListMessage != null && friendsListMessage.activeInHierarchy)
        {
            Popup pp = friendsListMessage.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseFade(friendsListMessage);
                return;
            }
            friendsListMessage.SetActive(false);
            return;
        }
        if(friendsListPopup != null && friendsListPopup.activeInHierarchy)
        {
            friendsListPopup.GetComponent<FriendsList>().Close(friendsListPopup);
            Popup pp = friendsListPopup.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(friendsListPopup);
                return;
            }
            friendsListPopup.SetActive(false);
            return;
        }
        if(achievementsListPopup != null && achievementsListPopup.activeInHierarchy)
        {
            Popup pp = achievementsListPopup.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseFade(achievementsListPopup);
                return;
            }
            achievementsListPopup.SetActive(false);
            return;
        }
        if(cuesListPopup != null && cuesListPopup.activeInHierarchy)
        {
            Popup pp = cuesListPopup.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseFade(cuesListPopup);
                return;
            }
            cuesListPopup.SetActive(false);
            return;
        }
        if(profileSettingsPopup != null && profileSettingsPopup.activeInHierarchy)
        {
            Popup pp = profileSettingsPopup.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(profileSettingsPopup);
                return;
            }
            profileSettingsPopup.SetActive(false);
            return;
        }
        if(globalRankPopup != null && globalRankPopup.activeInHierarchy)
        {
            Popup pp = globalRankPopup.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScale(globalRankPopup);
                return;
            }
            globalRankPopup.SetActive(false);
            return;
        }
        if(popupHelp != null && popupHelp.activeInHierarchy)
        {
            Popup pp = popupHelp.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(popupHelp);
                return;
            }
            popupHelp.SetActive(false);
            return;
        }
        if (popupTutorial != null && popupTutorial.activeInHierarchy)
        {
            Popup pp = popupTutorial.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(popupTutorial);
                return;
            }
            popupTutorial.SetActive(false);
            return;
        }
        if (popupChangeName != null && popupChangeName.activeInHierarchy)
        {
            Popup pp = popupChangeName.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(popupChangeName);
                return;
            }
            popupChangeName.SetActive(false);
            return;
        }
        if(popupChatOrder != null && popupChatOrder.activeInHierarchy)
        {
            Popup pp = popupChatOrder.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(popupChatOrder);
                return;
            }
            popupChatOrder.SetActive(false);
            return;
        }
        if(popupCredits != null && popupCredits.activeInHierarchy)
        {
            Popup pp = popupCredits.GetComponent<Popup>();
            if (pp) {
                pp.CloseScaleFadePopup(popupCredits);
                return;
            }
            popupCredits.SetActive(false);
            return;
        }
        if(popupSettings != null && popupSettings.activeInHierarchy)
        {
            Popup pp = popupSettings.GetComponent<Popup>();
            if (pp)
            {
                pp.CloseScaleFadePopup(popupSettings);
                return;
            }
            popupSettings.SetActive(false);
            return;
        }
        if(popupError != null && popupError.activeInHierarchy)
        {
			Debug.Log ("Unity: Game Scene 5");
            popupError.SetActive(false);
            MenuManager.Instance.BackToMainMenu();
            return;
        }
        if(notEnoughMoneyPopup != null && notEnoughMoneyPopup.activeInHierarchy)
        {
            notEnoughMoneyPopup.SetActive(false);
            return;
        }
        if(noNetworkMessagePopup != null && noNetworkMessagePopup.activeInHierarchy)
        {
            noNetworkMessagePopup.SetActive(false);
            return;
        }

        switch(MenuManager.Instance.GetCurrentScene())
        {
            case "Game":
			Debug.Log ("Unity: Game Scene 7");
                if (GameData.Instance.singleOrMulti == 0 && GameData.Instance.selectedGameMode == 0)
                {
				Debug.Log ("Unity: Game Scene");
				InGameUIManager.Instance.GoBack ();
//                    MenuManager.Instance.BackToMainMenu();
                    return;
                }
                if (warningExitMessagePopup != null && GameData.Instance.isInGame && GameData.Instance.gameStarted)
                    warningExitMessagePopup.SetActive(true);

                break;

#if UNITY_ANDROID || UNITY_EDITOR
            case "Guest main screen":
            case "Normal main screen":
            case "Login screen":
                if(quitPopup != null)
                    AskForQuit();
                break;
#endif
            case "Pool shop":
            case "Mode choosing screen":
			Debug.Log ("Unity: Game Scene 6");
                MenuManager.Instance.BackToMainMenu();
                break;

            case "GameStart":
                if (GameData.Instance.isInGame && !friendsListPopup.activeInHierarchy)
                    return;
                MenuManager.Instance.LoadLevel(MenuManager.Instance.singleOrMultiSceneName);
                break;

            case "Sign in screen":
                MenuManager.Instance.LoadLevel(MenuManager.Instance.loginSceneName);
                break;

            case "Siign up screen":
                MenuManager.Instance.LoadLevel(MenuManager.Instance.signInSceneName);
                break;
        }
    }
//#if UNITY_ANDROID || UNITY_EDITOR
    public void ConfirmQuit()
    {
        //quitPopup.SetActive(false);
        Application.Quit();
    }

    public void AskForQuit()
    {
        quitPopup.SetActive(true);
    }

//#endif
}
