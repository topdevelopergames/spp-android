﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

    [SerializeField]
    private Camera camera2D;
    [SerializeField]
    private Camera camera3D;

    private Camera currentCamera;

    GameObject go2D;
	GameObject go3D;

    private static CameraManager _instance;

    public static CameraManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        camera2D.enabled = camera3D.enabled = false;
		go2D = camera2D.gameObject;
		go3D = camera3D.gameObject;
	}

    public Camera CurrentCamera
    {
        get
        {
            return currentCamera;
        }
    }

    public void SwitchCamera()
    {
        bool is3D = !GameData.Instance.is3D;
        currentCamera = is3D ? camera3D : camera2D;
        camera3D.enabled = false;
        camera2D.enabled = false;
        currentCamera.enabled = true;
        GameData.Instance.is3D = is3D;
        GameData.Instance.Save();
		go2D.SetActive(camera2D.enabled);
		go3D.SetActive(camera3D.enabled);
	}

    public void SwitchTo2D()
    {
        currentCamera = camera2D;
        camera3D.enabled = false;
        camera2D.enabled = false;
        currentCamera.enabled = true;
        GameData.Instance.is3D = false;
        GameData.Instance.Save();
        go2D.SetActive(camera2D.enabled);
        go3D.SetActive(camera3D.enabled);
    }

    public void SwitchTo3D()
    {
        currentCamera = camera3D;
        camera3D.enabled = false;
        camera2D.enabled = false;
        currentCamera.enabled = true;
        GameData.Instance.is3D = true;
        GameData.Instance.Save();
        go2D.SetActive(camera2D.enabled);
        go3D.SetActive(camera3D.enabled);
    }


}
