﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CueItem : MonoBehaviour {

	[SerializeField]
    protected Image[] enabledForce;
    [SerializeField]
    protected Image[] enabledSpin;
    [SerializeField]
    protected Image[] enabledAim;
    [SerializeField]
    protected Text cueName;
    [SerializeField]
    protected Text description;
    [SerializeField]
    protected UnityEngine.UI.Button useButton;
    [SerializeField]
    protected Image cueImage;
    [SerializeField]
    protected GameObject selected;
    [SerializeField]
    protected GameObject center;
  
    protected bool switchedOff = false;
    protected int id;

    void Awake()
    {
        switchedOff = false;
    }

    public virtual void SetCueItem(CueEntity cue, string userId = null, UICueController controller = null)
    {
        if (cue == null)
        {
            if (!switchedOff)
            {
                SwitchAll(false);
                switchedOff = true;
            }
            return;
        }

        if (switchedOff)
        {
            SwitchAll(true);
            switchedOff = false;
        }

        

        controller.selectCue -= SelectCueHandler;
        controller.selectCue += SelectCueHandler;

        id = cue.cueId;

        cueImage.sprite = CuesStorage.Instance.cueImages[cue.cueId];
        SetupCueSpecs(enabledForce, cue.force);
        SetupCueSpecs(enabledAim, cue.aim);
        SetupCueSpecs(enabledSpin, cue.spin);
        cueName.text = cue.cueName;
        description.text = cue.cueDescription;

        if(!GameSparksManager.Instance.AuthorizedPlayerData.playerId.Equals(userId))
        {
            useButton.gameObject.SetActive(false);
            selected.SetActive(false);
            return;
        }

        selected.SetActive(false);
        useButton.gameObject.SetActive(true);
        useButton.onClick.AddListener(() => { UseButtonCallback(cue, controller); });
        if(GameData.Instance.selectedCue.cueId == cue.cueId)
        {
            useButton.gameObject.SetActive(false);
            selected.SetActive(true);
        }
    }

    public virtual void SwitchAll(bool show)
    {
        center.SetActive(show);
    }
    

    public virtual void SetupCueSpecs(Image[] source, int value)
    {
        for (int i = 0; i < value; i++)
        {
            source[i].enabled = true;
        }
        for (int i = value; i < source.Length; i++)
        {
            source[i].enabled = false;
        }
    }

    void UseButtonCallback(CueEntity cue, UICueController controller)
    {
        if (controller.selectCue == null)
            return;
        controller.selectCue(cue);
    }

    void SelectCueHandler(CueEntity cue)
    {
        if (cue.cueId == id)
        {
            useButton.gameObject.SetActive(false);
            selected.SetActive(true);
            if (GameSparks.Core.GS.Available)
            {
                VirtualGoodType vgType = VirtualGoodType.CUES_STANDARD;
                switch (cue.cueType)
                {
                    case Cue_Shop_Type.standard:
                        vgType = VirtualGoodType.CUES_STANDARD;
                        break;
                    case Cue_Shop_Type.premium:
                        vgType = VirtualGoodType.CUES_PREMIUM;
                        break;
                    case Cue_Shop_Type.country:
                        vgType = VirtualGoodType.CUES_COUNTRY;
                        break;
                }
                GameSparksManager.Instance.SetCurrentCue(new VirtualGood(vgType, cue.cueId).shortCode);
            }           
            GameData.Instance.selectedCue = CueEntity.DeepCopy(cue);
            GameData.Instance.Save();
            if (InGameUIManager.Instance)
                InGameUIManager.Instance.cueController.UpdateCueMaterial();
            return;
        }
        useButton.gameObject.SetActive(true);
        selected.SetActive(false);
    }
}
