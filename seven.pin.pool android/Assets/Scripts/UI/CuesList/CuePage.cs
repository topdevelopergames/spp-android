﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using System.Collections.Generic;

public class CuePage : EnhancedScrollerCellView {

    [SerializeField]
    private GameObject cueItemPrefab;

    private GameObject[] cues;
    private int cuesPerPage = 3;

    public void SetCues(List<CueEntity> cuesToShow, string userId, UICueController controller)
    {
        if (cues == null)
        {
            GenerateCues(cuesToShow, userId, controller);
        }
        else
        {
            UpdateCues(cuesToShow, userId, controller);
        }
    }

    void GenerateCues(List<CueEntity> cuesToGenerate, string userId, UICueController controller)
    {
        cues = new GameObject[cuesPerPage];
        for (int i = 0; i < cuesToGenerate.Count; i++)
        {
            GameObject item = GameObject.Instantiate(cueItemPrefab);
            item.transform.SetParent(transform, false);
            CueItem cueItem = item.GetComponent<CueItem>();
            cueItem.SetCueItem(cuesToGenerate[i], userId, controller);
            cues[i] = item;
        }

        int hideCount = cuesPerPage - cuesToGenerate.Count;

        if (hideCount > 0)
        {
            for (int i = cuesPerPage - hideCount; i < cuesPerPage; i++)
            {
                GameObject item = Instantiate(cueItemPrefab);
                item.transform.SetParent(transform, false);
                CueItem cueItem = item.GetComponent<CueItem>();
                cueItem.SetCueItem(null);
                cues[i] = item;
            }
        }

    }

    void UpdateCues(List<CueEntity> cuesToUpdate, string userId, UICueController controller)
    {
        for (int i = 0; i < cuesToUpdate.Count; i++)
        {
            CueItem citem = cues[i].GetComponent<CueItem>();
            citem.SetCueItem(cuesToUpdate[i], userId, controller);
        }

        int countToShow = cuesPerPage - cuesToUpdate.Count;

        if (countToShow > 0)
        {
            for (int i = cues.Length - countToShow; i < cues.Length; i++)
            {
                cues[i].GetComponent<CueItem>().SetCueItem(null);
            }           
        }
    }
	

}
