﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UICueController : MonoBehaviour, IEnhancedScrollerDelegate, IPointerDownHandler, IPointerUpHandler
{

    [SerializeField]
    private CuePage cuePagePrefab;
    [SerializeField]
    private EnhancedScroller scroller;
    [SerializeField]
    private UnityEngine.UI.Button prevButton, nextButton;

    public delegate void SelectCue(CueEntity cue);   
    public SelectCue selectCue;

    private string userId;
    private int cuesPerPage = 3;
    private int currentDataIndex = 0;
    private int fullPages;
    private List<GameObject> selectedObjectsList, useButtonsList;

    private List<CueEntity> cues, displayedCues;

    void Awake()
    {
        displayedCues = new List<CueEntity>();
        selectedObjectsList = new List<GameObject>();
        useButtonsList = new List<GameObject>();
        //userId = string.Empty;
    }

    void OnEnable()
    {
        fullPages = 0;
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;
        useButtonsList.Clear();
        selectedObjectsList.Clear();
        displayedCues.Clear();
        currentDataIndex = 0;
        scroller.scrollerScrollingChanged -= JumpComplete;
        scroller.scrollerScrollingChanged += JumpComplete;
        //selectCue = null;
    }

    void OnDisable()
    {
        selectedObjectsList.Clear();
        useButtonsList.Clear();
        displayedCues.Clear();
        scroller.scrollerScrollingChanged -= JumpComplete;
        selectCue = null;
    }

    public void OnPointerDown(PointerEventData data)
    {
        scroller.snapping = false;
    }

    public void OnPointerUp(PointerEventData data)
    {
        scroller.snapping = true;
        scroller.SnapJumpCompleteExt();
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CuePage cuePage = scroller.GetCellView(cuePagePrefab) as CuePage;
        int defaultCuesPerPage = cuesPerPage;
        int leftCues = cues.Count - (dataIndex * cuesPerPage);
        if (leftCues > 0 && leftCues < cuesPerPage)
        {
            defaultCuesPerPage = leftCues;
        }

        List<CueEntity> cuesToPage = cues.GetRange(dataIndex * cuesPerPage, defaultCuesPerPage);

        cuePage.SetCues(cuesToPage, userId, this);
        return cuePage;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        LayoutElement cueLayout = cuePagePrefab.GetComponent<LayoutElement>();
        if (cueLayout != null)
            return cueLayout.preferredWidth;
        return 1210f;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        fullPages = cues.Count / cuesPerPage;
        int restItems = cues.Count % cuesPerPage;
        fullPages += restItems > 0 ? 1 : 0;
        return fullPages;
    }

    public void SetCues(List<CueEntity> cuesList, string userId)
    {
        cues = cuesList;
        this.userId = userId;
        if(scroller.ScrollRect != null)
            scroller.ReloadData();
    }

    public void MoveNext()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex < fullPages - 1)
        {
            currentDataIndex++;
            scroller.snapping = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, false, EnhancedScroller.TweenType.spring, 1, JumpComplete);
        }
    }

    public void MovePrev()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex > 0)
        {
            scroller.snapping = false;
            currentDataIndex--;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, false, EnhancedScroller.TweenType.spring, 1, JumpComplete);
        }
    }

    void JumpComplete()
    {
        scroller.snapping = true;
        scroller.SnapJumpCompleteExt();
    }

    void JumpComplete(EnhancedScroller scroller, bool scrolling)
    {
        if (!scrolling)
        {
            Debug.Log("Jump complete handler");
            int integerPart = Mathf.RoundToInt(scroller.ScrollPosition / scroller.ScrollRectSize);
            float fractPart = (scroller.ScrollPosition / scroller.ScrollRectSize) - integerPart;
            currentDataIndex = fractPart > scroller.snapWatchOffset ? integerPart + 1 : integerPart;
        }
    }


}
