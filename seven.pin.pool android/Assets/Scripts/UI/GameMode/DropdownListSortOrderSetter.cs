﻿using UnityEngine;
using System.Collections;

public class DropdownListSortOrderSetter : MonoBehaviour {

    [SerializeField]
    private int _SortingOrder = 0;

    private Canvas canvas;

    void OnEnable()
    {
        if(canvas == null)
        {
            canvas = GetComponent<Canvas>();
        }
        if (MenuUIManager.Instance.invitePopup != null)
        {
            MenuUIManager.Instance.invitePopup.invite -= SetCanvasOrder;
            MenuUIManager.Instance.invitePopup.invite += SetCanvasOrder;
        }
    }

    void SetCanvasOrder(int order)
    {
        if(canvas != null)
            canvas.sortingOrder = order;
    }
	
}
