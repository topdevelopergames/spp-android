﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class ScrollModeSelector : MonoBehaviour {

    [SerializeField]
    private ScrollRect gameModeScroll;

    private GameData gameData;
    private HorizontalScrollSnap snap;
    int activeModes = 0;
      
    void OnEnable()
    {
        activeModes = 0;
        gameData = GameData.Instance;
        foreach(Transform t in gameModeScroll.content.transform)
        {
            if (t.gameObject.activeInHierarchy)
                activeModes++;
        }
    }

    public void DeterminePickedMode()
    {
        RectTransform content = gameModeScroll.content;
        int selectedMode = Mathf.RoundToInt(gameModeScroll.horizontalNormalizedPosition * (activeModes - 1));
        gameData.selectedGameMode = selectedMode;
        gameData.Save();
        GameModeManager.Instance.SetFee(gameData.entryFee);
    }

    public void PickPrevMode()
    {
        SoundManager.Instance.PlayButtonPress();
        if (gameData.selectedGameMode > 0)
        {
            gameData.selectedGameMode--;
            gameData.Save();
            GameModeManager.Instance.SetFee(gameData.entryFee);
            return;
        }

        if (gameData.selectedGameMode == 0)
        {
            gameData.selectedGameMode = 1;
            gameData.Save();
            GameModeManager.Instance.SetFee(gameData.entryFee);
        }
        
    }

    public void PickNextMode()
    {
        SoundManager.Instance.PlayButtonPress();
        if (gameData.selectedGameMode < 1)
        {
            gameData.selectedGameMode++;
            gameData.Save();
            GameModeManager.Instance.SetFee(gameData.entryFee);
            return;
        }

        if (gameData.selectedGameMode == 1)
        {
            gameData.selectedGameMode = 0;
            gameData.Save();
            GameModeManager.Instance.SetFee(gameData.entryFee);
        }
        
    }
}
