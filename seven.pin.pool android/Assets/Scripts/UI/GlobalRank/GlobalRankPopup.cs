﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using EnhancedUI.EnhancedScroller;
using System.Collections.Generic;

public class GlobalRankPopup : MonoBehaviour, IEnhancedScrollerDelegate
{

    [SerializeField]
    private ScrollRect tableScroll;
    [SerializeField]
    private EnhancedScroller scroller;
    [SerializeField]
    private float scrollSpeed;
    public EnhancedScrollerCellView tableRow;

    private List<GlobalLeaderboardEntry> topPlayers;
    private Coroutine scrollerr;
    private bool buttonHold;
    private float cellHeight;

    [SerializeField]
    private TableRow playerPos;
    [SerializeField]
    private ProfileStatisticsPopup profilePopup;

    private float oneRowMove;

    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        //if (GameData.Instance.User.Type != ProfileType.Guest)
        //{
            GameSparksManager.Instance.UpdateLeaderboard();
        //}
    

        GameSparksManager.Instance.GetLeaderboardData(LeaderboardType.GLOBAL_RANK_SCORE, (leaderboard) =>
        {
            topPlayers = leaderboard;
            oneRowMove = 3.65f / topPlayers.Count;
            scroller.Delegate = this;
            RectTransform cellRect = tableRow.GetComponent<RectTransform>();
            if (cellRect != null)
            {
                cellHeight = cellRect.rect.height;
            }
        });

        if (GameSparksManager.Instance.AuthorizedPlayerData.displayName.Equals("Guest"))
        {
            playerPos.gameObject.SetActive(false);
        }
        else
        {
            GameSparksManager.Instance.GetLeaderboardEntry(LeaderboardType.GLOBAL_RANK_SCORE, (player) =>
            {
                if (player != null && player.rank > 1000)
                {
                    playerPos.gameObject.SetActive(true);
                    playerPos.SetupRow(player.userName, player.score.ToString(), player.level.ToString(), player.games.ToString(), player.rank, player.userId);
                }
                else
                    playerPos.gameObject.SetActive(false);
            });
        }

    }



    public void MoveOneRowDown()
    {
        SoundManager.Instance.PlayButtonPress();
        if (tableScroll.verticalNormalizedPosition >= 1f)
            return;
        tableScroll.verticalNormalizedPosition += oneRowMove;
    }

    public void MoveOneRowUp()
    {
        SoundManager.Instance.PlayButtonPress();
        if (tableScroll.verticalNormalizedPosition <= 0)
            return;
        tableScroll.verticalNormalizedPosition -= oneRowMove;
    }

    public void ButtonUp()
    {
        SoundManager.Instance.PlayButtonPress();
        buttonHold = false;
        if (scrollerr != null)
            StopCoroutine(scrollerr);
    }

    public void ButtonDown(int sideSign)
    {
        SoundManager.Instance.PlayButtonPress();
        buttonHold = true;
        scrollerr = StartCoroutine(ScrollTable(sideSign * scrollSpeed));
    }


    public void SetTopPlayers(List<GlobalLeaderboardEntry> topPlayers, Action callBack)
    {
        if (topPlayers != null)
        {
            this.topPlayers = topPlayers;
            callBack();
        }
    }

    IEnumerator ScrollTable(float speed)
    {
        while (buttonHold)
        {
            if ((tableScroll.verticalNormalizedPosition < 0 && speed < 0) || (tableScroll.verticalNormalizedPosition > 1 && speed > 0))
                break;
            tableScroll.verticalNormalizedPosition += speed;
            yield return new WaitForEndOfFrame();
        }
    }


    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        TableRow row = scroller.GetCellView(tableRow) as TableRow;
        row.SetupRow(topPlayers[dataIndex].userName, topPlayers[dataIndex].score.ToString(), topPlayers[dataIndex].level.ToString(),
                     topPlayers[dataIndex].games.ToString(), topPlayers[dataIndex].rank, topPlayers[dataIndex].userId, ShowProfile);

        return row;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return cellHeight;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return topPlayers == null ? 0 : topPlayers.Count;
    }

    public void ShowProfile(string userId)
    {
        profilePopup.SetProfileToView(userId, true, () =>
        {
            profilePopup.gameObject.SetActive(true);
        });
    }



}
