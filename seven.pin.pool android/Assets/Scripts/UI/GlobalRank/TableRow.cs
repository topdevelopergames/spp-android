﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class TableRow : EnhancedScrollerCellView
{

    [SerializeField]
    private Text username, score, level, gamesFinished;
    [SerializeField]
    private GameObject highlighted;
    
    public void SetupRow(string name, string score, string level, string gamesFinished, long position, string userId, System.Action<string> callback = null)
    {
        System.Text.StringBuilder str = new System.Text.StringBuilder();
        str.Append(position);
        str.Append(".");
        str.Append(name);

        username.text = str.ToString();
        this.score.text = score;
        this.level.text = level;
        this.gamesFinished.text = gamesFinished;
        cellIdentifier = userId;
        if (callback != null)
        {
            username.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => {
                SoundManager.Instance.PlayButtonPress();
                callback(cellIdentifier);
            });
        }

        if (userId.Equals(GameSparksManager.Instance.AuthorizedPlayerData.playerId))
        {
            highlighted.SetActive(true);
        }
    }
   

	
}
