﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AngleController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    [SerializeField]
    private float radius = 0.75f, minRadius;
    [SerializeField]
    private CueController cueController;
    [SerializeField]
    private float minAngle, maxAngle;
    [System.NonSerialized]
    public float settingAngle;
    [System.NonSerialized]
    public float currentgAngle;
    [SerializeField]
    private RectTransform pointToRotate;
    [SerializeField]
    private RectTransform startPos;
    [SerializeField]
    private Image touchImage;

    float angleCorrector = 0;

    public float displacementZ;
    public float displacementX;

    private Vector3 startPosition;

    private bool pointerDown;
    

    Vector3 touchPosition;

    private Coroutine rotationCalc;

	public float MaxAngle { get { return maxAngle; } }

    void Start()
    {
        startPosition = startPos.localPosition;
        pointerDown = false;
        if (touchImage != null)
        {
            touchImage.enabled = false;
        }
        Reset();
        switch (GameData.Instance.forceSliderPosition)
        {
            ///place on right
            case 0:
            case 2:
            case 3:
                angleCorrector = 0;
                break;
            ///place on left
            case 1:
                angleCorrector = MenuControllerGenerator.controller.isTouchScreen ? - 180 : 0;
                break;
        }

        

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDown = true;
        MenuControllerGenerator.controller.canRotateCue = false;
        if (touchImage != null)
        {
            touchImage.enabled = true;
        }
        rotationCalc = StartCoroutine(RotationDataDetect());

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerDown = false;
        MenuControllerGenerator.controller.canRotateCue = true;
        touchPosition = startPosition;
        displacementX = 0;
        displacementZ = 0;
        if (touchImage != null)
        {
            touchImage.enabled = false;
        }
        StopCoroutine(rotationCalc);
    }

    IEnumerator RotationDataDetect()
    {
        var ray = GetComponentInParent<GraphicRaycaster>();

        Vector3 mousePosition = Vector3.zero;
        Vector2 localMousePosition = Vector2.zero;
        while (pointerDown)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), Input.mousePosition, ray.eventCamera, out localMousePosition);
            mousePosition.x = localMousePosition.x;
            mousePosition.y = localMousePosition.y;
            mousePosition.z = startPosition.z;
            float distance = Vector3.Distance(mousePosition, startPosition);
            if (distance < radius && distance > minRadius)
            {
                touchPosition = mousePosition;
            }
            else
            {
                if(distance < minRadius)
                {
                    Vector3 displacement = mousePosition - startPosition;
                    touchPosition = minRadius * displacement.normalized + startPosition;
                }
                else
                {
                    if(distance > radius)
                    {
                        Vector3 displacement = mousePosition - startPosition;
                        touchPosition = radius * displacement.normalized + startPosition;
                    }

                }
            }

            displacementX = Mathf.Clamp((1.0f / radius) * touchPosition.y, -1.0f, 1.0f);
            displacementZ = Mathf.Clamp((1.0f / radius) * touchPosition.x, -1.0f, 1.0f);

            ///SetAngleHere
            ///
            SetAngle();
            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary>
    /// Switching off object, on which this script attached
    /// </summary>
    public void HideControl()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Switching on object, on which this script attached
    /// </summary>
    public void ShowControl()
    {
        gameObject.SetActive(true);
    }

    void SetAngle()
    {
        if (ServerController.serverController && !ServerController.serverController.isMyQueue)
            return;
        MenuControllerGenerator.controller.canControlCue = false;

        float testAngle = Vector2.Angle(startPos.localPosition.normalized, new Vector2(displacementZ, displacementX));

        float angle = testAngle < 90 ? minAngle : testAngle - 90;
        angle = angle >= maxAngle ? maxAngle : angle;
        bool setMaxAngle = (GameData.Instance.forceSliderPosition == 1) ? displacementZ < 0 : displacementZ > 0;
        angle = setMaxAngle ? maxAngle : angle;
        pointToRotate.eulerAngles = new Vector3(pointToRotate.eulerAngles.x, pointToRotate.eulerAngles.y, angleCorrector > -1 ? -angle : angleCorrector + angle);
        cueController.CueVerticalAngle = angle;
        settingAngle = angle;
    }

    public void Reset()
    {
        pointToRotate.eulerAngles = new Vector3(0, 0, angleCorrector > -1 ? minAngle : angleCorrector + minAngle);
        cueController.CueVerticalAngle = minAngle;
        settingAngle = minAngle;
        currentgAngle = minAngle;
    }

    public void SetAngle(float angle)
    {
        pointToRotate.eulerAngles = new Vector3(pointToRotate.eulerAngles.x, pointToRotate.eulerAngles.y, angleCorrector > -1 ? - angle : angleCorrector + angle);
        currentgAngle = angle;
    }

}
