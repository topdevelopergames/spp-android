﻿using UnityEngine;
using System.Collections;

public class BallPivotMover : MonoBehaviour {

	public const float MAX_RADIUS = 50f;

    public CueBallSlider circleSlider;
    public float radius = 20;
    private Vector3 startPosition = Vector3.zero;

    float partRadius, newRadius;

    void Start()
    {
        partRadius = radius / 2;
        newRadius = radius - partRadius;
        radius = newRadius + partRadius * GameData.Instance.selectedCue.spin / CueEntity.MAX_CUE_FEATURE_LEVEL;
        circleSlider.circleSlider -= SlideBallPivot;
        circleSlider.circleSlider += SlideBallPivot;
        startPosition = transform.localPosition;
    }

    void SlideBallPivot(CircleSlider circleSlider)
    {
        if (ServerController.serverController && !ServerController.serverController.isMyQueue)
            return;
        radius = newRadius + partRadius * GameData.Instance.selectedCue.spin / CueEntity.MAX_CUE_FEATURE_LEVEL;
        MenuControllerGenerator.controller.canControlCue = false;
        transform.localPosition = new Vector3(circleSlider.displacementZ * radius, circleSlider.displacementX * radius, 0.0f);
        float distance = Vector3.Distance(transform.localPosition, startPosition);
        if (distance > circleSlider.radius)
        {
            transform.localPosition -= (distance - circleSlider.radius) * (transform.localPosition - startPosition).normalized;
        }
    }
    public void SetPosition(Vector3 localPosition)
    {
        transform.localPosition = circleSlider.radius * localPosition;
    }    

    public void Reset()
    {
        transform.localPosition = startPosition;
    }

    public void ShowSlider(GameObject popup)
    {
        if (ServerController.serverController && !ServerController.serverController.isMyQueue)
            return;
		GameData.Instance.IsChangingPointShot = true;
        circleSlider.showAnim.ShowPopup(popup);
    }


}
