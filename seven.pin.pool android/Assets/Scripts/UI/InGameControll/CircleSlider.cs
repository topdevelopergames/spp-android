﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class CircleSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
   
    public float radius = 0.75f;
    [SerializeField]
    protected Transform touch;
    

    public float displacementZ;
    public float displacementX;

    protected Vector3 startPosition;

    protected bool pointerDown;
    protected Image touchImage;

    protected Coroutine rotationCalc;

    public delegate void OnCircleSlider(CircleSlider circleSlider);
    public virtual event OnCircleSlider circleSlider;

    void Start()
    {
        startPosition = touch.localPosition;
        pointerDown = false;
        touchImage = touch.GetComponent<Image>();
        if(touchImage != null)
        {
            touchImage.enabled = false;
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        InGameUIManager.Instance.BlockForceSlider();
        pointerDown = true;
        MenuControllerGenerator.controller.canRotateCue = false;
        if (touchImage != null)
        {
            touchImage.enabled = true;
        }
        rotationCalc = StartCoroutine(RotationDataDetect());

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerDown = false;
        MenuControllerGenerator.controller.canRotateCue = true;

        displacementX = 0;
        displacementZ = 0;
        touch.localPosition = startPosition;
        if(touchImage != null)
        {
            touchImage.enabled = false;
        }
        if(rotationCalc != null)
            StopCoroutine(rotationCalc);

        InGameUIManager.Instance.UnblockForceSlider();
		GameData.Instance.IsChangingPointShot = false;
    }

    protected virtual IEnumerator RotationDataDetect()
    {
        var ray = GetComponentInParent<GraphicRaycaster>();

        Vector3 mousePosition = Vector3.zero;
        Vector2 localMousePosition = Vector2.zero;
        while(pointerDown)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), Input.mousePosition, ray.eventCamera, out localMousePosition);
            mousePosition.x = localMousePosition.x;
            mousePosition.y = localMousePosition.y;
            mousePosition.z = startPosition.z;            

            if (Vector3.Distance(mousePosition, startPosition) < radius)
            {
                touch.localPosition = mousePosition;
            }
            else
            {
                Vector3 displacement = mousePosition - startPosition;

                touch.localPosition = radius * displacement.normalized + startPosition;
            }

            displacementX = Mathf.Clamp((1.0f / radius) * touch.localPosition.y, -1.0f, 1.0f);
            displacementZ = Mathf.Clamp((1.0f / radius) *touch.localPosition.x, -1.0f, 1.0f);

            if (circleSlider != null)
                circleSlider(this);
            yield return new WaitForEndOfFrame();
        }
    } 
        
    /// <summary>
    /// Switching off object, on which this script attached
    /// </summary>
    public void HideControl()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Switching on object, on which this script attached
    /// </summary>
    public void ShowControl()
    {
        gameObject.SetActive(true);
    }

}
