﻿using UnityEngine;
using System.Collections;

public class CueAngleControllRotator : MonoBehaviour {
	[SerializeField]
    private CircleSlider circleSlider;
    [SerializeField]
    private float minAngle, maxAngle;
    [SerializeField]
    private CueController cueController;
	[System.NonSerialized]
	public float settingAngle;
	[System.NonSerialized]
	public float currentgAngle;

    float angleCorrector = 0;

    void Start()
    {
        circleSlider.circleSlider += SetAngle;
        Reset();
        switch(GameData.Instance.forceSliderPosition)
        {
            ///place on right
            case 0:
            case 2:
            case 3:
                angleCorrector = 180;
                break;
            ///place on left
            case 1:
                angleCorrector = 0;
                break;
        }
    }

    public void SetAngle(CircleSlider slider)
    {
        if (ServerController.serverController && !ServerController.serverController.isMyQueue)
            return;
        MenuControllerGenerator.controller.canControlCue = false;
        float angle = Mathf.Clamp(slider.displacementX * (maxAngle - minAngle) + minAngle, minAngle, maxAngle);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angleCorrector > 1 ? angleCorrector  - angle : angle);
        cueController.CueVerticalAngle = angle;
		settingAngle = angle;
    }

    public void Reset()
    {
        transform.eulerAngles = new Vector3(0, 0, angleCorrector > 1 ? angleCorrector - minAngle : minAngle);
        cueController.CueVerticalAngle = minAngle;
		settingAngle = minAngle;
		currentgAngle = minAngle;
    }

    public void SetAngle(float angle)
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angleCorrector > 1 ? angleCorrector - angle : angle);
		currentgAngle = angle;
    }

}
