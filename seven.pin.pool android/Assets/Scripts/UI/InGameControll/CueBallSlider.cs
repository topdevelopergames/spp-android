﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CueBallSlider : CircleSlider, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    protected Image verticalLine, horizontalLine;
    [SerializeField]
    protected Image defaultSizeCircle;
    [SerializeField]
    protected float xScale, yScale;
    [SerializeField]
    protected RectTransform angleControllerIndicator;
    public ShowAnimation showAnim;


    public override event OnCircleSlider circleSlider;

    float partRadius, newRadius;

    void Start()
    {
        partRadius = radius / 2;
        newRadius = radius - partRadius;
        radius = newRadius + partRadius * GameData.Instance.selectedCue.spin / CueEntity.MAX_CUE_FEATURE_LEVEL;
        startPosition = touch.localPosition;
        pointerDown = false;
        touchImage = touch.GetComponent<Image>();
        if (touchImage != null)
        {
            touchImage.enabled = false;
        }
        if (verticalLine != null)
        {
            verticalLine.enabled = false;
        }
        if (horizontalLine != null)
        {
            horizontalLine.enabled = false;
        }
    }

	public void MyPointerDown()
	{
		OnPointerUp(new PointerEventData(EventSystem.current));
	}

    public new void OnPointerDown(PointerEventData eventData)
    {
        //InGameUIManager.Instance.BlockCue();
        radius = newRadius + partRadius * GameData.Instance.selectedCue.spin / CueEntity.MAX_CUE_FEATURE_LEVEL;
        InGameUIManager.Instance.BlockForceSlider();
        pointerDown = true;
        MenuControllerGenerator.controller.canRotateCue = false;
        if (touchImage != null)
        {
            touchImage.enabled = true;
        }
        if (verticalLine != null)
        {
            verticalLine.enabled = true;
        }
        if (horizontalLine != null)
        {
            horizontalLine.enabled = true;
        }
        //if(defaultSizeCircle != null)
        //{
        //    defaultSizeCircle.rectTransform.localScale = new Vector3(xScale, yScale, 1);
        //    radius *= xScale;
        //}
        //if(angleControllerIndicator != null)
        //{
        //    angleControllerIndicator.localPosition = new Vector3(angleControllerIndicator.localPosition.x * xScale, angleControllerIndicator.localPosition.y, angleControllerIndicator.localPosition.z);
        //}
        rotationCalc = StartCoroutine(RotationDataDetect());

    }

    public new void OnPointerUp(PointerEventData eventData)
    {
        pointerDown = false;
        MenuControllerGenerator.controller.canRotateCue = true;
        //if (defaultSizeCircle != null)
        //{
        //    defaultSizeCircle.rectTransform.localScale = new Vector3(1, 1, 1);
        //    radius /= xScale;
        //    circleSlider(this);
        //}
        displacementX = 0;
        displacementZ = 0;
        touch.localPosition = startPosition;
        if (touchImage != null)
        {
            touchImage.enabled = false;
        }

        if (verticalLine != null)
        {
            verticalLine.enabled = false;
        }
        if (horizontalLine != null)
        {
            horizontalLine.enabled = false;
        }
        //if (angleControllerIndicator != null)
        //{
        //    angleControllerIndicator.localPosition = new Vector3(angleControllerIndicator.localPosition.x / xScale, angleControllerIndicator.localPosition.y, angleControllerIndicator.localPosition.z);
        //}
        if (rotationCalc != null)
            StopCoroutine(rotationCalc);
        InGameUIManager.Instance.UnblockForceSlider();
        if (showAnim)
            showAnim.HidePopup(gameObject.transform.parent.gameObject);
        GameData.Instance.IsChangingPointShot = false;
    }

    protected override IEnumerator RotationDataDetect()
    {
        var ray = GetComponentInParent<GraphicRaycaster>();

        Vector3 mousePosition = Vector3.zero;
        Vector2 localMousePosition = Vector2.zero;
        while (pointerDown)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), Input.mousePosition, ray.eventCamera, out localMousePosition);
            mousePosition.x = localMousePosition.x;
            mousePosition.y = localMousePosition.y;
            mousePosition.z = startPosition.z;

            if (Vector3.Distance(mousePosition, startPosition) < radius)
            {
                touch.localPosition = mousePosition;
            }
            else
            {
                Vector3 displacement = mousePosition - startPosition;

                touch.localPosition = radius * displacement.normalized + startPosition;
            }

            displacementX = Mathf.Clamp((1.0f / radius) * touch.localPosition.y, -1.0f, 1.0f);
            displacementZ = Mathf.Clamp((1.0f / radius) * touch.localPosition.x, -1.0f, 1.0f);
            //displacementX = Mathf.Clamp((float)(GameData.Instance.selectedCue.spin / CueEntity.MAX_CUE_FEATURE_LEVEL), 0.5f, 1f);
            //displacementZ = Mathf.Clamp((float)(GameData.Instance.selectedCue.spin / CueEntity.MAX_CUE_FEATURE_LEVEL), 0.5f, 1f);

            if (circleSlider != null)
                circleSlider(this);
            yield return new WaitForEndOfFrame();
        }
    }


}
