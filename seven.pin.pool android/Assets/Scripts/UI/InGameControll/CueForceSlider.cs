﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class CueForceSlider : MonoBehaviour/*, IPointerUpHandler */{

    [SerializeField]
    private float startValue = 1;
    [SerializeField]
    private UnityEngine.UI.Slider slider;
    [SerializeField]
    private CueController cueController;
    [HideInInspector]
    public float cueForceValue = 0.0f;
    [SerializeField]
    private UnityEngine.UI.Image fader;
	bool enabledSlider;
    Color color;

    public bool IsEnabledSlider { get { return enabledSlider; } }
    
    void Awake()
    {
        if (MenuControllerGenerator.controller)
        {
            if (!MenuControllerGenerator.controller.isTouchScreen)
            {
                transform.gameObject.SetActive(false);
                return;
            }
        }
    }

    void Start()
    {
        Color color = fader.color;
        SetForce();
        cueController.inTouchForceSlider = false;
        cueController.cueForceisActive = false;
    }

    public void SetForce()
    {
        if (!cueController.allIsSleeping)
            return;
        if (ServerController.serverController && !ServerController.serverController.isMyQueue)
            return;
        
        MenuControllerGenerator.controller.canRotateCue = false;
        cueController.inTouchForceSlider = true;
        cueController.cueForceisActive = true;
        cueForceValue = slider.value;
        color.a = 1 - slider.value;
        fader.color = color;
        cueController.cueDisplacement = cueController.cueMaxDisplacement * cueForceValue;
        enabledSlider = slider.interactable;
    }

    public void Reset()
    {
        StartCoroutine(WaitAndRessetValue());
    }

    IEnumerator WaitAndRessetValue()
    {
        yield return new WaitForEndOfFrame();
        slider.value = startValue;
        color.a = 1;
        fader.color = color;
        if (cueController.cueForceisActive)
        {
            MenuControllerGenerator.controller.canRotateCue = true;
            cueController.inTouchForceSlider = false;
            cueController.cueForceisActive = false;
        }
    }

    public void DisableForceSlider()
    {
        slider.interactable = false;
		enabledSlider = slider.interactable;
        Reset();
    }

    public void EnableSForceSlider()
    {
        if (!ServerController.serverController || (ServerController.serverController && ServerController.serverController.isMyQueue))
			slider.interactable = true;
		else
			slider.interactable = false;
		enabledSlider = slider.interactable;
	}

}
