﻿using UnityEngine;
using System.Collections;

public class InGameControllSwaper : MonoBehaviour {

    [SerializeField]
    private RectTransform[] cueBallsPositions;
    [SerializeField]
    private RectTransform ballControl;
    [SerializeField]
    private Transform enlargedBallControl;
    

    
    void Awake()
    {
        if(MenuControllerGenerator.controller.isTouchScreen)
            SetupCueControl();
    }
    

    void SetupCueControl()
    {
        switch(GameData.Instance.forceSliderPosition)
        {
            case 0:
                ballControl.SetParent(cueBallsPositions[0], false);
                break;

            case 1:
                ballControl.SetParent(cueBallsPositions[1], false);
                enlargedBallControl.localPosition = new Vector3(-enlargedBallControl.localPosition.x, enlargedBallControl.localPosition.y, enlargedBallControl.localPosition.z);
                break;

            case 2:
                ballControl.SetParent(cueBallsPositions[0], false);
                break;

            case 3:
                ballControl.SetParent(cueBallsPositions[0], false);
                break;
            
        }
    }
	

}
