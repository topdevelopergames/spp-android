﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class ShowAnimation : MonoBehaviour {

    [SerializeField]
    private Transform endPos, startPos;
    [SerializeField]
    private Vector3 fromScale;
    [SerializeField]
    private float duration;
    [SerializeField]
    private bool snaping;
    [SerializeField]
    private float fadeEndValue;


    public void ShowPopup(GameObject popup)
    {
        Transform t = popup.transform.childCount > 0 ? popup.transform.GetChild(0) : popup.GetComponent<Transform>();
        popup.SetActive(true);
        UnityEngine.UI.Button button = popup.GetComponent<UnityEngine.UI.Button>();
        if (button != null)
            button.interactable = false;
        CircleSlider cs = popup.GetComponentInChildren<CircleSlider>();
        if (cs != null)
            cs.enabled = false;
        t.DOLocalMove(endPos.localPosition, duration, snaping);
        t.DOScale(Vector3.one, duration).OnComplete( () => {
            if (button != null)
                button.interactable = true;
            if (cs != null)
                cs.enabled = true;
        });
        Image img = popup.GetComponent<Image>();
        if (img != null)
            img.DOFade(fadeEndValue, duration);
    }

    public void HidePopup(GameObject popup)
    {
        UnityEngine.UI.Button button = popup.GetComponent<UnityEngine.UI.Button>();
        if (button != null)
            button.interactable = false;
        CircleSlider cs = popup.GetComponentInChildren<CircleSlider>();
        if (cs != null)
            cs.enabled = false;
        Transform t = popup.transform.childCount > 0 ? popup.transform.GetChild(0) : popup.GetComponent<Transform>();        
        Vector3 pos = t.InverseTransformPoint(startPos.position);        
        t.DOLocalMove(pos, duration, snaping);
        Image img = popup.GetComponent<Image>();
        if (img != null)
            img.DOFade(0, duration);
        t.DOScale(fromScale, duration).OnComplete(() => {
            popup.SetActive(false);
            if (cs != null)
                cs.enabled = true;
        });
        
    }

	
}
