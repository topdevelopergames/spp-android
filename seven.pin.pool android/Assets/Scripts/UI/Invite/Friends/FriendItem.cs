﻿using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;


public class FriendItem : EnhancedScrollerCellView {

    [SerializeField]
    private Image avatar;
    [SerializeField]
    private Text username;
    [SerializeField]
    private Text status;
    [SerializeField]
    private UnityEngine.UI.Button addToGame;

    private bool selectedState = false;
    private Image buttonImage;


    void Awake()
    {
        selectedState = false;
        buttonImage = addToGame.gameObject.GetComponent<Image>();
    }

    public void SetFriend(PlayerData player, long fee, Sprite buttonState, bool isExist, System.Action<GameObject, bool, PlayerData> callback = null)
    {
        avatar.sprite = player.avatar;
        username.text = player.displayName;
        bool isEnoughMoney = player.coins >= fee;
        string playerStatus = player.online ? (isEnoughMoney ? "online" : "not enough money") : "offline";
        buttonImage.sprite = buttonState;
        selectedState = isExist;
        status.text = playerStatus;
        cellIdentifier = player.playerId;
        if (callback != null)
        {
            if (isEnoughMoney)
            {
                addToGame.onClick.AddListener(() => { ButtonStatusChange(addToGame.gameObject, player, callback); });
            }
            addToGame.interactable = isEnoughMoney && player.online;
        }
    }

    void ButtonStatusChange(GameObject button, PlayerData player, System.Action<GameObject, bool, PlayerData> callback)
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("Friend is added");
        Debug.Log("before press " + selectedState);
        selectedState = !selectedState;
        Debug.Log("after press " + selectedState);
        callback(button, selectedState, player);
    }

	
}
