﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class FriendListController : MonoBehaviour, IEnhancedScrollerDelegate {

    [SerializeField]
    private EnhancedScroller scroller;
    [SerializeField]
    private FriendItem friendItemPrefab;
    [SerializeField]
    private Sprite unselected, selected;
    [SerializeField]
    private float scrollSpeed = 1f;

    private ScrollRect tableScroll;
    private List<PlayerData> friends;
    private long fee;
    private List<string> invitedFriends;
    private Coroutine scrollerr;
    private bool buttonHold;
    private int currentDataIndex;
    private int friendsPerPage;

    public List<string> InvitedFriends
    {
        get
        {
            return invitedFriends;
        }
    }

    void Awake()
    {
        invitedFriends = new List<string>();
    }

    void OnEnable()
    {
        if(scroller.ScrollRect == null)
            scroller.RunAwake();

        scroller.Delegate = this;
        tableScroll = scroller.ScrollRect;
        invitedFriends.Clear();
        scroller.scrollerScrollingChanged -= JumpComplete;
        scroller.scrollerScrollingChanged += JumpComplete;
        currentDataIndex = 0;
        friendsPerPage = 2;
    }

    void OnDisable()
    {
        scroller.scrollerScrollingChanged -= JumpComplete;
    }


    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        FriendItem friendItem = scroller.GetCellView(friendItemPrefab) as FriendItem;
        bool isExist = false;
        if(invitedFriends != null)
        {
            isExist = invitedFriends.Contains(friends[dataIndex].playerId);
        }
        friendItem.SetFriend(friends[dataIndex], fee, isExist ? selected : unselected, isExist, AddToInviteList);

        return friendItem;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return friendItemPrefab.GetComponent<LayoutElement>().preferredHeight;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return friends.Count;
    }


    void AddToInviteList(GameObject button, bool isSelected, PlayerData player)
    {
        Image stateImage = button.GetComponent<Image>();
        stateImage.sprite = isSelected ? selected : unselected;
        if (!isSelected)
        {
            Debug.Log("Removing player from invite list");
            invitedFriends.Remove(player.playerId);
        }
        else
        {
            Debug.Log("Adding player to invite list");
            invitedFriends.Add(player.playerId);
        }
        
    }

    public void SetFriendsAndFee(List<PlayerData> friends, long fee)
    {
        this.friends = friends;
        this.fee = fee;
    }


    public void ButtonUp()
    {        
        buttonHold = false;
        if (scrollerr != null)
            StopCoroutine(scrollerr);
    }

    public void ButtonDown(int sideSign)
    {
        SoundManager.Instance.PlayButtonPress();
        buttonHold = true;
        scrollerr = StartCoroutine(ScrollTable(sideSign * scrollSpeed));
    }

    IEnumerator ScrollTable(float speed)
    {
        while (buttonHold)
        {
            if ((tableScroll.verticalNormalizedPosition < 0 && speed < 0) || (tableScroll.verticalNormalizedPosition > 1 && speed > 0))
                break;
            tableScroll.verticalNormalizedPosition += speed;
            yield return new WaitForEndOfFrame();
        }
    }

    public void Next()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex < friends.Count - friendsPerPage)
        {
            Debug.Log("<b>dataIndex: </b>" + currentDataIndex);
            currentDataIndex++;
            //scroller.snapping = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, false, EnhancedScroller.TweenType.linear, 1);
        }
    }

    public void Prev()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex > 0)
        {
            Debug.Log("<b>dataIndex: </b>" + currentDataIndex);
            currentDataIndex--;
            //scroller.snapping = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, false, EnhancedScroller.TweenType.linear, 1);
            Debug.Log("<b>after jump dataIndex: </b>" + currentDataIndex);
        }
    }

    void JumpComplete()
    {
        //scroller.snapping = true;
        //nextButton.interactable = true;
        //prevButton.interactable = true;
    }

    void JumpComplete(EnhancedScroller scroller, bool scrolling)
    {
        if (!scrolling)
        {
            Debug.Log("Jump complete handler");
            currentDataIndex = (scroller.EndDataIndex - scroller.StartDataIndex) / 2 + scroller.StartDataIndex;
            if (currentDataIndex >= friends.Count - friendsPerPage)
                currentDataIndex = friends.Count - friendsPerPage - 1;
            Debug.Log("new currentdataindex: " + currentDataIndex);
            //this.scroller.snapping = true;
            //nextButton.interactable = true;
            //prevButton.interactable = true;
        }
    }



}
