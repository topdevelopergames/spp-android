﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FriendsList : MonoBehaviour {

    [SerializeField]
    private FriendListController friendListController;
    [SerializeField]
    private GameObject noSelectedFriends;


    void OnEnable()
    {
        friendListController.gameObject.SetActive(false);
        GameSparksManager.Instance.ListGameFriends(ListFriendsCallback);

    }
    
    public void SendInvites()
    {
        SoundManager.Instance.PlayButtonPress();
        if (friendListController.InvitedFriends.Count == 0)
        {
            noSelectedFriends.SetActive(true);
            return;
        }

        GameData.Instance.needToUpdateInfo = true;

        ChallengeCreatingParam challenge = new ChallengeCreatingParam();
        challenge.challengerId = GameSparksManager.Instance.AuthorizedPlayerData.playerId;
        challenge.challengeType = ChallengeType.FRIEND;
        challenge.entryFee = GameData.entryFees[GameData.Instance.entryFee];
        challenge.shortCode = GameData.Instance.selectedGameMode == 0 ? ChallengeCode.MULTI_TIME_LIMIT : ChallengeCode.MULTI_21;
        challenge.tableType = GameData.Instance.playTable == 0 ? TableType.POCKET : TableType.NOPOCKET;
        challenge.timeLimit = GameData.Instance.selectedGameMode == 0 ? GameData.Instance.TimeForGame : 0;
        challenge.usersToChallenge = friendListController.InvitedFriends;
        challenge.authType = AuthType.SIGNED;
        GameData.Instance.needToUpdateInfo = true;
        GameData.Instance.challenge = challenge;
        GameData.Instance.acceptedChallengeId = "";
        GameData.Instance.Save();
        MenuManager.Instance.LoadTraining();
    }

    void ListFriendsCallback(List<PlayerData> friends)
    {
        friendListController.SetFriendsAndFee(friends, GameData.entryFees[GameData.Instance.entryFee]);
        friendListController.gameObject.SetActive(true);
    }

    public void Close(GameObject friendsListPopup)
    {
        //friendsListPopup.SetActive(false);
        GameData.Instance.isInGame = false;
    }

}
