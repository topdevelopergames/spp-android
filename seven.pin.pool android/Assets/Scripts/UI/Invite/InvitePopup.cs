﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InvitePopup : MonoBehaviour {

    [SerializeField]
    private Text inviteMessage;
    [SerializeField]
    private UnityEngine.UI.Button declineButton;
    [SerializeField]
    private float timeToDecline = 30;

    public delegate void OnInviteEnable( int order);
    public OnInviteEnable invite;

    private Coroutine popupShowTimer;

    public void SetMessage(string text)
    {
        inviteMessage.text = text;
    }

    void OnEnable()
    {
        if (invite != null) 
            invite(-1);
        popupShowTimer = StartCoroutine(PopupShowTimer());

    }

    void OnDisable()
    {
        if (invite != null)
            invite(1);
        if (popupShowTimer != null)
            StopCoroutine(popupShowTimer);
    }

    IEnumerator PopupShowTimer()
    {
        yield return new WaitForSeconds(timeToDecline);
        declineButton.onClick.Invoke();
    }

}
