﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using GameSparks.Core;

public class MenuManager : MonoBehaviour {  

    public string guestSceneName, playerSceneName, loginSceneName, signUpSceneName, signInSceneName, singleOrMultiSceneName, gameModeSceneName, poolShopScene;

    [SerializeField]
    private int freeCoinsCooldownMinutes = 30;
    private int freecoinsTimeLeft;
    private static MenuManager menuManager;

    private ChallengeCreatedData challenge;
    private Coroutine freeCoinsTimerCoroutine;

    public delegate void EnableButtonDelegate();
    public delegate void DisableButtonDelegate();
    public delegate void FreecoinsTimeReceived();
    public EnableButtonDelegate enableButtonDelegate;
    public DisableButtonDelegate disableButtonDelegate;
    public FreecoinsTimeReceived freecoinsTimeReceived;

    private bool timerStarted = false;

    /// <summary>
    /// timer in seconds
    /// </summary>
    public int FreeCoinsTimeLeft
    {
        get
        {
            return freecoinsTimeLeft;
        }
    }

    public static MenuManager Instance
    {
        get { return menuManager; }
    }

    void Awake()
    {
        if (menuManager == null)
        {
            menuManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        Debug.Log("Test");
        GameData.Instance.freecoinsTimerStarted = false;
        GameData.Instance.needToUpdateInfo = true;
		if (GameData.Instance.User == null) {
			GameData.Instance.User = new UserProfile ();
		}
		GameData.Instance.User.Type = ProfileType.Guest;
        timerStarted = false;
    }

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void LoadLevel(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void ShowPopup(GameObject popUp)
    {
        popUp.SetActive(true);
    }

    public void ClosePopUp(GameObject popUp)
    {
        popUp.SetActive(false);
    }

    public void ReloadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public string GetCurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void LoadPoolShop()
    {
        LoadLevel(poolShopScene);
    }

    public void LoadTraining()
    {
		ServerController.serverController = null;
		GameData.Instance.isInGame = true;
        string gameName;// = GameData.Instance.matchRules.type != SkillzMatchType.None ? SkillzCrossPlatform.GetPlayer().DisplayName: "Guest";
        gameName = "Guest";
        PlayerPrefs.SetString("BallPoolMultyplayerServerTemplateDemoPlayerName", gameName);
        PlayerPrefs.SetString("username", gameName);
        MenuControllerGenerator.controller.useNetwork = false;
        MenuControllerGenerator.controller.LoadLevel(MenuControllerGenerator.controller.game);
        MenuControllerGenerator.controller.masterServerGUI.SetActive(false);
    }

    public void LoadSinglePlayer(PlayerData player1, PlayerData player2)
    {
		ServerController.serverController = ServerController.saveServerController;
		List<string> list = new List<string>();
        list.Add(player1.playerId);
        list.Add(player2.playerId);
        list.Sort();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(list[0]);
        sb.Append(list[1]);
        GameData.Instance.hostPlayer = list[0];
        GameData.Instance.gameName = sb.ToString();
        GameData.Instance.Save();
        ServerController.serverController.myName = sb.ToString();
        PlayerPrefs.SetString("BallPoolMultyplayerServerTemplateDemoPlayerName", sb.ToString());
		MenuControllerGenerator.controller.masterServerGUI.SetActive(true);
		MenuControllerGenerator.controller.masterServerGUI.GetComponent<MasterServerGUI>().StartNetworkGame();
    }

    public void PlayAsGuest()
    {
        GameSparksManager.Instance.AuthenticationGuest();
        LoadLevel(guestSceneName);
    }

    public void PlayAsAuhtorizedPlayer()
    {
        LoadLevel(signInSceneName);
    }

    public void BackToMainMenu()
    {
		LoadLevel (loginSceneName);
		return;
        if (GameData.Instance.User == null || GameData.Instance.User.Type == ProfileType.Guest)
        {
            LoadLevel(guestSceneName);
        }
        else
        {
            LoadLevel(playerSceneName);
        }

    }

    public void SinglePlayer(string levelName)
    {
        GameData.Instance.singleOrMulti = 0;
        GameData.Instance.selectedGameMode = 0;
        GameData.Instance.Save();
        LoadLevel(levelName);
    }

    public void MultiPlayer(string levelName)
    {
        GameData.Instance.singleOrMulti = 1;
        GameData.Instance.selectedGameMode = 0;
        GameData.Instance.Save();
        LoadLevel(levelName);
    }

    public void CheckConnection(Action succesCallback, Action errorCallback)
    {
        GS.Reconnect();
        StartCoroutine(CheckConnectionTimer(succesCallback, errorCallback));
    }

    #region Freecoins handlers

    public void StartFreeCoinsTimer()
    {
        Debug.Log("Launching freecoins timer");
        CheckFreeCoinsTime();
        if (disableButtonDelegate != null)
            disableButtonDelegate();
    }

    public void StopFreeCoinsTimer()
    {
        GameData.Instance.freecoinsTimerStarted = false;
        Debug.LogWarning("Freecoins timer stopped");
        if (freeCoinsTimerCoroutine != null)
            StopCoroutine(freeCoinsTimerCoroutine);
        if (enableButtonDelegate != null)
            enableButtonDelegate();
    }    

    IEnumerator FreeCoinsTimer()
    {
        Debug.LogWarning("Freecoins timer started");
        while (freecoinsTimeLeft > 0)
        {
            yield return new WaitForSeconds(1);
            freecoinsTimeLeft--;
        }
        GameData.Instance.freecoinsTimerStarted = false;
        //enable button
        if (enableButtonDelegate != null)
        {
            Debug.Log("Executing button enable action");
            enableButtonDelegate();
        }
    }

    public void CheckFreeCoinsTime()
    {
        GameSparksManager.Instance.GetTimeToFreeCoins(TimeForFreeCoinsCallback);
    }

    void TimeForFreeCoinsCallback(int time)
    {
        Debug.Log("Time left: " + time);
        if (time == 0)
        {
            StopFreeCoinsTimer();
            return;
        }
        freecoinsTimeLeft = time;
        Debug.LogWarning("Is freecoins timer started: " + GameData.Instance.freecoinsTimerStarted);
        if (!GameData.Instance.freecoinsTimerStarted)
        {
            Debug.Log("Let's start freecoins timer");
            GameData.Instance.freecoinsTimerStarted = true;
            freeCoinsTimerCoroutine = StartCoroutine(FreeCoinsTimer());
        }
        if (freecoinsTimeReceived != null)
            freecoinsTimeReceived();
    }

    void OnApplicationPause(bool pauseState)
    {
        Debug.Log("Menu manager onapplicationpause: " + pauseState);
        if(!pauseState && GameData.Instance.freecoinsTimerStarted)
        {
            Debug.Log("Let's check freecois timer");
            StartFreeCoinsTimer();
        }
    }

    public void FreeCoins(Action callback)
    {
        GameSparksManager.Instance.GetFreeCoins(callback);
    }

    #endregion Freecoins handlers

    #region Invite handlers

    public void AcceptInvite()
    {
        Debug.Log("Acccepted invite");
        GameData.Instance.playTable = (int)challenge.tableType;
        GameData.Instance.singleOrMulti = 1;
        switch (challenge.shortCode)
        {
            case ChallengeCode.MULTI_21:
                GameData.Instance.selectedGameMode = 1;
                break;
            case ChallengeCode.MULTI_TIME_LIMIT:
                GameData.Instance.selectedGameMode = 0;
                break;
        }
        for (int i = 0; i < GameData.entryFees.Length; i++)
        {
            if (GameData.entryFees[i] == challenge.entryFee)
            {
                GameData.Instance.entryFee = i;
                break;
            }
        }
        GameData.Instance.isInGame = true;
        GameData.Instance.playWithFriends = true;
        switch (challenge.timeLimit)
        {
            case 60:
                GameData.Instance.timeForGame = 0;
                break;
            case 180:
                GameData.Instance.timeForGame = 1;
                break;
            case 300:
                GameData.Instance.timeForGame = 2;
                break;
        }
        GameData.Instance.acceptedChallengeId = challenge.challengeId;
        GameData.Instance.challenge = null;
        GameData.Instance.needToUpdateInfo = true;
        GameData.Instance.Save();
        LoadTraining();
    }

    public void DeclineInvite()
    {
        Debug.Log("Declined invite");
        GameSparksManager.Instance.DeclineChallenge(challenge.challengeId);
        RemoceInviteInfo();
    }

    public void RemoceInviteInfo()
    {
        challenge = null;
        GameData.Instance.challenge = null;
        GameData.Instance.acceptedChallengeId = "";
        GameData.Instance.isInGame = false;
        GameData.Instance.playWithFriends = false;
        GameData.Instance.Save();
    }

    public void ShowInvite(ChallengeCreatedData challengeData)
    {
        challenge = challengeData;
        if (GameData.Instance.isInGame)
        {
            DeclineInvite();
            return;
        }

        string gameType = string.Empty;
        switch(challengeData.shortCode)
        {
            case ChallengeCode.MULTI_101:
                gameType = "101";
                break;
            case ChallengeCode.MULTI_21:
                gameType = "21";
                break;
            case ChallengeCode.MULTI_51:
                gameType = "51";
                break;
            case ChallengeCode.MULTI_TIME_LIMIT:
                gameType = "time limit";
                break;
        }

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(challengeData.challengerData.displayName);
        sb.Append(" invites you to ");
        //sb.Append(challengeData.shortCode == ChallengeCode.MULTI_21 ? "21" : "time limit");
        sb.Append(gameType);
        sb.Append(" game with ");
        sb.Append(challengeData.entryFee);
        sb.Append(" entry fee on ");
        sb.Append("table ");
        sb.Append(challengeData.tableType == TableType.POCKET ? "with pockets" : "without pockets");
        MenuUIManager.Instance.ShowInvite(sb.ToString());
    }
    #endregion
    
    
    void OnDisable()
    {
        Debug.Log("OnDisable MenuManager");
        GameData.Instance.freecoinsTimerStarted = false;
        GameData.Instance.Save();
    }

    
    void OnAplicationQuit()
    {
        GameData.Instance.freecoinsTimerStarted = false;
        GameData.Instance.Save();
    }
    

    IEnumerator CheckConnectionTimer(Action successCallback, Action errorCallback)
    {
        int timeForCheck = 10;
        while(timeForCheck > 0)
        {
            if(GS.Available)
            {
                successCallback();
                break;
            }
            timeForCheck--;
            yield return new WaitForSeconds(1);
        }
        if (!GS.Available)
        {
            Debug.Log("No internet connection after attempt to reconnect");
            GS.Disconnect();
            errorCallback();
        }
    }

    

}
