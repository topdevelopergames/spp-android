﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class AvatarSpinItem : EnhancedScrollerCellView {

    [SerializeField]
    private Image avatar, facebookAvatar;
    [SerializeField]
    private GameObject facebookAvatarBack;

    public void SetAvatar(Sprite avatar, bool isFacebook)
    {
        if (isFacebook)
        {
            this.avatar.gameObject.SetActive(false);
            facebookAvatarBack.SetActive(true);
            facebookAvatar.sprite = avatar;
        }
        else
        {
            this.avatar.gameObject.SetActive(true);
            facebookAvatarBack.SetActive(false);
            this.avatar.sprite = avatar;
        }
    }

}
