﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OpponentSelectionUIManager : MonoBehaviour {

    [SerializeField]
    private Text username;
    [SerializeField]
    private Text userLevel;
    [SerializeField]
    private GameObject userDots;
    [SerializeField]
    private Image userVisualProgress;
    [SerializeField]
    private Text userGameBank;
    [SerializeField]
    private Image userAvatar;
    [SerializeField]
    private Image userFacebookAvatar;
    [SerializeField]
    private GameObject userFacebookAvatarBack, opponentFacebookBack;
    [SerializeField]
    private Text opponentName;
    [SerializeField]
    private Text opponentLevel;
    [SerializeField]
    private GameObject opponentDots;
    [SerializeField]
    private Image opponentVisualProgress;
    [SerializeField]
    private Text opponentGameBank;
    [SerializeField]
    private Image opponentAvatar, opponentFacebookAvatar;
    [SerializeField]
    private Text gameBank;
    [SerializeField]
    private GameObject playerNotFoundPopup, errorPopup;

    public int timeout = 30;

    private int maxNameLength = 9;

    private static OpponentSelectionUIManager _instance;

    public static OpponentSelectionUIManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }


    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        //disabling avatar gameobject with it's background object, when opponent was found
        opponentAvatar.transform.parent.gameObject.SetActive(false);

        bool isGuest = GameData.Instance.User.Type == ProfileType.Guest || GameData.Instance.User.Type == ProfileType.GuestOffline;
        string username = isGuest ? GameSparksManager.Instance.AuthorizedPlayerData.displayName + GameSparksManager.Instance.AuthorizedPlayerData.playerId : GameSparksManager.Instance.AuthorizedPlayerData.displayName;

        SetUserName(username);
        SetUserLevel(GameSparksManager.Instance.AuthorizedPlayerData.statistic.level.ToString());
        SetUserVisualProgress(GameSparksManager.Instance.AuthorizedPlayerData.statistic.score, GameSparksManager.Instance.AuthorizedPlayerData.statistic.level);
        SetUserAvatar(GameSparksManager.Instance.AuthorizedPlayerData.avatar);
        string entryFee = GameData.feeText[GameData.Instance.entryFee];
        SetUserBank(entryFee);
        SetOpponentBank(entryFee);
        SetBankAmount("");
    }

    public void SetUserName(string name)
    {
        userDots.SetActive(name.Length > maxNameLength);
        username.text = name.Length > maxNameLength ? name.Substring(0, maxNameLength) : name;
        username.alignment = name.Length > maxNameLength ? TextAnchor.MiddleRight : TextAnchor.UpperCenter;
    }

    public void SetUserLevel(string level)
    {
        userLevel.text = level;
    }

    public void SetUserVisualProgress(float currentPoints, int level)
    {
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(level);
        userVisualProgress.fillAmount = currentPoints / pointForNextLevel;
    }

    public void SetUserBank(string bankAmount)
    {
        userGameBank.text = bankAmount;
    }

    public void SetUserAvatar(Sprite avatar)
    {
        if (GameData.Instance.User.Type == ProfileType.FB)
        {
            userFacebookAvatarBack.SetActive(true);
            userAvatar.gameObject.SetActive(false);
            userFacebookAvatar.sprite = avatar;
        }
        else
        {
            userFacebookAvatarBack.SetActive(false);
            userAvatar.gameObject.SetActive(true);
            userAvatar.sprite = avatar;
        }
    }

    public void SetOpponentName(string name)
    {
        opponentDots.SetActive(name.Length > maxNameLength);
        opponentName.text = name.Length > maxNameLength ? name.Substring(0, maxNameLength) : name;
        opponentName.alignment = name.Length > maxNameLength ? TextAnchor.MiddleRight : TextAnchor.MiddleCenter;
    }

    public void SetOpponentLevel(string level)
    {
        opponentLevel.text = level;
    }

    public void SetOpponentVisualProgress(float currentPoints, int level)
    {
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(level);
        opponentVisualProgress.fillAmount = currentPoints / pointForNextLevel;
    }

    public void SetOpponentBank(string bankAmount)
    {
        opponentGameBank.text = bankAmount;
    }

    public void SetOpponentAvatar(Sprite avatar, bool isFacebook)
    {
        if (isFacebook)
        {
            opponentFacebookAvatar.sprite = avatar;
        }
        else
        {
            opponentAvatar.sprite = avatar;
        }
    }

    public void SetBankAmount(string bankAmount)
    {
        gameBank.text = bankAmount;
    }

    public void SetOpponent(PlayerData opponent)
    {
        bool isGuest = opponent.displayName.Equals("Guest");

        string opponentName = isGuest ? opponent.displayName + opponent.playerId : opponent.displayName;

        SetOpponentName(opponentName);
        SetOpponentLevel(opponent.statistic.level.ToString());
        SetOpponentVisualProgress(opponent.statistic.score, opponent.statistic.level);
        SetOpponentAvatar(opponent.avatar, opponent.facebookId != "");
    }

    public void EnableopponentAvatar()
    {
        opponentFacebookBack.SetActive(false);
        opponentAvatar.transform.parent.gameObject.SetActive(true);
    }

    public void EnableOpponentFacebookAvatar()
    {
        opponentFacebookBack.SetActive(true);
        opponentAvatar.transform.parent.gameObject.SetActive(false);
    }

    public void MoveMoneyToBank()
    {
        string bank = GameData.prizesText[GameData.Instance.entryFee];
        SetBankAmount(bank);
        SetUserBank("");
        SetOpponentBank("");
    }

    public void ShowPlayerNotFound()
    {
        playerNotFoundPopup.SetActive(true);
    }

    public void ShowError()
    {
        errorPopup.SetActive(true);
    }

}
