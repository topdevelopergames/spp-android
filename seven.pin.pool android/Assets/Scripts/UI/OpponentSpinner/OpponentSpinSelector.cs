﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using System.Collections.Generic;

public class OpponentSpinSelector : MonoBehaviour
{

    [SerializeField]
    private OpponentSpinSelectorController spinController;
    [SerializeField]
    private float minVelocity, maxVelocity;
    [SerializeField]
    private int timeout = 30;

    private bool opponentSelected;
    private Coroutine rollRoutine, timerRoutine;
    private bool snapped = false;

    private bool handlerAttached = false;
    private PlayerData opponent;
    private long entryFee;

    Coroutine timeoutTimer;

    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        if (spinController.scroller.ScrollRect == null)
            spinController.scroller.RunAwake();

        spinController.scroller.ScrollRect.enabled = false;
        spinController.scroller.enabled = false;
        GameSparksManager.Instance.GetOnlinePlayerDataList((playersOnline) =>
        {
            spinController.scroller.enabled = true;
            spinController.scroller.ScrollRect.enabled = true;
            spinController.SetPlayers(playersOnline);

        });
        opponentSelected = false;
        snapped = false;
        rollRoutine = StartCoroutine(RollUntilResponse());             
        entryFee = GameData.entryFees[GameData.Instance.entryFee];
        bool isGuest = GameSparksManager.Instance.AuthorizedPlayerData.displayName.Equals("Guest");
        GameMode gameMode = GameMode.FIRST51;
        /*switch(GameData.Instance.pointsForGame)
        {
            case 0:
                gameMode = GameMode.FIRST21;
                break;
            case 1:
                gameMode = GameMode.FIRST51;
                break;
            case 2:
                gameMode = GameMode.FIRST101;
                break;
        }*/
        TableType table = TableType.POCKET;
        switch (GameData.Instance.playTable)
        {
            case 0:
                table = TableType.POCKET;
                break;
            case 1:
                table = TableType.NOPOCKET;
                break;
        }

        MatchType type = new MatchType(
           isGuest ? AuthType.GUEST : AuthType.SIGNED,
           gameMode,
           table,
           entryFee);


        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
        Debug.Log("Single player match type: " + type.Value);
        GameSparksManager.Instance.MatchmakingSinglePlayer(GameSparksManager.Instance.AuthorizedPlayerData.statistic.level, type,
            //callbackChallengeStarted
            OpponentFound,
            //callbackChallengeChanged
            ChallengeChanged,
            //callbackChallengeComplete
            ChallengeCompleted,
            //callbackMatchNotFound
            MatchNotFound,
            //callbackChallengeChatMessage
			ChallengeChatMessageAdd,
            //callbackError
            ErrorCallback);
    }

    /// <summary>
    /// Handler for selected opponent.
    /// 
    /// Setting up Oppoent name, level, bank
    /// and loading level.
    /// </summary>
    /// <param name="scroller"></param>
    /// <param name="cellIndex"></param>
    /// <param name="dataIndex"></param>
    void ScrollerSnapped(EnhancedScroller scroller, int cellIndex, int dataIndex)
    {
        Debug.Log("Stop scroller");
        ///Play animations, setup single player game
        if (opponent == null)
        {
            opponent = GameSparksManager.Instance.AuthorizedPlayerData;
            GameData.Instance.opponentId = null;
            return;
        }
        GameSparksManager.Instance.AuthorizedPlayerData.coins -= entryFee;
        OpponentSelectionUIManager.Instance.SetOpponent(opponent);
        if (opponent.facebookId != "")
        {
            OpponentSelectionUIManager.Instance.EnableOpponentFacebookAvatar();
        }
        else
        {
            OpponentSelectionUIManager.Instance.EnableopponentAvatar();
        }
        OpponentSelectionUIManager.Instance.MoveMoneyToBank();
        GameData.Instance.opponentId = opponent.playerId;
        MenuManager.Instance.LoadSinglePlayer(GameSparksManager.Instance.AuthorizedPlayerData, opponent);
    }


    public void Roll()
    {
        rollRoutine = StartCoroutine(RollUntilResponse());
        opponentSelected = false;
    }

    public void RollEnd()
    {
        Debug.Log("Roll ended");
        opponentSelected = true;
        spinController.scroller.scrollerSnapped -= ScrollerSnapped;
        spinController.scroller.scrollerSnapped += ScrollerSnapped;
        if (rollRoutine != null)
            StopCoroutine(rollRoutine);
        if (timerRoutine != null)
            StopCoroutine(timerRoutine);
        

    }

    IEnumerator RollUntilResponse()
    {
        float speed = Random.Range(minVelocity, maxVelocity);
        spinController.SetVelocity(speed);
        spinController.scroller.snapping = true;
        do
        {
            spinController.SetVelocity(speed);
            yield return new WaitForEndOfFrame();
        }
        while (!opponentSelected);
    }

    IEnumerator TimeoutTimer(int timeout, System.Action callback)
    {
        yield return new WaitForSeconds(timeout);
        Debug.Log("Time to stop spinner or show timeout message");
        callback();
    }

    #region callbacks for matchmaking
    void OpponentFound(List<PlayerData> playerList)
    {
        Debug.Log("Opponent founded");
        opponent = GameSparksManager.Instance.AuthorizedPlayerData.playerId.Equals(playerList[0].playerId) ? playerList[1] : playerList[0];
        Debug.Log("Time to stop spin after 5 sec");
        timerRoutine = StartCoroutine(TimeoutTimer(5, RollEnd));
    }

    void ChallengeChanged(List<RuntimeLeaderboardEntry> leaders)
    {
        Debug.Log("Something changed Single");
    }

    void ChallengeCompleted(ChallengeCompletedData challengeCompletedData)
    {
        Debug.Log("Challenge Completed Single");
        InGameUIManager.Instance.ChallengeCompleteExtCall(challengeCompletedData);
    }

    void MatchNotFound()
    {
        MasterServerGUI.Disconnect();
        GameData.Instance.opponentId = null;
        GameData.Instance.Save();
        OpponentSelectionUIManager.Instance.ShowPlayerNotFound();
    }

    void ErrorCallback()
    {
        GameData.Instance.opponentId = null;
        GameData.Instance.Save();
        OpponentSelectionUIManager.Instance.ShowPlayerNotFound();
    }

	void ChallengeChatMessageAdd(ChallengeChatMessage chatMessage)
	{
		InGameUIManager.Instance.ForSinglePlayerChallengeChatMessageAdd(chatMessage);
	}
    #endregion
}
