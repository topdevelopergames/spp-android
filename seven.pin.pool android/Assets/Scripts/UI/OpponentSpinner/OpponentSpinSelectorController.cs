﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class OpponentSpinSelectorController : MonoBehaviour, IEnhancedScrollerDelegate
{
    public EnhancedScroller scroller;
    [SerializeField]
    private AvatarSpinItem avatarItemPrefab;

    private List<PlayerData> opponents;

    void Start()
    {
        scroller.Delegate = this;
        //scroller.snapping = false;
    }

    public void SetPlayers(List<PlayerData> players)
    {
        opponents = players;
        scroller.ReloadData();
    }

    public void SetVelocity(float velocity)
    {
        scroller.LinearVelocity = velocity;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {       
        AvatarSpinItem avatarItem = scroller.GetCellView(avatarItemPrefab) as AvatarSpinItem;
        avatarItem.SetAvatar(opponents[dataIndex].avatar, opponents[dataIndex].facebookId != "");
        OpponentSelectionUIManager.Instance.SetOpponent(opponents[dataIndex]);
        return avatarItem;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        LayoutElement rect = avatarItemPrefab.GetComponent<LayoutElement>();
        if (rect != null)
            return rect.minHeight;
        return 400f;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return opponents.Count;
    }

}
