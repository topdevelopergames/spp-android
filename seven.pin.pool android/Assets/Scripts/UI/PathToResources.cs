﻿using UnityEngine;
using System.Collections;

public class PathToResources : ScriptableObject {

    public string pathToImages;
    public string pathToSDAchievements;
    public string pathToHDAchievements;
    public string pathToHDCues;
    public string pathToSDCues;
    public string defaultAvatar;
    public string unlockedAchievement;
    public string lockedAchievement;

    private static PathToResources _instance = null;

    public static PathToResources Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = Resources.Load("PathToResources") as PathToResources;
            }
            return _instance;
        }
    }
	
}
