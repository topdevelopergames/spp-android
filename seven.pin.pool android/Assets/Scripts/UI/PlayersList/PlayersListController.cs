﻿using UnityEngine;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using System;
using UnityEngine.UI;

public class PlayersListController : MonoBehaviour, IEnhancedScrollerDelegate
{
    [SerializeField]
    private float scrollSpeed = 3f;

    [SerializeField]
    private EnhancedScroller scroller;

    [SerializeField]
    private PlayersListItem playerListItemPrefab;

    [SerializeField]
    private ProfileStatisticsPopup profilePopup;
    [SerializeField]
    GameObject ChatMultiPlayer;
	[SerializeField]
	GameObject MessageMultiPlayer;

    private int playersPerPage = 4;

    private List<MultiplayerData> players;
    private int currentDataIndex;


    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        currentDataIndex = 0;
        if(scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;
        playersPerPage = 3;
        scroller.scrollerScrollingChanged -= JumpComplete;
        scroller.scrollerScrollingChanged += JumpComplete;
        currentDataIndex = 0;
    }

    void OnDisable()
    {
        scroller.scrollerScrollingChanged -= JumpComplete;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        PlayersListItem currentPlayer = scroller.GetCellView(playerListItemPrefab) as PlayersListItem;
        currentPlayer.ChatMultiPlayerObject = ChatMultiPlayer;
		currentPlayer.MessageMultiPlayerObject = MessageMultiPlayer;
        currentPlayer.SetPlayer(players[dataIndex], ShowProfile);
        return currentPlayer;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return playerListItemPrefab.gameObject.GetComponent<LayoutElement>().preferredHeight;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return players.Count;
    }

    public void SetPlayers(List<MultiplayerData> players)
    {
        this.players = players;
        if (players != null)
            scroller.ReloadData();
    }

    public void ShowProfile(string userId)
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("Viewing profile of player");
        profilePopup.SetProfileToView(userId, true, () =>
        {
            profilePopup.gameObject.SetActive(true);
        });
    }

    public void ScrollUp(int sign)
    {
        scroller.LinearVelocity = sign * scrollSpeed;
    }

    public void ScrollDown(int sign)
    {
        scroller.LinearVelocity = sign * scrollSpeed;
    }

    public void Next()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex < players.Count - playersPerPage)
        {
            Debug.Log("<b>dataIndex: </b>" + currentDataIndex);
            currentDataIndex++;
            //scroller.snapping = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, scroller.snapUseCellSpacing, scroller.snapTweenType, scroller.snapTweenTime);
        }
    }

    public void Prev()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex > 0)
        {
            Debug.Log("<b>dataIndex: </b>" + currentDataIndex);
            currentDataIndex--;
            //scroller.snapping = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, scroller.snapUseCellSpacing, scroller.snapTweenType, scroller.snapTweenTime);
            Debug.Log("<b>after jump dataIndex: </b>" + currentDataIndex);
        }
    }

    void JumpComplete()
    {
        //scroller.snapping = true;
        //nextButton.interactable = true;
        //prevButton.interactable = true;
    }

    void JumpComplete(EnhancedScroller scroller, bool scrolling)
    {
        if (!scrolling)
        {
            Debug.Log("Jump complete handler");
            currentDataIndex = (scroller.EndDataIndex - scroller.StartDataIndex) / 2 + scroller.StartDataIndex;
            if (currentDataIndex >= players.Count - playersPerPage)
                currentDataIndex = players.Count - playersPerPage - 1;
            Debug.Log("new currentdataindex: " + currentDataIndex);
            //this.scroller.snapping = true;
            //nextButton.interactable = true;
            //prevButton.interactable = true;
        }
    }

}
