﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;

public class PlayersListItem : EnhancedScrollerCellView{

    [SerializeField]
    private Text userScore;
    [SerializeField]
    private Image userAvatar, facebookAvatar;
    [SerializeField]
    private GameObject facebookAvatrBack;
    [SerializeField]
    private UnityEngine.UI.Button showProfile;
    [SerializeField]
    private Text playerName;
	[SerializeField]
	private Transform transformPointForChatDialog;
	[SerializeField]
	private GameObject buttonChat;

	public GameObject ChatMultiPlayerObject;
	public GameObject MessageMultiPlayerObject;

    public void SetPlayer(MultiplayerData player, System.Action<string> callback)
    {
        if(player.player.facebookId != "")
        {
            facebookAvatar.sprite = player.player.avatar;
            facebookAvatrBack.SetActive(true);
            userAvatar.gameObject.SetActive(false);
        }
        else
        {
            facebookAvatrBack.SetActive(false);
            userAvatar.gameObject.SetActive(true);
            userAvatar.sprite = player.player.avatar == null ? Resources.Load<Sprite>(PathToResources.Instance.defaultAvatar) : player.player.avatar;
        }

        userScore.text = player.score.ToString();
        cellIdentifier = player.player.playerId;
        playerName.text = player.player.displayName;
        showProfile.onClick.AddListener(() => { callback(player.player.playerId); });
		buttonChat.SetActive(GameData.Instance.User.ServerPlayerId != player.player.playerId);
    }

    public void ActivateChat()
    {
        ChatMultiPlayerObject.SetActive(true);
		MessageMultiPlayerObject.transform.position = transformPointForChatDialog.position;
		ChatMultiPlayer.Opponent = cellIdentifier;
		ChatMultiPlayer.OpponentName = GameData.Instance.User.DisplayName;
		ChatMultiPlayer.ChatSetEnable();
    }
}
