﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Popup : MonoBehaviour {

    [SerializeField]
    private float duration, endFadeValue;
    [SerializeField]
    private Vector3 endScale;

    public void CloseScaleFadePopup(GameObject popup)
    {
        ClickSound();
        if (transform.childCount <= 0)
        {
            popup.SetActive(false);
            return;
        }
        Transform t = popup.transform.GetChild(0);
        if (t == null)
            return;
        t.DOScale(endScale, duration).OnComplete(() => { popup.SetActive(false); });
        UnityEngine.UI.Image img = t.GetComponent<UnityEngine.UI.Image>();
        if (img == null)
            return;
        img.DOFade(endFadeValue, duration);
    }

    public void ClickSound()
    {
        SoundManager.Instance.PlayButtonPress();
    }

    public void InstantClose(GameObject popup)
    {
        ClickSound();
        popup.SetActive(false);
    }

    public void InstantShow(GameObject popup)
    {
        ClickSound();
        popup.SetActive(true);
    }

    public void CloseFade(GameObject popup)
    {
        ClickSound();
        if (transform.childCount <= 0)
        {
            popup.SetActive(false);
            return;
        }
        Transform t = popup.transform.GetChild(0);
        if (t == null)
            return;
        UnityEngine.UI.Image img = t.GetComponent<UnityEngine.UI.Image>();
        if (img == null)
            return;
        img.DOFade(endFadeValue, duration).OnComplete(()=> { popup.SetActive(false); });
    }

    public void CloseScale(GameObject popup)
    {
        ClickSound();
        if (transform.childCount <= 0)
        {
            popup.SetActive(false);
            return;
        }
        Transform t = popup.transform.GetChild(0);
        if (t == null)
            return;
        t.DOScale(endScale, duration).OnComplete(() => { popup.SetActive(false); });
    }
}
