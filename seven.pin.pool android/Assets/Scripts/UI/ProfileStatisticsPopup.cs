﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileStatisticsPopup : MonoBehaviour
{
    [SerializeField]
    private Text playername;
    [SerializeField]
    private GameObject dots;
    [SerializeField]
    private Text rank;
    [SerializeField]
    private Text coins;
    [SerializeField]
    private Text points;
    [SerializeField]
    private Text level;
    [SerializeField]
    private Text totalMoneyWin;
    [SerializeField]
    private Text gamesWon;
    [SerializeField]
    private Text timePlayed;
    [SerializeField]
    private Text ballsPotted;
    [SerializeField]
    private Text winStreak;
    [SerializeField]
    private Image avatar;
    [SerializeField]
    private Image facebookAvatar;
    [SerializeField]
    private GameObject facebookAvatarBack;
    [SerializeField]
    private Text cueName;
    [SerializeField]
    private GameObject[] achivs;
    [SerializeField]
    private Image visualProgress;
    [SerializeField]
    private GameObject button;
    [SerializeField]
    private GameObject[] lastAchivs;
    [SerializeField]
    private AchievementController achivsController;
    [SerializeField]
    private UICueController cueController;
    [SerializeField]
    private UnityEngine.UI.Button achievementsButton, cuesButton;
    [SerializeField]
    private Image[] forceValue, aimValue, spinValue;
    [SerializeField]
    private Image cueImage;

    private int maxNameLength = 9;
    private PlayerData playerData;
    bool online;

    private int lastFourAchivs = 4;

    void OnEnable()
    {
        Init();
    }

    void OnDisable()
    {
        cueController.selectCue -= SetSelectedCue;
    }

    private void Init()
    {
        if (playerData == null)
            return;

        achivsController.SetAchievements(playerData.achievements);

        bool isAuthUser = GameSparksManager.Instance.AuthorizedPlayerData.playerId.Equals(playerData.playerId);

        if (online)
            cueController.SetCues(CuesStorage.Instance.GetOwned(playerData.CuesList), playerData.playerId);

        achievementsButton.interactable = online;
        cuesButton.interactable = online;

        //Debug.Log("<b>Player's current cue: </b>" + playerData.currentCue);

        bool isGuest = playerData.displayName.Equals("Guest");
        string playerName = isGuest ? playerData.displayName + playerData.playerId : playerData.displayName;

        bool isNameLong = playerName.Length > maxNameLength;
        playername.text = isNameLong ? playerName.Substring(0, maxNameLength) : playerName;
        ///If name length greater than 9 characters, then enable dots object
        ///to show user  that player name is longer than 12 characters
        dots.SetActive(isNameLong);
        playername.alignment = isNameLong ? TextAnchor.MiddleRight : TextAnchor.MiddleCenter;
        
        rank.text = GameData.ranks[playerData.statistic.level - 1];
        coins.text = isAuthUser ? playerData.coins.ToString() + " coins" : "";

		//points.text = playerData.statistic.score.ToString() + " of " + GameData.Instance.pointsForLevel[playerData.statistic.level];
		points.text = playerData.statistic.score.ToString() + " of " + LevelPointsStorage.Instance.GetNextLevelPoints(playerData.statistic.level);
		level.text = playerData.statistic.level.ToString();
        totalMoneyWin.text = playerData.statistic.winningsCoins.ToString();
        gamesWon.text = playerData.statistic.wonGames.ToString();
        timePlayed.text = GetTimeInGame();
        ballsPotted.text = playerData.statistic.pottedBalls.ToString();
        winStreak.text = playerData.statistic.winStreak.ToString();

        bool isFacebook = playerData.facebookId != "";
        if (isFacebook)
        {
            facebookAvatarBack.SetActive(true);
            avatar.gameObject.SetActive(false);
            facebookAvatar.sprite = playerData.avatar;
        }
        else
        {
            facebookAvatarBack.SetActive(false);
            avatar.gameObject.SetActive(true);
            avatar.sprite = playerData.avatar;
        }

        int playerLevel = playerData.statistic.level;
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(playerLevel);
        visualProgress.fillAmount = playerData.statistic.score / ((float)pointForNextLevel);

        CueEntity selectedCue = CuesStorage.Instance.Cues[(new VirtualGood(playerData.currentCue)).num];

        if (isAuthUser)
            GameData.Instance.selectedCue = CueEntity.DeepCopy(selectedCue);

        SetSelectedCue(online ? selectedCue : GameData.Instance.selectedCue);
        
        FillCueValue(forceValue, GameData.Instance.selectedCue.force);
        FillCueValue(aimValue, GameData.Instance.selectedCue.aim);
        FillCueValue(spinValue, GameData.Instance.selectedCue.spin);

        ///Determine to display "Log out" button
        if (button != null)
        {
            if (isAuthUser && !GameData.Instance.isInGame && online)
            {
                button.SetActive(true);
            }
            else
            {
                button.SetActive(false);
            }
        }

        ///Displaying last 4 earned achievements or less or hiding all objects to display them
        List<Achievement> earnedAchivs = playerData.achievements == null ? null : playerData.achievements.FindAll(x => x.earned == true);

        int achivsLength = earnedAchivs == null ? 0 : earnedAchivs.Count;

        if (achivsLength > 0 && online)
        {
            int achivsToShow = lastFourAchivs;
            if (achivsLength < lastFourAchivs)
            {
                achivsToShow = achivsLength;
            }
            //SetLastAchivs(earnedAchivs.GetRange(earnedAchivs.Count - achivsToShow, achivsToShow));
            SetLastAchivs(earnedAchivs, playerData.earnedAchievements, achivsToShow);
        }
        else
        {
            HideAllLastAchivs();
        }

        cueController.selectCue -= SetSelectedCue;
        cueController.selectCue += SetSelectedCue;

    }

    void SetSelectedCue(CueEntity cue)
    {
        cueImage.sprite = CuesStorage.Instance.cueImages[cue.cueId];
        cueName.text = cue.cueName;
    }

    void HideAllLastAchivs()
    {
        for (int i = 0; i < achivs.Length; i++)
        {
            achivs[i].GetComponent<AchievementLast>().SwitchOff();
        }
    }

    //void SetLastAchivs(List<Achievement> lastAchivsGot)
    //{
    //    for (int i = 0; i < lastAchivsGot.Count; i++)
    //    {
    //        Image img = achivs[i].GetComponent<Image>();
    //        if (!img.enabled)
    //            img.enabled = true;
    //        achivs[i].GetComponent<AchievementLast>().SetAchivImage(Resources.Load<Sprite>(PathToResources.Instance.pathToHDAchievements + lastAchivsGot[i].shortCode));
    //    }
    //    for (int i = achivs.Length - 1; i + 1  > lastAchivsGot.Count; i--)
    //    {
    //        achivs[i].GetComponent<AchievementLast>().SwitchOff();
    //    }
    //}

    void SetLastAchivs(List<Achievement> earnedAchievements, List<string> lastSortAchievements, int countShow)
    {
        for (int i = lastSortAchievements.Count - 1, j = 0; i >= lastSortAchievements.Count - countShow && i >= 0; i--, j++)
        {
            Achievement achievement = earnedAchievements.Find(x => x.shortCode.ToString() == lastSortAchievements[i]);
            Image img = achivs[j].GetComponent<Image>();
            if (!img.enabled)
                img.enabled = true;
            achivs[j].GetComponent<AchievementLast>().SetAchivImage(Resources.Load<Sprite>(PathToResources.Instance.pathToHDAchievements + achievement.shortCode));
        }
        for (int i = achivs.Length - 1; i + 1 > countShow; i--)
        {
            achivs[i].GetComponent<AchievementLast>().SwitchOff();
        }
    }

    string GetTimeInGame()
    {
        long timeInGame = playerData.statistic.timeInGame + (playerData.online ? GameSparksManager.Instance.TimeInGame : 0);
        long hours = timeInGame / 3600;
        long minutes = timeInGame % 3600 / 60;
        return hours + "h " + minutes + "m";
    }

    public void SetProfileToView(string playerId, bool isOnline, Action callback)
    {
        GameSparksManager.Instance.GetPlayerData(playerId, (respPlayerData) =>
        {
            playerData = respPlayerData;           
            online = isOnline;
            callback();
        });
    }

    public void SetProfileToView(PlayerData data, bool isOnline, Action callback)
    {
        playerData = data;        
        online = isOnline;
        callback();
    }

    public void LogOut()
    {
        SoundManager.Instance.PlayButtonPress();
        switch (GameData.Instance.User.Type)
        {
            case ProfileType.Normal:
            case ProfileType.Guest:
                GameSparksManager.Instance.EndSession();
                break;
#if !UNITY_WEBGL

            case ProfileType.FB:
                GameSparksManager.Instance.EndSession();
                FBManager.Instance.FacebookEndSession();
                break;
#endif       
        }
        GameData.Instance.freecoinsTimerStarted = false;

        GameData.Instance.isLogoutbyUser = true;
        GameData.Instance.isInGame = false;
        GameData.Instance.RemoveUserProfile();
        GameData.Instance.Save();
        MenuManager.Instance.LoadLevel(MenuManager.Instance.loginSceneName);
    }

    void FillCueValue(Image[] source, int value)
    {
        for (int i = 0; i < value; i++)
        {
            source[i].enabled = true;
        }
        for (int i = value; i < source.Length; i++)
        {
            source[i].enabled = false;
        }

    }

}
