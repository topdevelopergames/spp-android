﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResetScroll : MonoBehaviour {

    [SerializeField]
    private ScrollRect scroll;


    void OnDisable()
    {
        scroll.horizontalNormalizedPosition = 0;
    }

}
