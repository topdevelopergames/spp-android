﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScoreTable : MonoBehaviour {

    [SerializeField]
    private Text player1Score, player1Name;
    [SerializeField]
    private Text player2Score, player2Name;
    [SerializeField]
    private Text player3Score, player3Name;
    [SerializeField]
    private PlayersListController playersListController;
    [SerializeField]
    private GameObject playersList;

    private List<MultiplayerData> players;

    private static ScoreTable _instance;

    public static ScoreTable Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        ShowList();
    }

    void OnEnable()
    {
        HideList();
    }

    public void SetPlayer1Score(long score)
    {
        player1Score.text = score.ToString();
    }

    public void SetPlayer2Score(long score)
    {
        player2Score.text = score.ToString();
    }

    public void SetPlayer3Score(long score = long.MinValue)
    {
        if(score == long.MinValue)
        {
            player3Score.text = "";
            return;
        }
        player3Score.text = score.ToString();
    }

    public void SetPlayer1Name(string name)
    {
        player1Name.text = name;
    }

    public void SetPlayer2Name(string name)
    {
        player2Name.text = name;
    }

    public void SetPlayer3Name(string name)
    {
        player3Name.text = name;
    }

    public void SetPlayers(List<MultiplayerData> multiplayerData)
    {
        if (multiplayerData == null)
            return;

        int countToShow = 3;
        if (multiplayerData.Count < countToShow)
        {
            countToShow = multiplayerData.Count;
        }
        Debug.Log("How many players are shown: " + countToShow);
        players = multiplayerData;
        UpdateTop(players.GetRange(0, countToShow));
        playersListController.SetPlayers(multiplayerData);

    }

    public void UpdateTop(List<MultiplayerData> data)
    {
        bool isGuest = data[0].player.displayName.Equals("Guest");
        string user1Name = isGuest ? data[0].player.displayName + data[0].player.playerId : data[0].player.displayName;
        SetPlayer1Name(user1Name);
        SetPlayer1Score(data[0].score);
        if (data.Count > 1)
        {
            isGuest = data[1].player.displayName.Equals("Guest");
            string user2Name = isGuest ? data[1].player.displayName + data[1].player.playerId : data[1].player.displayName;
            SetPlayer2Name(user2Name);
            SetPlayer2Score(data[1].score);
            SetPlayer3Name("");
            SetPlayer3Score();
        }
        if (data.Count > 2)
        {
            isGuest = data[2].player.displayName.Equals("Guest");
            string user3Name = isGuest ? data[2].player.displayName + data[2].player.playerId : data[2].player.displayName;
            SetPlayer3Name(user3Name);
            SetPlayer3Score(data[2].score);
        }
    }

    public void ShowPlayersList()
    {
        SoundManager.Instance.PlayButtonPress();
        playersList.SetActive(!playersList.activeInHierarchy);
        if (playersList.activeInHierarchy)
            playersListController.SetPlayers(players);
    }

    void ShowList()
    {
        playersList.SetActive(true);
    }

    void HideList()
    {
        playersList.SetActive(false);
    }

}
