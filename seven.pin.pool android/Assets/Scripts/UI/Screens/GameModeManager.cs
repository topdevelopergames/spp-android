﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using GameSparks.Core;

public class GameModeManager : MonoBehaviour {

    [SerializeField]
    private Text prizeTimeMode;
    [SerializeField]
    private Text prize21;
    [SerializeField]
    private Dropdown timeModes;
    [SerializeField]
    private Dropdown pointsModes;
    [SerializeField]
    private Dropdown feeValuesTimeMode, feeValues21;
    [SerializeField]
    private GameObject opponentPicker, modePicker;

    [SerializeField]
    private UnityEngine.UI.Button freeCoins;
    [SerializeField]
    private GameObject noNetworkMessagePopup;
    [SerializeField]
    private GameObject notEnoughMoneyPopup;
    [SerializeField]
    private GameObject practiceItem, timeModeItem;
    [SerializeField]
    private Text freecoinsCaption;

    [SerializeField]
    private GameObject friendsList;

    private Coroutine timerUpdate;
    private bool timerUpdateStarted;

    private static GameModeManager _instance;

    public static GameModeManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        timerUpdateStarted = false;
        

    }

    void OnEnable()
    {

        switch(GameData.Instance.singleOrMulti)
        {
            case 0:
                practiceItem.SetActive(true);
                timeModeItem.SetActive(false);
                break;

            case 1:
                practiceItem.SetActive(false);
                timeModeItem.SetActive(true);
                break;
        }

        InitFeesDropdown();

        modePicker.SetActive(true);
        opponentPicker.SetActive(false);

        bool isInteractable = GS.Available;
        if(freeCoins != null)
            freeCoins.interactable = isInteractable;
        if (isInteractable)
        {
            MenuManager.Instance.enableButtonDelegate -= EnableFreecoinsButton;
            MenuManager.Instance.enableButtonDelegate += EnableFreecoinsButton;
            MenuManager.Instance.disableButtonDelegate -= DisableFreecoinsButton;
            MenuManager.Instance.disableButtonDelegate += DisableFreecoinsButton;
            MenuManager.Instance.freecoinsTimeReceived -= GotFreecoinsTime;
            MenuManager.Instance.freecoinsTimeReceived += GotFreecoinsTime;

            MenuManager.Instance.StartFreeCoinsTimer();

            
        }
    }

    #region Dropdowns setup
    public void UpdatePrizeTimeMode(string fee)
    {
        string prize = GameData.Instance.singleOrMulti == 0 ? fee : "min " + fee;
        prizeTimeMode.text = prize;
    }

    public void UpdatePrize21(string fee)
    {
        string prize = GameData.Instance.singleOrMulti == 0 ? fee : "min " + fee;
        prize21.text = prize;
    }

    public void SetTime(int time)
    {
        GameData.Instance.timeForGame = time;
        GameData.Instance.Save();
    }

    public void SetPoints(int points)
    {
        GameData.Instance.pointsForGame = points;
        GameData.Instance.Save();
    }

    public void SetFee(int fee)
    {
        GameData.Instance.entryFee = fee;
        switch(GameData.Instance.singleOrMulti)
        {
            ///single mode
            case 0:
                UpdatePrize21(GameData.prizesText[fee]);
                //UpdatePrizeTimeMode(GameData.prizesText[fee]);
                break;
            ///multiplayer mode
            case 1:
                UpdatePrizeTimeMode(GameData.prizesText[fee]);
                UpdatePrize21(GameData.prizesText[fee]);
                feeValues21.value = fee;
                feeValuesTimeMode.value = fee;
                break;
        }
        GameData.Instance.Save();
    }

    void InitFeesDropdown()
    {
        timeModes.ClearOptions();

        Dropdown.OptionData option1 = new Dropdown.OptionData("1 minute");
        Dropdown.OptionData option2 = new Dropdown.OptionData("3 minutes");
        //Dropdown.OptionData option3 = new Dropdown.OptionData("5 minutes");

        List<Dropdown.OptionData> optionsList = new List<Dropdown.OptionData>();
        optionsList.Add(option1);
        optionsList.Add(option2);
        //optionsList.Add(option3);

        timeModes.AddOptions(optionsList);

        timeModes.value = GameData.Instance.timeForGame;
        pointsModes.value = GameData.Instance.pointsForGame;
        feeValuesTimeMode.value = GameData.Instance.entryFee;
        feeValues21.value = GameData.Instance.entryFee;

        UpdatePrize21(GameData.prizesText[GameData.Instance.entryFee]);
        UpdatePrizeTimeMode(GameData.prizesText[GameData.Instance.entryFee]);
    }
    #endregion Dropdowns setup

    public void Play()
    {
        SoundManager.Instance.PlayButtonPress();
        bool isOnline = GS.Available;
//        bool isEnoughMoney = GameData.entryFees[GameData.Instance.entryFee] <= GameSparksManager.Instance.AuthorizedPlayerData.coins;
        bool isGuest = GameData.Instance.User.Type == ProfileType.Guest;
        
        ///Load training
        if(GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 0)
        {
            GameData.Instance.isInGame = true;
            MenuManager.Instance.LoadTraining();
            return;
        }

        ///Display no connection popup if we are offline
        if (!isOnline)
        {
            noNetworkMessagePopup.SetActive(!isOnline);
            return;
        }
        ///Display not enough money popup if no money for game
//        if (!isEnoughMoney)
//        {
//            notEnoughMoneyPopup.SetActive(true);
//            return;
//        }

        GameData.Instance.isInGame = true;
        ///Show Popup with list of friends
        if (GameData.Instance.playWithFriends)
        {
            friendsList.SetActive(true);
            return;
        }

        GameData.Instance.needToUpdateInfo = true;

        ///Load single player 21 mode
        if (GameData.Instance.selectedGameMode == 1 && GameData.Instance.singleOrMulti == 0)
        {
            opponentPicker.SetActive(true);
            modePicker.SetActive(false);
            return;
        }

        ///Load multiplayer 21 or time mode
        MenuManager.Instance.LoadTraining();
    }
  
    public void BackToMainMenu()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.BackToMainMenu();
    }

    public void FreeCoins()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.FreeCoins(CoinsAdded);
        DisableFreecoinsButton();
        MenuManager.Instance.disableButtonDelegate -= DisableFreecoinsButton;
    }
      
    
    #region Freecoins

    void CoinsAdded()
    {
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
        MenuManager.Instance.StartFreeCoinsTimer();
    }

    void EnableFreecoinsButton()
    {
        Debug.Log("Time to enable freecoins button");
        if (freeCoins != null)
            freeCoins.interactable = true;
        if (freecoinsCaption != null)
            freecoinsCaption.text = "Free coins";
        if (timerUpdate != null && this != null)
        {
            timerUpdateStarted = false;
            StopCoroutine(timerUpdate);

        }
    }

    void DisableFreecoinsButton()
    {
        Debug.Log("Disable freecoins button");
        if (freeCoins != null)
            freeCoins.interactable = false;
    }

    void GotFreecoinsTime()
    {
        Debug.Log("Received time for freecoins timer");
        if (!timerUpdateStarted && this != null)
        {
            timerUpdateStarted = true;
            timerUpdate = StartCoroutine(UpdateTimer());
        }
    }

    IEnumerator UpdateTimer()
    {
        Debug.Log("Updating freecoins timer value");
        while (MenuManager.Instance.FreeCoinsTimeLeft > 0)
        {
            SetTimerTime(MenuManager.Instance.FreeCoinsTimeLeft);
            yield return new WaitForSeconds(1);
        }
    }

    void SetTimerTime(int time)
    {
        int minutes = time / 60;
        int seconds = time % 60;
        int decSeconds = seconds / 10;
        int secs = seconds % 10;
        freecoinsCaption.text = string.Format("Left {0}:{1}{2}", minutes, decSeconds, secs);
    }

    #endregion

    void OnDisable()
    {
        Debug.Log("On disable gamemodemanager");
        _instance = null;
        MenuManager.Instance.disableButtonDelegate -= DisableFreecoinsButton;
        MenuManager.Instance.enableButtonDelegate -= EnableFreecoinsButton;
        MenuManager.Instance.freecoinsTimeReceived -= GotFreecoinsTime;
        if (timerUpdate != null)
        {
            timerUpdateStarted = false;
            StopCoroutine(timerUpdate);
        }
    }
}
