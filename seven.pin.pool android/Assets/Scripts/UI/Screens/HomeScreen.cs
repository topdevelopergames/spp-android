﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GameSparks.Core;

public class HomeScreen : MonoBehaviour {

    [SerializeField]
    private UnityEngine.UI.Button withPockets;

    [SerializeField]
    private UnityEngine.UI.Button withoutPockets;

    [SerializeField]
    private UnityEngine.UI.Button poolShop, playWithFriends, globalRank, facebookBtn;
    [SerializeField]
    private Sprite withPocketSprite, withoutPocketSprite, withPocketSpriteSelected, withoutPocketSpriteSelected, withPocketsHighlited, withoutPocketsHiglighted, withPocketsSelectedHighlted, withoutPocketsSelectedHiglighted;
    [SerializeField]
    private UnityEngine.UI.Button freecoinsButton;
    [SerializeField]
    private Text freecoinsButtonCaption;

    private Coroutine timerUpdate;
    private bool timerUpdateStarted;

    private static HomeScreen _instance;

    public static HomeScreen Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        timerUpdateStarted = false;
    }
    
    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        ///Checking connection to the Network. If it's none connection - disable interation
        ///for buttons, that depends on Network and hide all offers and minigames.
        bool isInteractable = GS.Available;
        if (poolShop != null)
            poolShop.interactable = isInteractable;
#if UNITY_WEBGL
        if (playWithFriends != null)
            playWithFriends.gameObject.SetActive(false);
#endif
#if !UNITY_WEBGL
        if (playWithFriends != null)
            playWithFriends.gameObject.SetActive(isInteractable && GameSparksManager.Instance.AuthorizedPlayerData.facebookId != string.Empty);
#endif
        if (globalRank != null)
            globalRank.interactable = isInteractable;
        if (facebookBtn != null)
            facebookBtn.interactable = isInteractable;
        if (freecoinsButton != null)
            freecoinsButton.interactable = isInteractable;

        SetSelectedTable();
        if (isInteractable)
        {
            MenuManager.Instance.enableButtonDelegate -= EnableFreecoinsButton;
            MenuManager.Instance.enableButtonDelegate += EnableFreecoinsButton;
            MenuManager.Instance.disableButtonDelegate -= DisableFreecoinsButton;
            MenuManager.Instance.disableButtonDelegate += DisableFreecoinsButton;
            MenuManager.Instance.freecoinsTimeReceived -= GotFreecoinsTime;
            MenuManager.Instance.freecoinsTimeReceived += GotFreecoinsTime;

            MenuManager.Instance.StartFreeCoinsTimer();
            GameData.Instance.ownedPacks = ChatPackStorage.Instance.GetOwned(GameSparksManager.Instance.AuthorizedPlayerData.ChatList);

        }
        
        System.Collections.Generic.List<ShopitemEntity> ownedPacks = isInteractable ? ChatPackStorage.Instance.GetOwned(GameSparksManager.Instance.AuthorizedPlayerData.ChatList) : GameData.Instance.ownedPacks;

        foreach (var item in ownedPacks)
        {
            MessagesForMultiplayer.AddNewMessages(ChatMessagesStorage.Instance.GetPackMessage(item.id));
        }

    }

    #region buttons handlers

    void SetSelectedTable()
    {
        bool isWithPockets = GameData.Instance.playTable == 0;
        withPockets.image.sprite = isWithPockets ? withPocketSpriteSelected : withPocketSprite;
        SpriteState sWithPockets = withPockets.spriteState;
        sWithPockets.highlightedSprite = isWithPockets ? withPocketsSelectedHighlted : withPocketsHighlited;
        withPockets.spriteState = sWithPockets;

        withoutPockets.image.sprite = isWithPockets ? withoutPocketSprite : withoutPocketSpriteSelected;
        SpriteState sWithoutPockets = withoutPockets.spriteState;
        sWithoutPockets.highlightedSprite = isWithPockets ? withoutPocketsHiglighted : withoutPocketsSelectedHiglighted;
        withoutPockets.spriteState = sWithoutPockets;
    }

    public void SetWithPockets()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.playTable = 0;
        GameData.Instance.Save();
        SetSelectedTable();
    }

    public void SetWithoutPockets()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.playTable = 1;
        GameData.Instance.Save();
        SetSelectedTable();
    }

    public void Play()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.playWithFriends = false;
        MenuManager.Instance.LoadLevel(MenuManager.Instance.singleOrMultiSceneName);
    }

    public void PlayWithFriends()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.playWithFriends = true;
        GameData.Instance.singleOrMulti = 1;
        GameData.Instance.Save();
        MenuManager.Instance.LoadLevel(MenuManager.Instance.gameModeSceneName);
    }

    public void PoolShop()
    {
        SoundManager.Instance.PlayButtonPress();
        if (GS.Available)
        {
            MenuManager.Instance.LoadPoolShop();
        }
    }
    #endregion button handlers  

    #region free coins
    public void FreeCoins()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.FreeCoins(CoinsAdded);
        DisableFreecoinsButton();
        MenuManager.Instance.disableButtonDelegate -= DisableFreecoinsButton;
    }

    void CoinsAdded()
    {       
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
        MenuManager.Instance.StartFreeCoinsTimer();
    }

    void EnableFreecoinsButton()
    {
        Debug.Log("Time to enable freecoins button");
        if(freecoinsButton != null)
            freecoinsButton.interactable = true;
        if(freecoinsButtonCaption != null)
            freecoinsButtonCaption.text = "Free coins";
        if (timerUpdate != null && this != null)
        {
            StopCoroutine(timerUpdate);
            timerUpdateStarted = false;
        }
    }

    void DisableFreecoinsButton()
    {
        Debug.Log("Disable freecoins button");
        if(freecoinsButton != null)
            freecoinsButton.interactable = false;
    }

    void GotFreecoinsTime()
    {
        Debug.Log("Received time for freecoins timer");
        if (!timerUpdateStarted && this != null)
        {
            timerUpdateStarted = true;
            timerUpdate = StartCoroutine(UpdateTimer());

        }
    }

    IEnumerator UpdateTimer()
    {
        Debug.Log("Updating freecoins timer value");
        while(MenuManager.Instance.FreeCoinsTimeLeft > 0)
        {
            SetTimerTime(MenuManager.Instance.FreeCoinsTimeLeft);
            yield return new WaitForSeconds(1);
        }
    }

    void SetTimerTime(int time)
    {
        int minutes = time / 60;
        int seconds = time % 60;
        int decSeconds = seconds / 10;
        int secs = seconds % 10;
        freecoinsButtonCaption.text = string.Format("Left {0}:{1}{2}", minutes, decSeconds, secs);
    }
    #endregion free coins

    void OnDisable()
    {
        Debug.Log("On disable homescreen");
        MenuManager.Instance.disableButtonDelegate -= DisableFreecoinsButton;
        MenuManager.Instance.enableButtonDelegate -= EnableFreecoinsButton;
        MenuManager.Instance.freecoinsTimeReceived -= GotFreecoinsTime;
        if (timerUpdate != null)
        {
            timerUpdateStarted = false;
            StopCoroutine(timerUpdate);
        }
    }

}
