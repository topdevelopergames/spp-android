﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Settings : MonoBehaviour {

    [SerializeField]
    private UnityEngine.UI.Image leftPosImage, rightPosImage, horizontalPosImage, verticalPosImage, soundImage;

    [SerializeField]
    private Sprite selectedLeft, selectedRight, selectedVertical, selectedHorizontal;
    [SerializeField]
    private Sprite unselectedLeft, unselectedRight, unselectedVertical, unselectedHorizontal;
    [SerializeField]
    private Sprite soundEnabled, soundDisabled;
    [SerializeField]
    private ChatMessageController chatController;
    [SerializeField]
    private GameObject chatOrderPopup;
    [SerializeField]
    private float duration, endFadeValue;
    [SerializeField]
    private Vector3 endScale;

    [SerializeField]
    private UnityEngine.UI.Button changeNameButton;

    private int vert, horiz;

    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        Debug.Log("slider position: " + GameData.Instance.forceSliderPosition);
        switch(GameData.Instance.forceSliderPosition)
        {
            case 0:               
                SetVertLeftState();
                break;
            case 1:
                SetVertRightState();
                break;
            case 2:
                SetHorizRightState();
                break;
            case 3:
                SetHorizLeftState();
                break;
        }
        bool canChangeName = GameData.Instance.User != null && (GameSparks.Core.GS.Available || GameData.Instance.User.Type != ProfileType.GuestOffline) && GameData.Instance.User.Type != ProfileType.Guest;
        changeNameButton.interactable = canChangeName;
        soundImage.sprite = GameData.Instance.soundEnabled ? soundEnabled : soundDisabled;
    }

    void SetVertLeftState()
    {
        SoundManager.Instance.PlayButtonPress();
        leftPosImage.sprite = selectedLeft;
        verticalPosImage.sprite = selectedVertical;
        rightPosImage.sprite = unselectedRight;
        horizontalPosImage.sprite = unselectedHorizontal;
        horiz = vert = 0;
    }

    void SetVertRightState()
    {
        SoundManager.Instance.PlayButtonPress();
        leftPosImage.sprite = unselectedLeft;
        verticalPosImage.sprite = selectedVertical;
        rightPosImage.sprite = selectedRight;
        horizontalPosImage.sprite = unselectedHorizontal;
        horiz = 1;
        vert = 0;
    }

    void SetHorizRightState()
    {
        SoundManager.Instance.PlayButtonPress();
        leftPosImage.sprite = unselectedLeft;
        verticalPosImage.sprite = unselectedVertical;
        rightPosImage.sprite = selectedRight;
        horizontalPosImage.sprite = selectedHorizontal;
        horiz = vert = 1;
    }

    void SetHorizLeftState()
    {
        SoundManager.Instance.PlayButtonPress();
        leftPosImage.sprite = selectedLeft;
        verticalPosImage.sprite = unselectedVertical;
        rightPosImage.sprite = unselectedRight;
        horizontalPosImage.sprite = selectedHorizontal;
        horiz = 0;
        vert = 1;
    }

    public void SetVertHoriz(int value)
    {
        SoundManager.Instance.PlayButtonPress();
        vert = value;
        switch(value)
        {
            ///vertical
            case 0:
                verticalPosImage.sprite = selectedVertical;
                horizontalPosImage.sprite = unselectedHorizontal;
                break;
            ///horizontal
            case 1:
                verticalPosImage.sprite = unselectedVertical;
                horizontalPosImage.sprite = selectedHorizontal;
                break;

        }
    }

    public void SetLeftRight(int value)
    {
        SoundManager.Instance.PlayButtonPress();
        horiz = value;
        switch (value)
        {
            ///left
            case 0:
                leftPosImage.sprite = selectedLeft;
                rightPosImage.sprite = unselectedRight;
                break;
            ///right
            case 1:
                leftPosImage.sprite = unselectedLeft;
                rightPosImage.sprite = selectedRight;
                break;

        }
    }


    void OnDisable()
    {
        ///left location
        if(horiz == 0)
        {
            if(vert == 0)
            {
                ///vertical
                GameData.Instance.forceSliderPosition = 0;
            }
            else
            {
                ///horizontal
                GameData.Instance.forceSliderPosition = 3;
            }
        }
        ///right location
        else
        {
            if (vert == 0)
            {
                ///vertical
                GameData.Instance.forceSliderPosition = 1;
            }
            else
            {
                ///horizontal
                GameData.Instance.forceSliderPosition = 2;
            }
        }
        GameData.Instance.Save();
   }    
    
    public void ShowChatMessagePopup()
    {
        SoundManager.Instance.PlayButtonPress();
        chatController.SetMessages(MessagesForMultiplayer.GetListMessages());
		chatOrderPopup.SetActive(true);
    }

    public void ShowPopup(GameObject popup)
    {
        SoundManager.Instance.PlayButtonPress();
        popup.SetActive(true);
    }

    public void SwitchSound()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.soundEnabled = !GameData.Instance.soundEnabled;
        soundImage.sprite = GameData.Instance.soundEnabled ? soundEnabled : soundDisabled;
    }

    public void ClosePopup()
    {
        SoundManager.Instance.PlayButtonPress();        
        if (transform.childCount <= 0)
        {
            gameObject.SetActive(false);
            return;
        }
        Transform t = transform.GetChild(0);
        if (t == null)
            return;
        t.DOScale(endScale, duration).OnComplete( () => { gameObject.SetActive(false); });
        UnityEngine.UI.Image img = t.GetComponent<UnityEngine.UI.Image>();
        if (img == null)
            return;
        img.DOFade(endFadeValue, duration);
    }

}
