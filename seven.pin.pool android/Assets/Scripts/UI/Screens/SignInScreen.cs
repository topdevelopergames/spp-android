﻿using UnityEngine;

public class SignInScreen : MonoBehaviour {

    [SerializeField]
    private UnityEngine.UI.Button play, playAsGuest, signUp, signIn, signUpBig;

#if !UNITY_WEBGL
    void Start()
    {
        FBManager.Instance.NotLoginAuthCallback = PlayFacebookFailed;
    }

    void OnEnable()
    {
        PlayFacebookFailed();
    }

    public void PlayFacebook()
    {
        SoundManager.Instance.PlayButtonPress();
        if (play != null)
            play.interactable = false;
        if (playAsGuest != null)
            playAsGuest.interactable = false;
        if (signUp != null)
            signUp.interactable = false;
        if (signIn != null)
            signIn.interactable = false;
        if (signUpBig != null)
            signUpBig.interactable = false;
    }

    public void PlayFacebookFailed()
    {
        if (play != null)
            play.interactable = true;
        if (playAsGuest != null)
            playAsGuest.interactable = true;
        if (signUp != null)
            signUp.interactable = true;
        if (signIn != null)
            signIn.interactable = true;
        if (signUpBig != null)
            signUpBig.interactable = true;
    }
#endif

    public void SignUp()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.LoadLevel(MenuManager.Instance.signUpSceneName);
    }

    

}
