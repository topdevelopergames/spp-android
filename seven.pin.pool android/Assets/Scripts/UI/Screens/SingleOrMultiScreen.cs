﻿using UnityEngine;
using System.Collections;

public class SingleOrMultiScreen : MonoBehaviour {

    [SerializeField]
    private UnityEngine.UI.Button multiplayer;
    


    void OnEnable()
    {
//        bool isInteractable = GameData.Instance.User.Type != ProfileType.GuestOffline;
//        if (multiplayer != null)
//            multiplayer.interactable = isInteractable;

    }


    public void Multiplayer()
    {
        SoundManager.Instance.PlayButtonPress();
        //SkillzCrossPlatform.LaunchSkillz (FindObjectOfType <MySkillzDelegate> ());
		GameData.Instance.gameMode = GameTypeMode.Multiplayer;
        MenuManager.Instance.MultiPlayer(MenuManager.Instance.gameModeSceneName);
    }

    public void SinglePlayer()
    {
		SkillzMatchRules matchRules = new SkillzMatchRules (SkillzMatchType.None, 0, 0);
		GameData.Instance.matchRules = matchRules;
        SoundManager.Instance.PlayButtonPress();
		GameData.Instance.selectedGameMode = 0;
		GameData.Instance.singleOrMulti = 0;
		GameData.Instance.gameMode = GameTypeMode.SinglePlayer;
		if(GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 0)
		{
			GameData.Instance.isInGame = true;
			MenuManager.Instance.LoadTraining();
			return;
		}
        MenuManager.Instance.SinglePlayer(MenuManager.Instance.gameModeSceneName);
    }
}
