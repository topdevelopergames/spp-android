﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrollHideElement : MonoBehaviour {

    [SerializeField]
    private Image target;
    [SerializeField]
    private bool left = true;
    
    public void OnScrollValueChanged(Vector2 value)
    {
        if (left)
        {
            if (value.x < 0.02f)
            {
                float alpha = value.x / 0.02f;
                target.color = new Color(target.color.r, target.color.g, target.color.b, alpha);
                //target.enabled = false;
            }
            else
                //target.enabled = true;
                target.color = new Color(target.color.r, target.color.g, target.color.b, 1);
        }
        else
        {
            if (value.x > 0.98f)
            {
                float alpha = 1 - (value.x - 0.98f) / 0.02f ;
                target.color = new Color(target.color.r, target.color.g, target.color.b, alpha);
                //target.enabled = false;
            }
            else
                //target.enabled = true;
                target.color = new Color(target.color.r, target.color.g, target.color.b, 1);
        }
            
    }

}
