﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeNamePopup : MonoBehaviour {

    [SerializeField]
    private Popup popup;
    [SerializeField]
    private Text currentName, error;
    [SerializeField]
    private InputField newNameField;
    [SerializeField]
    private GameObject dots;
    [SerializeField]
    private UnityEngine.UI.Button confirmButton;

    private int maxNameLength = 9;
    private string newName;

    void OnEnable()
    {
        newNameField.text = string.Empty;
        string username = GameSparksManager.Instance.AuthorizedPlayerData.displayName;
        SetName(username);
        newName = string.Empty;
        error.text = string.Empty;
    }
    
    public void OnEndEdit(string text)
    {
        newName = text;
    }

    public void ConfirmChange()
    {
        SoundManager.Instance.PlayButtonPress();
        if (newName.Length <= 0)
        {
            error.text = "Name must not be empty";
            newNameField.text = string.Empty;
            return;
        }
        GameSparksManager.Instance.ChangePlayerDisplayName(newName, OnSuccessNameChanged);
        //confirmButton.interactable = false;
        popup.CloseScaleFadePopup(this.gameObject);
    }

    void OnSuccessNameChanged()
    {
        confirmButton.interactable = true;
        if (MenuUIManager.Instance != null)
            MenuUIManager.Instance.SetUserName(GameSparksManager.Instance.AuthorizedPlayerData.displayName);
        SetName(GameSparksManager.Instance.AuthorizedPlayerData.displayName);
    }
    

    void OnDisable()
    {
        confirmButton.interactable = true;
    }

    void SetName(string username)
    {
        dots.SetActive(username.Length > maxNameLength);
        currentName.text = username.Length > maxNameLength ? username.Substring(0, maxNameLength) : username;
        currentName.alignment = username.Length > maxNameLength ? TextAnchor.MiddleRight : TextAnchor.MiddleCenter;
    }
}
