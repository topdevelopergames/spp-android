﻿using UnityEngine;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using System;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;


public class ChatMessageController : MonoBehaviour, IEnhancedScrollerDelegate {

    [SerializeField]
    private EnhancedScroller	scroller;

    [SerializeField]
    private ChatMessageItem messagePrefab;

    [SerializeField]
    private ReorderableList reorderableList;

    private List<string> messages;
    private ReorderableListContent listContent;

    private float stepShift;
    private int messagesPerPage = 6;

    private bool firstTime;

    private Coroutine moveOnOneItem;

	void Start()
	{
		reorderableList.ContentLayout = scroller.ScrollRect.content.GetComponent<LayoutGroup>();
		listContent = scroller.ScrollRect.content.gameObject.AddComponent<ReorderableListContent>();
		listContent.Init(reorderableList);
	}

    void OnEnable()
    {
		if (scroller.ScrollRect == null)
			scroller.RunAwake();
        scroller.Delegate = this;
        firstTime = true;
	}

    void OnDisable()
    {
		firstTime = false;
	}

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        ChatMessageItem messageItem = scroller.GetCellView(messagePrefab) as ChatMessageItem;
		messageItem.SetMessageItem(messages[dataIndex], !MessagesForMultiplayer.IsHiddenMessage(messages[dataIndex]), HideCallback, ShowCallback);
        return messageItem;        
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        float cellSize = 150f;
        LayoutElement element = messagePrefab.GetComponent<LayoutElement>();
        if (element != null)
            cellSize = element.preferredHeight;
        return cellSize;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return messages.Count;
    }

    public void SetMessages(List<string> messages)
    {
        this.messages = messages;
		if (scroller.ScrollRect != null)
			scroller.ReloadData();
		//stepShift = 1.73f / (messages.Count - 1);
    }
    /*
    public void Prev()
    {
        if(scroller.ScrollRect.verticalNormalizedPosition < 1)
        {
            if (moveOnOneItem != null)
                StopCoroutine(moveOnOneItem);
            float targetPos = scroller.ScrollRect.verticalNormalizedPosition + stepShift;
            moveOnOneItem = StartCoroutine(MoveToPosition(targetPos, stepShift));
        }
    }

    public void Next()
    {
        if (scroller.ScrollRect.verticalNormalizedPosition > 0)
        {
            if (moveOnOneItem != null)
                StopCoroutine(moveOnOneItem);
            float targetPos = scroller.ScrollRect.verticalNormalizedPosition - stepShift;
            moveOnOneItem = StartCoroutine(MoveToPosition(targetPos, -stepShift ));
        }
    } */

    void HideCallback(string item)
    {
        SoundManager.Instance.PlayButtonPress();
		MessagesForMultiplayer.HideMessage(item);
    }

	void ShowCallback(string item)
    {
        SoundManager.Instance.PlayButtonPress();
		MessagesForMultiplayer.ShowMessage(item);
	}

    public void OnItemDropped(ReorderableList.ReorderableListEventStruct eventData)
    {
		int dropIndex = eventData.FromIndex + scroller.StartDataIndex - 1;
		int insertIndex = Mathf.Clamp( eventData.ToIndex + scroller.StartDataIndex - 1, 0, messages.Count);

		string droppedMessage = messages[dropIndex];

//		Debug.LogWarning("droppedMessage " + droppedMessage);
//		Debug.LogWarning("placeToDropMessage " + placeToDropMessage);

		messages.RemoveAt(dropIndex);
		messages.Insert(insertIndex, droppedMessage);

		MessagesForMultiplayer.SaveListMessages(messages);
		float sp = scroller.ScrollPosition;
		scroller.ReloadData();
		scroller.ScrollPosition = sp;
    }

    /*
    IEnumerator MoveToPosition(float targetPos, float shift)
    {
        float eps = 0.01f;
        float dist = Mathf.Abs(scroller.ScrollRect.verticalNormalizedPosition - targetPos);
        while (dist > eps)
        {
            scroller.ScrollRect.verticalNormalizedPosition += shift * Time.deltaTime * 3;
            dist = Mathf.Abs(scroller.ScrollRect.verticalNormalizedPosition - targetPos);
            yield return new WaitForEndOfFrame();
        }
    }*/


	int newIndex = -1;
	int GetNewIndex(int offset)
	{
		if (newIndex == -1)
			newIndex = scroller.EndDataIndex - messagesPerPage;
		newIndex = Mathf.Clamp(newIndex + offset, 0, messages.Count - messagesPerPage);
		return newIndex;
	}

	public void Next()
    {
        SoundManager.Instance.PlayButtonPress();
		scroller.JumpToDataIndex(GetNewIndex(1), 0, 0, false, EnhancedScroller.TweenType.linear, 1, JumpCompleted);
	}

    public void Prev()
    {
        SoundManager.Instance.PlayButtonPress();
		scroller.JumpToDataIndex(GetNewIndex(-1), 0, 0, false, EnhancedScroller.TweenType.linear, 1, JumpCompleted);
	}

	void JumpCompleted()
	{
		newIndex = -1;
	}
}
