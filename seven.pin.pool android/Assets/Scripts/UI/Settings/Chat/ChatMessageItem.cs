﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class ChatMessageItem : EnhancedScrollerCellView {

    [SerializeField]
    private Text messageText;
    [SerializeField]
    private UnityEngine.UI.Button hideButton, showButton;

    public void SetMessageItem(string message, bool shown, System.Action<string> hideCallback, System.Action<string> showCallback)
    {
        messageText.text = message;
        hideButton.gameObject.SetActive(shown);
        showButton.gameObject.SetActive(!shown);
        hideButton.onClick.RemoveAllListeners();
		hideButton.onClick.AddListener(() => { hideCallback(message); hideButton.gameObject.SetActive(false); showButton.gameObject.SetActive(true); });
        showButton.onClick.RemoveAllListeners();
		showButton.onClick.AddListener(() => { showCallback(message); hideButton.gameObject.SetActive(true); showButton.gameObject.SetActive(false); });
    }

	
}