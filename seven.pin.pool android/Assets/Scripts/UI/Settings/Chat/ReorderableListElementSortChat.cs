﻿/// Credit Ziboo
/// Sourced from - http://forum.unity3d.com/threads/free-reorderable-list.364600/

using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{

	[RequireComponent(typeof(RectTransform))]
	public class ReorderableListElementSortChat : ReorderableListElement, IDragHandler
	{
		float offset;
		Vector3 smallScale;
		Vector3 bigScale;

		void Start()
		{
			smallScale = transform.parent.localScale;
			bigScale = smallScale;
			bigScale.y *= 1.1f;
		}

		public void OnDrag(PointerEventData eventData)
		{
			if (offset == 0f)
				offset = transform.position.x - transform.parent.position.x;
			Vector2 pos = eventData.position;
			pos.x -= offset;
			eventData.position = pos;
			base.OnDrag(eventData);
		}

		public void IncreseScale()
		{
			transform.parent.localScale = bigScale;
		}
		public void DecreseScale()
		{
			transform.parent.localScale = smallScale;
		}
	}
}