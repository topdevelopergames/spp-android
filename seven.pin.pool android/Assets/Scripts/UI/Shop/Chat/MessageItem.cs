﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class MessageItem : EnhancedScrollerCellView {

    [SerializeField]
    private Text messageText;

    public void SetMessage(string message)
    {
        messageText.text = message;
    }

}
