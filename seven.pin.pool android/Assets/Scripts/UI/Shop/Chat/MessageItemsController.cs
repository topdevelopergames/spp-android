﻿using UnityEngine;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;

public class MessageItemsController : MonoBehaviour, IEnhancedScrollerDelegate
{
    [SerializeField]
    private EnhancedScroller scroller;

    [SerializeField]
    private MessageItem messagePrefab;

    private List<string> messages;

    void OnEnable()
    {
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;

    }  
    
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        MessageItem messageItem = scroller.GetCellView(messagePrefab) as MessageItem;
        messageItem.SetMessage(messages[dataIndex]);
        return  messageItem;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        float cellSize = 131f;
        LayoutElement element = messagePrefab.GetComponent<LayoutElement>();
        if (element != null)
            cellSize = element.preferredHeight;
        return cellSize;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return messages.Count;
    }

    public void SetMessages(List<string> messages)
    {
        this.messages = messages;
        if (scroller.ScrollRect != null)
            scroller.ReloadData();
    }
}
