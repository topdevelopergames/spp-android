﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using System.Collections.Generic;

public class PackItem : EnhancedScrollerCellView {

    [SerializeField]
    private MessageItemsController messageController;
    [SerializeField]
    private Text messagePackName;
    [SerializeField]
    private UnityEngine.UI.Button buyButton;
    [SerializeField]
    private Text packPrice;
    [SerializeField]
    private Image currencyIcon;


    public void SetPack(List<string> messages, ShopitemEntity pack, Sprite currency, System.Action<ShopitemEntity, GameObject, GameObject> buyCallback, ScrollRect parent)
    {
        messageController.SetMessages(messages);
        ConflictScrollManager csm = messageController.GetComponent<ConflictScrollManager>();
        if(csm == null)
        {
            messageController.gameObject.AddComponent<ConflictScrollManager>();
            csm = messageController.GetComponent<ConflictScrollManager>();
            csm.ParentScrollRect = parent;
            csm.Init();
        }
        messagePackName.text = pack.title;
        packPrice.text = pack.price.ToString();
        packPrice.transform.parent.gameObject.SetActive(!pack.owned);
        currencyIcon.sprite = currency;
        buyButton.onClick.RemoveAllListeners();
        buyButton.onClick.AddListener(() => { BuyPack(pack, buyCallback); });
        buyButton.gameObject.SetActive(!pack.owned);
    }

    void BuyPack(ShopitemEntity item, System.Action<ShopitemEntity, GameObject, GameObject> callback)
    {
        callback(item, buyButton.gameObject, packPrice.transform.parent.gameObject);
    }
    
}
