﻿using UnityEngine;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using System;
using UnityEngine.UI;

public class PackItemsController : MonoBehaviour, IEnhancedScrollerDelegate
{

    [SerializeField]
    private EnhancedScroller scroller;

    [SerializeField]
    private PackItem packPrefab;

    [SerializeField]
    private Sprite coinsIcon, cashIcon;
    [SerializeField]
    private UnityEngine.UI.Button prevButton, nextButton;

    private int itemsPerPage = 3;
    private float cellSize;

    private List<ShopitemEntity> packs;

    private int currentDataIndex;

    void Awake()
    {
        itemsPerPage = 3;
    }

    void OnEnable()
    {
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;
        currentDataIndex = 0;
        //scroller.scrollerScrollingChanged -= JumpComplete;
        //scroller.scrollerScrollingChanged += JumpComplete;
        scroller.scrollerScrolled -= JumpComplete;
        scroller.scrollerScrolled += JumpComplete;
    }

    void OnDisable()
    {
        //scroller.scrollerScrollingChanged -= JumpComplete;
        scroller.scrollerScrolled -= JumpComplete;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        PackItem packItem = scroller.GetCellView(packPrefab) as PackItem;
        Sprite s = packs[dataIndex].currency == Currency_Type_Shop.Cash ? cashIcon : coinsIcon;
        packItem.SetPack(ChatMessagesStorage.Instance.GetPackMessage(dataIndex), packs[dataIndex], s, BuyCallback, scroller.ScrollRect);
        return packItem;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        cellSize = 486f;
        LayoutElement element = packPrefab.GetComponent<LayoutElement>();
        if (element != null)
            cellSize = element.preferredWidth;
        return cellSize;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return packs.Count;
    }

    public void SetPacks(List<ShopitemEntity> packs)
    {
        this.packs = packs;
        if (scroller.ScrollRect != null)
            scroller.ReloadData();
        
    }

    void BuyCallback(ShopitemEntity item, GameObject buyButtonObject, GameObject priceObject)
    {
        SoundManager.Instance.PlayButtonPress();
        bool isEnoughMoney = false;
        CurrencyType type = CurrencyType.COINS;
        switch (item.currency)
        {
            case Currency_Type_Shop.Coins:
                type = CurrencyType.COINS;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.coins;
                break;

            case Currency_Type_Shop.Cash:
                type = CurrencyType.CASH;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.cash;
                break;
        }
        if (!isEnoughMoney)
        {
            PoolShopManager.Instance.ShowNotEnoughMoney();
            return;
        }
        priceObject.SetActive(false);
        buyButtonObject.SetActive(false);
        VirtualGood vg = new VirtualGood(VirtualGoodType.CHAT, item.id);
        GameSparksManager.Instance.BuyVirtualGoods(type, 1, vg, PurchaseSuccessful);
        GameData.Instance.ownedPacks.Add(item);
        //ChatPackStorage.Instance.UpdateList(item);
        item.owned = true;
        var list = ChatMessagesStorage.Instance.GetPackMessage(item.id);
        MessagesForMultiplayer.AddNewMessages(list);           
    }

    void PurchaseSuccessful()
    {
        Debug.Log("from chat Updating player cash: " + GameSparksManager.Instance.AuthorizedPlayerData.cash);
        Debug.Log("from chat Updating player coins: " + GameSparksManager.Instance.AuthorizedPlayerData.coins);
        MenuUIManager.Instance.SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
    }

    public void Next()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex < packs.Count - itemsPerPage)
        {
            Debug.Log("<b>dataIndex: </b>" + currentDataIndex);
            currentDataIndex++;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, scroller.snapping, scroller.snapTweenType, scroller.snapTweenTime);
        }
    }

    public void Prev()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex > 0)
        {
            Debug.Log("<b>dataIndex: </b>" + currentDataIndex);
            currentDataIndex--;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, scroller.snapping, scroller.snapTweenType, scroller.snapTweenTime);
        }
    }

    void JumpComplete(EnhancedScroller scroller, Vector2 val, float scrollPosition)
    {
        int intValue = (int)(scroller.ScrollPosition / scroller.ScrollRectSize);
        float modulValue = scroller.ScrollPosition % scroller.ScrollRectSize;
        currentDataIndex = intValue * itemsPerPage + (modulValue > cellSize ? (int)(modulValue / cellSize) : 0);
        //Debug.Log("new currentdataindex: " + currentDataIndex);
    }
    

    void JumpComplete(EnhancedScroller scroller, bool scrolling)
    {
        if (!scrolling)
        {
            int intValue = Mathf.RoundToInt(scroller.ScrollPosition / scroller.ScrollRectSize);
            float modulValue = scroller.ScrollPosition % scroller.ScrollRectSize;
            currentDataIndex = intValue * itemsPerPage + (modulValue > cellSize ? (int)(modulValue / cellSize) : 0);
            Debug.Log("Jump complete handler");
            Debug.Log("new currentdataindex: " + currentDataIndex);
        }
    }


}
