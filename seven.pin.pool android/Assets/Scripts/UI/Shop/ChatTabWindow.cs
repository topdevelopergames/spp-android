﻿using UnityEngine;
using System.Collections.Generic;

public class ChatTabWindow : MonoBehaviour
{

    [SerializeField]
    private PackItemsController packController;

    void OnEnable()
    {
        Init();
    }

    void Init()
    {      
        List<ShopitemEntity> packs = ChatPackStorage.Instance.GetAllItemsWithOwned(GameSparksManager.Instance.AuthorizedPlayerData.ChatList);
        packController.SetPacks(packs);
        packController.gameObject.SetActive(true);

    }

}
