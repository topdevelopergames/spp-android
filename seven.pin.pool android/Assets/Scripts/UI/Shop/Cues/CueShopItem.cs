﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CueShopItem : CueItem {

    [SerializeField]
    private Text price;
    [SerializeField]
    private Image currencyIcon;
    [SerializeField]
    private UnityEngine.UI.Button buyButton;
    [SerializeField]
    private GameObject locked;
    [SerializeField]
    private Text requiredLevel;
   
    private CueEntity currentCue;


    void Awake()
    {
        switchedOff = false;
        id = -1;
        currentCue = null;
    }

    public void SetCueShopItem(CueEntity cue, Sprite coinSprite = null, Sprite cashSprite = null, CuesShopController controller = null)
    {
        if (cue == null)
        {
            if (!switchedOff)
            {
                SwitchAll(false);
                switchedOff = true;
            }
            return;
        }

        if (switchedOff)
        {
            switchedOff = false;
            SwitchAll(true);
        }
        
        selected.SetActive(false);
        id = cue.cueId;
        currentCue = cue;
        cueImage.sprite = Resources.Load<Sprite>(PathToResources.Instance.pathToHDCues + cue.cueId);
        if (cue.owned)
        {
            price.enabled = false;
            currencyIcon.enabled = false;
            requiredLevel.enabled = false;
        }
        else
        {
            if (cue.requiredLevel > GameSparksManager.Instance.AuthorizedPlayerData.statistic.level)
            {
                requiredLevel.enabled = true;
                requiredLevel.text = "Level " + cue.requiredLevel.ToString();
                price.enabled = false;
                currencyIcon.enabled = false;
            }
            else
            {
                price.enabled = true;
                currencyIcon.enabled = true;
                requiredLevel.enabled = false;
                price.text = cue.price.ToString();
                currencyIcon.sprite = cue.currency == Currency_Type_Shop.Cash ? cashSprite : coinSprite;
            }
        }
        cueImage.sprite = CuesStorage.Instance.cueImages[cue.cueId];
        cueName.text = cue.cueName;
        description.text = cue.cueDescription;
        SetupCueSpecs(enabledAim, cue.aim);
        SetupCueSpecs(enabledForce, cue.force);
        SetupCueSpecs(enabledSpin, cue.spin);
        SetupButtons(cue, controller);
        controller.selectItem -= SelectItemCallback;
        controller.selectItem += SelectItemCallback;
    }

    void SetupButtons(CueEntity cue, CuesShopController controller)
    {
        if (GameSparksManager.Instance.AuthorizedPlayerData.statistic.level < cue.requiredLevel)
        {
            buyButton.gameObject.SetActive(false);
            useButton.gameObject.SetActive(false);
            locked.SetActive(true);
        }
        else
        {
            locked.SetActive(false);
            useButton.onClick.RemoveAllListeners();
            buyButton.onClick.RemoveAllListeners();
            useButton.onClick.AddListener(() => { UseButtonCallback(cue, controller); });
            buyButton.onClick.AddListener(() => { BuyButtonCallback(cue, controller); });
            if (cue.owned)
            {
                if (GameData.Instance.selectedCue.cueId == cue.cueId)
                {
                    selected.SetActive(true);
                    useButton.gameObject.SetActive(false);
                    buyButton.gameObject.SetActive(false);
                }
                else
                {
                    useButton.gameObject.SetActive(true);
                    buyButton.gameObject.SetActive(false);
                    
                }
            }
            else
            {
                buyButton.gameObject.SetActive(true);
                useButton.gameObject.SetActive(false);
                
            }
        }
    }


    void UseButtonCallback(CueEntity cue, CuesShopController controller)
    {
        SoundManager.Instance.PlayButtonPress();      
        GameData.Instance.selectedCue = CueEntity.DeepCopy(cue);
        GameData.Instance.Save();
        if (controller.selectItem != null)
            controller.selectItem(cue);
    }

    void SelectItemCallback(CueEntity cue)
    {
        if (!currentCue.owned)
            return;
        if (cue.cueId == id)
        {
            selected.SetActive(true);
            useButton.gameObject.SetActive(false);
            if (GameSparks.Core.GS.Available)
            {
                VirtualGoodType vgType = VirtualGoodType.CUES_STANDARD;
                switch (cue.cueType)
                {
                    case Cue_Shop_Type.standard:
                        vgType = VirtualGoodType.CUES_STANDARD;
                        break;
                    case Cue_Shop_Type.premium:
                        vgType = VirtualGoodType.CUES_PREMIUM;
                        break;
                    case Cue_Shop_Type.country:
                        vgType = VirtualGoodType.CUES_COUNTRY;
                        break;
                }
                GameSparksManager.Instance.SetCurrentCue(new VirtualGood(vgType, cue.cueId).shortCode);
            }
            return;
        }
        useButton.gameObject.SetActive(true);
        selected.SetActive(false);
    }



    void BuyButtonCallback(CueEntity cue, CuesShopController controller)
    {
        SoundManager.Instance.PlayButtonPress();
        controller.BuyCueExt(cue, SetBoughtCueToUse);
    }

    void SetBoughtCueToUse()
    { 
        buyButton.gameObject.SetActive(false);
        useButton.gameObject.SetActive(true);
        price.enabled = false;
        currencyIcon.enabled = false;
    }



}
