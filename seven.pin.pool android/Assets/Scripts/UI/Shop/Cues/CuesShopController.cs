﻿using UnityEngine;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CuesShopController : MonoBehaviour, IEnhancedScrollerDelegate, IPointerDownHandler, IPointerUpHandler
{
    public delegate void ActionItem(CueEntity cue);
    public ActionItem selectItem;



    [SerializeField]
    private EnhancedScroller scroller;

    [SerializeField]
    private ScrollRect scrollRect;
    [SerializeField]
    private CuesShopPage cueShopPagePrefab;
    [SerializeField]
    private Sprite coins, cash;
    [SerializeField]
    private UnityEngine.UI.Button prevButton, nextButton;

    private int cuesPerPage = 2;
    private int currentDataIndex;
    private int fullPages;
    private List<CueEntity> cues;

    private List<CueEntity> unlockedCues;
    private List<GameObject> unlockedSelected;
    private List<GameObject> unlockedUseButtons;


    void OnEnable()
    {
        fullPages = currentDataIndex = 0;
        if(scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;
        unlockedCues = new List<CueEntity>();
        unlockedSelected = new List<GameObject>();
        unlockedUseButtons = new List<GameObject>();
        currentDataIndex = 0;
        scroller.scrollerScrollingChanged -= JumpComplete;
        scroller.scrollerScrollingChanged += JumpComplete;
        selectItem = null;
    }

    void OnDisable()
    {
        scroller.scrollerScrollingChanged -= JumpComplete;
        selectItem = null;
    }

    public void OnPointerDown(PointerEventData data)
    {
        scroller.snapping = false;
    }

    public void OnPointerUp(PointerEventData data)
    {
        scroller.snapping = true;
        scroller.LinearVelocity = 100;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CuesShopPage page = scroller.GetCellView(cueShopPagePrefab) as CuesShopPage;
        int defaultCuesPerPage = cuesPerPage;
        int leftCues = cues.Count - (dataIndex * cuesPerPage);
        if (leftCues > 0 && leftCues < cuesPerPage)
        {
            defaultCuesPerPage = leftCues;
        }

        List<CueEntity> cuesToShow = cues.GetRange(dataIndex * cuesPerPage, defaultCuesPerPage);
        page.SetCues(cuesToShow, cash, coins, this);
        return page;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        LayoutElement prefabLayout = cueShopPagePrefab.GetComponent<LayoutElement>();
        float width = 1083;
        if(prefabLayout != null)
        {
            width = prefabLayout.preferredWidth;
        }
        return width;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        fullPages = cues.Count / cuesPerPage;
        int restItems = cues.Count % cuesPerPage;
        fullPages += restItems > 0 ? 1 : 0;
        return fullPages;
    }

    public void SetCuesList(List<CueEntity> cues)
    {
        this.cues = cues;
        if(scroller.ScrollRect != null)
            scroller.ReloadData();
    }

    public void BuyCueExt(CueEntity cue, Action successfulPurchase)
    {
        BuyCueCallback(cue, successfulPurchase);
    }

    void BuyCueCallback(CueEntity cue, Action succesPurchaseCallback)
    {
        bool isEnoughMoney = false;
        CurrencyType currency = CurrencyType.CASH;
        switch (cue.currency)
        {
            case Currency_Type_Shop.Cash:
                currency = CurrencyType.CASH;
                isEnoughMoney = GameSparksManager.Instance.AuthorizedPlayerData.cash > cue.price;
                break;
            case Currency_Type_Shop.Coins:
                currency = CurrencyType.COINS;
                isEnoughMoney = GameSparksManager.Instance.AuthorizedPlayerData.coins > cue.price;
                break;
        }
        if(!isEnoughMoney)
        {
            PoolShopManager.Instance.ShowNotEnoughMoney();
            Debug.Log("Not enough money for purchase");
            return;
        }

        VirtualGoodType virtualGoodType = VirtualGoodType.CUES_STANDARD;
        switch (cue.cueType)
        {
            case Cue_Shop_Type.standard:
                virtualGoodType = VirtualGoodType.CUES_STANDARD;
                break;
            case Cue_Shop_Type.premium:
                virtualGoodType = VirtualGoodType.CUES_PREMIUM;
                break;
            case Cue_Shop_Type.country:
                virtualGoodType = VirtualGoodType.CUES_COUNTRY;
                break;
        }

        VirtualGood item = new VirtualGood(virtualGoodType, cue.cueId);
        item.name = cue.cueName;
        item.description = cue.cueDescription;
        GameSparksManager.Instance.BuyVirtualGoods(currency, 1, item, PurchaseSuccessful);
        cue.owned = true;
        GameData.Instance.ownedCues.Add(cue);
        GameData.Instance.Save();
        succesPurchaseCallback();
    }

    void PurchaseSuccessful()
    {
        Debug.Log("from cue shop Updating player cash: " + GameSparksManager.Instance.AuthorizedPlayerData.cash);
        Debug.Log("from cue shop Updating player coins: " + GameSparksManager.Instance.AuthorizedPlayerData.coins);
        MenuUIManager.Instance.SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
    }

    public void MoveNext()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex < fullPages - 1)
        {
            scroller.snapping = false;
            currentDataIndex++;
            //nextButton.interactable = false;
            //prevButton.interactable = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, false, EnhancedScroller.TweenType.spring, 1, JumpComplete);
        }
    }

    public void MovePrev()
    {
        SoundManager.Instance.PlayButtonPress();
        if (currentDataIndex > 0)
        {
            scroller.snapping = false;
            currentDataIndex--;
            //nextButton.interactable = false;
            //prevButton.interactable = false;
            scroller.JumpToDataIndex(currentDataIndex, 0, 0, false, EnhancedScroller.TweenType.spring, 1, JumpComplete);
        }
    }

    void JumpComplete()
    {
        scroller.snapping = true;
        scroller.SnapJumpCompleteExt();
        //nextButton.interactable = true;
        //prevButton.interactable = true;
    }

    void JumpComplete(EnhancedScroller scroller, bool scrolling)
    {
        if (!scrolling)
        {
            Debug.Log("Jump complete handler");
            int integerPart = Mathf.RoundToInt(scroller.ScrollPosition / scroller.ScrollRectSize);
            float fractPart = (scroller.ScrollPosition / scroller.ScrollRectSize) - integerPart;
            currentDataIndex = fractPart > scroller.snapWatchOffset ? integerPart + 1 : integerPart;
            //this.scroller.snapping = true;
            //nextButton.interactable = true;
            //prevButton.interactable = true;
        }
    }

}
