﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;

public class CuesShopPage : EnhancedScrollerCellView {


    [SerializeField]
    private GameObject cueShopItemPrefab;
    private GameObject[] cuesOnPage;
    private int cuesPerPage = 2;

    void Awake()
    {
        cuesPerPage = 2;
    }

    public void SetCues(List<CueEntity> cuesToShow, Sprite cashSprite, Sprite coinSprite, CuesShopController controller)
    {
        if(cuesOnPage == null)
        {
            CreateCues(cuesToShow, cashSprite, coinSprite, controller);
        }
        else
        {
            UpdateCues(cuesToShow, cashSprite, coinSprite, controller);
        }
    }

    void UpdateCues(List<CueEntity> cuesToShow, Sprite cashSprite, Sprite coinSprite, CuesShopController controller)
    { 
        for(int i = 0; i < cuesToShow.Count; i++)
        {
            CueShopItem cueItem = cuesOnPage[i].GetComponent<CueShopItem>();
            cueItem.SetCueShopItem(cuesToShow[i], coinSprite, cashSprite, controller);
        }

        int countToShow = cuesOnPage.Length - cuesToShow.Count;

        if(countToShow > 0)
        {
            for (int i = cuesOnPage.Length - countToShow; i < cuesOnPage.Length; i++)
            {
                cuesOnPage[i].GetComponent<CueShopItem>().SetCueShopItem(null);
            }
        }
    }

    void CreateCues(List<CueEntity> cuesToShow, Sprite cashSprite, Sprite coinSprite, CuesShopController controller)
    {
        cuesOnPage = new GameObject[cuesPerPage];
        for(int i = 0; i < cuesToShow.Count; i++)
        {
            GameObject item = Instantiate(cueShopItemPrefab);
            item.transform.SetParent(transform, false);
            CueShopItem cueItem = item.GetComponent<CueShopItem>();
            cueItem.SetCueShopItem(cuesToShow[i], coinSprite, cashSprite, controller);
            cuesOnPage[i] = item;
        }

        int hideCount = cuesPerPage - cuesToShow.Count;

        if (hideCount > 0)
        {
            for (int i = cuesPerPage - hideCount; i < cuesPerPage; i++)
            {
                GameObject item = Instantiate(cueShopItemPrefab);
                item.transform.SetParent(transform, false);
                CueShopItem cueItem = item.GetComponent<CueShopItem>();
                cueItem.SetCueShopItem(null);
                cuesOnPage[i] = item;
            }
        }
    }
	
}
