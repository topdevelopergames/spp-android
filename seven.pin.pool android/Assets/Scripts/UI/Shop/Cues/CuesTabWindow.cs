﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CuesTabWindow : MonoBehaviour {

    [SerializeField]
    private CuesShopController cuesShopController;
	
    void Awake()
    {
        cuesShopController.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        List<CueEntity> cues = GetAppropriateList(GameData.Instance.lastSelectedCueTab);
        cuesShopController.SetCuesList(cues);
        cuesShopController.gameObject.SetActive(true);
    }

    public void SelectStandardCues()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.lastSelectedCueTab = 0;
        GameData.Instance.Save();
        List<CueEntity> cuesToShow = GetAppropriateList(0);
        cuesShopController.SetCuesList(cuesToShow);
    }

    public void SelectPremiumCues()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.lastSelectedCueTab = 1;
        GameData.Instance.Save();
        List<CueEntity> cuesToShow = GetAppropriateList(1);
        cuesShopController.SetCuesList(cuesToShow);
    }

    public void SelectCountryCues()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.lastSelectedCueTab = 2;
        GameData.Instance.Save();
        List<CueEntity> cuesToShow = GetAppropriateList(2);
        cuesShopController.SetCuesList(cuesToShow);
    }

    public void SelectOwnedCues()
    {
        SoundManager.Instance.PlayButtonPress();
        GameData.Instance.lastSelectedCueTab = 3;
        GameData.Instance.Save();
        List<CueEntity> cuesToShow = GetAppropriateList(3);
        cuesShopController.SetCuesList(cuesToShow);
    }

    List<CueEntity> GetAppropriateList(int cueType)
    {
        List<CueEntity> cues;
        switch (cueType)
        {
            ///standard cues
            case 0:
                cues = GetNotOwnedCues(GameData.Instance.cues, Cue_Shop_Type.standard);
                return cues;
            ///premium cues
            case 1:
                cues = GetNotOwnedCues(GameData.Instance.cues, Cue_Shop_Type.premium);
                return cues;
            ///country cues
            case 2:
                cues = GetNotOwnedCues(GameData.Instance.cues, Cue_Shop_Type.country);
                return cues;
            ///owned cues
            case 3:
                cues = CuesStorage.Instance.GetOwned(GameSparksManager.Instance.AuthorizedPlayerData.CuesList);
                return cues;
        }
        return null;
    }

    List<CueEntity> GetNotOwnedCues(List<CueEntity> cues, Cue_Shop_Type cue_type)
    {
        List<CueEntity> resultCues = new List<CueEntity>();
        List<CueEntity> typeSpecifiedCuesStorage = cues.FindAll(x => x.cueType == cue_type);
        List<CueEntity> ownedCues = CuesStorage.Instance.GetOwned(GameSparksManager.Instance.AuthorizedPlayerData.CuesList); //GameData.Instance.ownedCues;
        foreach(var item in typeSpecifiedCuesStorage)
        {
            if(!ownedCues.Contains(item))
            {
                resultCues.Add(item);
            }
        }
        return resultCues;
    }


}
