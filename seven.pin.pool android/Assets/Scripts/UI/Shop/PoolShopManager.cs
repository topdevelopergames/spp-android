﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolShopManager : MonoBehaviour {

    [SerializeField]
    private GameObject coinsTab, cashTab, tablesTab, chatTab, cuesTab;

    [SerializeField]
    private GameObject coinsTabSelected, cashTabSelected, tablesTabSelected, chatTabSelected, cuesTabSelected;

    [SerializeField]
    private GameObject notEnoughMoneyPopup;

    [SerializeField]
    private UnityEngine.UI.Button cashButton, coinsButton;

    [SerializeField]
    private GameObject coinsInitPurchasesMessage, cashInitPurchasesMessage;

    private GameData gameData;

    private static PoolShopManager _instance;

    public static PoolShopManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        gameData = GameData.Instance;
        if (_instance == null)
            _instance = this;
    }

    void OnEnable()
    {
        LastOpenedTab();
#if UNITY_WEBGL
        cashButton.interactable = false;
        coinsButton.interactable = false;
#endif
    }

    void OnDisable()
    {
        _instance = null;
    }

    void LastOpenedTab()
    {
        int lastTab = GameData.Instance.lastOpenedTab;

        switch(lastTab)
        {
            case 0:
                OpenCoinsTab();
                break;
            case 1:
                OpenCashTab();
                break;
            case 2:
                OpenTablesTab();
                break;
            case 3:
                OpenChatTab();
                break;
            case 4:
                OpenCuesTab();
                break;
        }
    }


    public void OpenCoinsTab()
    {
        coinsTab.SetActive(true);
        cashTab.SetActive(false);
        tablesTab.SetActive(false);
        chatTab.SetActive(false);
        cuesTab.SetActive(false);

        coinsTabSelected.SetActive(true);
        cashTabSelected.SetActive(false);
        tablesTabSelected.SetActive(false);
        chatTabSelected.SetActive(false);
        cuesTabSelected.SetActive(false);
        
        gameData.lastOpenedTab = 0;
        gameData.Save();
    }

    public void OpenCashTab()
    {
        coinsTab.SetActive(false);
        cashTab.SetActive(true);
        tablesTab.SetActive(false);
        chatTab.SetActive(false);
        cuesTab.SetActive(false);

        coinsTabSelected.SetActive(false);
        cashTabSelected.SetActive(true);
        tablesTabSelected.SetActive(false);
        chatTabSelected.SetActive(false);
        cuesTabSelected.SetActive(false);

        gameData.lastOpenedTab = 1;
        gameData.Save();
    }

    public void OpenTablesTab()
    {
        coinsTab.SetActive(false);
        cashTab.SetActive(false);
        tablesTab.SetActive(true);
        chatTab.SetActive(false);
        cuesTab.SetActive(false);

        coinsTabSelected.SetActive(false);
        cashTabSelected.SetActive(false);
        tablesTabSelected.SetActive(true);
        chatTabSelected.SetActive(false);
        cuesTabSelected.SetActive(false);

        gameData.lastOpenedTab = 2;
        gameData.Save();
    }

    public void OpenChatTab()
    {
        coinsTab.SetActive(false);
        cashTab.SetActive(false);
        tablesTab.SetActive(false);
        chatTab.SetActive(true);
        cuesTab.SetActive(false);

        coinsTabSelected.SetActive(false);
        cashTabSelected.SetActive(false);
        tablesTabSelected.SetActive(false);
        chatTabSelected.SetActive(true);
        cuesTabSelected.SetActive(false);

        gameData.lastOpenedTab = 3;
        gameData.Save();
    }
    
	public void OpenCuesTab()
    {
        coinsTab.SetActive(false);
        cashTab.SetActive(false);
        tablesTab.SetActive(false);
        chatTab.SetActive(false);
        cuesTab.SetActive(true);

        coinsTabSelected.SetActive(false);
        cashTabSelected.SetActive(false);
        tablesTabSelected.SetActive(false);
        chatTabSelected.SetActive(false);
        cuesTabSelected.SetActive(true);

        gameData.lastOpenedTab = 4;
        gameData.Save();
    }
    
    public void BackToMainMenu()
    {
        PlayButtonClick();
        MenuManager.Instance.BackToMainMenu();
    }

    public void ShowNotEnoughMoney()
    {
        notEnoughMoneyPopup.SetActive(true);
    }

    public void PlayButtonClick()
    {
        SoundManager.Instance.PlayButtonPress();
    }

    public void UpdatePlayerData(List<string> TransactionId)
    {
        Debug.Log("Updating player data");
        GameSparksManager.Instance.UpdateAuthorizedPlayerData(UpdateInfo);
    }

    public void UpdateInfo()
    {
        Debug.Log("Updating player info");
        MenuUIManager.Instance.SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
    }

    public void ShowCashInitMessage()
    {
        cashInitPurchasesMessage.SetActive(true);
    }

    public void HideCashInitMessage()
    {
        cashInitPurchasesMessage.SetActive(false);
    }

    public void ShowCoinsInitMessage()
    {
        coinsInitPurchasesMessage.SetActive(true);
    }

    public void HideCoinsInitMessage()
    {
        coinsInitPurchasesMessage.SetActive(false);
    }
}
