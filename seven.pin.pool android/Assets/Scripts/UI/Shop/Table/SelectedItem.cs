﻿using UnityEngine;
using UnityEngine.UI;

public class SelectedItem : MonoBehaviour {

    [SerializeField]
    private Text priceValue;
    [SerializeField]
    private Image itemImage, currencyIcon;
    [SerializeField]
    private UnityEngine.UI.Button buyButton;
    [SerializeField]
    private GameObject priceObject;
    [SerializeField]
    private Table table;

    public void SetItem(Sprite currency, ShopitemEntity item, System.Action<ShopitemEntity, GameObject> buyCallback = null)
    {
        table.SetSelectedItem(GameData.Instance.playTable == 0 ? item.withPockets : item.withoutPockets, item.type);
        priceValue.text = item.price.ToString();
        this.itemImage.sprite = GameData.Instance.playTable == 0 ? item.itemIcoWithPockets : item.itemIcoWithoutPockets;
        currencyIcon.sprite = currency;
        if (buyCallback != null)
        {
            buyButton.onClick.RemoveAllListeners();
            buyButton.onClick.AddListener(() => { BuyItem(item, buyCallback); });
        }
        currencyIcon.gameObject.SetActive(!item.owned);
        priceValue.gameObject.SetActive(!item.owned);
        buyButton.gameObject.SetActive(!item.owned);
        priceObject.SetActive(!item.owned);

        if (item.owned)
        {
            switch(item.type)
            {
                case VirtualGoodType.TABLE_CLOTH:                   
                    GameData.Instance.selectedCloth = ShopitemEntity.DeepCopy(item);
                    break;

                case VirtualGoodType.TABLE_FRAME:
                    GameData.Instance.selectedFrame = ShopitemEntity.DeepCopy(item);
                    break;
            }
            GameData.Instance.Save();

        }
    }

    void BuyItem(ShopitemEntity item, System.Action<ShopitemEntity, GameObject> callback)
    {
        callback(item, priceObject);
    }


}
