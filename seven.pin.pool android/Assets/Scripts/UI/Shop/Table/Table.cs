﻿using UnityEngine;
using UnityEngine.UI;

public class Table : MonoBehaviour {

    [SerializeField]
    private Image frame, cloth, pattern;


    public void SetSelectedItem(Sprite image, VirtualGoodType id)
    {
        switch(id)
        {
            case VirtualGoodType.TABLE_FRAME:
                frame.sprite = image;
                break;
            case VirtualGoodType.TABLE_CLOTH:
                cloth.sprite = image;
                break;
            case VirtualGoodType.TABLE_PATTERN:
                pattern.sprite = image;
                break;
        }
    }

    public void SetFrame(Sprite frame)
    {
        this.frame.sprite = frame;
    }

    public void SetCloth(Sprite cloth)
    {
        this.cloth.sprite = cloth;
    }

    public void SetPattern(Sprite pattern)
    {
        this.pattern.sprite = pattern;
    }
	
}
