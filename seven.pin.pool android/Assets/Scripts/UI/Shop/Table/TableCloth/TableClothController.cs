﻿using UnityEngine;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;

public class TableClothController : MonoBehaviour, IEnhancedScrollerDelegate{

    [SerializeField]
    private EnhancedScroller scroller;
    [SerializeField]
    private TableClothItem clothPrefab;
    [SerializeField]
    private SelectedItem selectedCloth;
    [SerializeField]
    private Sprite coinsIcon, cashIcon;

    private List<ShopitemEntity> cloth;

    void OnEnable()
    {
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        TableClothItem clothItem = scroller.GetCellView(clothPrefab) as TableClothItem;
        clothItem.SetItem(cloth[dataIndex], SelectCallback);
        return clothItem;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        float cellSize = 196f;
        LayoutElement element = clothPrefab.GetComponent<LayoutElement>();
        if(element != null)
        {
            cellSize = element.preferredHeight;
        }
        return cellSize;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return cloth.Count;
    }

    public void SetCloth(List<ShopitemEntity> cloth)
    {
        this.cloth = cloth;
        if (scroller.ScrollRect != null)
            scroller.ReloadData();
    }

    public void SetSelected(ShopitemEntity item)
    {
        Sprite s = item.currency == Currency_Type_Shop.Cash ? cashIcon : coinsIcon;
        selectedCloth.SetItem(s, item);
    }

    void SelectCallback(ShopitemEntity item)
    {
        Sprite s = item.currency == Currency_Type_Shop.Cash ? cashIcon : coinsIcon;
        selectedCloth.SetItem(s, item, BuyCallback);
        if (item.owned)
            GameData.Instance.selectedCloth = ShopitemEntity.DeepCopy(item);
    }

    void BuyCallback(ShopitemEntity item, GameObject priceObject)
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("item for purchase: " + item.title + " currency: " + item.currency + " price: " + item.price);
        bool isEnoughMoney = false;
        CurrencyType type = CurrencyType.COINS;
        switch (item.currency)
        {
            case Currency_Type_Shop.Coins:
                type = CurrencyType.COINS;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.coins;
                break;

            case Currency_Type_Shop.Cash:
                type = CurrencyType.CASH;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.cash;
                break;
        }
        if (!isEnoughMoney)
        {
            PoolShopManager.Instance.ShowNotEnoughMoney();
            return;
        }
        priceObject.SetActive(false);
        VirtualGood vg = new VirtualGood(VirtualGoodType.TABLE_CLOTH, item.id);
        GameSparksManager.Instance.BuyVirtualGoods(type, 1, vg, PurchaseSuccessful);
        //TableClothStorage.Instance.UpdateList(item);
        item.owned = true;
        GameData.Instance.selectedCloth = ShopitemEntity.DeepCopy(item);
        scroller.ReloadData();
    }

    void PurchaseSuccessful()
    {
        Debug.Log("from table cloth Updating player cash: " + GameSparksManager.Instance.AuthorizedPlayerData.cash);
        Debug.Log("from table cloth Updating player coins: " + GameSparksManager.Instance.AuthorizedPlayerData.coins);
        MenuUIManager.Instance.SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
    }
}
