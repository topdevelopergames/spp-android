﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;

public class TableClothItem :  EnhancedScrollerCellView{

    [SerializeField]
    private UnityEngine.UI.Button selectButton;
    [SerializeField]
    private Image itemImage;


    public void SetItem(ShopitemEntity item, System.Action<ShopitemEntity> callback)
    {
        this.itemImage.sprite = GameData.Instance.playTable == 0 ? item.itemIcoWithPockets : item.itemIcoWithoutPockets;
        selectButton.onClick.AddListener(() => { callback(item); });
    }

}
