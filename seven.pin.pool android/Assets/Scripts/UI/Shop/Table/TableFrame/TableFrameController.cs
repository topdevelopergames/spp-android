﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using System;
using UnityEngine.UI;

public class TableFrameController : MonoBehaviour, IEnhancedScrollerDelegate {

    [SerializeField]
    private EnhancedScroller scroller;

    [SerializeField]
    private TableFrameItem framePrefab;
    [SerializeField]
    private SelectedItem selectedFrame;
    [SerializeField]
    private Sprite cashIcon, coinsIcon;
    

    private List<ShopitemEntity> frames;
   
    void OnEnable()
    {
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        TableFrameItem frameItem = scroller.GetCellView(framePrefab) as TableFrameItem;
        frameItem.SetFrameItem(frames[dataIndex], SelectCallback);
        return frameItem;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        float cellsize = 196f;
        LayoutElement element = framePrefab.GetComponent<LayoutElement>();
        if(element != null)
        {
            cellsize = element.preferredHeight;
        }
        return cellsize;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return frames.Count;
    }

    public void SetFrames(List<ShopitemEntity> frames)
    {
        this.frames = frames;
        if (scroller.ScrollRect != null)
            scroller.ReloadData();
    }

    public void SetSelected(ShopitemEntity item)
    {
        Sprite s = item.currency == Currency_Type_Shop.Cash ? cashIcon : coinsIcon;
        selectedFrame.SetItem(s, item);
    }


    void SelectCallback(ShopitemEntity item)
    {
        Sprite s = item.currency == Currency_Type_Shop.Cash ? cashIcon : coinsIcon;
        selectedFrame.SetItem(s, item, BuyCallback);
        if (item.owned)
            GameData.Instance.selectedFrame = ShopitemEntity.DeepCopy(item);
    }

    void BuyCallback(ShopitemEntity item, GameObject priceObject)
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("item for purchase: " + item.title + " currency: " + item.currency + " price: " + item.price);
        bool isEnoughMoney = false;
        CurrencyType type = CurrencyType.COINS;
        switch(item.currency)
        {
            case Currency_Type_Shop.Coins:
                type = CurrencyType.COINS;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.coins;
                break;

            case Currency_Type_Shop.Cash:
                type = CurrencyType.CASH;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.cash;
                break;
        }
        if(!isEnoughMoney)
        {
            PoolShopManager.Instance.ShowNotEnoughMoney();
            return;
        }
        priceObject.SetActive(false);        
        VirtualGood vg = new VirtualGood(VirtualGoodType.TABLE_FRAME, item.id);
        GameSparksManager.Instance.BuyVirtualGoods(type, 1, vg, PurchaseSuccessful);
        //TableFrameStorage.Instance.UpdateList(item);
        item.owned = true;
        GameData.Instance.selectedFrame = ShopitemEntity.DeepCopy(item);
        scroller.ReloadData();
    }

    void PurchaseSuccessful()
    {
        Debug.Log("from table frame Updating player cash: " + GameSparksManager.Instance.AuthorizedPlayerData.cash);
        Debug.Log("from table frame Updating player coins: " + GameSparksManager.Instance.AuthorizedPlayerData.coins);
        MenuUIManager.Instance.SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
    }

}
