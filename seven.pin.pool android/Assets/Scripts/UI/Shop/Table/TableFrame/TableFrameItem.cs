﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class TableFrameItem : EnhancedScrollerCellView {

    [SerializeField]
    private UnityEngine.UI.Button selectButton;
    [SerializeField]
    private Image itemImage;

    public void SetFrameItem(ShopitemEntity frame, System.Action<ShopitemEntity> callback)
    {
        this.itemImage.sprite = GameData.Instance.playTable == 0 ? frame.itemIcoWithPockets : frame.itemIcoWithoutPockets;
        selectButton.onClick.AddListener(() => { callback(frame); });
    }


	
}
