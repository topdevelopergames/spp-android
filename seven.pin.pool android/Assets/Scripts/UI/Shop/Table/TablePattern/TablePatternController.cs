﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using System;

public class TablePatternController : MonoBehaviour, IEnhancedScrollerDelegate
{
    [SerializeField]
    private EnhancedScroller scroller;
    [SerializeField]
    private TablePatternItem patternPrefab;
    [SerializeField]
    private SelectedItem selectedPattern;
    [SerializeField]
    private Sprite coinsIcon, cashIcon;
    
    private List<ShopitemEntity> patterns;

    void OnEnable()
    {
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;        
    }


    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        TablePatternItem patternItem = scroller.GetCellView(patternPrefab) as TablePatternItem;
        patternItem.SetItem(patterns[dataIndex], SelectCallback);
        return patternItem;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        float cellSize = 196f;
        LayoutElement element = patternPrefab.GetComponent<LayoutElement>();
        if (element != null)
            cellSize = element.preferredHeight;
        return cellSize;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return patterns.Count;
    }

    public void SetPatterns(List<ShopitemEntity> patterns)
    {
        this.patterns = patterns;
        if (scroller.ScrollRect != null)
            scroller.ReloadData();
    }

    public void SetSelected(ShopitemEntity item)
    {
        Sprite s = item.currency == Currency_Type_Shop.Cash ? cashIcon : coinsIcon;
        selectedPattern.SetItem(s, item);
    }

    void SelectCallback(ShopitemEntity item)
    {
        Sprite s = item.currency == Currency_Type_Shop.Cash ? cashIcon : coinsIcon;
        selectedPattern.SetItem(s, item, BuyCallback);
        if (item.owned)
            GameData.Instance.selectedPattern = ShopitemEntity.DeepCopy(item);
    }

    void BuyCallback(ShopitemEntity item, GameObject priceObject)
    {
        SoundManager.Instance.PlayButtonPress();
        Debug.Log("item for purchase: " + item.title + " currency: " + item.currency + " price: " + item.price);
        bool isEnoughMoney = false;
        CurrencyType type = CurrencyType.COINS;
        switch (item.currency)
        {
            case Currency_Type_Shop.Coins:
                type = CurrencyType.COINS;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.coins;
                break;

            case Currency_Type_Shop.Cash:
                type = CurrencyType.CASH;
                isEnoughMoney = item.price <= GameSparksManager.Instance.AuthorizedPlayerData.cash;
                break;
        }
        if (!isEnoughMoney)
        {
            PoolShopManager.Instance.ShowNotEnoughMoney();
            return;
        }
        priceObject.SetActive(false);
        VirtualGood vg = new VirtualGood(VirtualGoodType.TABLE_PATTERN, item.id);
        GameSparksManager.Instance.BuyVirtualGoods(type, 1, vg, PurchaseSuccessful);
        //TablePatternStorage.Instance.UpdateList(item);
        item.owned = true;
        scroller.ReloadData();
    }

    void PurchaseSuccessful()
    {
        Debug.Log("from table pattern Updating player cash: " + GameSparksManager.Instance.AuthorizedPlayerData.cash);
        Debug.Log("from table pattern Updating player coins: " + GameSparksManager.Instance.AuthorizedPlayerData.coins);
        MenuUIManager.Instance.SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
    }

}
