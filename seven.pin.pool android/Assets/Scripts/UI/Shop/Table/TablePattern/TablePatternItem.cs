﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;

public class TablePatternItem : EnhancedScrollerCellView{
    
    [SerializeField]
    private UnityEngine.UI.Button selectButton;
    [SerializeField]
    private Image itemImage;


    public void SetItem(ShopitemEntity item, System.Action<ShopitemEntity> callback)
    {
        this.itemImage.sprite = item.withPockets;
        selectButton.onClick.AddListener(() => { callback(item); });
    }


}
