﻿using UnityEngine;
using System.Collections.Generic;

public class TableTabWindow : MonoBehaviour {

    [SerializeField]
    private TableFrameController frameController;
    [SerializeField]
    private TableClothController clothController;
    [SerializeField]
    private TablePatternController patternController;

    void OnEnable()
    {
        frameController.SetSelected(GameData.Instance.selectedFrame);
        clothController.SetSelected(GameData.Instance.selectedCloth);
        //patternController.SetSelected(GameData.Instance.selectedPattern);
        switch(GameData.Instance.lastSelectedTableTab)
        {
            case 0:
                SelectFrames();
                break;

            case 1:
                SelectCloth();
                break;

            case 2:
                SelectPattern();
                break;
        }
    }


    public void SelectFrames()
    {
        List<ShopitemEntity> frames = TableFrameStorage.Instance.GetAllWithOwned(GameSparksManager.Instance.AuthorizedPlayerData.TableFrameList);
        frameController.SetFrames(frames);

        frameController.gameObject.SetActive(true);
        clothController.gameObject.SetActive(false);
        patternController.gameObject.SetActive(false);
        GameData.Instance.lastSelectedTableTab = 0;
    }

    public void SelectCloth()
    {
        List<ShopitemEntity> cloth = TableClothStorage.Instance.GetAllWithOwned(GameSparksManager.Instance.AuthorizedPlayerData.TableClothList);
        clothController.SetCloth(cloth);

        frameController.gameObject.SetActive(false);
        clothController.gameObject.SetActive(true);
        patternController.gameObject.SetActive(false);
        GameData.Instance.lastSelectedTableTab = 1;
    }

    public void SelectPattern()
    {
        List<ShopitemEntity> patterns = TablePatternStorage.Instance.GetAllWithOwned(GameSparksManager.Instance.AuthorizedPlayerData.TablePatternList);
        patternController.SetPatterns(patterns);

        frameController.gameObject.SetActive(false);
        clothController.gameObject.SetActive(false);
        patternController.gameObject.SetActive(true);
        GameData.Instance.lastSelectedTableTab = 2;
    }

}
