﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    [SerializeField]
    private AudioSource buttonPress;


    private static SoundManager _instance;

    public static SoundManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlayButtonPress(float volume = 1)
    {
        if (GameData.Instance.soundEnabled && !buttonPress.isPlaying)
        {
            buttonPress.volume = volume;
            buttonPress.Play();
        }
    }

    public void PlayAudioSource(AudioSource source, float volume)
    {
        if (GameData.Instance.soundEnabled)
        {
            source.volume = volume;
            source.Play();
        }
    }

    public bool IsPlaying(AudioSource source)
    {
        return source.isPlaying;
    }


}
