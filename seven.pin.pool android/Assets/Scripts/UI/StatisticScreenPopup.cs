﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class StatisticScreenPopup : MonoBehaviour {


    [SerializeField]
    private Image leftPlayerAvatar, leftPlayerFacebookAvatar;
    [SerializeField]
    private Image middlePlayerAvatar, middlePlayerFacebookAvatar;
    [SerializeField]
    private Image rightPlayerAvatar, rightPlayerFacebookAvatar;

    [SerializeField]
    private GameObject leftPlayerFacebookBack, middlePlayerFacebookBack, rightPlayerFacebookBack;

    [SerializeField]
    private Text leftPlayerName;
    [SerializeField]
    private Text middlePlayerName;
    [SerializeField]
    private Text rightPlayerName;

    [SerializeField]
    private Text leftDots, middleDots, rightDots;

    [SerializeField]
    private Text leftPlayerTime;
    [SerializeField]
    private Text middlePlayerTime;
    [SerializeField]
    private Text rightPlayerTime;

    [SerializeField]
    private Text leftPlayerScore;
    [SerializeField]
    private Text middlePlayerScore;
    [SerializeField]
    private Text rightPlayerScore;

    [SerializeField]
    private Text leftPlayerPrize;
    [SerializeField]
    private Text middlePlayerPrize;
    [SerializeField]
    private Text rightPlayerPrize;

    [SerializeField]
    private Text leftPlayerLevel;
    [SerializeField]
    private Text middlePlayerLevel;
    [SerializeField]
    private Text rightPlayerLevel;

    [SerializeField]
    private Image leftPlayerVisual;
    [SerializeField]
    private Image middlePlayerVisual;
    [SerializeField]
    private Image rightPlayerVisual;

    [SerializeField]
    private GameObject leftPlayerWinner;
    [SerializeField]
    private GameObject middlePlayerWinner;
    [SerializeField]
    private GameObject rightPlayerWinner;

    [SerializeField]
    private Image leftPlayerAvatarBack;
    [SerializeField]
    private Image middlePlayerAvatarBack;
    [SerializeField]
    private Image rightPlayerAvatarBack;

    [SerializeField]
    private Sprite winnerBackAvatar;
    [SerializeField]
    private Sprite defaultBackAvatar;

    [SerializeField]
    private Color winCollor;
    [SerializeField]
    private Color defaultColor;

    [SerializeField]
    private GameObject achievementPopup;
    [SerializeField]
    private float achivPopupShowDuration = 2;
    [SerializeField]
    private float achivPopupShowSpeedDuration = 0.1f;

    [SerializeField]
    private GameObject playAgainLeft, playAgainRight, noWinners, statsInfoObject, playAgainButton;
    [SerializeField]
    private GameObject leftPlayer, middlePlayer, rightPlayer;

    const float DELAY_REQUEST_LEVEL = 1f;

    private int maxLengthName = 9;

    private int activePlayersCount = 0;

    private ChallengeCompletedData results;
    private Coroutine showAchivs;

    private static StatisticScreenPopup _instance;
    bool isWinnerMe;

    private AchievementPopup achivPopup;
    private List<RuntimeLeaderboardEntry> activePlayers;

    public static StatisticScreenPopup Instance
    {
        get
        {
            return _instance;
        }
    }

    void OnEnable()
    {
        if (playAgainLeft != null && playAgainRight != null)
        {
            playAgainLeft.SetActive(false);
            playAgainRight.SetActive(false);
        }
        achievementPopup.SetActive(true);
        achivPopup = achievementPopup.GetComponent<AchievementPopup>();
        achivPopup.RunAwake();
        showAchivs = StartCoroutine(ShowAchievements());
        GameSparksManager.Instance.CallbackRepeatedChallengeAutoDeclined -= HidePlayAgainButton;
        GameSparksManager.Instance.CallbackRepeatedChallengeAutoDeclined += HidePlayAgainButton;
    }

    void OnDisable()
    {
        StopAllCoroutines();
        achievementPopup.SetActive(false);
        if(showAchivs != null)
            StopCoroutine(showAchivs);
        GameSparksManager.Instance.CallbackRepeatedChallengeAutoDeclined -= HidePlayAgainButton;
    }

    void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
        }
    }

    #region Set UI
    public void SetLeftUser(PlayerData player, float prize, long score, int time)
    {
        Debug.Log("Left user time: " + time);

        if (player.facebookId != "")
        {
            leftPlayerFacebookAvatar.sprite = player.avatar;
            leftPlayerFacebookBack.SetActive(true);
            leftPlayerAvatar.gameObject.SetActive(false);
        }
        else
        {
            leftPlayerFacebookBack.SetActive(false);
            leftPlayerAvatar.gameObject.SetActive(true);
            leftPlayerAvatar.sprite = player.avatar;
        }

        bool isGuest = player.displayName.Equals("Guest");
        string username = isGuest ? player.displayName + player.playerId : player.displayName;

        leftDots.gameObject.SetActive(username.Length > maxLengthName);
        leftPlayerName.text = username.Length > maxLengthName ? username.Substring(0, maxLengthName) : username;
        leftPlayerName.alignment = username.Length > maxLengthName ? TextAnchor.MiddleRight : TextAnchor.MiddleCenter;

        Debug.Log("left player " + leftPlayerName.alignment);

        leftPlayerLevel.text = player.statistic.level.ToString();
        if(leftPlayerPrize != null)
            leftPlayerPrize.text = Mathf.RoundToInt(prize).ToString();
        leftPlayerScore.text = score.ToString();
        int minutes = time / 60;
        int secs = time % 60;
        int decSeconds = secs / 10;
        int seconds = secs % 10;
        leftPlayerTime.text = string.Format("{0}:{1}{2}", minutes, decSeconds, seconds);

        int playerLevel = player.statistic.level;
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(playerLevel);

        leftPlayerVisual.fillAmount = player.statistic.score / pointForNextLevel;
    }

    public void SetMiddleUser(PlayerData player, float prize, long score, int time)
    {
        Debug.Log("Middle user time: " + time);
        
        if (player.facebookId != "")
        {
            middlePlayerFacebookBack.SetActive(true);
            middlePlayerAvatar.gameObject.SetActive(false);
            middlePlayerFacebookAvatar.sprite = player.avatar;
        }
        else
        {
            middlePlayerFacebookBack.SetActive(false);
            middlePlayerAvatar.gameObject.SetActive(true);
            middlePlayerAvatar.sprite = player.avatar;
        }

        bool isGuest = player.displayName.Equals("Guest");
        string username = isGuest ? player.displayName + player.playerId : player.displayName;

        middleDots.gameObject.SetActive(username.Length > maxLengthName);
        middlePlayerName.text = username.Length > maxLengthName ? username.Substring(0, maxLengthName) : username;
        middlePlayerName.alignment = username.Length > maxLengthName ? TextAnchor.MiddleRight : TextAnchor.MiddleCenter;

        Debug.Log("middle player " + middlePlayerName.alignment);

        middlePlayerLevel.text = player.statistic.level.ToString();
        if(middlePlayerPrize != null)
            middlePlayerPrize.text = Mathf.RoundToInt(prize).ToString();
        middlePlayerScore.text = score.ToString();
        int minutes = time / 60;
        int secs = time % 60;
        int decSeconds = secs / 10;
        int seconds = secs % 10;       
        middlePlayerTime.text = string.Format("{0}:{1}{2}", minutes, decSeconds, seconds);

        int playerLevel = player.statistic.level;
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(playerLevel);

        middlePlayerVisual.fillAmount = player.statistic.score / pointForNextLevel;
    }

    public void SetRightUser(PlayerData player, float prize, long score, int time)
    {
        Debug.Log("Right user time: " + time);

        if (player.facebookId != "")
        {
            rightPlayerAvatar.gameObject.SetActive(false);
            rightPlayerFacebookBack.SetActive(true);
            rightPlayerFacebookAvatar.sprite = player.avatar;
        }
        else
        {
            rightPlayerAvatar.gameObject.SetActive(true);
            rightPlayerFacebookBack.SetActive(false);
            rightPlayerAvatar.sprite = player.avatar;
        }

        bool isGuest = player.displayName.Equals("Guest");
        string username = isGuest ? player.displayName + player.playerId : player.displayName;

        rightDots.gameObject.SetActive(username.Length > maxLengthName);
        rightPlayerName.text = username.Length > maxLengthName ? username.Substring(0, maxLengthName) : username;
        rightPlayerName.alignment = username.Length > maxLengthName ? TextAnchor.MiddleRight : TextAnchor.MiddleCenter;

        Debug.Log("right player " + rightPlayerName.alignment);

        rightPlayerLevel.text = player.statistic.level.ToString();
        if(rightPlayerPrize != null)
            rightPlayerPrize.text = Mathf.RoundToInt(prize).ToString();
        rightPlayerScore.text = score.ToString();
        int minutes = time / 60;
        int secs = time % 60;
        int decSeconds = secs / 10;
        int seconds = secs % 10;        
        rightPlayerTime.text = string.Format("{0}:{1}{2}", minutes, decSeconds, seconds);

        int playerLevel = player.statistic.level;
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(playerLevel);

        rightPlayerVisual.fillAmount = player.statistic.score / pointForNextLevel;
    }
    #endregion

    #region Winner setup

    public void SetLeftWinner()
    {
        if (leftPlayerWinner != null)
        {
            leftPlayerWinner.SetActive(true);
        }
        if (middlePlayerWinner != null)
        {
            middlePlayerWinner.SetActive(false);
        }
        if (rightPlayerWinner != null)
        {
            rightPlayerWinner.SetActive(false);
        }

        if (leftPlayerName != null)
        {
            leftPlayerName.color = winCollor;
            leftDots.color = winCollor;
        }
        if (middlePlayerName != null)
        {
            middleDots.color = defaultColor;
            middlePlayerName.color = defaultColor;
        }
        if (rightPlayerName != null)
        {
            rightDots.color = defaultColor;
            rightPlayerName.color = defaultColor;
        }

        if (leftPlayerAvatarBack != null)
            leftPlayerAvatarBack.sprite = winnerBackAvatar;
        if (middlePlayerAvatarBack != null)
            middlePlayerAvatarBack.sprite = defaultBackAvatar;
        if (rightPlayerAvatarBack != null)
            rightPlayerAvatarBack.sprite = defaultBackAvatar;
    }

    public void SetMiddleWinner()
    {
        if(leftPlayerWinner != null)
        {
            leftPlayerWinner.SetActive(false);
        }
        if (middlePlayerWinner != null)
        {
            middlePlayerWinner.SetActive(true);
        }
        if (rightPlayerWinner != null)
        {
            rightPlayerWinner.SetActive(false);
        }

        if (leftPlayerName != null)
        {
            leftDots.color = defaultColor;
            leftPlayerName.color = defaultColor;
        }
        if (middlePlayerName != null)
        {
            middleDots.color = winCollor;
            middlePlayerName.color = winCollor;
        }
        if (rightPlayerName != null)
        {
            rightDots.color = defaultColor;
            rightPlayerName.color = defaultColor;
        }
            

        if (leftPlayerAvatarBack != null)
            leftPlayerAvatarBack.sprite = defaultBackAvatar;
        if (middlePlayerAvatarBack != null)
            middlePlayerAvatarBack.sprite = winnerBackAvatar;
        if (rightPlayerAvatarBack != null)
            rightPlayerAvatarBack.sprite = defaultBackAvatar;
    }

    public void SetRightWinner()
    {
        if (leftPlayerWinner != null)
        {
            leftPlayerWinner.SetActive(false);
        }
        if (middlePlayerWinner != null)
        {
            middlePlayerWinner.SetActive(false);
        }
        if (rightPlayerWinner != null)
        {
            rightPlayerWinner.SetActive(true);
        }

        if (leftPlayerName != null)
        {
            leftDots.color = defaultColor;
            leftPlayerName.color = defaultColor;
        }
        if (middlePlayerName != null)
        {
            middleDots.color = defaultColor;
            middlePlayerName.color = defaultColor;
        }
        if (rightPlayerName != null)
        {
            rightDots.color = winCollor;
            rightPlayerName.color = winCollor;
        }

        if(leftPlayerAvatarBack != null)
            leftPlayerAvatarBack.sprite = defaultBackAvatar;
        if (middlePlayerAvatarBack != null)
            middlePlayerAvatarBack.sprite = defaultBackAvatar;
        if (rightPlayerAvatarBack != null)
            rightPlayerAvatarBack.sprite = winnerBackAvatar;
    }

    public void SetPrizeForSinglePlayer(float prize)
    {
        if(middlePlayerPrize != null)
            middlePlayerPrize.text = prize.ToString();
    }
    #endregion

    #region Initialization
    public void SetGameResults(ChallengeCompletedData results, int activePlayersCount)
    {
        this.results = results;
        this.activePlayersCount = activePlayersCount;
        Debug.Log("Results null: " + (results == null));
        Debug.Log("Time in game: " + results.gameTime);
    }

    // Need, because request for Spark comes faster than new level saves in storage Spark
    IEnumerator GetPlayerDateWithDelay(string playerId, System.Action<PlayerData> callBack, float delay)
    {
        try
        {
            GameSparksManager.Instance.GetPlayerData(playerId, callBack);
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex);
        }
        yield return new WaitForSeconds(delay);
        try
        {
            GameSparksManager.Instance.GetPlayerData(playerId, callBack);
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex);
        }
    }

    public void Init()
    {
        Debug.Log("showing stats");
        if (results.prizeList.Count <= 0)
        {
            if (statsInfoObject != null)
                statsInfoObject.SetActive(false);
            if(noWinners != null)
                noWinners.SetActive(true);
            return;
        }
        GameSparksManager.Instance.UpdateAuthorizedPlayerData(UpdatedPlayerData);
        Debug.Log("<b>active player count in stats: </b>" + activePlayersCount);
        noWinners.SetActive(false);
        switch(GameData.Instance.singleOrMulti)
        {
            ///single player
            case 0:

                isWinnerMe = GameSparksManager.Instance.AuthorizedPlayerData.playerId.Equals(results.leaderboard[0].userId);
                if (isWinnerMe)
                {
                    InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(results.leaderboard[0].userId, SetLeftPlayer, DELAY_REQUEST_LEVEL));
                    InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(GameData.Instance.opponentId, SetRightPlayer, DELAY_REQUEST_LEVEL));
                    SetLeftWinner();
                }
                else
                {
                    InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(results.leaderboard[0].userId, SetRightPlayer, DELAY_REQUEST_LEVEL));
                    InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(GameSparksManager.Instance.AuthorizedPlayerData.playerId, SetLeftPlayer, DELAY_REQUEST_LEVEL));
                    SetRightWinner();
                }

                break;

            ///multiplayer
            case 1:

                ///Players are less or equal 10               
                if (results.prizeList.Count < 2)
                {
//                    try
//                    {
                        InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(results.leaderboard[0].userId, SetMiddleWinner, DELAY_REQUEST_LEVEL));
//                    }
//                    catch (System.Exception ex)
//                    {
//                        Debug.LogError(ex);
//                    }
                }
                ///Players are from 10 to 40
                else
                {
                    InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(results.leaderboard[0].userId, SetMiddleWinner, DELAY_REQUEST_LEVEL));
                    InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(results.leaderboard[1].userId, SetLeftPlayer, DELAY_REQUEST_LEVEL));
                    rightPlayer.SetActive(false);
                    if (results.prizeList.Count > 2)
                    {
                        rightPlayer.SetActive(true);
                        InGameUIManager.Instance.StartCoroutine(GetPlayerDateWithDelay(results.leaderboard[2].userId, SetRightPlayer, DELAY_REQUEST_LEVEL));
                    }
                }
                break;
        }

    }

    void UpdatedPlayerData()
    {
        playAgainButton.SetActive(activePlayersCount > 1 && GameSparksManager.Instance.AuthorizedPlayerData.coins >= results.entryFee);
    }

    #endregion

    #region Set winners data
    void SetMiddleWinner(PlayerData playerdata)
    {
        Debug.Log("Setting middle player stats");
        int time = GameData.Instance.selectedGameMode == 0 ? GameData.Instance.TimeForGame : results.gameTime;
        SetMiddleUser(playerdata, results.prizeList[0], results.leaderboard[0].score, time);
        if (results.prizeList.Count > 0)
            SetMiddleWinner();
    }      

    void SetLeftPlayer(PlayerData playerdata)
    {
        Debug.Log("Setting left player stats");
        switch (GameData.Instance.singleOrMulti)
        {
            case 0:
                if (isWinnerMe)
                    SetLeftUser(playerdata, results.prizeList[0], results.leaderboard[0].score, results.gameTime);
                else
                    SetLeftUser(playerdata, results.prizeList[0], results.leaderboard.Count > 1 ? results.leaderboard[1].score : 0, results.gameTime);

                SetPrizeForSinglePlayer(results.prizeList[0]);

                break;
            case 1:
                    SetLeftUser(playerdata, results.prizeList[1], results.leaderboard[1].score, results.gameTime);
                break;
        }
    }

    void SetRightPlayer(PlayerData playerdata)
    {
        Debug.Log("Setting right player stats");
        switch(GameData.Instance.singleOrMulti)
        {
            case 0:
                if (!isWinnerMe)
                    SetRightUser(playerdata, results.prizeList[0], results.leaderboard[0].score, results.gameTime);
                else
                    SetRightUser(playerdata, results.prizeList[0], results.leaderboard.Count > 1 ? results.leaderboard[1].score : 0, results.gameTime);

                SetPrizeForSinglePlayer(results.prizeList[0]);

                break;

            case 1:
                SetRightUser(playerdata, results.prizeList[2], results.leaderboard[2].score, results.gameTime);
                break;
        }       
    }
    #endregion

    #region Button hanlders

    public void PlayAgain()
    {
        SoundManager.Instance.PlayButtonPress();
        //Repeat multiplayer game
        if (GameData.Instance.singleOrMulti == 1)
        {
            GameData.Instance.playAgain = true;
            GameData.Instance.resultsPrevChallenge = results;
            GameData.Instance.Save();
            MenuManager.Instance.ReloadCurrentScene();
        }
        else
        {
            ///send play again to opponent
            GameData.Instance.playAgain = true;
            GameData.Instance.resultsPrevChallenge = results;
            GameData.Instance.Save();
            InGameUIManager.Instance.gameManager.PlayAgain();
        }
    }

    public void BackToMainMenu()
    {
		GameData.Instance.playAgain = false;
		SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.BackToMainMenu();
    }

    public void GetCoins()
    {
		GameData.Instance.playAgain = false;
		SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.LoadLevel(MenuManager.Instance.poolShopScene);
    }
    #endregion 
    

    IEnumerator ShowAchievements()
    {
        // achievements comes late, because wait always while active statistic
        while (true)
        {
            List<Achievement> achivsEarned = GameSparksManager.Instance.NewAchievementList;
            Debug.LogWarning("<b>Earned achivs count: </b>" + achivsEarned.Count);
            foreach (Achievement achiv in achivsEarned)
            {
                achivPopup.SetAchiv(achiv.shortCode, achiv.name);
                achivPopup.Show(achivPopupShowDuration);
                yield return new WaitForSeconds(achivPopupShowDuration);
                achivPopup.Hide(achivPopupShowDuration);
                yield return new WaitForSeconds(achivPopupShowDuration);
            }
            yield return new WaitForSeconds(DELAY_REQUEST_LEVEL);
        }
    }

    public void ShowPlayAgainOtherMessage()
    {
        playAgainRight.SetActive(true);
    }

    public void ShowPlayAgainMyMessage()
    {
        playAgainLeft.SetActive(true);
    }

    void HideAllStats()
    {
        leftPlayerAvatar.gameObject.SetActive(false);
        middlePlayerAvatar.gameObject.SetActive(false);
    }

    void HidePlayAgainButton()
    {
        Debug.Log("Challenge has been started. Hiding play again button");
        if(playAgainButton != null)
            playAgainButton.SetActive(false);
    }


}
