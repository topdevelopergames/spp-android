﻿using UnityEngine;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using DG;
using DG.Tweening;

public class StatsPlayerController : MonoBehaviour,  IEnhancedScrollerDelegate{

    [SerializeField]
    private EnhancedScroller scroller;

    [SerializeField]
    private StatsPlayerItem statsItemPrefab;

    [SerializeField]
    private Color winPositionColor, winNameColor, defaultPositionColor, defaultNameColor;
    [SerializeField]
    private Sprite winAvatarBack, defaultAvatarBack;
    [SerializeField]
    private GameObject center, noWinners, playAgainButton;

    [SerializeField]
    private GameObject achievementPopup;
    [SerializeField]
    private float achivPopupShowDuration = 2;
    [SerializeField]
    private float achivPopupShowSpeedDuration = 0.1f;
    [SerializeField]
    private GameObject RetrievingDataFromServerPopup;

    private ChallengeCompletedData results;

    private List<PlayerData> players;
    private Coroutine dataRetriever;


    private List<RuntimeLeaderboardEntry> activePlayers;

    private Coroutine showAchivs;
    private AchievementPopup achivPopup;

    private int activePlayersCount = 0;


    #region scroller interface
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        StatsPlayerItem currentPlayer = scroller.GetCellView(statsItemPrefab) as StatsPlayerItem;
        bool isFacebook = players[dataIndex].facebookId != "";
        Color colorName = dataIndex == 0 ? winNameColor : defaultNameColor;
        Color colorPosition = dataIndex == 0 ? winPositionColor : defaultPositionColor;
        Sprite avatarBack = dataIndex == 0 ? winAvatarBack : defaultAvatarBack;
        currentPlayer.SetPlayer(players[dataIndex], dataIndex + 1, isFacebook, colorName, colorPosition, results.prizeList[dataIndex], results.gameTime, results.leaderboard[dataIndex].score, avatarBack);
        return currentPlayer;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        LayoutElement element = statsItemPrefab.GetComponent<LayoutElement>();
        float width = 573f;
        if (element != null)
        {
            width = element.preferredWidth;
        }
        return width;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return players.Count;
    }

    #endregion scroller interface


    void OnEnable()
    {
        Init();
    }

    void OnDisable()
    {
		Debug.Log("achievementPopup == null? " + (achievementPopup == null));
		if (achievementPopup)
		{
			achievementPopup.SetActive(false);
			if (showAchivs != null)
				StopCoroutine(showAchivs);
			if (dataRetriever != null)
				StopCoroutine(dataRetriever);
		}
        GameSparksManager.Instance.CallbackRepeatedChallengeAutoDeclined -= HidePlayAgainButton;
    }

    void Init()
    {
		Debug.Log("achievementPopup == null? " + (achievementPopup == null));
		if (achievementPopup)
		{
			achievementPopup.SetActive(true);
			achivPopup = achievementPopup.GetComponent<AchievementPopup>();
			achivPopup.RunAwake();
			showAchivs = StartCoroutine(ShowAchievements());
		}
        GameSparksManager.Instance.CallbackRepeatedChallengeAutoDeclined -= HidePlayAgainButton;
        GameSparksManager.Instance.CallbackRepeatedChallengeAutoDeclined += HidePlayAgainButton;
    }

    void InitScroll()
    {
        scroller.gameObject.SetActive(true);
        if (scroller.ScrollRect == null)
            scroller.RunAwake();
        scroller.Delegate = this;       
    }

    public void InitResultScreen()
    {
        int winnersCount = results.prizeList.Count;
        if(winnersCount == 0)
        {
            RetrievingDataFromServerPopup.SetActive(false);
            center.SetActive(false);
            noWinners.SetActive(true);
            return;
        }
        noWinners.SetActive(false);
        center.SetActive(true);
        if(players == null)
            players = new List<PlayerData>();
        players.Clear();
        for (int i = 0; i < winnersCount; i++)
        {
            GameSparksManager.Instance.GetPlayerData(results.leaderboard[i].userId, AddPlayerToList);
        }
        dataRetriever = StartCoroutine(WaitForDownloadAndShow());
    }   

    void AddPlayerToList(PlayerData player)
    {
        players.Add(player);
    }

    IEnumerator WaitForDownloadAndShow()
    {
        RetrievingDataFromServerPopup.SetActive(true);
        scroller.gameObject.SetActive(false);
        while (players.Count < results.prizeList.Count)
        {
            yield return new WaitForEndOfFrame();
        }

		RetrievingDataFromServerPopup.SetActive(false);
		InitScroll();
		// TODO not work on android (Default constructor not found...)
		/*RetrievingDataFromServerPopup.transform.DOSpiral(0.5f, Vector3.up, SpiralMode.Expand, 1, 5, 3, false).OnComplete(
            () => 
            {
                //gameObject.SetActive(false);
				RetrievingDataFromServerPopup.SetActive(false);
                InitScroll();
            });*/
    }
    

    public void SetGameResults(ChallengeCompletedData results)
    {
        this.results = results;
    }
        

    void SetPlayers(List<PlayerData> players)
    {
        this.players = players;
        if (players != null)
            scroller.ReloadData();
    }

    IEnumerator ShowAchievements()
    {
        List<Achievement> achivsEarned = GameSparksManager.Instance.NewAchievementList;
        Debug.LogWarning("<b>Earned achivs count: </b>" + achivsEarned.Count);
        foreach (Achievement achiv in achivsEarned)
        {
            achivPopup.SetAchiv(achiv.shortCode, achiv.name);
            achivPopup.Show(achivPopupShowDuration);
            yield return new WaitForSeconds(achivPopupShowDuration);
            achivPopup.Hide(achivPopupShowDuration);
            yield return new WaitForSeconds(achivPopupShowDuration);
        }
    }

    

    #region Button hanlders

    public void PlayAgain()
    {
        SoundManager.Instance.PlayButtonPress();
        //Repeat multiplayer game
        if (GameData.Instance.singleOrMulti == 1)
        {
            GameData.Instance.playAgain = true;
            GameData.Instance.resultsPrevChallenge = results;
            GameData.Instance.Save();
            MenuManager.Instance.ReloadCurrentScene();
        }
        else
        {
            ///send play again to opponent
            GameData.Instance.playAgain = true;
            GameData.Instance.resultsPrevChallenge = results;
            GameData.Instance.Save();
            InGameUIManager.Instance.gameManager.PlayAgain();
        }
    }

    public void BackToMainMenu()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.BackToMainMenu();
    }

    public void GetCoins()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.LoadLevel(MenuManager.Instance.poolShopScene);
    }
    #endregion 

    void HidePlayAgainButton()
    {
        Debug.Log("Challenge has been started. Hiding play again button");
        if (playAgainButton != null)
            playAgainButton.SetActive(false);
    }

}
