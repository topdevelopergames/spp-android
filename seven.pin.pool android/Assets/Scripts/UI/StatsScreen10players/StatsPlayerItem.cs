﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsPlayerItem : EnhancedUI.EnhancedScroller.EnhancedScrollerCellView {

    [SerializeField]
    private Image userAvatar, facebookAvatar, avatarBack;
    [SerializeField]
    private GameObject facebookAvatarBack, dots;
    [SerializeField]
    private Text prize, place, time, score, username, level;
    [SerializeField]
    private Image visualProgress;

    private int maxLengthName = 9;

    public void SetPlayer(PlayerData player, int place, bool isFacebook, Color colorName, Color colorPosition, float prize, int time, long score, Sprite avatarBack)
    {
        bool isGuest = player.displayName.Equals("Guest");

        this.prize.text = prize.ToString();
        this.score.text = score.ToString();
        this.level.text = player.statistic.level.ToString();
        this.avatarBack.sprite = avatarBack;

        SetTime(time);
        SetPlace(place, colorPosition);
        SetName(isGuest ? player.displayName + player.playerId : player.displayName, colorName);
        SetAvatar(player.avatar, isFacebook);

        int nextLevelPoints = LevelPointsStorage.Instance.GetNextLevelPoints(player.statistic.level);
        SetVisualProgress(player.statistic.score, nextLevelPoints);
    }



    void SetTime(int time)
    {
        int minutes = time / 60;
        int secs = time % 60;
        int decSeconds = secs / 10;
        int seconds = secs % 10;
        this.time.text = string.Format("{0}:{1}{2}", minutes, decSeconds, seconds);
    }

    void SetAvatar(Sprite avatar, bool isFacebook)
    {
        if (isFacebook)
        {
            userAvatar.gameObject.SetActive(false);
            facebookAvatarBack.SetActive(true);
            facebookAvatar.sprite = avatar;
        }
        else
        {
            userAvatar.gameObject.SetActive(true);
            facebookAvatarBack.SetActive(false);
            facebookAvatar.sprite = avatar;
        }
    }

    void SetName(string username, Color color)
    {
        bool isLongName = username.Length > maxLengthName;
        dots.SetActive(isLongName);
        this.username.text = isLongName ? username.Substring(0, maxLengthName) : username;
        this.username.alignment = isLongName ? TextAnchor.MiddleRight : TextAnchor.MiddleCenter;
        this.username.color = color;
    }

    void SetVisualProgress(float currentPoints, float pointsForNextLevel)
    {
        visualProgress.fillAmount = currentPoints / pointsForNextLevel;
    }

    void SetPlace(int place, Color color)
    {
        this.place.text = place.ToString();
        this.place.color = color;
    }


}
