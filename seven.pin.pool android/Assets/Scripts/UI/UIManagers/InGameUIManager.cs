﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public class InGameUIManager : MonoBehaviour
{
    public const float TIME_FOR_TURN = 30;

    [SerializeField]
    Text time;
    [SerializeField]
    Text prizeBankAmount;
    [SerializeField]
    private GameObject prize;
    [SerializeField]
    Text leftUserScore;
    [SerializeField]
    Image leftUserAvatar, leftUserAvatarFacebook;
    [SerializeField]
    GameObject leftUserFacebookBack;
    [SerializeField]
    Text leftUserName;
    [SerializeField]
    GameObject leftDots;
    [SerializeField]
    Image leftUserLevelProgress;
    [SerializeField]
    Text leftUserCurrentLevel;
    [SerializeField]
    Image leftUserTurnTimeLeftVisual;
    [SerializeField]
    Text rightOpponentScore;
    [SerializeField]
    Image rightOpponentAvatar, rightOpponentFacebookAvatar;
    [SerializeField]
    GameObject rightOpponentFacebookBack;
    [SerializeField]
    Text rightOpponentName;
    [SerializeField]
    GameObject rightDots;
    [SerializeField]
    Image rightOpponentLevelProgress;
    [SerializeField]
    Text rightOpponentCurrentLevel;
    [SerializeField]
    Image rightOpponentTurnTimeLeftVisual;
    [SerializeField]
    private GameObject viewChatMessage;

    [SerializeField]
    Image cameraModeButtonImage;
    [SerializeField]
    Sprite camera2D;
    [SerializeField]
    Sprite camera3D;
    [SerializeField]
    private GameObject cameraCircularSlider;
    [SerializeField]
    private GameObject cueControlls;
    [SerializeField]
    public CueController cueController;


    [SerializeField]
    private GameObject singlePlayerStatistic;
    [SerializeField]
    private GameObject multiPlayerLessStatistic;
    [SerializeField]
    private GameObject multiPlayerGreaterStatistic;
    [SerializeField]
    private GameObject multiPlayerBiggerStatistic;

    [SerializeField]
    private GameObject rightHeadSingle;
	[SerializeField]
	private GameObject rightHeadMulti;
	[SerializeField]
	private GameObject leftHeadSingle;

    [SerializeField]
    private GameObject waitingPlayersPopup;
    [SerializeField]
    private GameObject profilePopup;

    [SerializeField]
    private GameObject matchNotFoundPopup;

    [SerializeField]
    private ScoreTable scoreTable;

    [SerializeField]
    private GameObject waitingForOpponentPopup;

    [SerializeField]
    private GameObject warningMessagePopup;
    [SerializeField]
    private Text totalPlayers;

    [HideInInspector]
    public CueForceSlider activeSlider;

    [SerializeField]
    private CueForceSlider sliderTopLeft, sliderTopRight, sliderBotRight, sliderBotLeft;
    [SerializeField]
    private GameObject errorPopup;
    [SerializeField]
    private int timeoutForWaiting = 30;
	[SerializeField]
	ChatSinglePlayer ChatSP;
	[SerializeField]
	ChatMultiPlayer ChatMP;
	[SerializeField]
	AchievementPopup achivPopup;
    [SerializeField]
    Text playersInGame;
    [SerializeField]
    GameObject playersWaitingPopup;

    [SerializeField]
    UnityEngine.UI.Button helpButton;

    public GameManager gameManager;

    private int playersCount;
    private int maxNameLength;
    //private Coroutine timeoutCounter;

    private static InGameUIManager _instace;

	public bool IsStatisticEnabled { get { return singlePlayerStatistic.GetActive() || multiPlayerLessStatistic.GetActive() || multiPlayerGreaterStatistic.GetActive(); } }

    public static InGameUIManager Instance
    {
        get
        {
            return _instace;
        }
    }

	void Awake()
    {
        Debug.Log("ingame awake start");
        if (_instace == null)
        {
            _instace = this;
        }
        Debug.Log("ingame awake end");
        maxNameLength = 9;
    }

    void OnEnable()
    {
        Init();
    }

    void OnDisable()
    {
        _instace = null;
        //if (timeoutCounter != null)
            //StopCoroutine(timeoutCounter);
    }

	IEnumerator GetAvatarSkillz (string url) {
		WWW www = new WWW(url);
		yield return www;
		leftUserAvatar.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
	}

    void Init()
    {
		Debug.Log("Init INGameUIManager");
        SetActiveCue();
		//if (GameData.Instance.matchRules.type != SkillzMatchType.None) {
        //    StartCoroutine (GetAvatarSkillz (SkillzCrossPlatform.GetPlayer ().AvatarURL));
        //    //SetLeftUsername (SkillzCrossPlatform.CurrentUserDisplayName ());
        //    SetLeftUsername(SkillzCrossPlatform.GetPlayer().DisplayName);

        //}
		SetLeftUserAvatar(GameSparksManager.Instance.AuthorizedPlayerData.avatar);
        SetLeftUserLevel(GameSparksManager.Instance.AuthorizedPlayerData.statistic.level.ToString());
        bool isGuest = GameData.Instance.User.Type == ProfileType.Guest || GameData.Instance.User.Type == ProfileType.GuestOffline;
        string username = isGuest ? GameSparksManager.Instance.AuthorizedPlayerData.displayName + GameSparksManager.Instance.AuthorizedPlayerData.playerId : GameSparksManager.Instance.AuthorizedPlayerData.displayName;
        SetLeftUsername(username);
        int level = GameSparksManager.Instance.AuthorizedPlayerData.statistic.level;
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(level);
        SetUserLevelProgress(GameSparksManager.Instance.AuthorizedPlayerData.statistic.score, pointForNextLevel);
        SetLeftUserScore("0");

        if (GameData.Instance.is3D)
        {
            SwitchCameraTo3D();
        }
        else
        {
            SwitchCameraTo2D();
        }

        SetTime(0);
        ///Setup ui for practic mode
        if (GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 0)
        {
            SetLeftUserTurnTimeVisual(0);
            EnablePractic();
            return;
        }


        GameData.Instance.gameStarted = false;

        ///Setuo ui for single 21 mode
        if (GameData.Instance.selectedGameMode == 1 && GameData.Instance.singleOrMulti == 0)
        {
            EnableSingle();

            if (GameData.Instance.playAgain)
            {
                Debug.Log("Playing again previous match");
                PlayAgain();
            }
            GameSparksManager.Instance.GetPlayerData(GameData.Instance.opponentId,
                (opponent) =>
                {
                    if (opponent == null)
                        return;
                    SetRightOpponentAvatar(opponent.avatar, opponent.facebookId != "");
                    SetRightOpponentLevel(opponent.statistic.level.ToString());

                    bool isOpponentGuest = opponent.displayName.Equals("Guest");
                    string opponentName = isOpponentGuest ? opponent.displayName + opponent.playerId : opponent.displayName;

                    SetRightOpponentName(opponentName);
                    SetRightOpponentScore("0");

//                    level = opponent.statistic.level;
//                    pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(level);
//                    SetOpponentLevelProgress(opponent.statistic.score, pointForNextLevel);

                    SetPrizeBankAmount(GameData.prizesText[GameData.Instance.entryFee]);
                    GameData.Instance.gameStarted = true;
                });
            return;
        }

        ///Set time for multiplayer time mode
        if (GameData.Instance.selectedGameMode == 0)
        {
            SetTime(1);
        }

        EnableMulti();
        SetPrizeBankAmount(0);
        if (GameData.Instance.playAgain)
            PlayAgain();
        else
        {
            if (!GameData.Instance.playWithFriends)
                SetupMultiplayer();
            if (GameData.Instance.playWithFriends)
            {
                if (GameData.Instance.challenge != null)
                    CreateFriendChallenge();
                if (GameData.Instance.acceptedChallengeId != "")
                    AcceptFriendChallenge();
            }
        }
    }

    #region Setting head UI
    public void SetLeftUserScore(string score)
    {
        leftUserScore.text = score;
    }

    public void SetLeftUserAvatar(Sprite avatar)
    {
        if (GameData.Instance.User.Type == ProfileType.FB)
        {
            leftUserAvatarFacebook.sprite = avatar;
            leftUserAvatar.gameObject.SetActive(false);
            leftUserFacebookBack.SetActive(true);
        }
        else
        {
            leftUserAvatar.sprite = avatar;
            leftUserAvatar.gameObject.SetActive(true);
            leftUserFacebookBack.SetActive(false);
        }
        
    }

    public void SetLeftUsername(string username)
    {
        bool isNameLong = username.Length > maxNameLength;
        leftDots.SetActive(isNameLong);
        leftUserName.alignment = TextAnchor.MiddleRight;
        if (isNameLong)
        {
            leftUserName.text = username.Substring(0, maxNameLength);
            return;
        }
        leftUserName.text = username;
    }

    public void SetRightOpponentName(string username)
    {
        if (rightOpponentName != null)
        {
            bool isNameLong = username.Length > maxNameLength;
            rightDots.SetActive(isNameLong);
            rightOpponentName.alignment = isNameLong ? TextAnchor.MiddleRight : TextAnchor.MiddleLeft;
            if (isNameLong)
            {
                rightOpponentName.text = username.Substring(0, maxNameLength);
                return;
            }
            rightOpponentName.text = username;
        }
    }

    public void SetRightOpponentScore(string score)
    {
        if (rightOpponentScore != null)
            rightOpponentScore.text = score;
    }

    public void SetRightOpponentAvatar(Sprite avatar, bool isFacebook)
    {
        if (isFacebook)
        {
            if(rightOpponentFacebookAvatar != null)
                rightOpponentFacebookAvatar.sprite = avatar;
            if(rightOpponentFacebookAvatar != null)
                rightOpponentAvatar.gameObject.SetActive(false);
            if(rightOpponentFacebookBack != null)
                rightOpponentFacebookBack.SetActive(true);
        }
        else
        {
            if (rightOpponentAvatar != null)
            {
                rightOpponentAvatar.sprite = avatar;
                rightOpponentAvatar.gameObject.SetActive(true);
            }
            if (rightOpponentFacebookBack != null)
                rightOpponentFacebookBack.SetActive(false);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="time">time in seconds</param>
    public void SetTime(int timeInSeconds)
    {
        int minutes = timeInSeconds / 60;
        int secs = timeInSeconds % 60;
        int decSeconds = secs / 10;
        int seconds = secs % 10;
        time.text = timeInSeconds == 0 ? "" : string.Format("{0}:{1}{2}", minutes, decSeconds, seconds);
    }

    public void SetPrizeBankAmount(long amount)
    {
        Debug.Log("public void SetPrizeBankAmount(long amount) " + amount);
        if (prizeBankAmount != null)
            prizeBankAmount.text = amount.ToString();
        else
            Debug.Log("prizeBankAmount == null ");
    }

    public void SetPrizeBankAmount(string amount)
    {
        Debug.Log("public void SetPrizeBankAmount(long amount) " + amount);
        if (prizeBankAmount != null)
            prizeBankAmount.text = amount;
        else
            Debug.Log("prizeBankAmount == null ");
    }

    public void SetLeftUserLevel(string level)
    {
        leftUserCurrentLevel.text = level;
    }

    public void SetRightOpponentLevel(string level)
    {
        if (rightOpponentCurrentLevel != null)
            rightOpponentCurrentLevel.text = level;
    }


    /// <summary>
    /// max time for shot is 15 sec
    /// </summary>
    /// <param name="timeForTurn"></param>
    public void SetLeftUserTurnTimeVisual(float currentTimeForTurn)
    {
        leftUserTurnTimeLeftVisual.fillAmount = currentTimeForTurn / TIME_FOR_TURN;
    }

    public void SetRightOpponentTurnTimeVisual(float currentTimeForTurn)
    {
        if (rightOpponentTurnTimeLeftVisual != null)
            rightOpponentTurnTimeLeftVisual.fillAmount = currentTimeForTurn / TIME_FOR_TURN;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="newLevelValue">current played games or received xp etc.</param>
    /// <param name="maxLevelValue">amount of games or xp or something like that to reach new level</param>

    public void SetUserLevelProgress(float newLevelValue, float maxLevelValue)
    {
        leftUserLevelProgress.fillAmount = newLevelValue / maxLevelValue;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="newLevelValue">current played games or received xp etc.</param>
    /// <param name="maxLevelValue">amount of games or xp or something like that to reach new level</param>
    public void SetOpponentLevelProgress(float newLevelValue, float maxLevelValue)
    {
        if (rightOpponentLevelProgress != null)
            rightOpponentLevelProgress.fillAmount = newLevelValue / maxLevelValue;
    }

    public void SetTotalPlayers(int playersCount)
    {
        totalPlayers.text = playersCount.ToString();
    }

    #endregion

    #region Waiting screen for multiplayer
    public void ShowWaitingScreen()
    {
        waitingPlayersPopup.SetActive(true);
    }

    public void HideWaitingScreen()
    {
        waitingPlayersPopup.SetActive(false);
    }
    #endregion

    #region Camera mode control
    public void SwitchCameraMode()
    {
        SoundManager.Instance.PlayButtonPress();
        CameraManager.Instance.SwitchCamera();
//		cameraCircularSlider.SetActive(GameData.Instance.is3D);
		cameraCircularSlider.SetActive(GameData.Instance.is3D && !MenuControllerGenerator.controller.isTouchScreen);
        //currently 2d is active and need to change sprite for 3d
        cameraModeButtonImage.sprite = GameData.Instance.is3D ? camera2D : camera3D;
    }

    public void SwitchCameraTo3D()
    {
        CameraManager.Instance.SwitchTo3D();
//        cameraCircularSlider.SetActive(true);
		cameraCircularSlider.SetActive(!MenuControllerGenerator.controller.isTouchScreen);
        GameData.Instance.is3D = true;
        GameData.Instance.Save();
        cameraModeButtonImage.sprite = camera2D;
    }

    public void SwitchCameraTo2D()
    {
        CameraManager.Instance.SwitchTo2D();
        cameraCircularSlider.SetActive(false);
        GameData.Instance.is3D = false;
        GameData.Instance.Save();
        cameraModeButtonImage.sprite = camera3D;
    }
    #endregion

    public void GoBack()
    {
        SoundManager.Instance.PlayButtonPress();
		GameData.Instance.isInGame = false;
		GameData.Instance.gameStarted = false;
		if (GameData.Instance.gameMode == GameTypeMode.Multiplayer) {
			Debug.Log ("Unity: MatchInProgress");
			//SkillzCrossPlatform.AbortMatch ();
            MenuManager.Instance.BackToMainMenu();
        } else {
			Debug.Log ("Unity: !MatchInProgress");
			MenuManager.Instance.BackToMainMenu();
		}
        if (GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 0 || matchNotFoundPopup.activeInHierarchy || errorPopup.activeInHierarchy)
        {
            MenuManager.Instance.BackToMainMenu();
            return;
        }
        warningMessagePopup.SetActive(true);
    }

    #region Chat
    public void ShowChatMessageIcon()
    {
        viewChatMessage.SetActive(true);
    }

    public void HideChatMessageIcon()
    {
        viewChatMessage.SetActive(false);
    }

    void ChallengeChatMessageAdd(ChallengeChatMessage chatMessage)
    {
        if (GameData.Instance.singleOrMulti == 0)
            ChatSP.AddMessageOpponent(chatMessage.text);
        else
            ChatMP.AddMessageOpponent(/*chatMessage.senderId + ": " + */chatMessage.text);
    }

    public void ForSinglePlayerChallengeChatMessageAdd(ChallengeChatMessage chatMessage)
    {
        ChallengeChatMessageAdd(chatMessage);
    }
    #endregion

    #region End game screen
    public void ShowRoundStatistic()
    {
        int mode = GameData.Instance.singleOrMulti;
        switch (mode)
        {
            case 0:
                ///Setup statistic screen
                ///
				ChatSP.ChatDisable();
                singlePlayerStatistic.SetActive(true);
                break;
            case 1:
                ///Setup statistic screen
                ///
				ChatMP.ChatDisable();
                multiPlayerLessStatistic.SetActive(true);
                break;
            case 2:
                ///Setup statistic screen
                ///
				ChatMP.ChatDisable();
                multiPlayerGreaterStatistic.SetActive(true);
                break;
        }
    }
    #endregion

    #region Switch head UI depending on game mode
    public void EnablePractic()
    {
        rightHeadMulti.SetActive(false);
        rightHeadSingle.SetActive(false);
		switch (GameData.Instance.matchRules.type) {
		case SkillzMatchType.Timed:
			time.gameObject.SetActive (true);			
			StartCoroutine (UpdateTime ());
			break;

		case SkillzMatchType.Points_51:
			StartCoroutine (UpdateTimeWithPoints ());
			time.gameObject.SetActive(false);			
			break;

		case SkillzMatchType.None:
			leftHeadSingle.SetActive (false);
			time.gameObject.SetActive(false);			
			break;

		}
        prize.SetActive(false);
        helpButton.gameObject.SetActive(true);
        //First time tutorial launch
        if(!GameData.Instance.firstLaunch)
        {
            GameData.Instance.firstLaunch = true;
            //Click on button
            helpButton.OnPointerClick(new UnityEngine.EventSystems.PointerEventData(UnityEngine.EventSystems.EventSystem.current));
        }
    }

    public void EnableSingle()
    {
        rightHeadSingle.SetActive(true);
        rightHeadMulti.SetActive(false);
        time.gameObject.SetActive(true);
        prize.SetActive(true);
        helpButton.gameObject.SetActive(false);
    }

    public void EnableMulti()
    {
        rightHeadMulti.SetActive(true);
        rightHeadSingle.SetActive(false);
        time.gameObject.SetActive(true);
        prize.SetActive(true);
        waitingPlayersPopup.SetActive(true);
        playersInGame.text = "1";
        helpButton.gameObject.SetActive(false);
        //timeoutCounter = StartCoroutine(WaitingTimeout(timeoutForWaiting));
    }
    #endregion

    #region Profile to show popup setup 
    public void ShowProfile()
    {
        SoundManager.Instance.PlayButtonPress();
        if (!GameSparks.Core.GS.Available)
            return;
        ProfileStatisticsPopup psp = profilePopup.GetComponent<ProfileStatisticsPopup>();
        psp.SetProfileToView(GameSparksManager.Instance.AuthorizedPlayerData.playerId, true, () =>
        {
            profilePopup.SetActive(true);
        });
    }

    public void ShowRightProfile()
    {
        SoundManager.Instance.PlayButtonPress();
        if (GameData.Instance.opponentId == "" || !GameSparks.Core.GS.Available)
            return;
        ProfileStatisticsPopup psp = profilePopup.GetComponent<ProfileStatisticsPopup>();
        psp.SetProfileToView(GameData.Instance.opponentId, true, () =>
        {
            profilePopup.SetActive(true);
        });
    }
    #endregion


    #region Multiplayer setup
    void SetupMultiplayer()
    {
        BlockControls();
        bool isGuest = GameData.Instance.User.Type == ProfileType.Guest;
        int selectedGameMode = GameData.Instance.selectedGameMode;
        int timeMode = GameData.Instance.timeForGame;
        int pointsMode = GameData.Instance.pointsForGame;
        int selectedTable = GameData.Instance.playTable;
        GameMode gameMode = GameMode.TIME_1;
        switch (selectedGameMode)
        {
            ///time mode
            case 0:
                switch (timeMode)
                {
                    case 0:
                        gameMode = GameMode.TIME_1;
                        break;
                    case 1:
                        gameMode = GameMode.TIME_3;
                        break;
                    case 2:
                        gameMode = GameMode.TIME_5;
                        break;
                }
                break;

            ///21 mode
            case 1:
                /*switch (pointsMode)
                {
                    case 0:
                        gameMode = GameMode.FIRST21;
                        break;
                    case 1:
                        gameMode = GameMode.FIRST51;
                        break;
                    case 2:
                        gameMode = GameMode.FIRST101;
                        break;
                }*/
                gameMode = GameMode.FIRST51;
                break;
        }
        TableType table = TableType.POCKET;
        switch (selectedTable)
        {
            case 0:
                table = TableType.POCKET;
                break;

            case 1:
                table = TableType.NOPOCKET;
                break;
        }

        MatchType match = new MatchType(isGuest ? AuthType.GUEST : AuthType.SIGNED, gameMode, table, GameData.entryFees[GameData.Instance.entryFee]);
        Debug.Log("<b>Match type: </b>" + match.ToString());
        GameSparksManager.Instance.MatchmakingMultiPlayer(GameSparksManager.Instance.AuthorizedPlayerData.statistic.level,
            match,
            ChallengeUpdated,
            ChallengeStarted,
            ChallengeChanged,
            ChallengeTimeOver,
            ChallengeComplete,
            MatchNotFound,
            ChallengeChatMessageAdd,
            ErrorCallback);
    }

    private List<MultiplayerData> multiplayersData;

    /// <summary>
    /// Matchsearching and add new players
    /// </summary>
    /// <param name="players"></param>
    void ChallengeUpdated(List<PlayerData> players)
    {
        Debug.Log("Looking for opponents...: " + players.Count);
        if(GameData.Instance.singleOrMulti == 1)
        {
            playersInGame.text = (players.Count + 1).ToString();
        }
    }

    Coroutine timeUpdate;

    /// <summary>
    /// Game started
    /// </summary>
    /// <param name="players"></param>
    void ChallengeStarted(List<PlayerData> players)
    {
        Debug.Log("Challenge is started");
        GameData.Instance.challengeTimeOver = false;
        GameData.Instance.gameStarted = true;
        //if (timeoutCounter != null)
            //StopCoroutine(timeoutCounter);

        UnblockControls();

        if (GameData.Instance.selectedGameMode == 0 && GameData.Instance.singleOrMulti == 1)
        {
            timeUpdate = StartCoroutine(UpdateTime());
        }

        if (GameData.Instance.singleOrMulti == 1)
            SetTotalPlayers(players.Count);

        playersCount = players.Count;
        long prize = playersCount * GameData.entryFees[GameData.Instance.entryFee];
        SetLeftUserTurnTimeVisual(0);
        SetPrizeBankAmount(prize);
        if (GameData.Instance.singleOrMulti == 1)
        {
            try
            {
                scoreTable.SetPlayers(SetupMultiplayerData(players));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
//            waitingPlayersPopup.SetActive(false);
        }
		waitingPlayersPopup.SetActive(false);
    }

    /// <summary>
    /// call after each hit
    /// </summary>
    /// <param name="players"></param>
    void ChallengeChanged(List<RuntimeLeaderboardEntry> leaders)
    {
        Debug.Log("Updating stats in table");
        Debug.Log("<i>leaders is null: </i>" + (leaders == null));
        Debug.Log("<b>runtime leaderboard count: </b>" + leaders.Count);
        if (GameData.Instance.singleOrMulti == 1)
        {
            try
            {
                scoreTable.SetPlayers(UpdateMultiplayerData(leaders));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }
        else
        {
            Debug.Log("Something changed single");
        }
    }

    public void ChallengeCompleteExtCall(ChallengeCompletedData players)
    {
        ChallengeComplete(players);
    }

    public void ChallengeStartedExtCall(List<PlayerData> players)
    {
        ChallengeStarted(players);
    }

    public void ChallengeChangedExtCall(List<RuntimeLeaderboardEntry> leaders)
    {
        ChallengeChanged(leaders);
    }

	bool IsIWinner(ChallengeCompletedData players)
	{
		int pos = players.leaderboard.FindIndex((x => x.userId == GameSparksManager.Instance.AuthorizedPlayerData.playerId));
		return pos < players.prizeList.Count && pos >= 0;
	}

    /// <summary>
    /// Game is finished
    /// </summary>
    /// <param name="players">Data with prizes list and game participants</param>
    void ChallengeComplete(ChallengeCompletedData players)
    {
        Debug.Log("Challenge is finished");
        Debug.Log("Leaderboard count: " + players.leaderboard.Count);
        Debug.Log("Prize list count: " + players.prizeList.Count);

        playersWaitingPopup.SetActive(false);

        List<RuntimeLeaderboardEntry> activePlayers = players.leaderboard.FindAll(x => x.excluded == false);
		Debug.Log("Active players count: " + activePlayers.Count);

		// achievemsts
		if (IsIWinner(players))
		{
            GameSparksManager.Instance.AddAchievement(AchievementType.WIN);
            // single
            if (GameData.Instance.singleOrMulti == 0)
			{
				if (!Rules.Instance.IsWasChangeQueue)
					GameSparksManager.Instance.AddAchievement(AchievementType.DENIAL);
				if (!Rules.Instance.IsWasOpponentOvertunePins)
					GameSparksManager.Instance.AddAchievement(AchievementType.PERFECT_WIN);
				if (players.leaderboard.FindIndex(x => (int)x.rank > (int)GameSparksManager.Instance.AuthorizedPlayerData.statistic.rank + 3) >= 0)
					GameSparksManager.Instance.AddAchievement(AchievementType.UNDERDOG);
			}

			if (!Rules.Instance.IsWasMoreHalfPowerQue && Rules.Instance.MaxPowerQue > 0f)
				GameSparksManager.Instance.AddAchievement(AchievementType.HALF_POWER);
			if (Rules.Instance.IsWasOnlyMaxPowerQue)
				GameSparksManager.Instance.AddAchievement(AchievementType.MAX_POWER);
			if (players.leaderboard.Count >= 100)
				GameSparksManager.Instance.AddAchievement(AchievementType.MULTIPLAYER_NINJA);
		}

		//if (GameData.Instance.User.Type != ProfileType.Guest)
		//{
		GameSparksManager.Instance.UpdateLeaderboard();
		//}

        if (timeUpdate != null)
            StopCoroutine(timeUpdate);

        BlockControls();
        switch (GameData.Instance.singleOrMulti)
        {
            case 0:
                Debug.Log("Single player challenge ended");
                Debug.Log("Single player statistic screen is null: " + (singlePlayerStatistic == null));
                ChatSP.ChatDisable();
                if (singlePlayerStatistic != null)
                {
                    StatisticScreenPopup singleScreenPopup = singlePlayerStatistic.GetComponent<StatisticScreenPopup>();
                    Debug.Log("single screen popup is null: " + (singleScreenPopup == null));
                    if (singleScreenPopup != null)
                    {
                        singleScreenPopup.SetGameResults(players, activePlayers.Count);
                        singleScreenPopup.Init();
                        singlePlayerStatistic.SetActive(true);
                    }
                }
                break;

            case 1:
                ChatMP.ChatDisable();
                    ///player are up to 10
                    if (players.prizeList.Count < 2)
                    {
                        Debug.Log("Players in game are less then 10");
                        StatisticScreenPopup screenPopup = multiPlayerLessStatistic.GetComponent<StatisticScreenPopup>();
                        screenPopup.SetGameResults(players, activePlayers.Count);
                        screenPopup.Init();
                        multiPlayerLessStatistic.SetActive(true);
                    }
                    else
                    {
                        ///players are from 10 to 40
                        if (players.prizeList.Count >= 2 && players.prizeList.Count < 4)
                        {
                            Debug.Log("Players in game are greater then 10");
                            StatisticScreenPopup screenPopup = multiPlayerGreaterStatistic.GetComponent<StatisticScreenPopup>();
                            screenPopup.SetGameResults(players, activePlayers.Count);
                            screenPopup.Init();
                            multiPlayerGreaterStatistic.SetActive(true);
                        }
                        ///players are 40 or more
                        else
                        {
                            StatsPlayerController statsController = multiPlayerBiggerStatistic.GetComponent<StatsPlayerController>();
                            statsController.SetGameResults(players);
                            multiPlayerBiggerStatistic.SetActive(true);
							statsController.InitResultScreen();
						}
                    }
                break;
        }
    }

    void MatchNotFound()
    {
        Debug.Log("Match hasn't been found...");
        if (GameData.Instance.playWithFriends)
        {
            GameData.Instance.challenge = null;
            GameData.Instance.acceptedChallengeId = "";
        }
        if (matchNotFoundPopup != null)
            matchNotFoundPopup.SetActive(true);
        //if (timeoutCounter != null)
            //StopCoroutine(timeoutCounter);
        
    }

    void ErrorCallback()
    {
        Debug.Log("Something wrong happened...");
        if (GameData.Instance.playWithFriends)
        {
            GameData.Instance.challenge = null;
            GameData.Instance.acceptedChallengeId = "";
        }
        if(errorPopup != null)
            errorPopup.SetActive(true);
        //if (timeoutCounter != null)
            //StopCoroutine(timeoutCounter);
        
    }

    void ChallengeTimeOver()
    {
        Debug.Log("Challenge time over");
        GameData.Instance.challengeTimeOver = true;
        BlockControls();
    }

    List<MultiplayerData> SetupMultiplayerData(List<PlayerData> players)
    {
        if (players != null)
        {
            multiplayersData = new List<MultiplayerData>();
            foreach (PlayerData item in players)
            {
                MultiplayerData mData = new MultiplayerData();
                mData.player = item;
                mData.score = 0;
                multiplayersData.Add(mData);
            }
        }
        return multiplayersData;
    }

    List<MultiplayerData> UpdateMultiplayerData(List<RuntimeLeaderboardEntry> leaders)
    {
        Debug.Log("Updating list of users");
        Debug.Log("<b>Multiplayer data is null:  </b>" + (multiplayersData == null));
		if (leaders != null && multiplayersData != null)
		{
			foreach (var item in leaders)
			{
				multiplayersData.Find(x => x.player.playerId == item.userId).score = item.score;
			}
			multiplayersData.Sort(MultiplayerData.CompareByScore);
		}
        return multiplayersData;
    }

    void CreateFriendChallenge()
    {
        BlockControls();
        GameSparksManager.Instance.CreateChallenge(GameData.Instance.challenge,
            ChallengeStarted,
            MatchNotFound,
            ChallengeChanged,
            ChallengeTimeOver,
            ChallengeComplete,
            ChallengeChatMessageAdd,
            ErrorCallback);
    }

    void AcceptFriendChallenge()
    {
        BlockControls();
        GameSparksManager.Instance.AcceptChallenge(GameData.Instance.acceptedChallengeId,
            ChallengeStarted,
            ChallengeChanged,
            ChallengeTimeOver,
            ChallengeComplete,
            ChallengeChatMessageAdd,
            ErrorCallback);
    }


    #endregion

    public void BackToMenu()
    {
        SoundManager.Instance.PlayButtonPress();
        warningMessagePopup.SetActive(false);
        Debug.Log("Returning to menu");
        GameSparksManager.Instance.RemovePlayerFromCurrentChallenge();
        MenuManager.Instance.BackToMainMenu();
    }


    /// <summary>
    /// Blocks control until match start
    /// </summary>
    void BlockControls()
    {
        Debug.LogWarning("block controlls method");
        Debug.Log("Camera circular slider is null: " + (cameraCircularSlider == null));
        if (cameraCircularSlider != null)
            cameraCircularSlider.SetActive(false);
        Debug.Log("Cue controlls is null: " + (cueControlls == null));
        if (cueControlls != null)
            cueControlls.SetActive(false);
        Debug.Log("Cue controller is null: " + (cueController == null));
        if (cueController != null)
            cueController.CanControlCue = false;
        Debug.Log("MenuControllerGenerator.controller is null: " + (MenuControllerGenerator.controller == null));
        if (MenuControllerGenerator.controller != null)
        {
            if (MenuControllerGenerator.controller.isTouchScreen)
            {
                Debug.Log("active slider is null: " + (activeSlider == null));
                if (activeSlider != null)
                {
                    activeSlider.Reset();
                    activeSlider.DisableForceSlider();
                }
            }
        }
    }

    public void BlockForceSlider()
    {
        if (MenuControllerGenerator.controller.isTouchScreen)
        {
            Debug.Log("active slider is null: " + (activeSlider == null));
            if (activeSlider != null)
            {
                activeSlider.Reset();
                activeSlider.DisableForceSlider();
            }
        }
    }

    public void UnblockForceSlider()
    {
        if (MenuControllerGenerator.controller.isTouchScreen)
        {
            Debug.Log("active slider is null: " + (activeSlider == null));
            if (activeSlider != null)
            {
                activeSlider.Reset();
                activeSlider.EnableSForceSlider();
            }
        }
    }

    public void BlockCue()
    {
        cueController.CanControlCue = false;
    }

    public void UnblockCue()
    {
        cueController.CanControlCue = true;
    }

    /// <summary>
    /// Unblocks control after natch started
    /// </summary>
    void UnblockControls()
    {
        Debug.LogWarning("Unblock controlls method");
        Debug.Log("Camera circular slider is null: " + (cameraCircularSlider == null));
        if (cameraCircularSlider != null)
        //    cameraCircularSlider.SetActive(GameData.Instance.is3D);
			cameraCircularSlider.SetActive(GameData.Instance.is3D && !MenuControllerGenerator.controller.isTouchScreen);
        Debug.Log("Cue controlls is null: " + (cueControlls == null));
        if (cueControlls != null)
            cueControlls.SetActive(true);
        Debug.Log("Cue controller is null: " + (cueController == null));
        if (cueController != null)
            cueController.CanControlCue = true;
        Debug.Log("MenuControllerGenerator.controller is null: " + (MenuControllerGenerator.controller == null));
        if (MenuControllerGenerator.controller != null)
        {
            if (MenuControllerGenerator.controller.isTouchScreen)
            {
                Debug.Log("active slider is null: " + (activeSlider == null));
                if (activeSlider != null)
                {
                    activeSlider.Reset();
                    activeSlider.EnableSForceSlider();
                }
            }
        }
    }

    /// <summary>
    /// Coroutine for time update in ui
    /// </summary>
    /// <returns></returns>
    IEnumerator UpdateTime()
    {
		timeInSecs = GameData.Instance.matchRules.timeinseconds; //GameData.Instance.TimeForGame;
        while (timeInSecs > 0)
        {
            SetTime(timeInSecs);
            yield return new WaitForSeconds(1);
            timeInSecs--;
        }
        SetTime(timeInSecs);
//        playersWaitingˇgameÏPopup.SetActive(true);
		//SkillzCrossPlatform.ReportFinalScore (leftUserScore.text);
    }

	IEnumerator UpdateTimeWithPoints () {
		timeInSecs = 0;
		bool isTargetAchieved = false;

		do {
			SetTime(timeInSecs);
			if (int.Parse (leftUserScore.text) >= GameData.Instance.matchRules.points) {
				isTargetAchieved = true;
			}
			yield return new WaitForSeconds(1);
			timeInSecs++;			
		} while (!isTargetAchieved);

		SetTime(timeInSecs);
		//        playersWaitingˇgameÏPopup.SetActive(true);
		//SkillzCrossPlatform.ReportFinalScore (timeInSecs.ToString ());
	}

    int timeInSecs;

    bool prevState = false;

    void OnApplicationPause(bool pauseState)
    {
        Debug.Log("OnApplicationPause InGameUIManagar: " + pauseState);
        if (pauseState)
            prevState = pauseState;
        if (prevState && !pauseState && GameData.Instance.singleOrMulti == 1)
        {
            Debug.Log("<b>Updating challenge time</b>");
            GameSparksManager.Instance.GetCurrentChallengeGameTime(CurrentChallengeTime);
        }
    }

    void CurrentChallengeTime(int time)
    {
        Debug.LogWarning("<b>Got new time. Let's update it. Time left:</b> " + time);
        timeInSecs = time;
        Debug.Log("Time in secs: " + timeInSecs);
    }




    public void HideBallControlls()
    {
        cueControlls.SetActive(false);
    }

    public void ShowBallControlls()
    {
        cueControlls.SetActive(true);
    }

    public void ShowWaitingForOpponentPopup()
    {
        waitingForOpponentPopup.SetActive(true);
    }

    public void HideWaitingForOpponentPopup()
    {
        waitingForOpponentPopup.SetActive(false);
    }

    void PlayAgain()
    {
        BlockControls();
        GameSparksManager.Instance.PlayAgain(GameData.Instance.resultsPrevChallenge,
                ChallengeStarted,
                MatchNotFound,
                ChallengeChanged,
                ChallengeTimeOver,
                ChallengeComplete,
                ChallengeChatMessageAdd,
                ErrorCallback);
        GameData.Instance.playAgain = false;
        GameData.Instance.resultsPrevChallenge = null;
        GameData.Instance.Save();

    }

    void SetActiveCue()
    {
        switch (GameData.Instance.forceSliderPosition)
        {
            case 0:
                sliderTopRight.gameObject.SetActive(false);
                sliderBotLeft.gameObject.SetActive(false);
                sliderBotRight.gameObject.SetActive(false);
                activeSlider = sliderTopLeft;
                cueController.SetActiveSlider(activeSlider);
                break;
            case 1:
                sliderTopLeft.gameObject.SetActive(false);
                sliderBotLeft.gameObject.SetActive(false);
                sliderBotRight.gameObject.SetActive(false);
                activeSlider = sliderTopRight;
                cueController.SetActiveSlider(activeSlider);
                break;
            case 2:
                sliderTopLeft.gameObject.SetActive(false);
                sliderBotLeft.gameObject.SetActive(false);
                sliderTopRight.gameObject.SetActive(false);
                activeSlider = sliderBotRight;
                cueController.SetActiveSlider(activeSlider);
                break;
            case 3:
                sliderTopLeft.gameObject.SetActive(false);
                sliderBotRight.gameObject.SetActive(false);
                sliderTopRight.gameObject.SetActive(false);
                activeSlider = sliderBotLeft;
                cueController.SetActiveSlider(activeSlider);
                break;
        }

    }


    void TimeToStartCallback(int time)
    {
        if (time == 0)
            MatchNotFound();
    }

    #region Debug mehtods

    public void AddAchievement()
    {
        GameSparksManager.Instance.AddAchievement(AchievementType.COMBINATION_SHOT, SuccesAdded);
    }

    public void RemoveAchievement()
    {
        GameSparksManager.Instance.RemoveAchievement(AchievementType.COMBINATION_SHOT, SuccesRemoved);
    }

    public void AddFewAchivements()
    {
        GameSparksManager.Instance.AddAchievement(AchievementType.COMBINATION_SHOT);
        GameSparksManager.Instance.AddAchievement(AchievementType.DENIAL);
        GameSparksManager.Instance.AddAchievement(AchievementType.DOUBLE_DIP);
        GameSparksManager.Instance.AddAchievement(AchievementType.HALF_POWER);
        GameSparksManager.Instance.AddAchievement(AchievementType.MAX_POWER);
        GameSparksManager.Instance.AddAchievement(AchievementType.OVERTURN_100);
        SuccesAddedFew();
    }

    public void RemoveFewAchievements()
    {
        GameSparksManager.Instance.RemoveAchievement(AchievementType.COMBINATION_SHOT);
        GameSparksManager.Instance.RemoveAchievement(AchievementType.DENIAL);
        GameSparksManager.Instance.RemoveAchievement(AchievementType.DOUBLE_DIP);
        GameSparksManager.Instance.RemoveAchievement(AchievementType.HALF_POWER);
        GameSparksManager.Instance.RemoveAchievement(AchievementType.MAX_POWER);
        GameSparksManager.Instance.RemoveAchievement(AchievementType.OVERTURN_100);
        SuccesRemovedFew();
    }

    void SuccesAdded()
    {
        Debug.Log("<b>Achiv added</b>");
    }

    void SuccesRemoved()
    {
        Debug.Log("<b>Achiv removed</b>");
    }

    void SuccesAddedFew()
    {
        Debug.LogWarning("<b>3 Achivs added</b>");
    }

    void SuccesRemovedFew()
    {
        Debug.LogWarning("<b>3 Achivs removed</b>");
    }

    #endregion

	IEnumerator ShowOneAchievement(Achievement achievement)
	{
		Debug.LogWarning("<b>Earned achivement: </b>" + achievement.description);
		achivPopup.gameObject.SetActive(true);
		achivPopup.SetAchiv(achievement.shortCode, achievement.name);
		achivPopup.Show(2f);
		yield return new WaitForSeconds(2f);
		achivPopup.Hide(2f);
		yield return new WaitForSeconds(2f);
		achivPopup.gameObject.SetActive(false);
	}

	public void ShowAchievement(Achievement achievement)
	{
		StartCoroutine(ShowOneAchievement(achievement));
	}

}
