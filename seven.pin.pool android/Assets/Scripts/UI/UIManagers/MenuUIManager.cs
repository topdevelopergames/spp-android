﻿using GameSparks.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUIManager : MonoBehaviour
{

    [SerializeField]
    private Image avatar;
    [SerializeField]
    private Image facebookAvatar;
    [SerializeField]
    private GameObject facebookAvatarBack;
    [SerializeField]
    private Text userName;
    [SerializeField]
    private GameObject dots;
    [SerializeField]
    private Text level;
    [SerializeField]
    private Text coins;
    [SerializeField]
    private Text cash;
    [SerializeField]
    private Image visualProgressBar;
    [SerializeField]
    private GameObject profilePopUp;
    [SerializeField]
    private GameObject inviteMessagePopup;
    [SerializeField]
    private GameObject noConnectionPopup;
    [SerializeField]
    private GameObject buyMoreCoins, buyMoreCash;
    [System.NonSerialized]
    public InvitePopup invitePopup;
    [SerializeField]
    private GameObject reconnectButton;

    private ChallengeCreatedData challenge;
    private int maxNameLength;
    private static MenuUIManager _instance;

    public static MenuUIManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private GameData gameData;


    void Awake()
    {
        maxNameLength = 9;
        if (_instance == null)
        {
            _instance = this;
            
        }
        if(inviteMessagePopup != null)
        {
            invitePopup = inviteMessagePopup.GetComponent<InvitePopup>();
        }
        gameData = GameData.Instance;
        if (gameData.User.Type == ProfileType.FB)
        {
            GameSparksManager.Instance.CallbackFriendChallengeIssued = MenuManager.Instance.ShowInvite;
            GameSparksManager.Instance.CallbackFriendChallengeAutoDeclined = AutoDeclineInvite;
        }
        gameData.pointsForLevel = LevelPointsStorage.Instance.nextLevelPoints.ToArray();
        if (!GS.Available)
        {
            if (gameData.ownedCues == null)
                gameData.InitOwnedLists();
            gameData.needToShowOfflinePopup = true;
            gameData.Save();
            if (reconnectButton != null)
                reconnectButton.SetActive(true);
        }
    }

    void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        if (userName == null)
            return;

        bool isGuest = gameData.User.Type == ProfileType.Guest || gameData.User.Type == ProfileType.GuestOffline;

        //if (buyMoreCash != null)
            //buyMoreCash.SetActive(!isGuest);
        //if (buyMoreCoins != null)
            //buyMoreCoins.SetActive(!isGuest);
        if (GS.Available)
        {
            if (gameData.needToUpdateInfo)
            {
                Debug.Log("Updating player info");
                gameData.needToUpdateInfo = false;
                gameData.Save();
                GameSparksManager.Instance.UpdateAuthorizedPlayerData(
                    () =>
                    {
                        if (avatar != null)
                        {
                            SetAvatar(GameSparksManager.Instance.AuthorizedPlayerData.avatar);
                        }
                        string username = isGuest ? GameSparksManager.Instance.AuthorizedPlayerData.displayName + GameSparksManager.Instance.AuthorizedPlayerData.playerId : GameSparksManager.Instance.AuthorizedPlayerData.displayName;
                        SetUserName(username);
                        SetLevel(GameSparksManager.Instance.AuthorizedPlayerData.statistic.level.ToString());
                        SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
                        SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
                        Debug.Log("before SetVisualProgressBar");
                        SetVisualProgressBar(GameSparksManager.Instance.AuthorizedPlayerData.statistic.score, GameSparksManager.Instance.AuthorizedPlayerData.statistic.level);
                        Debug.Log("after SetVisualProgressBar");
                        return;
                    });
            }
        }
//        if (avatar != null)
//        {
//            SetAvatar(GameSparksManager.Instance.AuthorizedPlayerData.avatar);
//        }
//        string usernamee = isGuest ? GameSparksManager.Instance.AuthorizedPlayerData.displayName + GameSparksManager.Instance.AuthorizedPlayerData.playerId : GameSparksManager.Instance.AuthorizedPlayerData.displayName;
//        SetUserName(usernamee);
//        SetLevel(GameSparksManager.Instance.AuthorizedPlayerData.statistic.level.ToString());
//        SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
//        SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
//        SetVisualProgressBar(GameSparksManager.Instance.AuthorizedPlayerData.statistic.score, GameSparksManager.Instance.AuthorizedPlayerData.statistic.level);
//        if (!GS.Available && !gameData.offlinePopupShown && gameData.needToShowOfflinePopup)
//            ShowNoConnetionPopup();

    }


    public void SetAvatar(Sprite avatar)
    {
        if (gameData.User.Type == ProfileType.FB)
        {
            facebookAvatarBack.SetActive(true);
            this.avatar.gameObject.SetActive(false);
            facebookAvatar.sprite = avatar;
        }
        else
        {
            facebookAvatarBack.SetActive(false);
            this.avatar.gameObject.SetActive(true);
            this.avatar.sprite = avatar;
        }
    }

    public void SetUserName(string username)
    {
        bool nameLong = username.Length > maxNameLength;
        ///Enable dots if player's name length gretear 9 characters
        if (dots != null)
            dots.SetActive(nameLong);
        userName.alignment = TextAnchor.MiddleRight;
        if(nameLong)
        {
            this.userName.text = username.Substring(0, maxNameLength);
            return;
        }
        this.userName.text = username;
    }

    public void SetLevel(string level)
    {
        this.level.text = level;
    }

    public void SetCoins(string coins)
    {
        this.coins.text = coins;
    }

    public void SetCash(string cash)
    {
        this.cash.text = cash;
    }

    public void SetVisualProgressBar(float currentPoints, int currentLevel)
    {
        float pointForNextLevel = LevelPointsStorage.Instance.GetNextLevelPoints(currentLevel);
        this.visualProgressBar.fillAmount = currentPoints / pointForNextLevel;
    }

    public void BuyMoreCoins()
    {
#if UNITY_ANDROID || UNITY_IOS
        SoundManager.Instance.PlayButtonPress();
        if (!GS.Available || gameData.isInGame)
            return;
        gameData.lastOpenedTab = 0;
        MenuManager.Instance.LoadPoolShop();
#endif
    }

    public void BuyMoreCash()
    {
#if UNITY_ANDROID || UNITY_IOS
        SoundManager.Instance.PlayButtonPress();
        if (!GS.Available || gameData.isInGame)
            return;
        gameData.lastOpenedTab = 1;
        MenuManager.Instance.LoadPoolShop();
#endif
    }

    public void ShowProfile()
    {
        SoundManager.Instance.PlayButtonPress();
        ProfileStatisticsPopup psp = profilePopUp.GetComponent<ProfileStatisticsPopup>();
        if (!GS.Available)
        {
            psp.SetProfileToView(GameSparksManager.Instance.AuthorizedPlayerData, GS.Available, ()=>
            {
                profilePopUp.SetActive(true);
            });
            return;
        }
        psp.SetProfileToView(GameSparksManager.Instance.AuthorizedPlayerData.playerId, GS.Available, () =>
        {
            profilePopUp.SetActive(true);
        });

    }

    public void ShowInvite(string message)
    {
        inviteMessagePopup.GetComponent<InvitePopup>().SetMessage(message);
        inviteMessagePopup.SetActive(true);
    }

    public void AcceptInvite()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.AcceptInvite();
    }

    void AutoDeclineInvite(string challengeId)
    {
        MenuManager.Instance.RemoceInviteInfo();
        inviteMessagePopup.SetActive(false);
    }

    public void DeclineInvite()
    {
        SoundManager.Instance.PlayButtonPress();
        MenuManager.Instance.DeclineInvite();
    }

    public void ShowNoConnetionPopup()
    {
        noConnectionPopup.SetActive(true);
        gameData.offlinePopupShown = true;
    }

    public void Reconnect()
    {
        SoundManager.Instance.PlayButtonPress();
        if (noConnectionPopup.activeInHierarchy)
            noConnectionPopup.SetActive(false);
        if(reconnectButton != null)
        {
            reconnectButton.GetComponentInChildren<Text>().text = "Reconnecting";
            reconnectButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
        }
        MenuManager.Instance.CheckConnection(ConnectionEstablished, ConnectionFailed);
        
    }

    void ConnectionEstablished()
    {
        if (reconnectButton != null)
        {
            reconnectButton.SetActive(false);
            reconnectButton.GetComponentInChildren<Text>().text = "Reconnect";
            reconnectButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
        }
        if (gameData.User.Type == ProfileType.GuestOffline)
        {
            MenuManager.Instance.LoadLevel(MenuManager.Instance.loginSceneName);
            return;
        }
        MenuManager.Instance.ReloadCurrentScene();
    }

    void ConnectionFailed()
    {
        if (reconnectButton != null)
        {
            reconnectButton.GetComponentInChildren<Text>().text = "Reconnect";
            reconnectButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
        }
        ShowNoConnetionPopup();
    }

}
