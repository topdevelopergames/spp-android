﻿using UnityEngine;
using System.Collections;

public class BallOutFromHolle : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		BallController ballController = other.GetComponent<BallController>();
		if (ballController)
			ballController.ballIsHolle = false;
	}
}
