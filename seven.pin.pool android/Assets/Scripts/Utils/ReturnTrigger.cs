﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class ReturnTrigger : MonoBehaviour, IPointerClickHandler, IBeginDragHandler
{

    [SerializeField]
    private UnityEngine.UI.Button button;

    private EventSystem _system;

    void OnEnable()
    {
        _system = EventSystem.current;
        _system.SetSelectedGameObject(button.gameObject);        
    }
    
    void OnDisable()
    {
        _system.SetSelectedGameObject(_system.firstSelectedGameObject);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _system.SetSelectedGameObject(button.gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _system.SetSelectedGameObject(button.gameObject);
    }
}
