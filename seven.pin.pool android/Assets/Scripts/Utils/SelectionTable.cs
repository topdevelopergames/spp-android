﻿using UnityEngine;
using System.Collections;

public class SelectionTable : MonoBehaviour {
	
	[SerializeField]
	GameObject table;
	[SerializeField]
	GameObject tableWithOutPocket;
	[SerializeField]
	CueController cc;

	void Start () {
		/*
		 * 0 - table with pockets.
		 * 1 - table without pockets.
		 */
		table.SetActive(GameData.Instance.playTable != 1);
		tableWithOutPocket.SetActive(GameData.Instance.playTable == 1);
		cc.UpdateCueMaterial();
		gameObject.SetActive(false);
	}
	
}
