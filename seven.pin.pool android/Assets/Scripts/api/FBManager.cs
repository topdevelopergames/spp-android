﻿#if !UNITY_WEBGL
using Facebook.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//541750322982564 && 0390625301934006804678
public class FBManager : MonoBehaviour
{
    [SerializeField]
    private string m_startLevelName;

    private static FBManager instance = null;

    public Action NotLoginAuthCallback { get; set; }

    public static FBManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject); // and make this object persistant as we load new scenes            
        }
        else
        {
            Destroy(gameObject);
        }

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void FacebookLogin()
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        if (FB.IsLoggedIn)
        {
            gameSparksAuthenticationFacebook();
        }
        else
        {
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }
    }

    private void gameSparksAuthenticationFacebook()
    {
        // AccessToken class will have session details
        var aToken = AccessToken.CurrentAccessToken;
        // Print current access token's User ID
        Debug.Log(aToken.UserId);
        // Print current access token's granted permissions
        foreach (string perm in aToken.Permissions)
        {
            Debug.Log(perm);
        }
        GameSparksManager.Instance.AuthenticationFacebook(aToken.TokenString, () =>
        {
            SceneManager.LoadScene(m_startLevelName);
        });
    }

    private void AuthCallback(ILoginResult result)
    {
        Debug.Log(result);
        if (FB.IsLoggedIn)
        {
            gameSparksAuthenticationFacebook();
        }
        else
        {
            Debug.Log("User cancelled login");
            if (NotLoginAuthCallback != null)
            {
                NotLoginAuthCallback();
            }
        }
    }

    public void FacebookEndSession()
    {
        FB.LogOut();
    }
}
#endif