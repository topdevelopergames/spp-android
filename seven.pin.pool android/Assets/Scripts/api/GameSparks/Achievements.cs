﻿public enum AchievementType
{
    COMBINATION_SHOT,
    DENIAL,
    DOUBLE_DIP,
    HALF_POWER,
    HARRY_POTTER,
    MAX_POWER,
    MULTIPLAYER_NINJA,
    OVERTURN_100,
    OVERTURN_1000,
    OVERTURN_10000,
    OVERTURN_SUPER_COMBO,
    PERFECT_WIN,
    POT_10,
    POT_50,
    POT_100,
    POT_1000,
    POWER_COMBO_SHOT,
    SUPER_LOSER,
    TOP_OF_THE_CLASS,
    UNDERDOG,
    WIN,
    WIN_10,
    WIN_100,
    WIN_1000,
    WIN_10000,
    WIN_STREAK_5,
    WIN_STREAK_10,
    WIN_STREAK_20
}

public class Achievement
{
    public AchievementType shortCode { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public bool earned { get; set; }
}

