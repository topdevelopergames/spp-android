﻿using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CurrencyType : long
{
    CASH = 1,
    COINS = 2
}

public class GameSparksManager : MonoBehaviour
{

    private static GameSparksManager instance = null;
    private static PlayerData authorizedPlayerData;

    #region Private fields
    private DateTime loginTime;
    private List<Achievement> newAchievementList;
    private string currentChallengeId;
    private string repeatedChallengeId;
    private List<string> issuedFriendChallengeList = new List<string>();
    #endregion Private fields

    #region Callbacks
    private Action CallbackMatchNotFound;
    private Action<List<PlayerData>> CallbackMatchUpdatedMulti;
    private Action<List<PlayerData>> CallbackChallengeStarted;
    private Action<List<RuntimeLeaderboardEntry>> CallbackChallengeChanged;
    private Action<ChallengeCompletedData> CallbackChallengeComplete;
    private Action CallbackChallengeDeclined;
    private Action<ChallengeChatMessage> CallbackChallengeChatMessage;
    private Action CallbackChallengeTimeOver;
    #endregion Callbacks

    #region Properties
    public static GameSparksManager Instance
    {
        get
        {
            return instance;
        }
    }

    public PlayerData AuthorizedPlayerData
    {
        get
        {
            return authorizedPlayerData;
        }

        set
        {
            Debug.LogWarning("GameSparksManager: authorizedPlayerData is readonly");
        }
    }

    public long TimeInGame
    {
        get
        {
            return (long)DateTime.Now.Subtract(loginTime).TotalSeconds;
        }
    }

    public List<Achievement> NewAchievementList
    {
        get
        {
            List<Achievement> result = new List<Achievement>(newAchievementList);
            newAchievementList.Clear();
            return result;
        }
    }

    public int CurrentChallengeTimer { get; private set; }

    /// <summary>
    /// Callback of invitation to play with friend  
    /// </summary>
    public Action<ChallengeCreatedData> CallbackFriendChallengeIssued { get; set; }
    public Action<string> CallbackFriendChallengeAutoDeclined { get; set; }
    public Action CallbackRepeatedChallengeAutoDeclined { get; set; }

    #endregion Properties

    #region MonoBehaviour
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //  GSMessageHandler._AllMessages = HandleGameSparksMessageReceived;
            ScriptMessage.Listener += scriptMessageListener;

            newAchievementList = new List<Achievement>();

            //MatchFoundMessage.Listener += matchFoundMessageListener;
            MatchNotFoundMessage.Listener += matchNotFoundMessageHandler;
            MatchUpdatedMessage.Listener += matchUpdatedMessageHandler;
            AchievementEarnedMessage.Listener += achievementEarnedMessageHandler;
            ChallengeStartedMessage.Listener += challengeStartedMessageHandler;
            ChallengeChangedMessage.Listener += challengeChangedMessageHandler;
            ChallengeDeclinedMessage.Listener += challengeDeclinedMessageHandler;
			SessionTerminatedMessage.Listener += sessionTerminatedMessage;

            DontDestroyOnLoad(gameObject); // and make this object persistant as we load new scenes            
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void OnApplicationQuit()
    {
        Debug.Log("GameSparksManagerApplicationQuit");
        RemovePlayerFromCurrentChallenge();
        if (repeatedChallengeId != null)
        {
            DeclineChallenge(repeatedChallengeId);
        }
        if (GameData.Instance.User != null && (GameData.Instance.User.Type == ProfileType.Guest || GameData.Instance.User.Type == ProfileType.GuestOffline))
            GameData.Instance.RemoveUserProfile();
        foreach (string challengeId in issuedFriendChallengeList)
        {
            DeclineChallenge(challengeId);
        }
        EndSession();
    }
    #endregion MonoBehaviour

    #region Message handlers

    private void HandleGameSparksMessageReceived(GSMessage message)
    {
        Debug.Log("[HandleGameSparksMessageReceived] MSG:" + message.JSONString);
    }

    private void scriptMessageListener(ScriptMessage message)
    {
        Debug.Log("GameSparksManager scriptMessageListener message.ExtCode = " + message.ExtCode);
        switch (message.ExtCode)
        {
            case "ChallengeCompletedMessage":
                ChallengeCompletedData data = JsonConvert.DeserializeObject<ChallengeCompletedData>(message.Data.JSON);
                Debug.Log("ChallengeCompletedMessage challengeId: " + data.challengeId);
                currentChallengeId = null;
                CallbackChallengeComplete(data);
                break;
            case "ChallengeNoParticipantsMessage":
                currentChallengeId = null;
                break;
            case "ChallengeCreatedMessage":
                ChallengeCreatedData createdData = JsonConvert.DeserializeObject<ChallengeCreatedData>(message.Data.JSON);

				if (currentChallengeId == null)
				{
					switch (createdData.challengeType)
					{
						case ChallengeType.FRIEND:
							issuedFriendChallengeList.Add(createdData.challengeId);
							if (CallbackFriendChallengeIssued != null)
							{
								CallbackFriendChallengeIssued(createdData);
							}
							else
							{
								Debug.LogWarning("[Warning] CallbackFriendChallengeIssued is undefined");
							}
							//StartCoroutine(declineChallengeCoroutine(createdData.challengeId, 60));
							break;
						case ChallengeType.REPEATED:
							repeatedChallengeId = createdData.challengeId;
							//StartCoroutine(declineChallengeCoroutine(repeatedChallengeId, 30));
							break;
					}
				}
				else
				{
					DeclineChallenge(createdData.challengeId);
				}


                StartCoroutine(setAvatar(createdData.challengerData, (challengerDataWithAvatar) =>
                {
                    createdData.challengerData = challengerDataWithAvatar;
/*                    if (currentChallengeId == null)
                    {
                        switch (createdData.challengeType)
                        {
                            case ChallengeType.FRIEND:
                                issuedFriendChallengeList.Add(createdData.challengeId);
                                if (CallbackFriendChallengeIssued != null)
                                {
                                    CallbackFriendChallengeIssued(createdData);
                                }
                                else
                                {
                                    Debug.LogWarning("[Warning] CallbackFriendChallengeIssued is undefined");
                                }
                                //StartCoroutine(declineChallengeCoroutine(createdData.challengeId, 60));
                                break;
                            case ChallengeType.REPEATED:
                                repeatedChallengeId = createdData.challengeId;
                                //StartCoroutine(declineChallengeCoroutine(repeatedChallengeId, 30));
                                break;
                        }
                    }
                    else
                    {
                        DeclineChallenge(createdData.challengeId);
                    }*/
                }));
                break;
            case "ChallengeChatMessage":
                if (CallbackChallengeChatMessage != null)
                {
                    ChallengeChatMessage challengeChangedMessage = JsonConvert.DeserializeObject<ChallengeChatMessage>(message.Data.JSON);
                    CallbackChallengeChatMessage(challengeChangedMessage);
                }
                break;
            case "ChallengeTimeOver":
                if (currentChallengeId != null)
                {
                    CallbackChallengeTimeOver();
                }
                break;
            case "ChallengeAutoDeclinedMessage":
                AutoDeclinedData autoDeclinedData = JsonConvert.DeserializeObject<AutoDeclinedData>(message.Data.JSON);
                switch (autoDeclinedData.challengeType)
                {
                    case ChallengeType.FRIEND:
                        if (CallbackFriendChallengeAutoDeclined != null)
                            CallbackFriendChallengeAutoDeclined(autoDeclinedData.challengeId);
                        issuedFriendChallengeList.Remove(autoDeclinedData.challengeId);
                        break;
                    case ChallengeType.REPEATED:
                        if (CallbackRepeatedChallengeAutoDeclined != null && repeatedChallengeId != null)
                        {
                            CallbackRepeatedChallengeAutoDeclined();
                            repeatedChallengeId = null;
                        }
                        break;
                }
                break;
            case "CheckOnlineMessage":
                confirmOnline();
                break;
            default:
                break;
        }
    }

    private void matchUpdatedMessageHandler(MatchUpdatedMessage message)
    {
        Debug.Log("MatchUpdatedMessage");
        if ("MULTI_PLAYER".Equals(message.MatchShortCode) && CallbackMatchUpdatedMulti != null)
        {
            GSEnumerable<MatchUpdatedMessage._Participant> participants = message.Participants;
            List<PlayerData> playerDataList = new List<PlayerData>();
            foreach (MatchUpdatedMessage._Participant participant in participants)
            {
                if (!authorizedPlayerData.playerId.Equals(participant.Id))
                {
                    PlayerData playerData = getMatchPlayerData(participant);
                    playerDataList.Add(playerData);
                    StartCoroutine(setAvatar(playerData, (respData) =>
                    {
                        PlayerData foundItem = playerDataList.Find((item) => { return item.playerId.Equals(respData.playerId); });
                        foundItem.avatar = respData.avatar;
                    }));
                }
            }
            CallbackMatchUpdatedMulti(playerDataList);
        }
    }

    private void matchNotFoundMessageHandler(MatchNotFoundMessage message)
    {
        Debug.Log("MatchNotFoundMessage");
        currentChallengeId = null;
        CallbackMatchNotFound();
    }

    private void achievementEarnedMessageHandler(AchievementEarnedMessage message)
    {
        Debug.Log("AchievementEarnedMessage");

        Achievement achievement = new Achievement();
        achievement.shortCode = (AchievementType)Enum.Parse(typeof(AchievementType), message.AchievementShortCode);
        achievement.name = message.AchievementName;
        achievement.earned = true;

		// Only practic mode - show emmidiate!
		if (achievement.shortCode == AchievementType.TOP_OF_THE_CLASS)
			InGameUIManager.Instance.ShowAchievement(achievement);
		else
	        newAchievementList.Add(achievement);
    }

    private void challengeStartedMessageHandler(ChallengeStartedMessage message)
    {
        Debug.Log("ChallengeStartedMessage ChallengeId: " + message.Challenge.ChallengeId);
        List<GSData> gsDataList = message.ScriptData.GetGSDataList("playerDataList");
        if (gsDataList != null && gsDataList.Count > 0)
        {
            List<PlayerData> playerDataList = new List<PlayerData>();
            foreach (GSData gsData in gsDataList)
            {
                PlayerData playerData = JsonConvert.DeserializeObject<PlayerData>(gsData.JSON);
                playerDataList.Add(playerData);
                StartCoroutine(setAvatar(playerData, (respData) =>
                {
                    PlayerData foundItem = playerDataList.Find((item) => { return item.playerId.Equals(respData.playerId); });
                    foundItem.avatar = respData.avatar;
                }));
            }
            currentChallengeId = message.Challenge.ChallengeId;
            issuedFriendChallengeList.Clear();
            CallbackChallengeStarted(playerDataList);
        }
    }

    private void challengeChangedMessageHandler(ChallengeChangedMessage message)
    {
        Debug.Log("ChallengeChangedMessage ChallengeId: " + message.Challenge.ChallengeId);
        List<RuntimeLeaderboardEntry> leaderboard = new List<RuntimeLeaderboardEntry>();
        foreach (GSData entry in message.BaseData.GetGSDataList("leaderboardData"))
        {
            RuntimeLeaderboardEntry leaderboardEntry = JsonConvert.DeserializeObject<RuntimeLeaderboardEntry>(entry.JSON);
            leaderboard.Add(leaderboardEntry);
        }
        CallbackChallengeChanged(leaderboard);
    }

    private void challengeDeclinedMessageHandler(ChallengeDeclinedMessage message)
    {
        Debug.Log("ChallengeDeclinedMessageHandler ChallengeId: " + message.Challenge.ChallengeId);
        if ("DECLINED".Equals(message.Challenge.State) && CallbackChallengeDeclined != null)
        {
            CallbackChallengeDeclined();
        }
    }

	private void sessionTerminatedMessage(SessionTerminatedMessage message)
	{
		Debug.Log("SessionTerminatedMessageHandler ChallengeId: " + message.MessageId);
        ///if user not logouted - show relogin message
        if (GameData.Instance.User != null)
        {
            GameData.Instance.IsSessionTerminatedMessage = true;
            MenuManager.Instance.LoadLevel(MenuManager.Instance.loginSceneName);
        }
	}

    #endregion Message handlers

    #region Private methods
    private PlayerData getMatchPlayerData(MatchFoundMessage._Participant participant)
    {
        PlayerData playerData = new PlayerData();
        playerData.Init(participant.Id,
        participant.DisplayName,
        participant.ExternalIds.GetString("FB"),
        participant.Online.Value,
        JsonConvert.DeserializeObject<PlayerStatistic>(participant.ScriptData.GetGSData("Statistic").JSON));
        return playerData;
    }

    private PlayerData getMatchPlayerData(MatchUpdatedMessage._Participant participant)
    {
        PlayerData playerData = new PlayerData();
        playerData.Init(participant.Id,
        participant.DisplayName,
        participant.ExternalIds.GetString("FB"),
        participant.Online.Value,
        JsonConvert.DeserializeObject<PlayerStatistic>(participant.ScriptData.GetGSData("Statistic").JSON));
        return playerData;
    }

    private IEnumerator setAvatar(PlayerData playerData, Action<PlayerData> callback)
    {
        Sprite avatar;
        if (playerData.facebookId != null && !"".Equals(playerData.facebookId))
        {
            var www = new WWW("http://graph.facebook.com/" + playerData.facebookId + "/picture?type=square&redirect=true&width=300&height=300");
            yield return www;
            Debug.LogWarning("Download avatar image Error: " + www.error);
            Debug.LogWarning("Download avatar image response: " + www.responseHeaders);
            if(www.error != null)
            {
                www = new WWW("http://graph.facebook.com/" + playerData.facebookId + "/picture?type=square&redirect=true&width=300&height=300");
                yield return www;
            }
            if(www.error == null)
            {
                Texture2D tempPic = new Texture2D(300, 300, TextureFormat.ARGB32, false);
                www.LoadImageIntoTexture(tempPic);
                avatar = Sprite.Create(tempPic, new Rect(0, 0, tempPic.width, tempPic.height), new Vector2(0.5f, 0.5f));
            }
            else
            {
                avatar = Resources.Load<Sprite>(PathToResources.Instance.defaultAvatar);                
            }
        }
        else
        {            
            avatar = Resources.Load<Sprite>(PathToResources.Instance.defaultAvatar);            
        }
        playerData.avatar = avatar;
        callback(playerData);
    }

    //private int getTimeLimit(MatchType matchType)
    //{
    //    if (matchType.ToString().Contains("TIME_1"))
    //    {
    //        return 60;
    //    }
    //    if (matchType.ToString().Contains("TIME_3"))
    //    {
    //        return 180;
    //    }
    //    if (matchType.ToString().Contains("TIME_5"))
    //    {
    //        return 300;
    //    }
    //    if (matchType.ToString().Contains("TIME_10"))
    //    {
    //        return 600;
    //    }
    //    return 0;
    //}

    //private IEnumerator completeChallengeCoroutine(int timeLimit)
    //{
    //    if (currentChallengeId != null && timeLimit > 0)
    //    {
    //        CurrentChallengeTimer = timeLimit;
    //        while (CurrentChallengeTimer > 0)
    //        {
    //            yield return new WaitForSeconds(1);
    //            CurrentChallengeTimer--;
    //        }
    //        CompleteChallenge(currentChallengeId);
    //    }
    //}

    //private IEnumerator declineChallengeCoroutine(string challengeId, float timeoutSec)
    //{
    //    yield return new WaitForSeconds(timeoutSec);
    //    if (!challengeId.Equals(currentChallengeId))
    //    {
    //        issuedFriendChallengeList.Remove(challengeId);
    //        DeclineChallenge(challengeId);
    //    }
    //}

    #endregion Private methods

    #region Authorization
    public void Authentication(string login, string password, Action successCallback = null, Action<string> failCallback = null)
    {
        new AuthenticationRequest()
           .SetUserName(login)
           .SetPassword(password)
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   loginTime = DateTime.Now;
                   Debug.Log("Player Authenticated... \n User Name: " + response.DisplayName);
                   GameData.Instance.SaveUserProfile(login, password, response.DisplayName, response.UserId);
                   GetPlayerData(response.UserId, (playerData) =>
                   {
                       authorizedPlayerData = playerData;
                       GameData.Instance.playerData = playerData;
                       GameData.Instance.EncodedAvatar = playerData.facebookId != "" ? playerData.avatar.texture.EncodeToJPG() : null;
                       if (successCallback != null)
                       {
                           successCallback();
                       }
                   });

               }
               else
               {
                   Debug.LogError("Error Authenticating Player... \n " + response.Errors.JSON);
                   if (failCallback != null)
                   {
                       string error = response.Errors.GetString("DETAILS");
                       failCallback(error);
                   }
               }

           });
    }

    public void AuthenticationGuest(Action successCallback = null, Action<string> failCallback = null)
    {
        new DeviceAuthenticationRequest()
           .SetDisplayName("Guest")
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   loginTime = DateTime.Now;
                   Debug.Log("Device Authenticated...");
                   GameData.Instance.InitGuestProfile(response.DisplayName, response.UserId);
                   //GameData.Instance.SaveGuestProfile(response.DisplayName, response.UserId);
                   GetPlayerData(response.UserId, (playerData) =>
                   {
                       authorizedPlayerData = playerData;
                       GameData.Instance.playerData = playerData;
                       if (successCallback != null)
                       {
                           successCallback();
                       }
                   });
               }
               else
               {
                   Debug.Log("Error Authenticating Device...\n " + response.Errors.JSON);
                   if (failCallback != null)
                   {
                       string error = response.Errors.GetString("DETAILS");
                       failCallback(error);
                   }
               }
           });
    }

    public void AuthenticationOfflineGuest(Action callback)
    {
        loginTime = DateTime.Now;
        if (GameData.Instance.User == null)
        {
            GameData.Instance.InitOfflineGuestProfile("GuestOffline");
            authorizedPlayerData = new PlayerData();
            authorizedPlayerData.InitOfflinePlayerData("GuestOffline");
        }
        else
            authorizedPlayerData = GameData.Instance.playerData;
        GS.Disconnect();
        callback();
    }

    public void AuthenticationFacebook(string accessToken, Action successCallback = null, Action<string> failCallback = null)
    {

        //If so, we can use that acces token to log in to Facebook
        new FacebookConnectRequest().SetAccessToken(accessToken)
            .SetSyncDisplayName(true).SetSwitchIfPossible(true)
            .Send((response) =>
        {
            //If our response has errors we can check what went wrong
            if (!response.HasErrors)
            {
                loginTime = DateTime.Now;
                Debug.Log("Gamesparks Facebook Login Successful \n User Name: " + response.DisplayName);
                GameData.Instance.SaveFBProfile(response.DisplayName, response.UserId);
                GetPlayerData(response.UserId, (playerData) =>
                {
                    authorizedPlayerData = playerData;
                    if (!("FB_" + playerData.facebookId).ToLower().Equals(playerData.userName.ToLower()))
                    {
                        ChangePlayerDetails("FB_" + playerData.facebookId, successCallback);
                    }
                    else
                    {
                        if (successCallback != null)
                        {
                            successCallback();
                        }
                    }
                    GameData.Instance.playerData = playerData;
                    GameData.Instance.EncodedAvatar = playerData.avatar.texture.EncodeToJPG();
                });
            }
            else
            {

                Debug.Log("Something failed when connecting with Facebook \n " + response.Errors.JSON.ToString());
                if (failCallback != null)
                {
                    string error = response.Errors.GetString("DETAILS");
                    failCallback(error);
                }
            }
        });
    }

    public void Registration(string login, string password, string displayName, Action successCallback = null, Action<string> failCallback = null)
    {
        new RegistrationRequest()
           .SetDisplayName(displayName)
           .SetUserName(login)
           .SetPassword(password)
           .Send((response) =>
           {

               if (!response.HasErrors)
               {
                   loginTime = DateTime.Now;
                   Debug.Log("Player Registered \n User Name: " + response.DisplayName);
                   GameData.Instance.SaveUserProfile(login, password, response.DisplayName, response.UserId);
                   GetPlayerData(response.UserId, (playerData) =>
                   {
                       authorizedPlayerData = playerData;
                       GameData.Instance.playerData = playerData;
                       //GameData.Instance.EncodedAvatar = playerData.avatar.texture.EncodeToJPG();
                       if (successCallback != null)
                       {
                           successCallback();
                       }
                   });
               }
               else
               {
                   Debug.LogError("Error Registering Player... \n " + response.Errors.JSON.ToString());
                   if (failCallback != null)
                   {
                       string error = "TAKEN".Equals(response.Errors.GetString("USERNAME")) ? "The username is already in use" : "Unknown registration error";
                       failCallback(error);
                   }
               }
           });
    }

    public void EndSession()
    {
        Debug.Log("GameSparks logout START.");
        if (authorizedPlayerData != null)
        {
            long timeInGame = TimeInGame;
            Debug.Log("Time in game = " + timeInGame + " sec.");
            IncreaseNumberField("timeInGame", timeInGame);
        }
        new EndSessionRequest().Send((response) =>
        {
            if (!response.HasErrors)
            {
                authorizedPlayerData = null;
                Debug.Log("GameSparks logout COMPLETE.");
            }
            else
            {
                Debug.LogError("GameSparks logout FAILED.\n " + response.Errors.JSON.ToString());
            }
        });
    }

    public void ListGameFriends(Action<List<PlayerData>> callback)
    {
        new ListGameFriendsRequest()
            .Send((response) =>
            {
                // GSEnumerable<ListGameFriendsResponse._Player> friends = response.Friends;
                List<GSData> gsDataList = response.ScriptData.GetGSDataList("friendDataList");
                List<PlayerData> friendDataList = new List<PlayerData>();
                foreach (GSData gsData in gsDataList)
                {
                    PlayerData friendData = JsonConvert.DeserializeObject<PlayerData>(gsData.JSON);
                    friendDataList.Add(friendData);
                    StartCoroutine(setAvatar(friendData, (respData) =>
                    {
                        PlayerData foundItem = friendDataList.Find((item) => { return item.playerId.Equals(respData.playerId); });
                        foundItem.avatar = respData.avatar;
                    }));
                }
                callback(friendDataList);
            });
    }
    #endregion Authorization

    #region Player data
    public void GetPlayerData(string playerId, Action<PlayerData> callback)
    {
        new LogEventRequest().SetEventKey("GetPlayerData")
            .SetEventAttribute("id", playerId)
            .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log("Received Player Data From GameSparks...");

                fillVirtualGoodsFullList(() =>
                {
                    GSData playerDataGS = response.ScriptData.GetGSData("playerData");
                    if (playerDataGS != null)
                    {
                        PlayerData playerData = JsonConvert.DeserializeObject<PlayerData>(playerDataGS.JSON);
                        StartCoroutine(setAvatar(playerData, callback));
                    }
                });


            }
            else
            {
                Debug.LogError("Error Loading Player Data... \n " + response.Errors.JSON);
            }
        });
    }

    public void GetFreeCoins(Action callback = null)
    {
        new LogEventRequest().SetEventKey("GetFreeCoins")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GetFreeCoins...");
                    UpdateAuthorizedPlayerData(callback);

                }
                else
                {
                    Debug.LogError("Error GetFreeCoins... \n " + response.Errors.JSON);
                }
            });
    }

    public void GetTimeToFreeCoins(Action<int> callback)
    {
        new LogEventRequest().SetEventKey("GetTimeToFreeCoins")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GeTtimeToFreeCoins...");
                    int? timeToFreeCoinsRef = response.ScriptData.GetInt("timeToFreeCoins");
                    int timeToFreeCoins = timeToFreeCoinsRef.HasValue ? timeToFreeCoinsRef.Value : 1800;
                    callback(timeToFreeCoins);
                }
                else
                {
                    Debug.LogError("Error GeTtimeToFreeCoins... \n " + response.Errors.JSON);
                }
            });
    }

    public void UpdateAuthorizedPlayerData(Action callback = null)
    {
        GetPlayerData(authorizedPlayerData.playerId, (playerData) =>
        {
            Debug.Log("Updating player data");
            authorizedPlayerData = playerData;
            GameData.Instance.playerData = playerData;
            if (callback != null)
            {
                callback();
            }
        });
    }

    public void GetOnlinePlayerDataList(Action<List<PlayerData>> callback)
    {
        new LogEventRequest().SetEventKey("GetOnlinePlayerDataList")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Received Online Player Data List From GameSparks...");
                    List<GSData> gsDataList = response.ScriptData.GetGSDataList("playerDataList");
                    if (gsDataList != null && gsDataList.Count > 0)
                    {
                        List<PlayerData> playerDataList = new List<PlayerData>();
                        foreach (GSData gsData in gsDataList)
                        {
                            PlayerData playerData = JsonConvert.DeserializeObject<PlayerData>(gsData.JSON);
                            playerDataList.Add(playerData);
                            StartCoroutine(setAvatar(playerData, (respData) =>
                            {
                                PlayerData foundItem = playerDataList.Find((item) => { return item.playerId.Equals(respData.playerId); });
                                foundItem.avatar = respData.avatar;
                            }));
                        }
                        callback(playerDataList);
                    }
                }
                else
                {
                    Debug.LogError("Error Loading Online Player Data List... \n " + response.Errors.JSON);
                }
            });
    }

    public void IncreaseNumberField(string fieldName, long addedValue, Action callback = null)
    {
        new LogEventRequest().SetEventKey("IncreaseNumberField")
            .SetEventAttribute("fieldName", fieldName)
            .SetEventAttribute("addedValue", addedValue)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Increase " + fieldName + " Complete...");
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("Error Increasing field... \n " + response.Errors.JSON);
                }
            });
    }

    public void SetNumberField(string fieldName, long fieldValue, Action callback = null)
    {
        new LogEventRequest().SetEventKey("SetNumberField")
            .SetEventAttribute("fieldName", fieldName)
            .SetEventAttribute("fieldValue", fieldValue)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Set field " + fieldName + " Complete...");
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("Error Setting field... \n " + response.Errors.JSON);
                }
            });
    }

    public void CashAdd(long cashValue, string reason, Action callback = null)
    {
        new LogEventRequest().SetEventKey("CashAdd")
            .SetEventAttribute("cash", cashValue)
            .SetEventAttribute("reason", reason)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Adding cash Complete...");
                    authorizedPlayerData.cash += cashValue;
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("ErrorAdding cash... \n " + response.Errors.JSON);
                }
            });
    }

    public void CashPay(long cashValue, string reason, Action callback = null)
    {
        new LogEventRequest().SetEventKey("CashPay")
            .SetEventAttribute("cash", cashValue)
            .SetEventAttribute("reason", reason)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Paying cash Complete...");
                    authorizedPlayerData.cash -= cashValue;
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("Error Paying cash... \n " + response.Errors.JSON);
                }
            });
    }

    public void CoinsAdd(long coinsValue, string reason, Action callback = null)
    {
        new LogEventRequest().SetEventKey("CoinsAdd")
            .SetEventAttribute("coins", coinsValue)
            .SetEventAttribute("reason", reason)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Adding coins Complete...");
                    authorizedPlayerData.coins += coinsValue;
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("ErrorAdding coins... \n " + response.Errors.JSON);
                }
            });
    }

    public void CoinsPay(long coinsValue, string reason, Action callback = null)
    {
        new LogEventRequest().SetEventKey("CoinsPay")
            .SetEventAttribute("coins", coinsValue)
            .SetEventAttribute("reason", reason)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Paying coins Complete...");
                    authorizedPlayerData.coins -= coinsValue;
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("Error Paying coins... \n " + response.Errors.JSON);
                }
            });
    }

    public void BuyVirtualGoods(CurrencyType currencyType, long quantity, VirtualGood virtualGood, Action callback = null)
    {
        Debug.Log("Buying Virtual Goods...");
        new BuyVirtualGoodsRequest()
            .SetCurrencyType((long)currencyType)
            .SetQuantity(quantity)
            .SetShortCode(virtualGood.shortCode)
            .Send((response) =>
            {

                if (!response.HasErrors)
                {
                    Debug.Log("Virtual Goods Bought Successfully...");
                    UpdateAuthorizedPlayerData(callback);
                }
                else
                {
                    Debug.Log("Error Buying Virtual Goods...\n " + response.Errors.JSON);
                }
            });
    }

    public void ConsumeVirtualGood(long quantity, VirtualGood virtualGoodsCode, Action callback = null)
    {
        new ConsumeVirtualGoodRequest()
            .SetQuantity(quantity)
            .SetShortCode(virtualGoodsCode.ToString())
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Virtual Goods Consumed Successfully...");
                    UpdateAuthorizedPlayerData(callback);
                }
                else
                {
                    Debug.Log("Error Consuming Virtual Goods...\n " + response.Errors.JSON);
                }
            });
    }

    public void ChangePlayerDetails(string displayName, string oldPassword, string newPassword, Action callback = null)
    {
        new ChangeUserDetailsRequest()
            .SetDisplayName(displayName)
            .SetOldPassword(oldPassword)
            .SetNewPassword(newPassword)
            .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log("Changing Account Detail...");
                UpdateAuthorizedPlayerData(callback);
            }
            else
            {
                Debug.LogError("Error Changing Account Detail... \n " + response.Errors.JSON);
            }
        });
    }

    public void ChangePlayerDisplayName(string displayName, Action callback = null)
    {
        new ChangeUserDetailsRequest()
            .SetDisplayName(displayName)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Changing Account Detail...");
                    UpdateAuthorizedPlayerData(callback);
                }
                else
                {
                    Debug.LogError("Error Changing Account Detail... \n " + response.Errors.JSON);
                }
            });
    }

    public void ChangePlayerDetails(string userName, Action callback = null)
    {
        new ChangeUserDetailsRequest()
            .SetUserName(userName)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Changing Account userName...");
                    UpdateAuthorizedPlayerData(callback);
                }
                else
                {
                    Debug.LogError("Error Changing Account userName... \n " + response.Errors.JSON);
                }
            });
    }

    public void ChangeGuestToPlayer(string userName, string displayName, string newPassword, Action callback, Action<string> errorCallback)
    {
        new ChangeUserDetailsRequest()
            .SetUserName(userName)
            .SetDisplayName(displayName)
            .SetNewPassword(newPassword)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("ChangeGuestToPlayer...");
                    GameData.Instance.SaveUserProfile(userName, newPassword, displayName, GameData.Instance.User.ServerPlayerId);
                    UpdateAuthorizedPlayerData(callback);
                }
                else
                {
                    Debug.LogError("ChangeGuestToPlayer... \n " + response.Errors.JSON);
                    if (errorCallback != null)
                    {
                        string error = "TAKEN".Equals(response.Errors.GetString("USERNAME")) ? "The username is already in use" : "Unknown registration error";
                        errorCallback(error);
                    }
                }
            });
    }

    public void AddAchievement(AchievementType achievement, Action callback = null)
    {
        new LogEventRequest().SetEventKey("AddAchievement")
            .SetEventAttribute("achievement", achievement.ToString())
            .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log("AddAchievement Complete...");
                UpdateAuthorizedPlayerData(callback);
            }
            else
            {
                Debug.LogError("Error AddAchievement... \n " + response.Errors.JSON);
            }
        });
    }

    public void RemoveAchievement(AchievementType achievement, Action callback = null)
    {
        new LogEventRequest().SetEventKey("RemoveAchievement")
            .SetEventAttribute("achievement", achievement.ToString())
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("RemoveAchievement Complete...");
                    UpdateAuthorizedPlayerData(callback);
                }
                else
                {
                    Debug.LogError("Error RemoveAchievement... \n " + response.Errors.JSON);
                }
            });
    }
    /// <summary>
    /// Returns list of all achievements with mark achieved it or not
    /// </summary>
    /// <param name="callback"></param>
    public void GetListAchievements(Action<List<Achievement>> callback)
    {
        new ListAchievementsRequest()
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GetListAchievements Complete...");
                    List<Achievement> achievementList = new List<Achievement>();
                    foreach (var entry in response.Achievements)
                    {
                        Achievement achievement = JsonConvert.DeserializeObject<Achievement>(entry.BaseData.JSON);
                        achievementList.Add(achievement);
                    }
                    callback(achievementList);
                }
                else
                {
                    Debug.LogError("Error GetListAchievements... \n " + response.Errors.JSON);
                }
            });
    }

	public void SetNotStatisticFieldValue(string fieldName, string fieldValue, Action callback = null)
	{
		new LogEventRequest().SetEventKey("SetNotStatisticFieldValue")
			.SetEventAttribute("fieldName", fieldName)
			.SetEventAttribute("fieldValue", fieldValue)
			.Send((response) =>
			{
				if (!response.HasErrors)
				{
					Debug.Log("Set field " + fieldName + " Complete...");
					if (callback != null)
					{
						callback();
					}
				}
				else
				{
					Debug.LogError("Error Setting field... \n " + response.Errors.JSON);
				}
			});
	}

	public void GetNotStatisticFieldValue(string fieldName, Action<string> callback = null)
	{
		new LogEventRequest().SetEventKey("GetNotStatisticFieldValue")
			.SetEventAttribute("fieldName", fieldName)
			.Send((response) =>
			{
				if (!response.HasErrors)
				{
					Debug.Log("Get field " + fieldName + " Complete...");
					string fieldValue = response.ScriptData.GetString("fieldValue");
					if (callback != null)
					{
						callback(fieldValue);
					}
				}
				else
				{
					Debug.LogError("Error Getting field... \n " + response.Errors.JSON);
				}
			});
	}

	public void SetCurrentCue(string nameCue, Action callback = null)
	{
		SetNotStatisticFieldValue("currentCue", nameCue, callback);
	}

	#endregion Player data

    #region Static server data
    public void GetLevelingUpArray(Action<int[]> callback)
    {
        new LogEventRequest().SetEventKey("GetLevelingUpArray").Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log("Geting LevelingUpArray From GameSparks...");
                List<int> respList = response.ScriptData.GetIntList("levelingUpArray");
                callback(respList.ToArray());
            }
            else
            {
                Debug.LogError("Error Geting LevelingUpArray From GameSparks... \n " + response.Errors.JSON);
            }
        });
    }
    #endregion Static server data

    #region Multiplayer
    public void MatchmakingSinglePlayer(long level, MatchType matchType,
        Action<List<PlayerData>> callbackChallengeStarted,
        Action<List<RuntimeLeaderboardEntry>> callbackChallengeChanged,
        Action<ChallengeCompletedData> callbackChallengeComplete,
        Action callbackMatchNotFound,
        Action<ChallengeChatMessage> callbackChallengeChatMessage,
        Action callbackError)
    {
        CallbackMatchNotFound = null;
        CallbackChallengeStarted = null;
        CallbackChallengeChanged = null;
        CallbackChallengeComplete = null;
        CallbackChallengeChatMessage = null;
        CallbackChallengeTimeOver = null;
        currentChallengeId = null;
        repeatedChallengeId = null;
        new MatchmakingRequest()
            .SetMatchGroup(matchType.Value)
            .SetMatchShortCode("SINGLE_PLAYER")
            .SetSkill(level)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("MatchmakingResponse SinglePlayer...");
                    CallbackChallengeStarted = callbackChallengeStarted;
                    CallbackChallengeChanged = callbackChallengeChanged;
                    CallbackChallengeComplete = callbackChallengeComplete;
                    CallbackMatchNotFound = callbackMatchNotFound;
                    CallbackChallengeChatMessage = callbackChallengeChatMessage;
                }
                else
                {
                    Debug.LogError("Error Matchmaking SinglePlayer... \n " + response.Errors.JSON);
                    callbackError();
                }
            });
    }

    public void MatchmakingMultiPlayer(long level, MatchType matchType,
        Action<List<PlayerData>> callbackMatchUpdatedMulti,
        Action<List<PlayerData>> callbackChallengeStarted,
        Action<List<RuntimeLeaderboardEntry>> callbackChallengeChanged,
        Action callbackChallengeTimeOver,
        Action<ChallengeCompletedData> callbackChallengeComplete,
        Action callbackMatchNotFound,
        Action<ChallengeChatMessage> callbackChallengeChatMessage,
        Action callbackError)
    {
        //GetMultiPlayerOpponentComplete = null;
        CallbackMatchUpdatedMulti = null;
        CallbackChallengeStarted = null;
        CallbackChallengeChanged = null;
        CallbackChallengeComplete = null;
        CallbackMatchNotFound = null;
        CallbackChallengeChatMessage = null;
        CallbackChallengeTimeOver = null;
        repeatedChallengeId = null;
        currentChallengeId = null;
        new MatchmakingRequest()
            .SetMatchGroup(matchType.Value)
            .SetMatchShortCode("MULTI_PLAYER")
            .SetSkill(level)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("MatchmakingResponse MultiPlayer...");
                    CallbackMatchUpdatedMulti = callbackMatchUpdatedMulti;
                    CallbackChallengeStarted = callbackChallengeStarted;
                    CallbackChallengeChanged = callbackChallengeChanged;
                    CallbackChallengeComplete = callbackChallengeComplete;
                    CallbackMatchNotFound = callbackMatchNotFound;
                    CallbackChallengeChatMessage = callbackChallengeChatMessage;
                    CallbackChallengeTimeOver = callbackChallengeTimeOver;
                }
                else
                {
                    Debug.LogError("Error Matchmaking MultiPlayer... \n " + response.Errors.JSON);
                    callbackError();
                }
            });
    }

    public void UpdateGameRuntimeLeaderboard(long turnScore)
    {
        new LogChallengeEventRequest().SetEventKey("UpdateGameRuntimeLeaderboard")
            .SetChallengeInstanceId(currentChallengeId)
            .SetEventAttribute("score", turnScore)
            .SetEventAttribute("shot", 1)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("UpdateGameRuntimeLeaderboard...");
                    GSData scriptData = response.ScriptData;
                }
                else
                {
                    Debug.LogError("Error UpdateGameRuntimeLeaderboard... \n " + response.Errors.JSON);
                }
            });
    }

    public void ReadyToCompleteCurrentChallenge(Action callback = null)
    {
        if (currentChallengeId != null && !"".Equals(currentChallengeId))
        {
            new LogEventRequest().SetEventKey("ReadyToCompleteChallenge")
                .SetEventAttribute("challengeId", currentChallengeId)
                .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("ReadyToCompleteChallenge...");
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("Error ReadyToCompleteChallenge... \n " + response.Errors.JSON);
                }
            });
        }
    }

    private void confirmOnline(Action callback = null)
    {
        if (currentChallengeId != null && !"".Equals(currentChallengeId))
        {
            new LogEventRequest().SetEventKey("ConfirmOnline")
                .SetEventAttribute("challengeId", currentChallengeId)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        Debug.Log("ConfirmOnline...");
                        if (callback != null)
                        {
                            callback();
                        }
                    }
                    else
                    {
                        Debug.LogError("Error ConfirmOnline... \n " + response.Errors.JSON);
                    }
                });
        }
    }

    public void CheckChallengeParticipantsOnline(Action callback = null)
    {
        if (currentChallengeId != null && !"".Equals(currentChallengeId))
        {
            new LogEventRequest().SetEventKey("CheckChallengeParticipantsOnline")
                .SetEventAttribute("challengeId", currentChallengeId)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        Debug.Log("CheckChallengeParticipantsOnline...");
                        if (callback != null)
                        {
                            callback();
                        }
                    }
                    else
                    {
                        Debug.LogError("Error CheckChallengeParticipantsOnline... \n " + response.Errors.JSON);
                    }
                });
        }
    }

    //public void CompleteCurrentChallenge(Action callback = null)
    //{
    //    CompleteChallenge(currentChallengeId, callback);
    //}

    //private void CompleteChallenge(string challengeId, Action callback = null)
    //{
    //    if (challengeId != null && !"".Equals(challengeId))
    //    {
    //        new LogEventRequest().SetEventKey("CompleteChallenge")
    //            .SetEventAttribute("challengeId", challengeId)
    //            .Send((response) =>
    //        {
    //            if (!response.HasErrors)
    //            {
    //                Debug.Log("CompleteChallenge...");
    //                if (callback != null)
    //                {
    //                    callback();
    //                }
    //            }
    //            else
    //            {
    //                Debug.LogError("Error CompleteChallenge... \n " + response.Errors.JSON);
    //            }
    //        });
    //    }
    //}

    public void CreateChallenge(ChallengeCreatingParam challengeParam,
        Action<List<PlayerData>> callbackChallengeStarted,
        Action callbackChallengeDeclined,
        Action<List<RuntimeLeaderboardEntry>> callbackChallengeChanged,
        Action callbackChallengeTimeOver,
        Action<ChallengeCompletedData> callbackChallengeComplete,
        Action<ChallengeChatMessage> callbackChallengeChatMessage,
        Action callbackError)
    {
        repeatedChallengeId = null;
        currentChallengeId = null;
        CallbackChallengeStarted = null;
        CallbackChallengeDeclined = null;
        CallbackChallengeChanged = null;
        CallbackChallengeTimeOver = null;
        CallbackChallengeComplete = null;
        CallbackChallengeChatMessage = null;
        GSRequestData gsRequestData = new GSRequestData(JsonConvert.SerializeObject(challengeParam));
        gsRequestData.AddString("shortCode", challengeParam.shortCode.ToString());
        gsRequestData.AddString("tableType", challengeParam.tableType.ToString());
        gsRequestData.AddString("challengeType", challengeParam.challengeType.ToString());
        gsRequestData.AddString("authType", challengeParam.authType.ToString());
        new LogEventRequest().SetEventKey("CreateChallenge")
            .SetEventAttribute("challengeParam", gsRequestData)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("CreateChallenge Complete");
                    CallbackChallengeStarted = callbackChallengeStarted;
                    CallbackChallengeDeclined = callbackChallengeDeclined;
                    CallbackChallengeChanged = callbackChallengeChanged;
                    CallbackChallengeComplete = callbackChallengeComplete;
                    CallbackChallengeChatMessage = callbackChallengeChatMessage;
                    CallbackChallengeTimeOver = callbackChallengeTimeOver;
                }
                else
                {
                    Debug.LogError("Error CreateFriendChallenge... \n " + response.Errors.JSON);
                    callbackError();
                }
            });
    }

    public void AcceptChallenge(string challengeInstanceId,
        Action<List<PlayerData>> callbackChallengeStarted,
        Action<List<RuntimeLeaderboardEntry>> callbackChallengeChanged,
        Action callbackChallengeTimeOver,
        Action<ChallengeCompletedData> callbackChallengeComplete,
        Action<ChallengeChatMessage> callbackChallengeChatMessage,
        Action callbackError)
    {
        CallbackChallengeStarted = null;
        CallbackChallengeChanged = null;
        CallbackChallengeComplete = null;
        CallbackChallengeTimeOver = null;
        CallbackChallengeChatMessage = null;
        new AcceptChallengeRequest()
            .SetChallengeInstanceId(challengeInstanceId)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("AcceptChallenge Complete; challengeInstanceId: " + response.ChallengeInstanceId);
                    issuedFriendChallengeList.Remove(challengeInstanceId);
                    CallbackChallengeStarted = callbackChallengeStarted;
                    CallbackChallengeChanged = callbackChallengeChanged;
                    CallbackChallengeTimeOver = callbackChallengeTimeOver;
                    CallbackChallengeComplete = callbackChallengeComplete;
                    CallbackChallengeChatMessage = callbackChallengeChatMessage;
                    repeatedChallengeId = null;
                }
                else
                {
                    Debug.LogError("Error AcceptChallenge... \n " + response.Errors.JSON);
                    callbackError();
                }
            });
    }

    public void DeclineChallenge(string challengeInstanceId)
    {
        new DeclineChallengeRequest()
            .SetChallengeInstanceId(challengeInstanceId)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("DeclineChallenge Complete; challengeInstanceId: " + response.ChallengeInstanceId);
                    issuedFriendChallengeList.Remove(challengeInstanceId);
                    currentChallengeId = null;
                    repeatedChallengeId = null;
                }
                else
                {
                    Debug.LogError("Error DeclineChallenge... \n " + response.Errors.JSON);
                }
            });
    }

    public void PlayAgain(ChallengeCompletedData prevChallengeData,
        Action<List<PlayerData>> callbackChallengeStarted,
        Action callbackChallengeDeclined,
        Action<List<RuntimeLeaderboardEntry>> callbackChallengeChanged,
        Action callbackChallengeTimeOver,
        Action<ChallengeCompletedData> callbackChallengeComplete,
        Action<ChallengeChatMessage> callbackChallengeChatMessage,
        Action callbackError)
    {
        //if (repeatedChallengeId == null)
        {
            ChallengeCreatingParam creatingParam = new ChallengeCreatingParam();
            creatingParam.challengerId = authorizedPlayerData.playerId;
            creatingParam.shortCode = prevChallengeData.shortCode;
            prevChallengeData.participantsIds.Remove(authorizedPlayerData.playerId);
            if (prevChallengeData.participantsIds.Count == 0)
            {
                Debug.Log("PlayAgain participantsIds.Count = 0; Challenge Declined...");
                callbackChallengeDeclined();
                return;
            }
            creatingParam.usersToChallenge = prevChallengeData.participantsIds;
            creatingParam.challengeType = ChallengeType.REPEATED;
            creatingParam.timeLimit = prevChallengeData.timeLimit;
            creatingParam.tableType = prevChallengeData.tableType;
            creatingParam.authType = prevChallengeData.authType;
            creatingParam.prevChallengeId = prevChallengeData.challengeId;
            creatingParam.entryFee = prevChallengeData.entryFee;
			Debug.Log("PlayAgain: I created challenge for play again");
            CreateChallenge(
                creatingParam,
                callbackChallengeStarted,
                callbackChallengeDeclined,
                callbackChallengeChanged,
                callbackChallengeTimeOver,
                callbackChallengeComplete,
                callbackChallengeChatMessage,
                callbackError);
        }
/*        else
        {
			Debug.Log("PlayAgain: I conneted for challenge for play again. Created who is other!");
			AcceptChallenge(
                repeatedChallengeId,
                callbackChallengeStarted,
                callbackChallengeChanged,
                callbackChallengeTimeOver,
                callbackChallengeComplete,
                callbackChallengeChatMessage,
                callbackError);
        }*/
    }

    public void RemovePlayerFromCurrentChallenge(Action callback = null, Action callbackError = null)
    {
        if (currentChallengeId != null)
        {
            new LogEventRequest().SetEventKey("RemovePlayerFromChallenge")
                .SetEventAttribute("challengeId", currentChallengeId)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        Debug.Log("RemovePlayerFromChallenge...");
                        currentChallengeId = null;
                        repeatedChallengeId = null;
                        if (callback != null)
                        {
                            callback();
                        }
                    }
                    else
                    {
                        Debug.LogError("Error RemovePlayerFromChallenge... \n " + response.Errors.JSON);
                        if (callbackError != null)
                        {
                            callbackError();
                        }
                    }
                });
        }
    }

    public void SendChallengeChatMessage(string destinationId, string text)
    {
        new LogEventRequest().SetEventKey("SendChallengeChatMessage")
            .SetEventAttribute("destinationId", destinationId)
            .SetEventAttribute("text", text)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("SendChallengeChatMessage complete...");
                }
                else
                {
                    Debug.LogError("Error ChatOnCurrentChallenge... \n " + response.Errors.JSON);
                }
            });
    }

    public void GetCurrentChallengeGameTime(Action<int> callback)
    {
        if (currentChallengeId != null && !"".Equals(currentChallengeId))
        {
            new LogEventRequest().SetEventKey("GetChallengeGameTime")
                .SetEventAttribute("challengeId", currentChallengeId)
                .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GetChallengeGameTime...");
                    int? resRef = response.ScriptData.GetInt("gameTime");
                    callback(resRef.HasValue ? resRef.Value : 0);
                }
                else
                {
                    Debug.LogError("Error GetChallengeGameTime... \n " + response.Errors.JSON);
                }
            });
        }
    }

    public void GetChallengeTimeToStart(string challengeId, Action<int> callback)
    {
        if (challengeId != null && !"".Equals(challengeId))
        {
            new LogEventRequest().SetEventKey("GetChallengeTimeToStart")
                .SetEventAttribute("challengeId", challengeId)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        Debug.Log("GetChallengeTimeToStart...");
                        int? resRef = response.ScriptData.GetInt("timeToStart");
                        callback(resRef.HasValue ? resRef.Value : 0);
                    }
                    else
                    {
                        Debug.LogError("Error GetChallengeTimeToStart... \n " + response.Errors.JSON);
                    }
                });
        }
    }
    public void SinglePlayerClientsReady(string challengeId, Action callback = null)
    {
        if (challengeId != null && !"".Equals(challengeId))
        {
            new LogEventRequest().SetEventKey("SinglePlayerClientsReady")
                .SetEventAttribute("challengeId", challengeId)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        Debug.Log("SinglePlayerClientsReady...");
                        if (callback != null)
                        {
                            callback();
                        }
                    }
                    else
                    {
                        Debug.LogError("Error SinglePlayerClientsReady... \n " + response.Errors.JSON);
                    }
                });
        }
    }
    #endregion Multiplayer

    #region Leaderboard
    public void UpdateLeaderboard(Action callback = null)
    {
        UpdateAuthorizedPlayerData(() =>
        {
            new LogEventRequest().SetEventKey("UpdateLeaderboard")
            .SetEventAttribute("score", authorizedPlayerData.statistic.score)
            .SetEventAttribute("level", authorizedPlayerData.statistic.level)
            .SetEventAttribute("games", authorizedPlayerData.statistic.totalGames)
            .SetEventAttribute("wonGames", authorizedPlayerData.statistic.wonGames)
			.SetEventAttribute("streak", authorizedPlayerData.statistic.winStreak)
			.SetEventAttribute("countBalls", authorizedPlayerData.statistic.pottedBalls)
			.SetEventAttribute("countPins", authorizedPlayerData.statistic.countPins)
			.Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("UpdateLeaderboard Complete...");
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    Debug.LogError("Error UpdateLeaderboard... \n " + response.Errors.JSON);
                }
            });
        });
    }

    public void GetLeaderboardData(LeaderboardType type, Action<List<GlobalLeaderboardEntry>> callback)
    {
        new LeaderboardDataRequest()
            .SetEntryCount(1000)
            .SetLeaderboardShortCode(type.ToString())
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GetLeaderboardData Complete...");
                    List<GlobalLeaderboardEntry> leaderboard = new List<GlobalLeaderboardEntry>();
                    foreach (var entry in response.Data)
                    {
                        GlobalLeaderboardEntry leaderboardEntry = JsonConvert.DeserializeObject<GlobalLeaderboardEntry>(entry.BaseData.JSON);
                        leaderboard.Add(leaderboardEntry);
                    }
                    callback(leaderboard);
                }
                else
                {
                    Debug.LogError("Error GetLeaderboardData... \n " + response.Errors.JSON);
                }
            });
    }

    public void GetLeaderboardEntry(LeaderboardType type, Action<GlobalLeaderboardEntry> callback)
    {
        new GetLeaderboardEntriesRequest()
            .SetLeaderboards(new List<string>() { type.ToString() })
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GetLeaderboardEntry Complete...");

                    GSData gsEntry = response.BaseData.GetGSData(type.ToString());
                    GlobalLeaderboardEntry leaderboardEntry = JsonConvert.DeserializeObject<GlobalLeaderboardEntry>(gsEntry.JSON);
                    callback(leaderboardEntry);
                }
                else
                {
                    Debug.LogError("Error GetLeaderboardEntry... \n " + response.Errors.JSON);
                }
            });
    }
    #endregion Leaderboard

    #region In-app purchases

    public void IOSBuyGoods(string receipt, bool sandbox, Action<List<string>> callback = null)
    {
        new IOSBuyGoodsRequest()
            .SetReceipt(receipt)
            .SetSandbox(sandbox)
            //.SetUniqueTransactionByPlayer(false)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    GSEnumerable<BuyVirtualGoodResponse._Boughtitem> boughtItems = response.BoughtItems;
                    long? currency1Added = response.Currency1Added;
                    long? currency2Added = response.Currency2Added;
                    long? currencyConsumed = response.CurrencyConsumed;
                    int? currencyType = response.CurrencyType;
                    List<string> transactionIds = response.TransactionIds;
                    if (callback != null)
                    {
                        callback(transactionIds);
                    }
                }
                else
                {
                    Debug.LogError("Error IOSBuyGoods... \n " + response.Errors.JSON);
                }
            });
    }

    public void GooglePlayBuyGoods(string signature, string signedData, Action<List<string>> callback = null)
    {
        new GooglePlayBuyGoodsRequest()
            .SetSignature(signature)
            .SetSignedData(signedData)
            //.SetUniqueTransactionByPlayer(false)
            .Send((response) =>
            {

                if (!response.HasErrors)
                {
                    GSEnumerable<BuyVirtualGoodResponse._Boughtitem> boughtItems = response.BoughtItems;
                    long? currency1Added = response.Currency1Added;
                    long? currency2Added = response.Currency2Added;
                    long? currencyConsumed = response.CurrencyConsumed;
                    int? currencyType = response.CurrencyType;
                    List<string> transactionIds = response.TransactionIds;
                    if (callback != null)
                    {
                        callback(transactionIds);
                    }
                }
                else
                {
                    Debug.LogError("Error GooglePlayBuyGoods... \n " + response.Errors.JSON);
                }
            });

    }

    private void fillVirtualGoodsFullList(Action callback = null)
    {
        if (VirtualGood.FullList == null)
        {
            getVirtualGoodsList((respList) =>
               {
                   VirtualGood.FullList = respList;
                   if (callback != null)
                   {
                       callback();
                   }
               });
        }
        else
        {
            if (callback != null)
            {
                callback();
            }
        }
    }

    private void getVirtualGoodsList(Action<List<VirtualGood>> callback, List<string> tags = null)
    {

        ListVirtualGoodsRequest listVirtualGoodsRequest = new ListVirtualGoodsRequest();
        if (tags != null && tags.Count > 0)
        {
            listVirtualGoodsRequest.SetTags(tags);
        }

        listVirtualGoodsRequest.Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log("ListVirtualGoods Complete...");


                List<VirtualGood> virtualGoodsList = new List<VirtualGood>();

                foreach (ListVirtualGoodsResponse._VirtualGood respVG in response.VirtualGoods)
                {
                    if ("VGOOD".Equals(respVG.Type))
                    {
                        VirtualGood virtualGood = new VirtualGood(respVG.ShortCode);
                        virtualGood.name = respVG.Name;
                        virtualGood.description = respVG.Description;
                        virtualGood.currency1Cost = respVG.Currency1Cost.HasValue ? respVG.Currency1Cost.Value : 0;
                        virtualGood.currency2Cost = respVG.Currency2Cost.HasValue ? respVG.Currency2Cost.Value : 0;
                        virtualGood.count = 0;
                        virtualGoodsList.Add(virtualGood);
                    }
                }

                callback(virtualGoodsList);

            }
            else
            {
                Debug.LogError("Error ListVirtualGoods... \n " + response.Errors.JSON);
            }
        });
    }

    #endregion In-app purchases
}