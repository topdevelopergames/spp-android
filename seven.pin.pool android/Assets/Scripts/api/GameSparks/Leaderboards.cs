﻿public enum LeaderboardType
{
    GLOBAL_RANK_SCORE = 0,
    GLOBAL_RANK_LEVEL,
    GLOBAL_RANK_GAMES
}

public class LeaderboardEntry
{
    public string userId { get; set; }
    public string userName { get; set; }
    public long rank { get; set; }
    public string city { get; set; }
    public string country { get; set; }
}


public class GlobalLeaderboardEntry : LeaderboardEntry
{
    public long score { get; set; }
    public long level { get; set; }
    public long games { get; set; }
}


public class RuntimeLeaderboardEntry : LeaderboardEntry
{
    public bool excluded { get; set; }
    public bool noshot { get; set; }
    public long score { get; set; }
    public long shot { get; set; }
}
