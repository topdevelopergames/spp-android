﻿using System.Collections.Generic;

public enum AuthType
{
    GUEST,
    SIGNED
}

public enum GameMode
{
    FIRST21,
    TIME_1,
    TIME_3,
    TIME_5,
    TIME_10,
    FIRST51,
    FIRST101
}

public enum TableType
{
    POCKET = 0,
    NOPOCKET
}

public class MatchType
{
    public AuthType authType { get; set; }
    public GameMode gameMode { get; set; }
    public TableType tableType { get; set; }
    public long entryFee { get; set; }
    public MatchType(AuthType _authType, GameMode _gameMode, TableType _tableType, long _entryFee = 0)
    {
        authType = _authType;
        gameMode = _gameMode;
        tableType = _tableType;
        entryFee = _entryFee;
    }
    public string Value { get { return authType + "_" + gameMode + "_" + tableType + "_" + entryFee; } }
    public override string ToString()
    {
        return Value;
    }
}

public enum ChallengeType
{
    NORMAL,
    FRIEND,
    REPEATED
}

public enum ChallengeCode
{
    MULTI_21,
    MULTI_51,
    MULTI_101,
    MULTI_TIME_LIMIT,
    SINGLE_21,
    SINGLE_51,
    SINGLE_101,
    SINGLE_TIME_LIMIT
}

public class ChallengeCreatingParam
{
    public string challengerId { get; set; }
    public ChallengeCode shortCode { get; set; }
    public List<string> usersToChallenge { get; set; }
    public int timeLimit { get; set; }
    public TableType tableType { get; set; }
    public long entryFee { get; set; }
    public ChallengeType challengeType { get; set; }
    public AuthType authType { get; set; }
    public string prevChallengeId { get; set; }
    public int pointsLimit { get; set; }
}

public class ChallengeCreatedData : ChallengeCreatingParam
{
    public string challengeId { get; set; }
    public PlayerData challengerData { get; set; }
}

public class ChallengeCompletedData : ChallengeCreatedData
{
    /// <summary>
    /// Contains full players id list from game
    /// </summary>
    public List<string> participantsIds { get; set; }
    /// <summary>
    /// Contains full players list from game
    /// </summary>
    public List<RuntimeLeaderboardEntry> leaderboard { get; set; }
    /// <summary>
    /// contains prize for each winner and winners count
    /// </summary>
    public List<float> prizeList { get; set; }
    /// <summary>
    /// The duration of the game in seconds
    /// </summary>
    public int gameTime { get; set; }
}

public class AutoDeclinedData
{
    public string challengeId { get; set; }
    public ChallengeType challengeType { get; set; }
}