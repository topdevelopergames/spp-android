﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum GameRank
{
    Beginner = 0,
    Amateur,
    Intermediare,
    SemiProfessional,
    Professional,
    Master
}

public class PlayerStatistic
{
    public int level { get; set; }
    public GameRank rank { get; set; }
    public int points { get; set; }
    public long totalGames { get; set; }
    public long wonGames { get; set; }
    public long winStreak { get; set; }
    public long winningsCoins { get; set; }
    public long timeInGame { get; set; }
    public long pottedBalls { get; set; }
    public long score { get; set; }
	public long countPins { get; set; }

    public PlayerStatistic()
    {
        level = 1;
    }
}


public class PlayerData : IComparable<PlayerData>
{
    public string playerId { get; set; }
    public string userName { get; set; }
    public string displayName { get; set; }
    public PlayerStatistic statistic { get; set; }
    /// <summary>
    /// List of all achievements with marked achieved it or not
    /// </summary>
    public List<Achievement> achievements { get; set; }
    public List<string> earnedAchievements { get; set; }
    public Dictionary<string, long> virtualGoods { get; set; }
    public long coins { get; set; }
    public long cash { get; set; }
    public bool online { get; set; }
    public string facebookId { get; set; }
    [Newtonsoft.Json.JsonIgnore]
    public Sprite avatar { get; set; }
    public string currentCue { get; set; }
    public int timeToFreeCoins { get; set; }

    //VirtualGoods
    [Newtonsoft.Json.JsonIgnore]
    public List<VirtualGood> CuesList
    {
        get { return VirtualGood.FullList.FindAll((item) => { return virtualGoods.ContainsKey(item.shortCode) && (item.type == VirtualGoodType.CUES_COUNTRY || item.type == VirtualGoodType.CUES_PREMIUM || item.type == VirtualGoodType.CUES_STANDARD); }); }
    }
    [Newtonsoft.Json.JsonIgnore]
    public List<VirtualGood> TableFrameList { get { return VirtualGood.FullList.FindAll((item) => { return virtualGoods.ContainsKey(item.shortCode) && item.type == VirtualGoodType.TABLE_FRAME; }); } }
    [Newtonsoft.Json.JsonIgnore]
    public List<VirtualGood> TableClothList { get { return VirtualGood.FullList.FindAll((item) => { return virtualGoods.ContainsKey(item.shortCode) && item.type == VirtualGoodType.TABLE_CLOTH; }); } }
    [Newtonsoft.Json.JsonIgnore]
    public List<VirtualGood> TablePatternList { get { return VirtualGood.FullList.FindAll((item) => { return virtualGoods.ContainsKey(item.shortCode) && item.type == VirtualGoodType.TABLE_PATTERN; }); } }
    [Newtonsoft.Json.JsonIgnore]
    public List<VirtualGood> ChatList { get { return VirtualGood.FullList.FindAll((item) => { return virtualGoods.ContainsKey(item.shortCode) && item.type == VirtualGoodType.CHAT; }); } }
    [Newtonsoft.Json.JsonIgnore]
    public long ScratchCardCount
    {
        get
        {
            long scratchCardCount = 0;
            virtualGoods.TryGetValue("scratch_card_001", out scratchCardCount);
            return scratchCardCount;
        }
    }
    [Newtonsoft.Json.JsonIgnore]
    public long SpinTicketCount
    {
        get
        {
            long spinTicketCount = 0;
            virtualGoods.TryGetValue("spin_ticket_001", out spinTicketCount);
            return spinTicketCount;
        }
    }

    public void InitOfflinePlayerData(string _displayName)
    {
        playerId = "";
        displayName = _displayName;
        statistic = new PlayerStatistic();
        avatar = Resources.Load<Sprite>(PathToResources.Instance.defaultAvatar);
    }

    public void Init(string _playerId, string _displayName, string _facebookId, bool _online, PlayerStatistic _statistic)
    {
        playerId = _playerId;
        displayName = _displayName;
        facebookId = _facebookId;
        online = _online;
        statistic = _statistic;
        avatar = Resources.Load<Sprite>(PathToResources.Instance.defaultAvatar);
    }

    public int CompareTo(PlayerData obj)
    {
        return string.Compare(displayName, obj.displayName);
    }
}

public class MultiplayerData
{
    public PlayerData player { get; set; }
    public long score { get; set; }

    public static int CompareByScore(MultiplayerData x, MultiplayerData y)
    {
        Debug.LogWarning("Sorting multiplayer data");

        if (x == null)
        {
            if (y == null)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            if (y == null)
            {
                return -1;
            }
            else
            {
                if (x.score > y.score)
                {
                    return -1;
                }
                else
                {
                    if (x.score < y.score)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }

        }
    }

}