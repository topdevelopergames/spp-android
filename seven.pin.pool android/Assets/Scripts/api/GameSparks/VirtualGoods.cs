﻿using System;
using System.Collections.Generic;

public enum VirtualGoodType
{
    CUES_STANDARD,
    CUES_PREMIUM,
    CUES_COUNTRY,
    TABLE_FRAME,
    TABLE_CLOTH,
    TABLE_PATTERN,
    CHAT,
    SCRATCH_CARD,
    SPIN_TICKET
}

public class VirtualGood
{
    public static List<VirtualGood> FullList { get; set; }

    /// <summary>
    /// shop item type
    /// </summary>
    public VirtualGoodType type { get; private set; }
    /// <summary>
    /// shop item id
    /// </summary>
    public int num { get; private set; }

    public VirtualGood(VirtualGoodType _type, int _num)
    {
        type = _type;
        num = _num;
    }

    public VirtualGood(string _shortCode)
    {
        shortCode = _shortCode;
    }

    public string shortCode
    {
        get
        {
            return type.ToString().ToLower() + "_" + num.ToString("D3");
        }
        set
        {
            type = (VirtualGoodType)Enum.Parse(typeof(VirtualGoodType), value.Substring(0, value.Length - 4).ToUpper()); ;
            num = int.Parse(value.Substring(value.Length - 3));
        }
    }

    public string name { get; set; }
    public string description { get; set; }

    /// <summary>
    /// Cost in cash
    /// </summary>
    public long currency1Cost { get; set; }

    /// <summary>
    /// Cost in coins
    /// </summary>
    public long currency2Cost { get; set; }
    public long count { get; set; }
}