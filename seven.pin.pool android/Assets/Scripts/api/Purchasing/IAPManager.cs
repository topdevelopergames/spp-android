﻿using Newtonsoft.Json;
using System;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    // Use this for initialization
    void Start()
    {
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    /*
    // Update is called once per frame
    void Update()
    {

    }*/

    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }
        PoolShopManager.Instance.ShowCashInitMessage();
        PoolShopManager.Instance.ShowCoinsInitMessage();
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        foreach (Products product in Enum.GetValues(typeof(Products)))
        {
            Debug.Log("<b>INIT Product id: </b>" + ProductsManager.getExternalId(product));
            builder.AddProduct(ProductsManager.getExternalId(product), ProductType.Consumable);
        }
        UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
        PoolShopManager.Instance.HideCashInitMessage();
        PoolShopManager.Instance.HideCoinsInitMessage();
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.LogWarning("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        // A consumable product has been purchased by this user.
        if (ProductsManager.checkId(args.purchasedProduct.definition.id))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            if (args.purchasedProduct.hasReceipt)
            {
                Debug.Log("args.purchasedProduct.receipt = " + args.purchasedProduct.receipt);
                Receipt receipt = JsonConvert.DeserializeObject<Receipt>(args.purchasedProduct.receipt);
#if UNITY_IOS
                GameSparksManager.Instance.IOSBuyGoods(receipt.Payload, true, PoolShopManager.Instance.UpdatePlayerData);
#endif
#if UNITY_ANDROID
                GooglePlayReceipt gpReceipt = JsonConvert.DeserializeObject<GooglePlayReceipt>(receipt.Payload);
                GameSparksManager.Instance.GooglePlayBuyGoods(gpReceipt.signature, gpReceipt.json, PoolShopManager.Instance.UpdatePlayerData);
#endif
                if (AppFlyerManager.Instance != null)
                    AppFlyerManager.Instance.TrackPurchaseUSD(args.purchasedProduct.metadata.localizedPrice.ToString(), args.purchasedProduct.metadata.isoCurrencyCode);

                Debug.Log("Updating user data IAPManager");
                GameSparksManager.Instance.UpdateAuthorizedPlayerData(PoolShopManager.Instance.UpdateInfo);

            }

        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }
        GameSparksManager.Instance.UpdateAuthorizedPlayerData(PoolShopManager.Instance.UpdateInfo);
        return PurchaseProcessingResult.Complete;
    }

    public void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
                if (AppFlyerManager.Instance != null)
                    AppFlyerManager.Instance.TrackClickEvent(productId);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
}



