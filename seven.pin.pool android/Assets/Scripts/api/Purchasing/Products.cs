﻿using System;

enum Products
{
    cash_10,
    cash_25,
    cash_55,
    cash_125,
    cash_400,
    cash_1000,

    coins_12500,
    coins_32500,
    coins_70000,
    coins_160k,
    coins_500k,
    coins_1250000,
}

class ProductsManager
{
    public static string getExternalId(Products product)
    {
        if (product == Products.coins_1250000)
        {
            return "coins.1.25m";
        }
        else
        {
            string[] splitArr = product.ToString().Split('_');
            return splitArr[0] + "." + splitArr[1];
        }
    }

    public static bool checkId(string productId)
    {
        if ("coins.1.25m".Equals(productId))
        {
            return true;
        }
        else
        {
            string[] splitArr = productId.Split('.');
            string productString = splitArr[0] + "_" + splitArr[1];
            Products productValue = (Products)Enum.Parse(typeof(Products), productString);
            return (Enum.IsDefined(typeof(Products), productValue));
        }
    }
}