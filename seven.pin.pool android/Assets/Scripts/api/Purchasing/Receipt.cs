﻿
class Receipt
{
    public string Store { get; set; }
    public string TransactionID { get; set; }
    public string Payload { get; set; }
}

class GooglePlayReceipt
{
    public string json { get; set; }
    public string signature { get; set; }
}
