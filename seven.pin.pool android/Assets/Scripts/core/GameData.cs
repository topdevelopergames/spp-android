﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using GameSparks.Core;

public class GameData
{
	private static string s_version = "v_0.0.1";
	private static GameData s_instance;

	public UserProfile User { get; set; }

	public SkillzMatchRules matchRules;
	public int[] pointsForLevel;
	/// <summary>
	/// 0:
	/// singleorMulti == 0 - practic mode,
	/// singleOrMulti == 1 - 21 mode;
	/// 1:
	/// singleOrMulti == 0 - time mode,
	/// singleOrMulti == 1 - 21 mode.
	/// </summary>
	public int selectedGameMode;
	public int lastOpenedTab;
	/// <summary>
	/// 0 - table with pockets;
	/// 1 - table without pockets
	/// </summary>
	public int playTable;
	/// <summary>
	/// 0 - single player;
	/// 1 - multiplayer
	/// </summary>
	public int singleOrMulti;
	/// <summary>
	/// index for entryfees array
	/// </summary>
	public int entryFee;
	/// <summary>
	/// true - sound enabled,
	/// false - sound disabled
	/// </summary>
	public bool soundEnabled;
	public int timeForGame;
	public int pointsForGame;
	[JsonIgnore]
	public bool isInGame;
	public CueEntity selectedCue;
	public ShopitemEntity selectedFrame, selectedCloth, selectedPattern;
	public List<Achievement> achievements;
	[JsonIgnore]
	public List<CueEntity> cues;
	[JsonIgnore]
	public List<ShopitemEntity> frames, clothes, patterns, chatPacks;
	[JsonIgnore]
	public string gameName;
	[JsonIgnore]
	public string opponentId;
	[JsonIgnore]
	public string hostPlayer;
	[JsonIgnore]
	public bool playWithFriends;
	public bool is3D;
	[JsonIgnore]
	public ChallengeCreatingParam challenge;
	[JsonIgnore]
	public ChallengeCompletedData resultsPrevChallenge;
	[JsonIgnore]
	public bool playAgain;
	[JsonIgnore]
	public string acceptedChallengeId;
	[JsonIgnore]
	public bool needToUpdateInfo;
	[JsonIgnore]
	public bool challengeTimeOver;
	[JsonIgnore]
	public bool freecoinsTimerStarted;
	[JsonIgnore]
	public bool offlinePopupShown;
	[JsonIgnore]
	public bool needToShowOfflinePopup;
	[JsonIgnore]
	public bool gameStarted;
	/// <summary>
	/// 0 - vertical left,
	/// 1 - vertical right,
	/// 2 - horizontal right,
	/// 3 - horizontal left;
	/// </summary>
	public int forceSliderPosition;
	/// <summary>
	/// 0 - standard cues, 
	/// 1 - premium cues, 
	/// 2 - country cues, 
	/// 3 - owned cues.
	/// </summary>
	public int lastSelectedCueTab;
	/// <summary>
	/// 0 - frames,
	/// 1 - cloth,
	/// 2 - patterns
	/// </summary>
	public int lastSelectedTableTab;

	public bool isLogoutbyUser;

	public bool defaultAvatar;

	public bool firstLaunch;
	public List<CueEntity> ownedCues;
	public List<ShopitemEntity> ownedFrames, ownedClothes, ownedPatterns, ownedPacks;
	private byte[] encodedAvatar;

	public PlayerData playerData { get; set; }

	public List<string> ListSortedMessageForChat;
	public List<string> ListHiddenMessageForChat;

	[JsonIgnore]
	public bool IsChangingPointShot;
	[JsonIgnore]
	public bool IsSessionTerminatedMessage;

	public byte[] EncodedAvatar {
		get {
			return encodedAvatar;
		}
		set {
			encodedAvatar = value;
		}
	}


	public static long[] entryFees = new long[] {
		50,
		100,
		2500,
		10000,
		50000,
		100000,
		250000,
		500000,
		2500000,
		5000000,
		10000000,
		15000000
	};
	public static string[] prizesText = new string[] {
		"100",
		"200",
		"5K",
		"20K",
		"100K",
		"200K",
		"500K",
		"1M",
		"5M",
		"10M",
		"20M",
		"30M"
	};
	public static string[] feeText = new string[] {
		"50",
		"100",
		"2.5K",
		"10K",
		"50K",
		"100K",
		"250K",
		"0.5M",
		"2.5M",
		"5M",
		"10M",
		"15M"
	};
	public static string[] ranks = new string[] {"beginner", "beginner", "beginner", "beginner", "beginner", "student", "student", "student", "student",
		"amateur", "amateur", "amateur", "amateur", "amateur", "skilled", "skilled", "skilled", "skilled", "skilled", "skilled",
		"Semi-pro", "Semi-pro", "Semi-pro", "Semi-pro", "Semi-pro", "Semi-pro", "Semi-pro",
		"professional", "professional", "professional", "professional", "professional", "professional", "professional", "professional",
		"virtuoso", "virtuoso", "virtuoso", "virtuoso", "virtuoso", "virtuoso", "virtuoso", "virtuoso", "virtuoso",
		"expert", "expert", "expert", "expert", "expert", "expert", "expert", "expert", "expert", "expert",
		"veteran", "veteran", "veteran", "veteran", "veteran", "veteran", "veteran", "veteran", "veteran", "veteran", "veteran",
		"master", "master", "master", "master", "master", "master", "master", "master", "master", "master", "master", "master",
		"grand master", "grand master", "grand master", "grand master", "grand master", "grand master", "grand master",
		"grand master", "grand master", "grand master", "grand master", "grand master", "grand master",
		"VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP", "VIP",
		"superstar", "superstar", "superstar", "superstar", "superstar", "superstar", "superstar", "superstar",
		"superstar", "superstar", "superstar", "superstar", "superstar", "superstar", "superstar",
		"king", "king", "king", "king", "king", "king", "king", "king", "king", "king", "king", "king", "king", "king", "king", "king", "king",
		"pool god", "pool god", "pool god", "pool god", "pool god", "pool god", "pool god", "pool god", "pool god",
		"pool god", "pool god", "pool god", "pool god", "pool god",
	};

	[JsonIgnore]
	public int TimeForGame {
		get {
			switch (timeForGame) {
			case 0:
				return 60;                   

			case 1:
				return 180;                       

			case 2:
				return 300;
			}
			return 60;
		}
	}

	public GameData ()
	{
		cues = CuesStorage.Instance.Cues;
		frames = TableFrameStorage.Instance.Frames;
		clothes = TableClothStorage.Instance.Clothes;
		patterns = TablePatternStorage.Instance.Patterns;
		chatPacks = ChatPackStorage.Instance.ChatPacks;
		soundEnabled = true;
		firstLaunch = false;
		isLogoutbyUser = true;
		ResetUserSpecificData ();
	}


	public void InitOwnedLists ()
	{
		ownedCues = new List<CueEntity> ();
		ownedCues.Add (CueEntity.DeepCopy (cues [0]));

		ownedFrames = new List<ShopitemEntity> ();
		ownedFrames.Add (ShopitemEntity.DeepCopy (frames [0]));

		ownedPatterns = new List<ShopitemEntity> ();
		ownedPatterns.Add (ShopitemEntity.DeepCopy (patterns [0]));

		ownedClothes = new List<ShopitemEntity> ();
		ownedClothes.Add (ShopitemEntity.DeepCopy (clothes [0]));

		ownedPacks = new List<ShopitemEntity> ();        
	}

	public static GameData Instance {
		get {
			return getInstance ();
		}
	}

	public GameTypeMode gameMode = GameTypeMode.SinglePlayer;

	private static GameData getInstance ()
	{
		bool save = false;

		// s_isLoaded = true;

		if (s_instance == null) {
			string savedGameData = PlayerPrefs.GetString (s_version, null);
			GameData gdata = null;
			if (savedGameData != null && savedGameData.Length > 0) {
				// s_isLoaded = true;
				gdata = JsonConvert.DeserializeObject<GameData> (savedGameData);
				if (gdata == null) {
					PlayerPrefs.DeleteKey (s_version);
					PlayerPrefs.Save ();
				} else {
					if (gdata.User != null && gdata.playerData != null) {
						if (gdata.User.Type == ProfileType.FB && !gdata.defaultAvatar) {
							Texture2D t2d = new Texture2D (300, 300, TextureFormat.ARGB32, true);
							t2d.LoadImage (gdata.EncodedAvatar);
							gdata.playerData.avatar = Sprite.Create (t2d, new Rect (0, 0, t2d.width, t2d.height), new Vector2 (0.5f, 0.5f));
						} else
							gdata.playerData.avatar = Resources.Load<Sprite> (PathToResources.Instance.defaultAvatar);
					}

					gdata.selectedCloth.itemIcoWithoutPockets = TableClothStorage.Instance.Clothes [gdata.selectedCloth.id].itemIcoWithoutPockets;
					gdata.selectedCloth.itemIcoWithPockets = TableClothStorage.Instance.Clothes [gdata.selectedCloth.id].itemIcoWithPockets;
					gdata.selectedCloth.withoutPockets = TableClothStorage.Instance.Clothes [gdata.selectedCloth.id].withoutPockets;
					gdata.selectedCloth.withPockets = TableClothStorage.Instance.Clothes [gdata.selectedCloth.id].withPockets;

					gdata.selectedFrame.itemIcoWithoutPockets = TableFrameStorage.Instance.Frames [gdata.selectedFrame.id].itemIcoWithoutPockets;
					gdata.selectedFrame.itemIcoWithPockets = TableFrameStorage.Instance.Frames [gdata.selectedFrame.id].itemIcoWithPockets;
					gdata.selectedFrame.withoutPockets = TableFrameStorage.Instance.Frames [gdata.selectedFrame.id].withoutPockets;
					gdata.selectedFrame.withPockets = TableFrameStorage.Instance.Frames [gdata.selectedFrame.id].withPockets;

					s_instance = gdata;
				}
			}
			if (gdata == null) {
				//s_isLoaded = false;
				gdata = new GameData ();
				gdata.InitOwnedLists ();
				s_instance = gdata;
				save = true;
			}
		}

		if (save) {
			s_instance.Save ();
		}
		return s_instance;
	}

	public void Save ()
	{       
		string jsonToSave = JsonConvert.SerializeObject (this);
		PlayerPrefs.SetString (s_version, jsonToSave);
		PlayerPrefs.Save ();
	}


	public void ResetPrefs ()
	{
		PlayerPrefs.DeleteKey (s_version);
		PlayerPrefs.Save ();
	}

	public void SaveUserProfile (string login, string password, string displayName, string serverPlayerId)
	{
		if (s_instance.User == null) {
			s_instance.User = new UserProfile ();
		}
		s_instance.User.Login = login;
		s_instance.User.Password = Encrypt (password);
		s_instance.User.DisplayName = displayName;
		s_instance.User.ServerPlayerId = serverPlayerId;
		s_instance.User.Type = ProfileType.Normal;
		Save ();
	}

	public void InitGuestProfile (string displayName, string serverPlayerId)
	{
		if (s_instance.User == null) {
			s_instance.User = new UserProfile ();
		}
		s_instance.User.Login = "";
		s_instance.User.Password = "";
		s_instance.User.DisplayName = displayName;
		s_instance.User.ServerPlayerId = serverPlayerId;
		s_instance.User.Type = ProfileType.Guest;
		//Save();
	}

	public void InitOfflineGuestProfile (string displayName)
	{
		if (s_instance.User == null) {
			s_instance.User = new UserProfile ();
		}
		s_instance.User.Login = "";
		s_instance.User.Password = "";
		s_instance.User.DisplayName = displayName;
		s_instance.User.ServerPlayerId = "";
		s_instance.User.Type = ProfileType.GuestOffline;      
	}

	public void SaveFBProfile (string displayName, string serverPlayerId)
	{
		if (s_instance.User == null) {
			s_instance.User = new UserProfile ();
		}
		s_instance.User.Login = "";
		s_instance.User.Password = "";
		s_instance.User.DisplayName = displayName;
		s_instance.User.ServerPlayerId = serverPlayerId;
		s_instance.User.Type = ProfileType.FB;
		Save ();
	}

	public void RemoveUserProfile ()
	{
		s_instance.User = null;
		s_instance.playerData = null;
		s_instance.encodedAvatar = null;
		ListSortedMessageForChat = null;
		ListHiddenMessageForChat = null;
		ResetUserSpecificData ();
		InitOwnedLists ();
		TableClothStorage.Instance.ResetList ();
		clothes = TableClothStorage.Instance.Clothes;
		TablePatternStorage.Instance.ResetList ();
		patterns = TablePatternStorage.Instance.Patterns;
		TableFrameStorage.Instance.ResetList ();
		frames = TableFrameStorage.Instance.Frames;
		ChatPackStorage.Instance.ResetList ();
		chatPacks = ChatPackStorage.Instance.ChatPacks;
		CuesStorage.Instance.ResetList ();
		cues = CuesStorage.Instance.Cues;
		Save ();
	}

	public string Encrypt (string plainText)
	{
		//encrypt data
		byte[] encrypted = Encoding.Unicode.GetBytes (plainText);
		//return as base64 string
		return Convert.ToBase64String (encrypted);
	}

	public string Decrypt (string cipher)
	{
		//parse base64 string
		byte[] decrypted = Convert.FromBase64String (cipher);
		//decrypt data
		return Encoding.Unicode.GetString (decrypted);
	}

	public void ResetUserSpecificData ()
	{
		gameName = hostPlayer = opponentId = string.Empty;
		selectedGameMode = 0;
#if UNITY_WEBGL
        lastOpenedTab = 2;
#else
		lastOpenedTab = 0;
#endif
		playTable = 0;
		singleOrMulti = 0;
		timeForGame = 0;
		entryFee = 0;
		forceSliderPosition = 0;
		lastSelectedCueTab = 0;
		lastSelectedTableTab = 0;
		isInGame = false;
		playWithFriends = false;
		is3D = true;
		challenge = null;
		playAgain = false;
		acceptedChallengeId = "";
		needToUpdateInfo = false;
		freecoinsTimerStarted = false;
		challengeTimeOver = false;
		offlinePopupShown = false;
		gameStarted = false;
		needToShowOfflinePopup = false;
		defaultAvatar = true;
        

		selectedCue = CueEntity.DeepCopy (cues [0]);
		selectedCloth = ShopitemEntity.DeepCopy (clothes [0]);
		selectedPattern = ShopitemEntity.DeepCopy (patterns [0]);
		selectedFrame = ShopitemEntity.DeepCopy (frames [0]);
		ownedCues = null;
		ownedFrames = null;
		ownedPacks = null;
		ownedClothes = null;
		ownedPatterns = null;
	}
}

public enum GameTypeMode
{
	SinglePlayer,
	Multiplayer
}
