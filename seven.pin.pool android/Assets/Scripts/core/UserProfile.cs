﻿public enum ProfileType
{
    Guest = 0,
    GuestOffline,
    FB,
    Normal
}

public class UserProfile
{
    public string Login { get; set; }
    public string Password { get; set; }
    public ProfileType Type { get; set; }
    public string DisplayName { get; set; }
    public string ServerPlayerId { get; set; }
}

