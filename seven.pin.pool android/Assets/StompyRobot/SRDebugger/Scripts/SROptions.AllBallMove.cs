#define ENABLE_SKILLS_SROPTIONS

using System;
using System.ComponentModel;
using SRDebugger;
using SRDebugger.Services;
using SRF;
using SRF.Service;
using UnityEngine;
using Random = UnityEngine.Random;

public partial class SROptions
{
#if ENABLE_SKILLS_SROPTIONS
	
	[Category("Ball")]
	public void AllBallsMove()
	{
		CueController.allBallMove = true;
	}

	[Category("Ball")]
	public void DisableAllBallsMove()
	{
		CueController.allBallMove = false;
	}

#endif
}
