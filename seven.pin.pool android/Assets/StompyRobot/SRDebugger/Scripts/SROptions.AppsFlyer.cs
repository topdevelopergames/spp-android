﻿#define ENABLE_SKILLS_SROPTIONS
using System;
using System.Collections.Generic;
using System.ComponentModel;

  public partial class SROptions
  {
#if ENABLE_SKILLS_SROPTIONS
	
	[Category("AppFlyer")]
    public void RegistrationComplete()
    {
        Dictionary<string, string> eventData = new Dictionary<string, string>();
        eventData.Add("date", DateTime.Now.ToString());
        eventData.Add("quantity", "1");        
        AppsFlyer.trackRichEvent(AFInAppEvents.COMPLETE_REGISTRATION, eventData);
    }

    [Category("AppFlyer")]
    public void Description()
    {
        Dictionary<string, string> eventData = new Dictionary<string, string>();
        eventData.Add("StepA","Do step A");
        eventData.Add("StepB", "Do step B");
        AppsFlyer.trackRichEvent(AFInAppEvents.DESCRIPTION, eventData);
    }

    [Category("AppFlyer")]
    public void EventStarted()
    {
        Dictionary<string, string> eventData = new Dictionary<string, string>();
        eventData.Add("Started event", "started event");
        AppsFlyer.trackRichEvent(AFInAppEvents.EVENT_START, eventData);
    }

    [Category("AppFlyer")]
    public void Level()
    {
        Dictionary<string, string> eventData = new Dictionary<string, string>();
        eventData.Add("level", "10");
        AppsFlyer.trackRichEvent(AFInAppEvents.LEVEL, eventData);
    }

    [Category("AppFlyer")]
    public void Tutorial()
    {
        Dictionary<string, string> eventData = new Dictionary<string, string>();
        eventData.Add("date", DateTime.Now.ToString());
        eventData.Add("Tutorial completed", "yup");
        AppsFlyer.trackRichEvent(AFInAppEvents.TUTORIAL_COMPLETION, eventData);
    }


#endif
 }
