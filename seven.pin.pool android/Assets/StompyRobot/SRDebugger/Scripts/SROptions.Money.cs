﻿#define ENABLE_SKILLS_SROPTIONS

using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions {

#if ENABLE_SKILLS_SROPTIONS
	
    [Category("Money")]
    public void AddCoins1000000()
    {
        GameSparksManager.Instance.CoinsAdd(1000000, "Debug", CoinsAddedDebug);
    }

    [Category("Money")]
    public void AddCoins10000()
    {
        GameSparksManager.Instance.CoinsAdd(10000, "Debug", CoinsAddedDebug);
    }

    [Category("Money")]
    public void AddCoins100()
    {
        GameSparksManager.Instance.CoinsAdd(100, "Debug", CoinsAddedDebug);
    }

    [Category("Money")]
    public void AddCash1000000()
    {
        GameSparksManager.Instance.CashAdd(1000000, "Debug", CoinsAddedDebug);
    }

    [Category("Money")]
    public void AddCash10000()
    {
        GameSparksManager.Instance.CashAdd(10000, "Debug", CoinsAddedDebug);
    }

    [Category("Money")]
    public void AddCash100()
    {
        GameSparksManager.Instance.CashAdd(100, "Debug", CoinsAddedDebug);
    }

    void CoinsAddedDebug()
    {
        GameSparksManager.Instance.UpdateAuthorizedPlayerData(PlayerDataUpdatedDebug);
    }

    void PlayerDataUpdatedDebug()
    {
        MenuUIManager.Instance.SetCoins(GameSparksManager.Instance.AuthorizedPlayerData.coins.ToString());
        MenuUIManager.Instance.SetCash(GameSparksManager.Instance.AuthorizedPlayerData.cash.ToString());
    }

#endif

}
