#define ENABLE_SKILLS_SROPTIONS

using System;
using System.ComponentModel;
using SRDebugger;
using SRDebugger.Services;
using SRF;
using SRF.Service;
using UnityEngine;
using Random = UnityEngine.Random;

public partial class SROptions
{
#if ENABLE_SKILLS_SROPTIONS
	
	[Category("Pins")]
	public void DefaultPrice()
	{
		Rules.coefPricePins = 1;
	}

	[Category("Pins")]
	public void IncresePricePinsIn2()
	{
		Rules.coefPricePins = 2;
	}

	[Category("Pins")]
	public void IncresePricePinsIn3()
	{
		Rules.coefPricePins = 3;
	}

	[Category("Pins")]
	public void IncresePricePinsIn4()
	{
		Rules.coefPricePins = 4;
	}

	[Category("Pins")]
	public void IncresePricePinsIn21()
	{
		Rules.coefPricePins = 21;
	}

    [Category("Pins")]
    public void IncresePricePinsIn50()
    {
        Rules.coefPricePins = 50;
    }

    [Category("Pins")]
    public void IncresePricePinsIn51()
    {
        Rules.coefPricePins = 51;
    }

    [Category("Pins")]
    public void IncresePricePinsIn101()
    {
        Rules.coefPricePins = 101;
    }

    [Category("Pins")]
    public void IncresePricePinsIn100()
    {
        Rules.coefPricePins = 100;
    }

#endif
}
