#define ENABLE_SKILLS_SROPTIONS

using System;
using System.ComponentModel;
using SRDebugger;
using SRDebugger.Services;
using SRF;
using SRF.Service;
using UnityEngine;
using Random = UnityEngine.Random;

public partial class SROptions
{
#if ENABLE_SKILLS_SROPTIONS
	
	[Category("Debug")]
	public void EnableDebug()
	{
		CueController.SpecialDebug = true;
	}

	[Category("Debug")]
	public void DisableDebug()
	{
		CueController.SpecialDebug = false;
	}

    [Category("Debug")]
    public void DelayMessage1S()
    {
        CueController.DelayDebugMessage = 1f;
    }

    [Category("Debug")]
    public void DelayMessage2S()
    {
        CueController.DelayDebugMessage = 2f;
    }

    [Category("Debug")]
    public void DelayMessage5S()
    {
        CueController.DelayDebugMessage = 5f;
    }

    [Category("Debug")]
    public void DelayMessage10S()
    {
        CueController.DelayDebugMessage = 10f;
    }
#endif
}
