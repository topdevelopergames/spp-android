﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions
{
    [Category("Ball speed")]
    [NumberRange(0, 2)]
    [Increment(0.01)]
    public float BallSpeed
    {
        get { return BallController.speedCoef; }
        set { BallController.speedCoef = value; }
    }

    
    
}
